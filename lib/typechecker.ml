module Lisa = Syntax

module S = struct
  type 'a structure =
    | TyUnit
    | TyInt
    | TyBool
    | TyList of 'a
    | TyStream of 'a
    | TyThunk of 'a
    | TyArrow of 'a * 'a
    | TyProduct of 'a list

  let map f = function
    | TyUnit -> TyUnit
    | TyInt -> TyInt
    | TyBool -> TyBool
    | TyList t ->
        let t = f t in
        TyList t
    | TyStream t -> TyStream (f t)
    | TyThunk t -> TyThunk (f t)
    | TyArrow (t1, t2) ->
        let t1 = f t1 in
        let t2 = f t2 in
        TyArrow (t1, t2)
    | TyProduct ts ->
        let ts = List.map f ts in
        TyProduct ts

  let fold f t accu =
    match t with
    | TyUnit | TyInt | TyBool -> accu
    | TyList t | TyStream t | TyThunk t -> f t accu
    | TyArrow (t1, t2) ->
        let accu = f t1 accu in
        let accu = f t2 accu in
        accu
    | TyProduct ts -> List.fold_right f ts accu

  let iter f t =
    let _ = map f t in
    ()

  exception Iter2

  let iter2 f t u =
    match (t, u) with
    | TyUnit, TyUnit | TyInt, TyInt | TyBool, TyBool -> ()
    | TyList t, TyList u | TyStream t, TyStream u | TyThunk t, TyThunk u ->
        f t u
    | TyArrow (t1, t2), TyArrow (u1, u2) ->
        f t1 u1;
        f t2 u2
    | TyProduct ts, TyProduct us -> (
        try List.iter2 f ts us with Invalid_argument _ -> raise Iter2)
    | _, _ -> raise Iter2

  let conjunction f t u =
    iter2 f t u;
    t

  exception InconsistentConjunction = Iter2

  open PPrint

  let kw_arrow = string "->"
  let kw_unit_ty = string "unit"
  let kw_int_ty = string "int"
  let kw_bool_ty = string "bool"
  let kw_list_ty = string "list"
  let kw_star = star
  let kw_stream = string "stream"
  let kw_u = string "u"

  let rec print_type_arrow leaf t =
    match t with
    | TyArrow (t1, t2) -> infix 2 1 kw_arrow (leaf t1) (leaf t2)
    | _ -> print_type_atom leaf t

  and print_type_atom leaf t =
    match t with
    | TyUnit -> kw_unit_ty
    | TyInt -> kw_int_ty
    | TyBool -> kw_bool_ty
    | TyList ty -> prefix 0 1 (leaf ty) kw_list_ty
    | TyStream ty -> prefix 0 1 (leaf ty) kw_stream
    | TyThunk ty -> prefix 0 1 (leaf ty) kw_u
    | TyProduct tys -> braces (separate_map kw_star leaf tys)
    | TyArrow _ -> parens (print_type leaf t)

  and print_type leaf ty = group (print_type_arrow leaf ty)
  and pprint leaf = print_type leaf
end

module O = struct
  type tyvar = int
  type 'a structure = 'a S.structure
  type ty = Lisa.nominal_type

  let solver_tyvar n = n
  let inject n = 2 * n
  let variable x = Lisa.TyVar x

  let structure t =
    match t with
    | S.TyUnit -> Lisa.TyUnit
    | S.TyInt -> Lisa.TyInt
    | S.TyBool -> Lisa.TyBool
    | S.TyList t -> Lisa.TyList t
    | S.TyStream t -> Lisa.TyStream t
    | S.TyThunk t -> Lisa.TyThunk t
    | S.TyArrow (t1, t2) -> Lisa.TyArrow (t1, t2)
    | S.TyProduct ts -> Lisa.TyProduct ts

  let mu x t =
    ignore (x, t);
    assert false

  type scheme = tyvar list * ty
end

module X = struct
  type t = Var of string | Sym of int

  let fresh : unit -> t =
    let gensym = Inferno.Utils.gensym () in
    fun () -> Sym (gensym ())

  let hash = Hashtbl.hash
  let compare v1 v2 = Stdlib.compare v1 v2
  let equal v1 v2 = compare v1 v2 = 0
  let to_string v = match v with Var v -> v | Sym n -> string_of_int n
end

module Solver = Inferno.Solver.Make (X) (S) (O)
open Solver

let ty_unit = S.TyUnit
let ty_int = S.TyInt
let ty_bool = S.TyBool
let ty_list x = S.TyList x
let ty_stream x = S.TyStream x
let ty_thunk x = S.TyThunk x
let ty_arrow x y = S.TyArrow (x, y)
let ty_product xs = S.TyProduct xs

let existn_ n f =
  let rec aux vs accu =
    if accu = n then f (List.rev vs)
    else
      let@ v = exist in
      aux (v :: vs) (succ accu)
  in
  aux [] 0

let rec defs xs ts c =
  match (xs, ts) with
  | [], [] -> c
  | x :: xs, t :: ts -> def x t (defs xs ts c)
  | _, _ -> assert false

let conj_of_term f cs t v =
  let+ x = f t v and+ xs = cs in
  x :: xs

let conj_of_list f ts vs = List.fold_left2 (conj_of_term f) (pure []) ts vs

let xtuple ctx tyenv ts hastype tau =
  existn_ (List.length ts) @@ fun vs ->
  ctx vs
    (let+ () = tau --- ty_product vs
     and+ t = conj_of_list (hastype tyenv) ts vs in
     t)

let tuple tyenv = xtuple (fun _ x -> x) tyenv

let mu_tuple tyenv bs =
  let xs, ts = List.split bs in
  let tyenv = List.filter (fun (x, _) -> not (List.mem x xs)) tyenv in
  xtuple
    (fun vs c ->
      let+ ts = defs xs vs c in
      List.(combine xs (rev ts)))
    tyenv ts

let exist2n_ n k =
  let rec aux vs accu =
    match accu with
    | 0 -> k (List.rev vs)
    | n ->
        let@ v = exist in
        let@ w = exist in
        aux ((v, w) :: vs) (pred n)
  in
  aux [] n

let rec defs_thunked xs ts c =
  match (xs, ts) with
  | [], [] -> c
  | x :: xs, (v, w) :: ts ->
      let+ () = v --- ty_thunk w
      and+ t = def (Var x) v (defs_thunked xs ts c) in
      t
  | _, _ -> assert false

let conj_of_list2 f ts vs =
  List.fold_left2
    (fun cs t (_, w) ->
      let+ x = f t w and+ xs = cs in
      x :: xs)
    (pure []) ts vs

let mutuple tyenv ds hastype t =
  let n = List.length ds in
  let xs, ts = List.split ds in
  let+ ts =
    exist2n_ n (fun vs ->
        defs_thunked xs vs
          (let+ () = t --- ty_product (List.split vs |> snd)
           and+ ts = conj_of_list2 (hastype tyenv) ts vs in
           ts))
  in
  List.(combine xs (rev ts))

let rec convert_deep (ty : int Lisa.typ) : deep_ty =
  let conv = convert_deep in
  let deeps t = DeepStructure t in
  match ty with
  | TyVar _ -> failwith "types should be monomorphic"
  | TyUnit -> deeps S.TyUnit
  | TyInt -> deeps S.TyInt
  | TyBool -> deeps S.TyBool
  | TyList ty -> deeps (S.TyList (conv ty))
  | TyArrow (ty1, ty2) ->
      let ty1 = conv ty1 in
      let ty2 = conv ty2 in
      deeps (S.TyArrow (ty1, ty2))
  | TyStream ty -> deeps (S.TyStream (conv ty))
  | TyThunk ty -> deeps (S.TyThunk (conv ty))
  | TyProduct tys -> deeps (S.TyProduct (List.map conv tys))

let convert (ty : Lisa.nominal_type) =
  let deep_ty = convert_deep ty in
  deep deep_ty

let annot_co hastype (ty_opt, t) =
  match ty_opt with
  | None -> hastype t
  | Some ty -> fun _ -> convert ty (hastype t)

let rec_values tyenv hastype bs c =
  let typed_exp t = function Some ty -> Lisa.TypedExp (t, ty) | _ -> t in
  let xs =
    List.fold_right (fun (x, ty, t) xs -> (x, typed_exp t ty) :: xs) bs []
  in
  let@ v = exist in
  let+ xs = mutuple tyenv xs hastype v and+ xs', tyenv = c in
  ( List.map
      (function
        | x, Lisa.TypedExp (t, ty) -> (x, Some ty, t) | x, t -> (x, None, t))
      xs,
    xs',
    tyenv )

exception Poly of Lisa.nominal_type

let rec is_var ty =
  match ty with
  | Lisa.TyVar _ -> Some ty
  | Lisa.TyInt -> None
  | Lisa.TyBool -> None
  | Lisa.TyUnit -> None
  | Lisa.TyList x | Lisa.TyStream x | Lisa.TyThunk x -> is_var x
  | Lisa.TyArrow (x, y) -> ( match is_var x with None -> is_var y | t -> t)
  | Lisa.TyProduct tys ->
      List.fold_left
        (fun maybe_var ty -> match maybe_var with None -> is_var ty | t -> t)
        None tys

let check_mono ans ty =
  match is_var ty with Some _ -> raise (Poly ty) | None -> ans

let rec hastype tyenv t w =
  match t with
  | Lisa.Var x -> (
      match List.assoc_opt x tyenv with
      | Some ty ->
          let@ k = convert ty in
          let+ () = w -- k in
          t
      | None ->
          let+ _ = instance (Var x) w in
          t)
  | Lisa.Fun (x, u) ->
      let tyenv = List.filter (fun (y, _) -> x <> y) tyenv in
      let@ v1 = exist in
      let@ v2 = exist in
      let+ () = w --- ty_arrow v1 v2
      and+ u' = def (Var x) v1 (hastype tyenv u v2)
      and+ ty1 = decode v1
      and+ ty2 = decode v2 in
      check_mono
        (Lisa.TypedExp (Fun (x, u'), TyArrow (ty1, ty2)))
        (TyArrow (ty1, ty2))
  | Lisa.App (t1, t2) ->
      let@ v = exist in
      let+ a = lift (hastype tyenv) t1 (ty_arrow v w)
      and+ b = hastype tyenv t2 v
      and+ ty = decode v in
      Lisa.(App (a, TypedExp (b, ty)))
  | Lisa.Tuple ts ->
      let+ ts = tuple tyenv ts hastype w in
      Lisa.Tuple (List.rev ts)
  | Lisa.Const Unit as k ->
      let+ () = w --- ty_unit in
      Lisa.TypedExp (k, TyUnit)
  | Lisa.Const (Int _) as k ->
      let+ () = w --- ty_int in
      Lisa.TypedExp (k, TyInt)
  | Lisa.Const (Bool _) as k ->
      let+ () = w --- ty_bool in
      Lisa.TypedExp (k, TyBool)
  | Lisa.Const Nil as k ->
      let@ v = exist in
      let+ () = w --- ty_list v and+ ty = decode v in
      check_mono Lisa.(TypedExp (k, TyList ty)) (TyList ty)
  | Lisa.Primitive _ as p -> hastype_prim tyenv p w
  | Lisa.Fby (x, xs) ->
      let@ v = exist in
      let+ () = w --- ty_stream v
      and+ c' = hastype tyenv x v
      and+ s' = lift (hastype tyenv) xs (ty_thunk w)
      and+ ty = decode v in
      Lisa.TypedExp (Fby (c', s'), TyStream ty)
  | Lisa.At (s, t) ->
      let@ v = exist in
      let@ vs = exist in
      let+ () = vs --- ty_stream w
      and+ s' = lift (hastype tyenv) s (ty_thunk vs)
      and+ () = v --- ty_int
      and+ t' = lift (hastype tyenv) t (ty_arrow v v)
      and+ ty = decode w in
      Lisa.TypedExp (At (s', t'), ty)
  | Lisa.MuTuple ts ->
      let+ ts = mutuple tyenv ts hastype w in
      Lisa.MuTuple ts
  | Lisa.TypedExp (term, ty) ->
      let+ _ = convert ty (hastype tyenv term) in
      t
  | Lisa.Thunk u ->
      let@ v = exist in
      let+ () = w --- ty_thunk v
      and+ u = hastype tyenv u v
      and+ ty = decode v in
      check_mono (Lisa.TypedExp (Thunk u, TyThunk ty)) (TyThunk ty)
  | Lisa.Force t ->
      let+ t = lift (hastype tyenv) t (ty_thunk w) in
      Lisa.Force t
  | Lisa.Fix (x, e) ->
      let@ v = exist in
      let+ () = v --- ty_thunk w and+ _ = def (Var x) v (hastype tyenv e w) in
      t
  | Lisa.Iterate _ -> assert false

and hastype_prim tyenv p w =
  match p with
  | Lisa.Primitive (Input l) ->
      let+ ty = decode w in
      Lisa.TypedExp (Primitive (Input l), ty)
  | Lisa.Primitive (Proj (i, m, t)) ->
      let@ v = exist in
      let@ vs = existn_ m in
      let vs = List.mapi (fun j v -> if j = i then w else v) vs in
      let+ () = v --- S.TyProduct vs
      and+ t' = lift (hastype tyenv) t (S.TyProduct vs)
      and+ ty = decode v in
      Lisa.Primitive (Proj (i, m, Lisa.TypedExp (t', ty)))
  | Lisa.Primitive (NegB x) as p ->
      let+ () = w --- ty_bool and+ _ = lift (hastype tyenv) x ty_bool in
      Lisa.TypedExp (p, TyBool)
  | ( Lisa.Primitive (Plus (x, y))
    | Lisa.Primitive (Minus (x, y))
    | Lisa.Primitive (Mult (x, y))
    | Lisa.Primitive (Div (x, y)) ) as p ->
      let+ () = w --- ty_int
      and+ _ = hastype tyenv x w
      and+ _ = hastype tyenv y w in
      Lisa.TypedExp (p, TyInt)
  | (Lisa.Primitive (Eq (x, y)) | Lisa.Primitive (Neq (x, y))) as p ->
      let@ v = exist in
      let+ _ = hastype tyenv x v
      and+ _ = hastype tyenv y v
      and+ () = w --- ty_bool in
      Lisa.TypedExp (p, TyBool)
  | (Lisa.Primitive (AndB (x, y)) | Lisa.Primitive (OrB (x, y))) as p ->
      let+ () = w --- ty_bool
      and+ _ = hastype tyenv x w
      and+ _ = hastype tyenv y w in
      Lisa.TypedExp (p, TyBool)
  | Lisa.Primitive (Cons (x, y)) ->
      let@ v = exist in
      let+ () = w --- ty_list v
      and+ x' = hastype tyenv x v
      and+ y' = hastype tyenv y w
      and+ ty = decode v in
      Lisa.(TypedExp (Primitive (Cons (x', y')), TyList ty))
  | Lisa.Primitive (Map (x, y)) ->
      let@ v1 = exist in
      let@ v2 = exist in
      let+ () = w --- ty_list v2
      and+ x' = lift (hastype tyenv) x (ty_arrow v1 v2)
      and+ y' = lift (hastype tyenv) y (ty_list v1)
      and+ ty = decode v2 in
      Lisa.(TypedExp (Primitive (Map (x', y')), TyList ty))
  | Lisa.Primitive (Iter (x, y)) ->
      let@ v1 = exist in
      let+ () = w --- ty_unit
      and+ x' = lift (hastype tyenv) x (ty_arrow v1 w)
      and+ y' = lift (hastype tyenv) y (ty_list v1) in
      Lisa.(TypedExp (Primitive (Iter (x', y')), TyUnit))
  | Lisa.Primitive (FoldL (x, y, z)) ->
      let@ v1 = exist in
      let@ v2 = exist in
      let@ v3 = exist in
      let+ () = v2 --- ty_arrow v3 v1
      and+ () = w -- v1
      and+ x' = lift (hastype tyenv) x (ty_arrow v1 v2)
      and+ y' = hastype tyenv y v1
      and+ z' = lift (hastype tyenv) z (ty_list v3)
      and+ ty = decode v1 in
      Lisa.(TypedExp (Primitive (FoldL (x', y', z')), ty))
  | Lisa.Primitive (FoldR (x, y, z)) ->
      let@ v1 = exist in
      let@ v2 = exist in
      let@ v3 = exist in
      let+ () = v2 --- ty_arrow v3 v3
      and+ () = w -- v1
      and+ x' = lift (hastype tyenv) x (ty_arrow v1 v2)
      and+ y' = lift (hastype tyenv) y (ty_list v3)
      and+ z' = hastype tyenv z v1
      and+ ty = decode v3 in
      Lisa.(TypedExp (Primitive (FoldR (x', y', z')), ty))
  | Lisa.Primitive (IfThenElse (c, x, y)) ->
      let+ c' = lift (hastype tyenv) c ty_bool
      and+ x' = hastype tyenv x w
      and+ y' = hastype tyenv y w in
      Lisa.Primitive (IfThenElse (c', x', y'))
  | Lisa.Primitive (Head xs) ->
      let+ t = lift (hastype tyenv) xs (ty_stream w) in
      Lisa.Primitive (Head t)
  | Lisa.Primitive (Tail xs) ->
      let@ v = exist in
      let+ () = w --- ty_stream v and+ t = hastype tyenv xs w in
      Lisa.Primitive (Tail t)
  | _ -> assert false

let rec bindings tyenv = function
  | [] -> pure ([], tyenv)
  | Lisa.Def (x, None, t) :: xs ->
      let co =
        let@ v = exist in
        let+ t' = hastype tyenv t v and+ ty = decode v in
        (ty, t')
      in
      let+ _, _, (ty, t'), (xs, tyenv) =
        let1 (Var x) (fun _ -> co) (bindings tyenv xs)
      in
      (Lisa.Def (x, Some ty, t') :: xs, (x, ty) :: tyenv)
  | Lisa.Def (x, Some ty, t) :: xs ->
      let co = convert ty (hastype tyenv t) in
      let+ _a, _b, t', (xs, tyenv) =
        let1 (Var x) (fun _ -> co) (bindings tyenv xs)
      in
      (Lisa.Def (x, Some ty, t') :: xs, (x, ty) :: tyenv)
  | Lisa.RecDef (x, None, t) :: xs ->
      let co =
        let@ v = exist in
        let+ t' = hastype tyenv (Fix (x, t)) v and+ ty = decode v in
        (ty, t')
      in
      let+ _, _, (ty, t'), (xs, tyenv) =
        let1 (Var x) (fun _ -> co) (bindings tyenv xs)
      in
      (Lisa.RecDef (x, Some ty, t') :: xs, (x, ty) :: tyenv)
  | Lisa.RecDef (x, Some ty, t) :: xs ->
      let co = convert ty (hastype tyenv (Fix (x, t))) in
      let+ _, _, t', (xs, tyenv) =
        let1 (Var x) (fun _ -> co) (bindings tyenv xs)
      in
      (Lisa.RecDef (x, Some ty, t') :: xs, (x, ty) :: tyenv)
  | Lisa.RecValues xs :: xs' ->
      let+ xs, xs', tyenv =
        (rec_values tyenv hastype xs) (bindings tyenv xs')
      in
      (Lisa.RecValues xs :: xs', tyenv)
(* let (rec x = u) in
 *     a
 * Γ, x : τ U ⊢ u : τ
 * Γ, x : τ ⊢ a : τ₂ *)

exception Unbound = Solver.Unbound
exception Unify = Solver.Unify
exception Cycle = Solver.Cycle

let translate tyenv t =
  solve ~rectypes:false
    (let+ _, t = let0 (bindings tyenv t) in
     t)
