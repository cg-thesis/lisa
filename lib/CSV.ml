let rec get_col_timed_def i (x : Libformula.Syntax.expression list) =
  match x with
  | [] -> None
  | [ Var _; x ] -> Some (0, x)
  | [ Var _ ] -> None
  | [ x ] -> Some (i, x)
  | _ :: xs -> get_col_timed_def i xs

let get_timed_defs s = List.map (get_col_timed_def 0) s

let append_timed_defs timed_defs prog =
  let update_and_append_last col timed_def =
    match timed_def with None -> col | Some (_, def) -> col @ [ def ]
  in
  List.map2 update_and_append_last prog timed_defs

let csv_of_lisa ids values timed_defs =
  LisaCompiler.Values.csv_of_lisa_val ids values |> append_timed_defs timed_defs

let eval callback ids (sheet : Libformula.Syntax.program)
    (defs : Syntax.program) (n : int) =
  let eval env x = BigStepSemantics.eval callback env n x in
  let timed_defs = get_timed_defs sheet in
  let program = CSVCompiler.lisa_of_csv defs sheet in
  let _env, (_ty, vals) =
    List.fold_left
      (fun prev def ->
        let env, _ = prev in
        eval env def)
      ([], (None, Syntax.VUnit))
      program
  in
  csv_of_lisa ids vals timed_defs
