open SemanticsCommon
open Syntax

let time_of_prim t prim time_of_ =
  match prim with
  | CNegB c
  | CHead c
  | CTail c
  | ProjArg (_, _, c)
  | LePlus (c, _)
  | RiPlus (_, c)
  | LeMinus (c, _)
  | RiMinus (_, c)
  | LeTimes (c, _)
  | RiTimes (_, c)
  | LeDiv (c, _)
  | RiDiv (_, c)
  | LeEq (c, _)
  | RiEq (_, c)
  | LeNeq (c, _)
  | RiNeq (_, c)
  | LeAndB (c, _)
  | RiAndB (_, c)
  | LeOrB (c, _)
  | RiOrB (_, c)
  | LeCons (c, _)
  | RiCons (_, c)
  | LeMap (c, _)
  | RiMap (_, c)
  | LeIter (c, _)
  | RiIter (_, c)
  | FstFoldL (c, _, _)
  | SndFoldL (_, c, _)
  | ThrdFoldL (_, _, c)
  | FstFoldR (c, _, _)
  | SndFoldR (_, c, _)
  | ThrdFoldR (_, _, c)
  | CIfThenElse (c, _, _) ->
      time_of_ t c

let bin_subst sigma subst x y = (subst sigma x, subst sigma y)

let subst_prim sigma p subst =
  let bsubst = bin_subst sigma subst in
  match p with
  | Input _ -> Primitive p
  | NegB x -> negb (subst sigma x)
  | Head x -> head (subst sigma x)
  | Tail x -> tail (subst sigma x)
  | Proj (i, m, ts) -> prim (Proj (i, m, subst sigma ts))
  | Plus (x, y) -> plus (bsubst x y)
  | Minus (x, y) -> minus (bsubst x y)
  | Mult (x, y) -> mult (bsubst x y)
  | Div (x, y) -> div (bsubst x y)
  | Eq (x, y) -> eq (bsubst x y)
  | Neq (x, y) -> neq (bsubst x y)
  | AndB (x, y) -> andb (bsubst x y)
  | OrB (x, y) -> orb (bsubst x y)
  | Cons (x, y) -> cons (bsubst x y)
  | Map (x, y) -> map (bsubst x y)
  | Iter (x, y) -> iter (bsubst x y)
  | FoldL (x, y, z) -> foldl (subst sigma x, subst sigma y, subst sigma z)
  | FoldR (x, y, z) -> foldr (subst sigma x, subst sigma y, subst sigma z)
  | IfThenElse (c, x, y) -> ite (subst sigma c, subst sigma x, subst sigma y)

let apply_prim_context c a apply_context =
  let exp c' = apply_context c' a in
  match c with
  | CNegB c -> negb (exp c)
  | CHead c -> head (exp c)
  | CTail c -> tail (exp c)
  | ProjArg (i, m, c) -> prim (Proj (i, m, exp c))
  | LePlus (c, y) -> plus (exp c, y)
  | RiPlus (x, c) -> plus (x, exp c)
  | LeMinus (c, y) -> minus (exp c, y)
  | RiMinus (x, c) -> minus (x, exp c)
  | LeTimes (c, y) -> mult (exp c, y)
  | RiTimes (x, c) -> mult (x, exp c)
  | LeDiv (c, y) -> div (exp c, y)
  | RiDiv (x, c) -> div (x, exp c)
  | LeEq (c, y) -> eq (exp c, y)
  | RiEq (x, c) -> eq (x, exp c)
  | LeNeq (c, y) -> neq (exp c, y)
  | RiNeq (x, c) -> neq (x, exp c)
  | LeAndB (c, y) -> andb (exp c, y)
  | RiAndB (x, c) -> andb (x, exp c)
  | LeOrB (c, y) -> orb (exp c, y)
  | RiOrB (x, c) -> orb (x, exp c)
  | LeCons (c, y) -> cons (exp c, y)
  | RiCons (x, c) -> cons (x, exp c)
  | LeMap (c, y) -> map (exp c, y)
  | RiMap (x, c) -> map (x, exp c)
  | LeIter (c, y) -> iter (exp c, y)
  | RiIter (x, c) -> iter (x, exp c)
  | FstFoldL (c, y, z) -> foldl (exp c, y, z)
  | SndFoldL (x, c, z) -> foldl (x, exp c, z)
  | ThrdFoldL (x, y, c) -> foldl (x, y, exp c)
  | FstFoldR (c, y, z) -> foldr (exp c, y, z)
  | SndFoldR (x, c, z) -> foldr (x, exp c, z)
  | ThrdFoldR (x, y, c) -> foldr (x, y, exp c)
  | CIfThenElse (c, x, y) -> ite (exp c, x, y)

let destruct_prim p destruct =
  match p with
  | Input _ -> (Hole, prim p)
  | Head (Fby _ as xs) when is_val xs -> (Hole, prim p)
  | Head x ->
      let c, t = destruct x in
      (cprim (CHead c), t)
  | Tail (Fby _ as xs) when is_val xs -> (Hole, prim p)
  | Tail x ->
      let c, t = destruct x in
      (cprim (CTail c), t)
  | NegB (Const (Bool _)) -> (Hole, prim p)
  | NegB x ->
      let c, t = destruct x in
      (cprim (CNegB c), t)
  | Proj (i, m, (MuTuple _ as t)) -> (cprim (ProjArg (i, m, Hole)), t)
  | Proj (i, m, Tuple ts) when List.length ts = m && i < m -> (Hole, prim p)
  | Proj (i, m, ts) ->
      let c, t = destruct ts in
      (cprim (ProjArg (i, m, c)), t)
  | Plus (Const (Int _), Const (Int _)) -> (Hole, prim p)
  | Plus (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiPlus (x, c)), t)
  | Plus (x, y) ->
      let c, t = destruct x in
      (cprim (LePlus (c, y)), t)
  | Minus (Const (Int _), Const (Int _)) -> (Hole, prim p)
  | Minus (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiMinus (x, c)), t)
  | Minus (x, y) ->
      let c, t = destruct x in
      (cprim (LeMinus (c, y)), t)
  | Mult (Const (Int _), Const (Int _)) -> (Hole, prim p)
  | Mult (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiTimes (x, c)), t)
  | Mult (x, y) ->
      let c, t = destruct x in
      (cprim (LeTimes (c, y)), t)
  | Div (Const (Int _), Const (Int _)) -> (Hole, prim p)
  | Div (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiDiv (x, c)), t)
  | Div (x, y) ->
      let c, t = destruct x in
      (cprim (LeDiv (c, y)), t)
  | Eq (x, y) when is_val x && is_val y -> (Hole, prim p)
  | Eq (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiEq (x, c)), t)
  | Eq (x, y) ->
      let c, t = destruct x in
      (cprim (LeEq (c, y)), t)
  | Neq (x, y) when is_val x && is_val y -> (Hole, prim p)
  | Neq (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiNeq (x, c)), t)
  | Neq (x, y) ->
      let c, t = destruct x in
      (cprim (LeNeq (c, y)), t)
  | AndB (Const (Bool _), Const (Bool _)) -> (Hole, prim p)
  | AndB (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiAndB (x, c)), t)
  | AndB (x, y) ->
      let c, t = destruct x in
      (cprim (LeAndB (c, y)), t)
  | OrB (Const (Bool _), Const (Bool _)) -> (Hole, prim p)
  | OrB (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiOrB (x, c)), t)
  | OrB (x, y) ->
      let c, t = destruct x in
      (cprim (LeOrB (c, y)), t)
  | Map (Fun _, (Primitive (Cons _) as xs)) when is_val xs -> (Hole, prim p)
  | Map (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiMap (x, c)), t)
  | Map (x, y) ->
      let c, t = destruct x in
      (cprim (LeMap (c, y)), t)
  | Iter (Fun _, (Primitive (Cons _) as xs)) when is_val xs -> (Hole, prim p)
  | Iter (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiIter (x, c)), t)
  | Iter (x, y) ->
      let c, t = destruct x in
      (cprim (LeIter (c, y)), t)
  | FoldL (Fun _, x, (Primitive (Cons _) as xs)) when is_val x && is_val xs ->
      (Hole, prim p)
  | FoldL (x, y, z) when is_val x && is_val y ->
      let c, t = destruct z in
      (cprim (ThrdFoldL (x, y, c)), t)
  | FoldL (x, y, z) when is_val x ->
      let c, t = destruct y in
      (cprim (SndFoldL (x, c, z)), t)
  | FoldL (x, y, z) ->
      let c, t = destruct x in
      (cprim (FstFoldL (c, y, z)), t)
  | FoldR (Fun _, (Primitive (Cons _) as xs), x) when is_val x && is_val xs ->
      (Hole, prim p)
  | FoldR (x, y, z) when is_val x && is_val y ->
      let c, t = destruct z in
      (cprim (ThrdFoldR (x, y, c)), t)
  | FoldR (x, y, z) when is_val x ->
      let c, t = destruct y in
      (cprim (SndFoldR (x, c, z)), t)
  | FoldR (x, y, z) ->
      let c, t = destruct x in
      (cprim (FstFoldR (c, y, z)), t)
  | Cons (x, y) when is_val (cons (x, y)) -> raise Normal_form
  | Cons (x, y) when is_val x ->
      let c, t = destruct y in
      (cprim (RiCons (x, c)), t)
  | Cons (x, y) ->
      let c, t = destruct x in
      (cprim (LeCons (c, y)), t)
  | IfThenElse (Const (Bool _), _, _) -> (Hole, prim p)
  | IfThenElse (c, x, y) ->
      let c, t = destruct c in
      (cprim (CIfThenElse (c, x, y)), t)

let apply_map f xs =
  let rec aux acc = function
    | TypedExp (xs, _) -> aux acc xs
    | Const Nil ->
        List.map (fun x -> app f x) acc
        |> List.fold_left (fun acc x -> cons (x, acc)) (Const Nil)
    | Primitive (Cons (x, xs)) -> aux (x :: acc) xs
    | _ -> assert false
  in
  aux [] xs

let apply_fold_left f x xs =
  let rec aux acc = function
    | TypedExp (xs, _) -> aux acc xs
    | Const Nil -> List.fold_right (fun x acc -> app (app f acc) x) acc x
    | Primitive (Cons (x, xs)) -> aux (x :: acc) xs
    | _ -> assert false
  in
  aux [] xs

let apply_fold_right f xs x =
  let rec aux acc = function
    | TypedExp (xs, _) -> aux acc xs
    | Const Nil -> List.fold_left (fun acc x -> app (app f x) acc) x acc
    | Primitive (Cons (x, xs)) -> aux (x :: acc) xs
    | _ -> assert false
  in
  aux [] xs

let get_input (label, time) =
  let open IO in
  let pr = !prompt in
  let message =
    Printf.sprintf "Need a value for %s at time %d : \n" label time
  in
  set_prompt message;
  let exp = parse_expression (read ()) in
  set_prompt pr;
  exp

let delta_reduce time p =
  match p with
  | Input label -> (
      match get_value label time with
      | Some x -> x
      | None ->
          let exp = get_input (label, time) in
          set_value label time exp;
          exp)
  | NegB (Const (Bool b)) -> boolean (not b)
  | Head (Fby (x, _)) -> x
  | Tail (Fby (_, xs)) -> xs
  | Proj (i, m, Tuple ts) when List.length ts = m && i < m -> List.nth ts i
  | Plus (Const (Int x), Const (Int y)) -> integer (x + y)
  | Minus (Const (Int x), Const (Int y)) -> integer (x - y)
  | Mult (Const (Int x), Const (Int y)) -> integer (x * y)
  | Div (Const (Int x), Const (Int y)) -> integer (x / y)
  | Eq (a, b) when is_val a && is_val b -> boolean (a = b)
  | Neq (a, b) when is_val a && is_val b -> boolean (a <> b)
  | AndB (Const (Bool a), Const (Bool b)) -> boolean (a && b)
  | OrB (Const (Bool a), Const (Bool b)) -> boolean (a || b)
  | Map ((Fun _ as f), (Primitive (Cons _) as xs)) -> apply_map f xs
  | Iter ((Fun _ as f), (Primitive (Cons _) as xs)) ->
      app (lam "x" (Const Unit)) (apply_map f xs)
  | FoldL ((Fun _ as f), x, (Primitive (Cons _) as xs)) ->
      apply_fold_left f x xs
  | FoldR ((Fun _ as f), (Primitive (Cons _) as xs), x) ->
      apply_fold_right f xs x
  | IfThenElse (Const (Bool c), a, b) -> if c then a else b
  | _ -> failwith "Ill typed program."
