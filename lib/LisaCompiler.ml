(* Source language *)
module S = Syntax

(* Target language *)
module T = Libformula.Syntax

exception NotTranslatable

let env = ref []

let get_tuple (p : S.program) : S.expression =
  let cols = List.(hd (rev p)) in
  match cols with
  | Def _ -> assert false
  | RecValues xs ->
      let xs = List.map (fun (x, _, t) -> (x, t)) xs in
      S.MuTuple xs
  | RecDef _ -> assert false
(*
   To translate a stream expression, Var, Const, Primitive are
   almost equivalent, Fun, Def and some primitives cases cannot be
   translated. Amongst wich, inputs are translated as empty cells,
   projections can not be translated and only some map comibnators can
   be translated. With respect to TypedExpressions, types cannot be
   tranlated so only the expression translated. Fby are translated
   into cells of the same row (order should be kept). Ats should be
   translated as abolute references. For tuples only pairs can be
   translated.
*)

let rec translate id (e : S.expression) i : T.expression =
  match e with
  | S.Force (Var v) when id <> v -> Var v
  | S.Var "__nil__" -> Primitive Input
  | S.Var v when id <> v -> Var v
  | S.App (Var a, _) when List.mem a !env -> T.(Var a)
  | S.(App (App (App (App (Var "map_update", key), value), ufun), map)) ->
      let key = translate id key i
      and value = translate id value i
      and ufun = translate id ufun i
      and map = translate id map i in
      T.(Primitive (MapUpdate (key, value, ufun, map)))
  | S.App (a, b) ->
      let a = translate id a i and b = translate id b i in
      T.App (a, b)
  | S.(At (a, Fun (_, Primitive (Minus (Var _, Const (Int k)))))) ->
      translate_at id a k (Some T.RNeg) i
  | S.(At (a, Fun (_, Primitive (Plus (Var _, Const (Int k)))))) ->
      translate_at id a k (Some T.RPos) i
  | S.(At (a, Fun (_, Const (Int k)))) -> translate_at id a k None i
  | S.Tuple [ a; b ] -> translate_pair id a b i
  | S.Primitive (Proj (0, 1, MuTuple [ (id, Fby (e, _)) ])) -> translate id e i
  | S.Primitive (Cons (x, Const Nil)) -> translate id x i
  | S.Primitive p -> translate_primitive id p i
  | S.Const c -> translate_const c
  | S.TypedExp (e, _) -> translate id e i
  | _ -> raise NotTranslatable

and translate_pair id a b i =
  let a = translate id a i and b = translate id b i in
  T.Pair (a, b)

and translate_at id a k rel i =
  let a = translate id a i in
  T.(At (a, k, rel))

and translate_primitive id (e : S.primitive) i : T.expression =
  let tr x = translate id x i in
  let p x = T.Primitive x in
  match e with
  | S.NegB e ->
      let e = tr e in
      p @@ T.NegB e
  | S.Plus (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.Plus (x, y)
  | S.Minus (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.Minus (x, y)
  | S.Mult (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.Times (x, y)
  | S.Div (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.Div (x, y)
  | S.Eq (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.EqB (x, y)
  | S.Neq (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.(NegB (Primitive (EqB (x, y))))
  | S.AndB (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.AndB (x, y)
  | S.OrB (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.OrB (x, y)
  | S.Map (x, y) ->
      let x = tr x and y = tr y in
      p @@ T.MapMap (x, y)
  | S.IfThenElse (c, x, y) ->
      let c = tr c and x = tr x and y = tr y in
      p @@ T.IfThenElse (c, x, y)
  | S.Input _ -> p @@ T.Input
  | S.Proj (0, 2, p) -> T.Primitive (Fst (tr p))
  | S.Proj (1, 2, p) -> T.Primitive (Snd (tr p))
  | S.Proj (x, n, MuTuple xs) ->
      assert (List.length xs = n);
      tr (snd (List.nth xs x))
  | _ -> raise NotTranslatable

and translate_const (e : S.const) : T.expression =
  match e with
  | S.Unit -> T.Const Unit
  | S.Bool b -> T.(Const (Bool b))
  | S.Int i -> T.(Const (Int i))
  | S.Nil -> T.(List [])

let rec translate_stream id (e : S.expression) i : T.expression list =
  match e with
  | Fby (Primitive (Input _label), _) -> [ T.(Primitive Input) ]
  | Fby (x, Thunk y) | Fby (x, y) -> (
      let ys = translate_stream id y (succ i) in
      try translate id x i :: ys with NotTranslatable -> ys)
  | _ -> ( try [ translate id e i ] with NotTranslatable -> [])

let column_of_stream (v : string) (e : S.expression) : T.expression list =
  let b = T.Var v in
  match e with
  | S.Fby _ -> b :: translate_stream v e 1
  | _ -> [ b; translate v e 1 ]

let csv_of_tuple labels (e : S.expression) : Libformula.Syntax.program =
  match e with
  | MuTuple streams ->
      let streams = List.split streams |> snd in
      List.map2 column_of_stream labels streams
  | Tuple streams -> List.map2 column_of_stream labels streams
  | _ -> []

let csv_of_lisa labels defs xs =
  env := defs;
  let x = csv_of_tuple labels (get_tuple xs) in
  env := [];
  x

module Values = struct
  let rec translate_val x =
    match x with
    | S.VInt i -> T.(Const (Int i))
    | S.VBool b -> T.(Const (Bool b))
    | S.VFby _ -> assert false
    | S.VTuple [ a; b ] ->
        let a = translate_val a in
        let b = translate_val b in
        T.(Pair (a, b))
    | S.VList [] -> T.(List [])
    | S.VList xs -> T.List (List.map translate_val xs)
    | VUnit -> T.Const Unit
    | VTuple _ -> assert false
    | VThunk _ -> assert false
    | VClosure (_, _exp, _) -> assert false

  and translate_column label v =
    let label = T.Var label in
    match v with
    | S.VFby (v, VThunk t) ->
        let vs = values_from_thunk t.value in
        label :: List.map translate_val (v :: vs)
    | _ -> [ label; translate_val v ]

  and values_from_thunk t =
    let open Thunk in
    match t with
    | Running s ->
        let history = s.last.history in
        let offset = s.offset in
        let values = list_from history offset in
        values

  and csv_of_lisa_val_aux labels x =
    match x with
    | S.VTuple cs -> List.map2 translate_column labels cs
    | _ -> raise NotTranslatable

  and csv_of_lisa_val labels x = csv_of_lisa_val_aux labels x
end
