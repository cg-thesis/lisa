%{ (* Emacs, open this with -*- tuareg -*- *)
   open Utils.Error
   open Syntax

   let parsing_error pos msg =
     Printf.eprintf "%s:\n %s\n" (Utils.Position.string_of_pos pos) msg;
     exit 1

   type binop =
     | PPlus
     | PMinus
     | PMult
     | PDiv
     | PEq
     | PNeq
     | PAndB
     | POrB

   let make_binop lhs op rhs =
     match op with
     | PPlus  ->
        Plus (lhs, rhs)
     | PMinus ->
        Minus (lhs, rhs)
     | PMult ->
        Mult (lhs, rhs)
     | PDiv ->
        Div (lhs, rhs)
     | PEq ->
        Eq (lhs, rhs)
     | PNeq ->
        Neq (lhs, rhs)
     | PAndB ->
        AndB (lhs, rhs)
     | POrB ->
        OrB (lhs, rhs)

   let tuple_or_term  = function
     |[] ->
       Const Unit
     |[x] ->
       x
     | _::_ as ts->
        Tuple ts

   let type_or_typrod startpos endpos = function
     | [] ->
        parsing_error (Utils.Position.lex_join startpos endpos) "Syntax error."
     | [x] -> x
     | tys -> TyProduct tys

   let make_list xs =
     match xs with
     | [] -> Const Nil
     | _ ->
        List.fold_right (fun x acc -> Primitive (Cons (x, acc))) xs (Const Nil)
 %}

%token EOF
%token <int> INT
%token MUTUP
%token AND
%token LET
%token REC
%token THUNK
%token FORCE
%token FUN
%token HEAD
%token TAIL
%token NOT
%token IF
%token THEN
%token ELSE
%token EQUAL
%token NEQUAL
%token PROJ
%token FBY
%token UBY
%token ARROW
%token COMMA
%token LPAREN
%token RPAREN
%token PLUS
%token MINUS
%token MULT
%token DIV
%token LAND
%token LOR
%token INPUT
%token NIL
%token MAP
%token ITER
%token FOLD_LEFT
%token FOLD_RIGHT
%token CONS
%token DOT
%token SEMI_COLON
%token COLON
%token DOUBLE_QUOTE
%token LBRACKET
%token RBRACKET
%token <bool> TRUE
%token <bool> FALSE
%token <string> ID
%token INTTY
%token BOOLTY
%token UNITTY
%token STREAMTY
%token LISTTY
%token TYTHUNK


%right FBY
%right UBY
%right ARROW
%nonassoc ELSE
%nonassoc EQUAL NEQUAL LAND LOR
%left PLUS MINUS
%left MULT DIV

%start <Syntax.program> program
%start <Syntax.expression> exp
%%

exp:
  | a=expression EOF { a }
  | error
    {
      raise (SyntaxError (Utils.Position.lex_join $startpos $endpos))
    }

program:
  | ds=binding* EOF { ds }
  | error
    {
      parsing_error (Utils.Position.lex_join $startpos $endpos) "Syntax error."
    }

binding:
  | LET d=definition
    {
      let (i, ty, t) = d in Def (i, ty, t)
    }
  | MUTUP xs=separated_nonempty_list (AND, definition)
    {
      RecValues xs
    }
  | LET REC d=definition
    {
      let (i, ty, t) = d in RecDef(i, ty, t)
    }

expression:
  | a=term { a }
  | xs=mutuple { MuTuple xs }

%inline mutuple:
  | MUTUP xs=separated_nonempty_list (AND, rectupdef) { xs }

%inline rectupdef :
  | i=ID EQUAL t=term {(i, t)}

%inline definition:
  | i=id EQUAL t=term { (fst i, snd i, t) }

%inline id :
 | i=ID { (i,None) }
 | i=ID COLON ty=typ { (i,Some ty) }

term:
  | t=abstraction
    {
      t
    }
  | IF c=term THEN x=term ELSE y=term
    {
      Primitive (IfThenElse (c, x, y))
    }
  | t=term FBY u=term
    {
      Fby(t, u)
    }
  | t=term UBY u=term
    {
      Fby(t, Thunk u)
    }
  | lhs=term op=binop rhs=term
    {
      Primitive (make_binop lhs op rhs)
    }
  | t=simple_term
    {
      t
    }

%inline abstraction:
  | FUN bs=nonempty_list(ID) ARROW t=term { List.fold_right  (fun x t -> Fun(x , t)) bs t }

simple_term:
  | t=simple_term u=atomic_term
    {
      App (t, u)
    }
  | NOT x=atomic_term
    {
      Primitive (NegB x)
    }

  | HEAD x=atomic_term
   {
      Primitive (Head x)
   }
  | TAIL x=atomic_term
   {
     Primitive (Tail x)
   }
  | FORCE x=atomic_term
    {
      Force x
    }
  | THUNK x=atomic_term
    {
      Thunk x
    }
  | INPUT l=delimited(DOUBLE_QUOTE, ID, DOUBLE_QUOTE)
    {
      Primitive (Input l)
    }
  | PROJ i=INT m=INT t=atomic_term
    {
      Primitive (Proj (i, m, t))
    }
  | CONS t=atomic_term u=atomic_term
    {
      Primitive (Cons (t, u))
    }
  | MAP t=atomic_term u=atomic_term
    {
      Primitive (Map (t, u))
    }
  | ITER t=atomic_term u=atomic_term
    {
      Primitive (Iter (t, u))
    }
  | FOLD_LEFT t=atomic_term u=atomic_term v=atomic_term
    {
      Primitive (FoldL (t, u, v))
    }
  | FOLD_RIGHT t=atomic_term u=atomic_term v=atomic_term
    {
      Primitive (FoldR (t, u, v))
    }
  | s=simple_term LBRACKET id=ID DOT t=term RBRACKET
    {
      At (s, Fun (id,t))
    }
  | t=atomic_term
    {
      t
    }

atomic_term:
  | xs=tuple
    {
      tuple_or_term xs
    }
  | xs=tlist
    {
      make_list xs
    }
  | x=literal_or_var
    {
      x
    }

literal_or_var:
  | x=ID
    {
      Var x
    }
  | NIL
    {
      Const Nil
    }
  | b=TRUE
  | b=FALSE
    {
      Const (Bool b)
    }
  | x=INT
    {
      Const (Int x)
    }

%inline tlist:
  | LBRACKET xs=separated_list (SEMI_COLON, expression) RBRACKET { xs }

%inline tuple:
  | LPAREN xs=separated_list (COMMA, expression) RPAREN { xs }

%inline binop:
  | PLUS   { PPlus   }
  | MINUS  { PMinus  }
  | MULT   { PMult  }
  | DIV    { PDiv    }
  | EQUAL  { PEq     }
  | NEQUAL { PNeq    }
  | LAND   { PAndB   }
  | LOR    { POrB    }

typ:
  | ty1 = atomic_typ ARROW ty2 = typ
    {TyArrow (ty1, ty2)}
  | ty= atomic_typ STREAMTY
    { TyStream ty}
  | ty=atomic_typ LISTTY
    { TyList ty}
  | ty=atomic_typ TYTHUNK
    { TyThunk ty}
  | ty=atomic_typ {ty}

atomic_typ:
  | INTTY { TyInt }
  | BOOLTY { TyBool }
  | UNITTY { TyUnit }
  | LPAREN xs = separated_nonempty_list(MULT, typ) RPAREN
           {type_or_typrod $startpos $endpos xs}
