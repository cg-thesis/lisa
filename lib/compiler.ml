let translate = CSVCompiler.lisa_of_csv

let pretty_print p c =
  let ppf = Format.formatter_of_out_channel c in
  List.iter (fun x -> Format.fprintf ppf "%s" (IO.print_binding x)) p;
  Format.pp_print_flush ppf ()

let write_out filename p = pretty_print p (open_out filename)

let compile csv_filename lisa_filename out_filename =
  let input_sheet = IO.parse_csv csv_filename in
  let lisa_header =
    match lisa_filename with None -> [] | Some f -> IO.parse_file f
  in
  let lisa_program = translate lisa_header input_sheet in
  write_out out_filename lisa_program;
  let _ = Typechecker.translate [] lisa_program in
  flush_all

let compile_lisa fs =
  match fs with
  | [ csv; lisa; fout ] -> compile csv (Some lisa) fout
  | [ csv; fout ] -> compile csv None fout
  | _ -> failwith "Provide at least a source and destination path."

let input_file =
  Cmdliner.(
    Arg.(
      value
      & (pos_all string) []
      & info [] ~docv:"CMD" ~doc:"input lisa program."))

let cmd =
  Cmdliner.(
    let doc =
      "Compile the input CSV Formula spreadsheet program into a Lisa program."
    in
    let exits = Term.default_exits in
    (Term.(const compile_lisa $ input_file), Term.info "compile" ~doc ~exits))
