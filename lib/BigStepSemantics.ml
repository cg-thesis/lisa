open Syntax

let extend env x v = (x, ref v) :: env
let update env x v = List.assoc x env := v
let next = succ
let get_int = function VInt k -> k | _ -> assert false
let bind t f c = f (t c) c
let return v _ = v
let tick f c = f { c with t = c.t + 1 }
let reset f c = f { c with t = 0 }
let get c = c.env
let now c = c.t
let ( let* ) = bind

exception Blackhole of int

let rec expression e =
  match e with
  | App (m, n) ->
      let* m = expression m in
      let* n = expression n in
      app m n
  | Fby (m, Var l) ->
      let* env = get in
      let* time = now in
      let* m = expression m in
      let n =
        match !(List.assoc l env) with
        | VThunk { value = Running t } ->
            let def = t.definition in
            let context = t.def_context in
            VThunk
              {
                value =
                  Running
                    {
                      definition = def;
                      def_context = context;
                      last =
                        {
                          code = def;
                          context = { context with t = time + 1 };
                          history = Utils.Vector.make 0 VUnit;
                          access = ANil;
                        };
                      offset = 0;
                      kind = KStream;
                    };
              }
        | _ -> failwith "Unbound variable"
      in
      VFby (m, n) |> return
  | Fby (m, n) ->
      let* m = expression m in
      let* n = tick (expression n) in
      VFby (m, n) |> return
  | Tuple xs -> fun c -> VTuple (List.map (fun x -> expression x c) xs)
  | At (m, f) ->
      let* t = now in
      let* f' = expression f in
      let* time = app f' (VInt t) in
      at m (get_int time)
  | MuTuple ts ->
      fun c ->
        let env' =
          List.fold_left (fun env' (l, _) -> extend env' l VUnit) c.env ts
        in
        List.iter
          (fun (l, exp) ->
            let v =
              VThunk { value = Running (make_state { c with env = env' } exp) }
            in
            update env' l v)
          ts;

        VTuple
          (List.map
             (fun (l, _) -> expression (Force (Var l)) { c with env = env' })
             ts)
  | Primitive p -> primitive p
  | TypedExp (e, _) -> expression e
  | Var x ->
      let* env = get in
      (try !(List.assoc x env)
       with Not_found -> failwith (Printf.sprintf "`%s` is unbound.\n" x))
      |> return
  | Const x -> constant x |> return
  | Fun (p, exp) ->
      let* env = get in
      VClosure (p, exp, env) |> return
  | Force t ->
      let* v = expression t in
      force v
  | Thunk t -> fun c -> VThunk { value = Running (make_state c t) }
  | Fix (x, m) ->
      fun c ->
        let env = extend c.env x VUnit in
        let v = VThunk { value = Running (make_state { c with env } m) } in
        update env x v;
        expression m { c with env }
  | Iterate _ -> assert false

and force v =
  match v with
  (* | VThunk ({ value = Initialized code } as thunk) -> *)
  (*     let s = make_state code in *)
  (*     thunk.value <- Running s; *)
  (*     force_state callback s (s.offset + 1); *)
  (*     (c, get_current_value s) *)
  | VThunk { value = Running s } ->
      force_state s (s.offset + 1);
      get_current_value s
  | _ -> failwith "Ill typed term"

and make_state context code =
  let open Thunk in
  let offset = 0 in
  let history = Utils.Vector.make 0 VUnit in
  let access = ANil in
  let kind = KRegular in
  let last = { history; access; context; code } in
  let definition = code in
  let def_context = context in
  { offset; definition; def_context; last; kind }

and force_state s n =
  match s.last.access with
  | AInProgress k when k > n -> ()
  | ADone k when k >= n -> ()
  | (ANil | ADone _) when n > 0 ->
      iter_state s;
      s.last.context <- { s.last.context with t = s.last.context.t + 1 };
      force_state s n
  | AInProgress k when k <= n ->
      if k = 0 then s.last.access <- ANil else s.last.access <- ADone (k - 1);
      raise (Blackhole k)
  | _ -> ()

and get_current_value (s : (expression, bcontext, value) Thunk.state) =
  let open Thunk in
  match s.kind with
  | KRegular -> Utils.Vector.get s.last.history s.offset |> return
  | KStream ->
      let a = Utils.Vector.get s.last.history s.offset in
      let thunk_state = { s with offset = s.offset + 1 } in
      VFby (a, VThunk { value = Running thunk_state }) |> return

and iter_state s =
  let prev = s.last.access in
  let old_fuel = Thunk.set_in_progress s.last in
  (* Printf.printf "term:%s; time:%d\n" *)
  (*   (PrettyPrinter.string_of_term s.code) *)
  (*   (old_fuel + 1); *)
  (* flush_all (); *)
  (* ignore (read_line ()); *)
  try
    match expression s.last.code s.last.context with
    (* | _, VFby (a, VThunk { value = Initialized c }) -> *)
    (*     Thunk.update_history s a c (Thunk.ADone (old_fuel + 1)) KStream *)
    | VFby (a, VThunk { value = Running s' }) ->
        Thunk.update_history s a
          { s.last.context with env = s'.last.context.env }
          s'.last.code
          (Thunk.ADone (old_fuel + 1))
          KStream
    | v ->
        Thunk.update_history s v s.last.context s.last.code
          (Thunk.ADone max_int) KRegular
  with e ->
    s.last.access <- prev;
    raise e

and unfold_stream a n =
  let s =
    match a with
    | VFby (_, VThunk { value = Running state }) -> state
    (* | VFby (_, VThunk ({ value = Initialized code } as t)) -> *)
    (*     let s = make_state code in *)
    (*     t.value <- Running s; *)
    (*     s *)
    | _ -> assert false
  in
  force_state s (n + s.offset);
  s

and at m n c =
  match force (reset (expression m) c) c with
  | VFby (h, _) when n = 0 -> h
  | VFby (_, _) as a ->
      let s = unfold_stream a n in
      Thunk.get_history s (n - 1)
  | _ -> failwith "Ill typed term 1"

and app f v c =
  match f with
  | VClosure (x, exp, env) ->
      let env = extend env x v in
      let x = expression exp { c with env } in
      x
  | _ ->
      Printf.printf "--\n%s\n--\n" (ValuePrettyPrinter.string_of_value f);
      flush_all ();
      assert false
(*by typing *)

and constant x =
  match x with
  | Int i -> VInt i
  | Bool b -> VBool b
  | Unit -> VUnit
  | Nil -> VList []

and primitive p =
  match p with
  | Proj (i, j, tuple) ->
      let* tuple = expression tuple in
      projection i j tuple |> return
  | Input label ->
      fun c ->
        let exp = c.callback (label, c.t) in
        expression exp c
  | Head x ->
      let* x = expression x in
      head x |> return
  | Tail x ->
      let* x = expression x in
      tail x |> return
  | NegB x ->
      let* x = expression x in
      neg x |> return
  | Plus (i, j) ->
      let* i = expression i in
      let* j = expression j in
      bin_arith ( + ) i j |> return
  | Minus (i, j) ->
      let* i = expression i in
      let* j = expression j in
      bin_arith ( - ) i j |> return
  | Mult (i, j) ->
      let* i = expression i in
      let* j = expression j in
      bin_arith ( * ) i j |> return
  | Div (i, j) ->
      let* i = expression i in
      let* j = expression j in
      bin_arith ( / ) i j |> return
  | Eq (x, y) ->
      let* x = expression x in
      let* y = expression y in
      bin_comp ( = ) x y |> return
  | Neq (x, y) ->
      let* x = expression x in
      let* y = expression y in
      bin_comp ( <> ) x y |> return
  | AndB (x, y) ->
      let* x = expression x in
      let* y = expression y in
      bin_bool ( && ) x y |> return
  | OrB (x, y) ->
      let* x = expression x in
      let* y = expression y in
      bin_bool ( || ) x y |> return
  | Map (x, y) ->
      let* x = expression x in
      let* y = expression y in
      mapfun x y
  | Iter (x, y) ->
      let* x = expression x in
      let* y = expression y in
      let* _ = mapfun x y in
      return VUnit
  | FoldL (x, y, z) ->
      let* x = expression x in
      let* y = expression y in
      let* z = expression z in
      foldleft x y z
  | FoldR (x, y, z) ->
      let* x = expression x in
      let* y = expression y in
      let* z = expression z in
      foldright x y z
  | Cons (x, y) ->
      let* x = expression x in
      let* y = expression y in
      cons_list x y |> return
  | IfThenElse (b, x, y) ->
      let* b = expression b in
      if_then_else b x y

and foldleft f acc vs c =
  match vs with
  | VList vs -> List.fold_left (fun a v -> app (app f a c) v c) acc vs
  | _ -> assert false

(*by typing *)
and foldright f vs acc c =
  match vs with
  | VList vs -> List.fold_right (fun v a -> app (app f v c) a c) vs acc
  | _ -> assert false

(*by typing *)
and mapfun x y c =
  let app = app in
  match y with
  | VList vs -> VList (List.map (fun v -> app x v c) vs)
  | _ -> assert false

(* by typing *)
and cons_list x y =
  match y with VList xs -> VList (x :: xs) | _ -> assert false

(* by typing *)
and projection i j v =
  match v with
  | VTuple vs when i < j && j = List.length vs -> List.nth vs i
  | _ -> assert false

(* by typing *)
and neg b = match b with VBool b -> VBool (not b) | _ -> assert false
and head l = match l with VFby (h, _) -> h | _ -> assert false
and tail l = match l with VFby (_, t) -> t | _ -> assert false

and bin_arith op i j =
  match (i, j) with VInt i, VInt j -> VInt (op i j) | _ -> assert false

and bin_comp op x y = VBool (op x y)

and bin_bool op x y =
  match (x, y) with VBool x, VBool y -> VBool (op x y) | _ -> assert false

and if_then_else b x y c =
  let expression = expression in
  match b with
  | VBool b when b -> expression x c
  | VBool _ -> expression y c
  | _ -> assert false
(* by typing *)

and map_unfold n v =
  match v with
  | VFby (_, _) ->
      let _ = unfold_stream v n in
      v
  | VTuple vs ->
      let vs = List.map (map_unfold n) vs in
      VTuple vs
  | v -> v

let take callback n env exp =
  let context = { t = 0; env; callback } in
  let v = expression exp context in
  map_unfold n v

let eval callback env n = function
  | Def (x, _, e) ->
      let v = expression e { t = 0; env; callback } in
      (extend env x v, (Some x, v))
  | RecValues xs ->
      let xs = List.map (fun (x, _, t) -> (x, t)) xs in
      let v = take callback n env (MuTuple xs) in
      (env, (None, v))
  | RecDef (x, _, e) ->
      let v = expression (Fix (x, e)) { t = 0; env; callback } in
      (extend env x v, (Some x, v))
