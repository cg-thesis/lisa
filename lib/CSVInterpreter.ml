type source = Syntax.program * Libformula.Syntax.program
type index = int
type process = int -> Syntax.value

let current_index = ref 0
let empty = ([], (None, Syntax.VUnit))
let eval cb env x = BigStepSemantics.eval cb env !current_index x
let define cb (prev_env, _) current = eval cb prev_env current

let value_of cb program : Syntax.value =
  let _env, (_ty, value) = List.fold_left (define cb) empty program in
  value

let parse cb (header : string) (csv : string) =
  ignore header;

  let sheet_csv = Csv.load ~separator:'|' csv in
  let sheet = IO.(csv_mapi parseij_formula sheet_csv) in
  let labels = List.hd sheet_csv in
  let program = CSVCompiler.lisa_of_csv [] sheet in
  (labels, value_of cb program)

let process_of value n = BigStepSemantics.map_unfold n value

let wait_step_print_loop (labels, source) =
  let proc = ref (process_of source) in
  let rec loop () =
    print_endline "Press enter to compute current line.";
    match read_line () with
    | "" ->
        let v = !proc !current_index in
        proc := process_of v;
        incr current_index;
        Csv.print ~separator:'|'
          (IO.pprint_sheet (LisaCompiler.Values.csv_of_lisa_val labels v));
        loop ()
    | "exit" -> exit 0
    | _ -> loop ()
  in
  flush_all () |> loop
