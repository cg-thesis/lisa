open Syntax
module S = Syntax
open DirectSemantic

type ('a, 'b) eq = Refl : ('a, 'a) eq
type 'a box = Box of 'a

type _ typ =
  | TyInt : int typ
  | TyTime : time typ
  | TyMonad : 'a typ -> 'a DirectSemantic.t typ
  | TyArrow : 'a typ * 'b typ -> ('a -> 'b) typ
  | TyStream : 'a typ -> 'a seq typ
  | TyTuple1 : 'a typ -> 'a typ
  | TyTuple2 : 'a typ * 'b typ -> ('a * 'b) typ

let rec string_of_typ : type a. a typ -> string =
  Printf.(
    function
    | TyInt -> "int"
    | TyTime -> "time"
    | TyMonad t -> sprintf "monad (%s)" (string_of_typ t)
    | TyArrow (ty1, ty2) ->
        sprintf "(%s -> %s)" (string_of_typ ty1) (string_of_typ ty2)
    | TyStream s -> sprintf "stream (%s)" (string_of_typ s)
    | TyTuple1 t -> sprintf "tuple1 (%s)" (string_of_typ t)
    | TyTuple2 (a, b) ->
        sprintf "tuple2 (%s, %s)" (string_of_typ a) (string_of_typ b))

let rec eq_ty : type a b. a typ -> b typ -> (a, b) eq option =
 fun a b ->
  match (a, b) with
  | TyInt, TyInt -> Some Refl
  | TyTime, TyTime -> Some Refl
  | TyArrow (ity1, oty1), TyArrow (ity2, oty2) -> (
      match (eq_ty ity1 ity2, eq_ty oty1 oty2) with
      | Some Refl, Some Refl -> Some Refl
      | _, _ -> None)
  | TyTuple2 (ity1, oty1), TyTuple2 (ity2, oty2) -> (
      match (eq_ty ity1 ity2, eq_ty oty1 oty2) with
      | Some Refl, Some Refl -> Some Refl
      | _, _ -> None)
  | TyStream a, TyStream b -> (
      match eq_ty a b with Some Refl -> Some Refl | None -> None)
  | TyTuple1 a, TyTuple1 b -> (
      match eq_ty a b with Some Refl -> Some Refl | None -> None)
  | _, _ -> None

type some_typ = ExTy : 'a typ -> some_typ

let from_source_type =
  let rec aux : _ -> some_typ = function
    | S.TyInt -> ExTy TyInt
    | S.TyArrow (a, b) -> (
        match (aux a, aux b) with ExTy a, ExTy b -> ExTy (TyArrow (a, b)))
    | S.TyStream a -> ( match aux a with ExTy a -> ExTy (TyStream a))
    | S.TyProduct [ a ] -> ( match aux a with ExTy a -> ExTy (TyTuple1 a))
    | S.TyProduct [ a; b ] -> (
        match (aux a, aux b) with ExTy a, ExTy b -> ExTy (TyTuple2 (a, b)))
    | _ -> failwith "This type is not handled yet."
  in
  aux

module SM = Map.Make (String)

type dyn = Dyn : 'a typ * 'a t -> dyn

let coerce (type a b) (eq : (a, b) eq) : a -> b =
 fun x -> match eq with Refl -> x

exception CastError of string

let cast (type a b) (a : a typ) (b : b typ) : a t -> b t =
 fun x ->
  match eq_ty a b with
  | Some Refl -> x
  | None ->
      raise
        (CastError
           (Printf.sprintf "%s <> %s" (string_of_typ a) (string_of_typ b)))

let dyn_cast (type a) (ty : a typ) (d : dyn) =
  match d with Dyn (dty, x) -> cast dty ty x

type named_env = dyn SM.t

let lookup nenv ty x = dyn_cast ty (SM.find x nenv)
let bind nenv x dyn = SM.add x dyn nenv
let empty_env = SM.empty

let rec expression : type a. named_env -> a typ -> expression -> a t =
 fun nenv ty e ->
  match e with
  | Var x -> lookup nenv ty x
  | Fun (x, e) -> (
      match ty with
      | TyArrow (ity, oty) ->
          lam @@ fun a ->
          let nenv = bind nenv x (Dyn (ity, pure a)) in
          expression nenv oty e
      | _ -> assert false (* By welltypedness. *))
  | App (a, b) -> (
      match b with
      | TypedExp (arg, source_b_ty) -> (
          match from_source_type source_b_ty with
          | ExTy b_ty ->
              let a_ty = TyArrow (b_ty, ty) in
              app (expression nenv a_ty a) (expression nenv b_ty arg))
      | _ -> assert false (* By type elaboration. *))
  | Fby (x, xs) -> (
      match ty with
      | TyStream a -> emit (expression nenv a x) (expression nenv ty xs)
      | _ -> assert false (* By welltypedness. *))
  | At (s, time_mapping) ->
      observe
        (expression nenv (TyStream ty) s)
        (expression nenv TyTime time_mapping)
  | Tuple [ e1; e2 ] -> (
      match ty with
      | TyTuple2 (a, b) ->
          let* x = expression nenv a e1 in
          let* y = expression nenv b e2 in
          pure (x, y)
      | _ -> assert false (* By welltypedness. *))
  | Tuple _ -> failwith "Only pairs are supported for now."
  | MuTuple [ (x, e) ] -> (
      match ty with
      | TyTuple1 a ->
          rec1 (fun self ->
              let nenv = bind nenv x (Dyn (a, self)) in
              let* a = expression nenv a e in
              pure a)
      | _ -> assert false (* By welltypedness. *))
  | MuTuple [ (x1, e1); (x2, e2) ] -> (
      match ty with
      | TyTuple2 (a, b) ->
          rec2 (fun self1 self2 ->
              let nenv = bind nenv x1 (Dyn (a, self1)) in
              let nenv = bind nenv x2 (Dyn (b, self2)) in
              let* a = expression nenv a e1 in
              let* b = expression nenv b e2 in
              pure (a, b))
      | _ -> assert false (* By welltypedness. *))
  | MuTuple _ ->
      failwith "Only recursive values and pairs are supported for now."
  | Primitive p -> (
      match p with
      | Proj (0, 1, TypedExp (e, _)) -> expression nenv (TyTuple1 ty) e
      | Proj (0, 2, TypedExp (e, S.TyProduct [ _; ty2 ])) -> (
          match from_source_type ty2 with
          | ExTy ety2 ->
              let* x, _ = expression nenv (TyTuple2 (ty, ety2)) e in
              pure x)
      | Proj (1, 2, TypedExp (e, S.TyProduct [ ty1; _ ])) -> (
          match from_source_type ty1 with
          | ExTy ety1 ->
              let* _, y = expression nenv (TyTuple2 (ety1, ty)) e in
              pure y)
      | Plus (e1, e2) -> (
          match ty with
          | TyInt ->
              let* v1 = expression nenv TyInt e1 in
              let* v2 = expression nenv TyInt e2 in
              pure (v1 + v2)
          | _ -> assert false (* By welltypedness. *))
      | Minus (e1, e2) -> (
          match ty with
          | TyInt ->
              let* v1 = expression nenv TyInt e1 in
              let* v2 = expression nenv TyInt e2 in
              pure (v1 - v2)
          | _ -> assert false (* By welltypedness. *))
      | p ->
          failwith
            (Printf.sprintf "Unsupported primitive: %s"
               (PrettyPrinter.string_of_term (Primitive p))))
  | Const (Int i) -> (
      match ty with
      | TyInt -> pure i
      | _ -> assert false (* By welltypedness. *))
  | TypedExp (e, _) -> expression nenv ty e
  | _ -> assert false
(* Colin : FIX ME: add case for booleans *)

let translate (type a) e (ety : a typ) : a t = expression empty_env ety e
