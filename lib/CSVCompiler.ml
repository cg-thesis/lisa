module S = Libformula.Syntax
(** Source language **)

module T = Syntax
(** Target language **)

open SemanticsCommon

let env = ref []

let rec translate label i (formula : S.expression) : T.expression =
  match formula with
  | Var v -> Var v
  | Const Unit -> T.(Const Unit)
  | Const (Bool b) -> T.(Const (Bool b))
  | Const (Int t) -> T.(Const (Int t))
  | App (m, n) ->
      let m = translate label i m in
      let n = translate label i n in
      T.App (m, n)
  | At (Var s, t, rel) ->
      let k = integer t in
      obs (Var s) k rel
  | List es ->
      List.fold_right
        (fun x acc ->
          let x = translate label i x in
          T.(Primitive (Cons (x, acc))))
        es
        T.(Const Nil)
  | Primitive Input -> T.(Primitive (Input label))
  | Primitive (Fst e) -> T.(Primitive (Proj (0, 2, translate label i e)))
  | Primitive (Snd e) -> T.(Primitive (Proj (1, 2, translate label i e)))
  | Primitive (NegB e) -> T.(Primitive (NegB (translate label i e)))
  | Primitive (Plus (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (Plus (a, b)))
  | Primitive (Minus (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (Minus (a, b)))
  | Primitive (Div (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (Div (a, b)))
  | Primitive (Times (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (Mult (a, b)))
  | Primitive (EqB (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (Eq (a, b)))
  | Primitive (AndB (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (AndB (a, b)))
  | Primitive (OrB (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (OrB (a, b)))
  | Primitive (IfThenElse (a, b, c)) ->
      let a = translate label i a
      and b = translate label i b
      and c = translate label i c in
      T.(Primitive (IfThenElse (a, b, c)))
  | Pair (a, b) ->
      let a = translate label i a and b = translate label i b in
      T.(Tuple [ a; b ])
  | Primitive (MapMap (a, b)) ->
      let a = translate label i a and b = translate label i b in
      T.(Primitive (Map (a, b)))
  | Primitive (MapUpdate (key, value, upfn, map)) ->
      if List.mem_assoc "map_update" !env then ()
      else env := ("mem_assoc", mem_assoc) :: ("map_update", map_update) :: !env;
      List.map (translate label i) [ key; value; upfn; map ]
      |> List.fold_left app (var "map_update")
  | At (_, _, _) -> assert false

let translate_column (c : S.expression list) =
  match c with
  | Var l :: _ when l <> "__nil__" -> (
      let xs = List.mapi (fun i x -> (translate l i) x) c in
      match xs with
      | [] -> failwith "empty column"
      | [ Var v ] when l <> "__nil__" ->
          (v, None, T.(fby (Primitive (Input v)) (var v)))
      | [ Var v; x ] when l <> "__nil__" -> (v, None, fby x (var v))
      | Var v :: xs when l <> "__nil__" ->
          let rec aux xs =
            match xs with
            | [ x ] ->
                let st_name = "st_" ^ v in
                proj 0 1 (mtup [ (st_name, fby x (var st_name)) ])
            | T.Var "__nil__" :: xs ->
                fby (Primitive (Input v)) (thunk (aux xs))
            | x :: xs -> fby x (thunk (aux xs))
            | [] -> fby (Primitive (Input v)) (var v)
          in
          (v, None, aux xs)
      | x :: _ ->
          let s = PrettyPrinter.string_of_term x in
          Printf.eprintf "%s \n" s;
          raise Exit)
  | _ -> failwith "malformed column"

let lisa_of_csv (header : T.program) (source : S.program) : T.program =
  let st = T.RecValues (List.map translate_column source) in
  let h =
    List.fold_right (fun (x, y) h -> T.(Def (x, None, y) :: h)) !env header
  in
  env := [];
  h @ [ st ]
