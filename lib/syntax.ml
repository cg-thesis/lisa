type variable = string
and program = definition list

and definition =
  | Def of variable * nominal_type option * expression
  | RecValues of (variable * nominal_type option * expression) list
  | RecDef of variable * nominal_type option * expression

and expression =
  | Var of variable
  | Fun of variable * expression
  | App of expression * expression
  | Fby of expression * expression
  | Fix of variable * expression
  | Thunk of expression
  | Force of expression
  | Iterate of int * int * expression list * expression
  | At of expression * expression
  | Tuple of expression list
  | MuTuple of (variable * expression) list
  | Primitive of primitive
  | Const of const
  | TypedExp of typed_expression

and const = Unit | Nil | Bool of bool | Int of int

and primitive =
  | Input of string
  | NegB of expression
  | Head of expression
  | Tail of expression
  | Proj of int * int * expression
  | Plus of expression * expression
  | Minus of expression * expression
  | Mult of expression * expression
  | Div of expression * expression
  | Eq of expression * expression
  | Neq of expression * expression
  | AndB of expression * expression
  | OrB of expression * expression
  | Cons of expression * expression
  | Map of expression * expression
  | Iter of expression * expression
  | FoldL of expression * expression * expression
  | FoldR of expression * expression * expression
  | IfThenElse of expression * expression * expression

and typed_expression = expression * nominal_type
and bcontext = { t : int; env : environment; callback : callback }
and callback = string * int -> expression
and environment = (variable * value ref) list
and thunk = { mutable value : (expression, bcontext, value) Thunk.t }

and value =
  | VInt of int
  | VBool of bool
  | VUnit
  | VClosure of variable * expression * environment
  | VFby of value * value
  | VTuple of value list
  | VThunk of thunk
  | VList of value list

and context =
  | Hole
  | LeApp of context * expression
  | RiApp of expression * context
  | LeFby of context * expression
  | RiFby of expression * context
  | LeAt of context * expression
  | RiAt of expression * context
  | CForce of context
  | EltTuple of expression list * context * expression list
  | More of int * int * expression list * context
  | PrimArgs of prim_args

and prim_args =
  | CNegB of context
  | CTail of context
  | CHead of context
  | ProjArg of int * int * context
  | LeMinus of context * expression
  | RiMinus of expression * context
  | LePlus of context * expression
  | RiPlus of expression * context
  | LeTimes of context * expression
  | RiTimes of expression * context
  | LeDiv of context * expression
  | RiDiv of expression * context
  | LeEq of context * expression
  | RiEq of expression * context
  | LeNeq of context * expression
  | RiNeq of expression * context
  | LeAndB of context * expression
  | RiAndB of expression * context
  | LeOrB of context * expression
  | RiOrB of expression * context
  | LeCons of context * expression
  | RiCons of expression * context
  | LeMap of context * expression
  | RiMap of expression * context
  | LeIter of context * expression
  | RiIter of expression * context
  | FstFoldL of context * expression * expression
  | SndFoldL of expression * context * expression
  | ThrdFoldL of expression * expression * context
  | FstFoldR of context * expression * expression
  | SndFoldR of expression * context * expression
  | ThrdFoldR of expression * expression * context
  | CIfThenElse of context * expression * expression

and 'a typ =
  | TyVar of 'a
  | TyUnit
  | TyInt
  | TyBool
  | TyList of 'a typ
  | TyArrow of 'a typ * 'a typ
  | TyStream of 'a typ
  | TyThunk of 'a typ
  | TyProduct of 'a typ list

and nominal_type = int typ
