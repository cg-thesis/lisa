open Cmdliner

let print_out (l, v) ty =
  let l = match l with Some l -> l | None -> "-" in
  let ty =
    match ty with
    | Some ty -> Printf.sprintf ": %s" (PrettyPrinter.string_of_type ty)
    | None -> ""
  in
  Printf.printf "%s %s = %s\n" l ty (ValuePrettyPrinter.string_of_value v)

let get_input (label, time) =
  let open IO in
  let pr = !prompt in
  let message =
    Printf.sprintf "Need a value for %s at index %d : \n" label time
  in
  set_prompt message;
  let exp = parse_expression (read ()) in
  set_prompt pr;
  exp

let rec extract_ty tyenv env =
  let open Syntax in
  let get_ty x = List.assoc_opt x tyenv in
  match env with
  | [] -> None
  | [ Def (x, _, _) ] | [ RecDef (x, _, _) ] -> get_ty x
  | [ RecValues xs ] -> (
      let tys = List.map (fun (x, _, _) -> get_ty x) xs in
      try
        let tys =
          List.fold_right
            (fun x ts ->
              match x with
              | Some (TyThunk ty) -> ty :: ts
              | _ -> raise Not_found)
            tys []
        in
        Some (TyProduct tys)
      with Not_found -> None)
  | _ :: xs -> extract_ty tyenv xs

let rec interactive env ty_env () =
  IO.set_prompt "lisa>";
  let line = IO.read () in
  let input_def = IO.parse_binding line in
  let t, ty_env = Typechecker.translate ty_env input_def in
  let ty = extract_ty ty_env t in
  (* Format.printf "%a" (IO.print_binding ~memoize:false)
   * (Semantics.binding get_input env 5 (List.hd input_def));
   * Format.print_flush (); *)
  let env, res = BigStepSemantics.eval get_input env 10 (List.hd input_def) in
  print_out res ty;
  Format.print_flush ();
  try_interactive env ty_env ()

and try_interactive env tyenv () =
  try interactive env tyenv () with
  | Typechecker.Unify (_, ty1, ty2) ->
      Printf.eprintf "Type %s is expected but received %s.\n"
        (PrettyPrinter.string_of_type ty1)
        (PrettyPrinter.string_of_type ty2);
      flush_all ();
      try_interactive env tyenv ()
  | Typechecker.Poly ty ->
      Printf.eprintf
        "Type %s should be monomorph you may provide a type annotation.\n"
        (PrettyPrinter.string_of_type ty);
      flush_all ();
      try_interactive env tyenv ()
  | Typechecker.Unbound (_, Var x) ->
      Printf.eprintf "Type `%s` is unbound.\n" x;
      flush_all ();
      try_interactive env tyenv ()
  | End_of_file -> exit 0

let run_interactive () = ignore (try_interactive [] [] ())

let csv_interactive fname =
  CSVInterpreter.(wait_step_print_loop (parse get_input "" fname))

let repl =
  let doc = "Interactive interpreter for Lisa." in
  let exits = Term.default_exits in
  Term.(const (try_interactive [] []) $ const (), info "interactive" ~doc ~exits)

let eval_defs csv lisa lines =
  let src_l = match lisa with None -> [] | Some x -> IO.parse_file x in
  let src_csv = Csv.load ~separator:'|' csv in
  let src = IO.(csv_mapi parseij_formula src_csv) in
  let labels = List.hd src_csv in
  let s = CSV.eval get_input labels src src_l lines in
  let csv_s = IO.pprint_sheet s in
  Csv.save ~separator:'|' (csv ^ ".out") csv_s;
  flush_all

let csv =
  Arg.(
    required
    & pos ~rev:true 0 (some string) None
    & info [] ~docv:"SRC" ~doc:"input csv program")

let lisa =
  Arg.(
    value
    & opt (some string) None
    & info [ "h"; "header" ] ~docv:"SRC"
        ~doc:"Add $(docv) as lisa header program to the evaluate a CSV")

let cbpv_src =
  Arg.(
    required
    & pos ~rev:true 0 (some string) None
    & info [] ~docv:"SRC" ~doc:"input cbpv program")

let cbpv_ts =
  Arg.(
    value & opt string "core"
    & info [ "c"; "type-checker" ] ~docv:"TC"
        ~doc:
          "Select the $(docv) type checker for Core Lisa (core) or Pure Lisa \
           (pure)")

let lines =
  Arg.(
    value & opt int 10
    & info [ "n"; "lines" ] ~docv:"NUM" ~doc:"$(docv) of lines to produce")

let take =
  let doc =
    "Batch evaluation for CSV Formula spreadsheets with an optional Lisa \
     header."
  in
  let exits = Term.default_exits in
  (Term.(const eval_defs $ csv $ lisa $ lines), Term.info "eval" ~doc ~exits)

let csv_loop =
  let doc = "Interactive interpreter for CSV Formula spreadsheets." in
  let exits = Term.default_exits in
  Term.(const csv_interactive $ csv, info "csv" ~doc ~exits)

let cbpv_repl =
  let doc = "Interpreter for cbpv." in
  let exits = Term.default_exits in
  let open Libcbpv_compiler in
  let open Interpreter in
  Term.(const read_eval_print $ cbpv_ts $ const (), info "cbpv" ~doc ~exits)

let eval_file_to_file ts fin =
  let open Libcbpv_compiler in
  let open Interpreter in
  let fout = fin ^ ".out" in
  let c_out = open_out fout in
  eval_file ts fin |> to_string |> Printf.fprintf c_out "%s\n";
  flush_all

let cbpv_eval_file =
  let doc = "Batch interpreter for cbpv." in
  let exits = Term.default_exits in
  Term.
    ( const eval_file_to_file $ cbpv_ts $ cbpv_src,
      Term.info "batch-cbpv" ~doc ~exits )
