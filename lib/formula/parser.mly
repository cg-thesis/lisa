%{ (* Emacs, open this with -*- tuareg -*- *)
   open Syntax
   open Utils.Error

   type binop =
     | PPlus
     | PMinus
     | PTimes
     | PDiv
     | PEq

   let make_binop lhs op rhs =
     match op with
     | PPlus  ->
        Plus (lhs, rhs)
     | PMinus ->
        Minus (lhs, rhs)
     | PTimes ->
        Times (lhs, rhs)
     | PDiv ->
        Div (lhs, rhs)
     | PEq ->
        EqB (lhs, rhs)
%}

%token IF
%token FST
%token SND
%token NOT
%token PAIR
%token AND
%token OR
%token PLUS
%token MINUS
%token DIV
%token MULT
%token MAP
%token UPD
%token EMPTY
%token EQUAL
%token <int> INT
%token <bool> TRUE
%token <bool> FALSE
%token <string> ID
%token DOT
%token COMMA
%token LPAREN
%token RPAREN
%token LBRACKET
%token RBRACKET
%token NIL
%token EOF

%nonassoc EQUAL
%left PLUS MINUS
%left MULT DIV

%start <Syntax.expression> formula

%%

formula:
  | NIL { Var "__nil__" }
  | EQUAL a=expression EOF { a }
  | t=lit_or_var EOF { t }
  | error { raise (SyntaxError (Utils.Position.lex_join $startpos $endpos) )}

%inline fpair:
  | PAIR LPAREN a=expression COMMA b=expression RPAREN
    { Pair (a, b)}

expression:
  | a=term b=expression
    { App(a,b) }
  | FST LPAREN e=expression RPAREN
    { Primitive (Fst e) }
  | SND LPAREN e=expression RPAREN
    { Primitive (Snd e) }
  | NOT LPAREN e=expression RPAREN
    { Primitive (NegB e) }
  | AND LPAREN a=expression COMMA b=expression RPAREN
    { Primitive (AndB (a, b)) }
  | OR  LPAREN a=expression COMMA b=expression RPAREN
    { Primitive (OrB  (a, b)) }
  | IF LPAREN a=expression COMMA b=expression COMMA c=expression RPAREN
    { Primitive (IfThenElse (a, b ,c)) }
  | MAP LPAREN ps=separated_nonempty_list(COMMA, fpair) RPAREN
    { List ps }
  | MAP DOT EMPTY LPAREN RPAREN
    { List [] }
  | MAP DOT MAP LPAREN a=expression COMMA b=expression RPAREN
    { Primitive (MapMap(a, b)) }
  | MAP DOT UPD LPAREN a=expression COMMA b=expression COMMA c=expression COMMA d=expression RPAREN
    { Primitive (MapUpdate(a, b, c, d)) }
  | t=term { t }

term:
  | lhs=term op=binop rhs=term
    { Primitive (make_binop lhs op rhs) }
  | v=ID LBRACKET RBRACKET { At(Var v,0, Some RPos) }
  | v=ID LBRACKET i=INT RBRACKET { At(Var v,i, None) }
  | v=ID LBRACKET PLUS i=INT RBRACKET { At(Var v,i, Some (RPos)) }
  | v=ID LBRACKET MINUS i=INT RBRACKET { At(Var v, i, Some (RNeg)) }
  | t=atomic_term { t }

atomic_term:
  | t=lit_or_var { t }
  | p = fpair
    { p }
  | LPAREN x=expression RPAREN
    { x }

lit_or_var:
  | LPAREN RPAREN { Const Unit }
  | b=TRUE | b=FALSE { Const (Bool b) }
  | i=INT { Const (Int i) }
  | v=ID { Var v }

%inline binop:
  | PLUS   { PPlus   }
  | MINUS  { PMinus  }
  | MULT   { PTimes  }
  | DIV    { PDiv    }
  | EQUAL  { PEq     }
