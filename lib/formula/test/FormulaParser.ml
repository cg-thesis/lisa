open Lisa.IO
open Libformula.Printer

let parse_print_formula s = parse_formula s |> string_of_formula

let parse_print_sheet s =
  let exp_csv = csv_mapi parseij_formula s in
  let string_csv = map_pprint exp_csv in
  let b = Buffer.create 256 in
  let out = Csv.to_buffer ~separator:'|' b in
  Csv.(output_all out (transpose string_csv));
  Buffer.contents b

let alice =
  [
    [ "user"; "amount"; "value"; "operations" ];
    [ ""; ""; "=amount[1]"; "=IF(user[1] = 0, MAP(PAIR(0, 0)), MAP.EMPTY())" ];
    [
      "";
      "";
      "=IF(user[2] = 0, 0, value[1] + amount[2])";
      "= IF(user[2] = 0, MAP(PAIR(0, value[1])), MAP.EMPTY())";
    ];
  ]

let nat = [ [ "nat" ]; [ "0" ]; [ "=(nat[1]) + 1" ] ]

let parse_alice () =
  Alcotest.(check string)
    "same string"
    "user|amount|value|operations\n\
     ||=amount[1]|=IF(user[1] = 0, MAP(PAIR(0, 0)), MAP.EMPTY())\n\
     ||=IF(user[2] = 0, 0, value[1] + amount[2])|=IF(user[2] = 0, MAP(PAIR(0, \
     value[1])), MAP.EMPTY())\n"
    (parse_print_sheet alice)

let parse_nat () =
  Alcotest.(check string)
    "same string" "nat\n0\n=nat[1] + 1\n" (parse_print_sheet nat)

let parse_unit () =
  Alcotest.(check string) "same string" "()" (parse_print_formula "()")

let parse_var () =
  Alcotest.(check string) "same string" "x" (parse_print_formula "x")

let parse_int () =
  Alcotest.(check string) "same string" "1" (parse_print_formula "1")

let parse_int2 () =
  Alcotest.(check string)
    "same string" "23424564"
    (parse_print_formula "23424564")

let parse_empty () =
  Alcotest.(check string) "same string" "()" (parse_print_formula "()")

let parse_true () =
  Alcotest.(check string) "same string" "true" (parse_print_formula "true")

let parse_false () =
  Alcotest.(check string) "same string" "false" (parse_print_formula "false")

let parse_pair () =
  Alcotest.(check string)
    "same string" "=PAIR(x, y)"
    (parse_print_formula "=PAIR(x, y)")

let parse_fst () =
  Alcotest.(check string)
    "same string" "=FST(x)"
    (parse_print_formula "=FST(x)")

let parse_snd () =
  Alcotest.(check string)
    "same string" "=SND(y)"
    (parse_print_formula "=SND(y)")

let parse_arith_plus () =
  Alcotest.(check string) "same string" "=x + y" (parse_print_formula "= x + y")

let parse_arith_minus () =
  Alcotest.(check string)
    "same string" "=42 - 43"
    (parse_print_formula "=42 - 43")

let parse_arith_times () =
  Alcotest.(check string)
    "same string" "=42 * x"
    (parse_print_formula "= 42 * x")

let parse_arith_div () =
  Alcotest.(check string) "same string" "=y / 1" (parse_print_formula "= y / 1")

let parse_arith () =
  Alcotest.(check string)
    "same string" "=(1 + (2 * 3)) - (1 / 2)"
    (parse_print_formula "= 1 + 2 * 3 - 1 / 2")

let parse_bool_neg () =
  Alcotest.(check string)
    "same string" "=NOT(a)"
    (parse_print_formula "=NOT(a)")

let parse_bool_and () =
  Alcotest.(check string)
    "same string" "=AND(x, false)"
    (parse_print_formula "=AND(x,false)")

let parse_bool_and2 () =
  Alcotest.(check string)
    "same string" "=AND(x, AND(false, false))"
    (parse_print_formula "=AND(x,AND(false, false))")

let parse_bool_or () =
  Alcotest.(check string)
    "same string" "=OR(x, y)"
    (parse_print_formula "=OR(x, y)")

let parse_bool_eq () =
  Alcotest.(check string) "same string" "=x = y" (parse_print_formula "=x = y")

let parse_map_map () =
  Alcotest.(check string)
    "same string" "=MAP.MAP(succ, xs)"
    (parse_print_formula "=MAP.MAP(succ,xs)")

let parse_map_upd () =
  Alcotest.(check string)
    "same string" "=MAP.UPD(x, y, succ, xs)"
    (parse_print_formula "=MAP.UPD(x,y,succ,xs)")

let parse_if_then_else () =
  Alcotest.(check string)
    "same string" "=IF(condition, branch_one, branch_two)"
    (parse_print_formula "=IF(condition, branch_one, branch_two)")

let parse_if_then_else2 () =
  Alcotest.(check string)
    "same string" "=IF(true, IF(false, 1, 2), 3)"
    (parse_print_formula "=IF(true, IF(false, 1, 2), 3)")

let parse_if_then_else3 () =
  Alcotest.(check string)
    "same string" "=IF(true, 2, IF(false, 1, 3))"
    (parse_print_formula "=IF(true, 2, IF(false, 1, 3))")

let parse_if_then_else4 () =
  Alcotest.(check string)
    "same string" "=IF(IF(true, true, false), 2, 3)"
    (parse_print_formula "=IF(IF(true, true, false), 2, 3)")

let parse_at () =
  Alcotest.(check string) "same string" "=x[1]" (parse_print_formula "=x[1]")

let parse_app () =
  Alcotest.(check string)
    "same string" "=h (g x)"
    (parse_print_formula "=h (g x)")

let () =
  Alcotest.run "Formula parser"
    [
      ( "Literals",
        [
          Alcotest.test_case "unit" `Quick parse_unit;
          Alcotest.test_case "integers" `Quick parse_int;
          Alcotest.test_case "integers" `Quick parse_int2;
          Alcotest.test_case "bool" `Quick parse_true;
          Alcotest.test_case "bool" `Quick parse_false;
          Alcotest.test_case "pair" `Quick parse_pair;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "pair" `Quick parse_fst;
          Alcotest.test_case "pair" `Quick parse_snd;
          Alcotest.test_case "arith" `Quick parse_arith_plus;
          Alcotest.test_case "arith" `Quick parse_arith_minus;
          Alcotest.test_case "arith" `Quick parse_arith_times;
          Alcotest.test_case "arith" `Quick parse_arith_div;
          Alcotest.test_case "arith" `Quick parse_arith;
          Alcotest.test_case "bool" `Quick parse_bool_neg;
          Alcotest.test_case "bool" `Quick parse_bool_and;
          Alcotest.test_case "bool" `Quick parse_bool_and2;
          Alcotest.test_case "bool" `Quick parse_bool_or;
          Alcotest.test_case "bool" `Quick parse_bool_eq;
          Alcotest.test_case "if" `Quick parse_if_then_else;
          Alcotest.test_case "if" `Quick parse_if_then_else2;
          Alcotest.test_case "if" `Quick parse_if_then_else3;
          Alcotest.test_case "if" `Quick parse_if_then_else4;
          Alcotest.test_case "at" `Quick parse_at;
          Alcotest.test_case "app" `Quick parse_app;
          Alcotest.test_case "map" `Quick parse_map_map;
          Alcotest.test_case "map" `Quick parse_map_upd;
        ] );
      ( "Examples",
        [
          Alcotest.test_case "Alice" `Quick parse_alice;
          Alcotest.test_case "Nat" `Quick parse_nat;
        ] );
    ]
