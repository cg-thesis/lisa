type var = string

(** Description of the language:

   The formula language allows to do basic arithmetic, boolean algebra
   and map primitives. Maps are used to store date and to
   represent transfer operations as well.

   Expressions:

     An expression is formed of either a variable, a constant, pairs,
   indexed expressions and primitives.

     Indexed expressions, should only be used to access value in
   another column in the current or previous rows but not in later
   rows.

   Primitives:

     Traditional operators of integer algebra are provided and
   similarly for boolean algebra.

     Besides that, conidtional expressions are supported as well as
   few operations on maps. Maps are the chosen data structures for
   operations where recipients are mapped onto amounts.

   MAP.MAP : 'a map -> ('a -> 'b ) -> 'b map

   This function will map a function on all the values of the map
   while keeping the same keys.


   MAP.UPD : 'a -> 'b -> ('a -> 'b -> 'b) -> 'b map

   This function will update a the value of map's key with the value
   of the function. Can be used to delete or add values as well.

**)

and expression =
  | App of expression * expression
  | Var of var
  | Const of const
  | Primitive of primitive
  | At of expression * int * rel option
  | Pair of expression * expression
  | List of expression list

and const = Unit | Bool of bool | Int of int
and rel = RPos | RNeg

and primitive =
  | Input
  | Fst of expression
  | Snd of expression
  | NegB of expression
  | Plus of expression * expression
  | Minus of expression * expression
  | Times of expression * expression
  | Div of expression * expression
  | EqB of expression * expression
  | AndB of expression * expression
  | OrB of expression * expression
  | IfThenElse of expression * expression * expression
  | MapMap of expression * expression
  | MapUpdate of expression * expression * expression * expression

and program = expression list list
and t = program
