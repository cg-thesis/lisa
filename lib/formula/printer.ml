open Syntax
open PPrint

let rec print_const c =
  match c with
  | Unit -> parens empty
  | Bool b -> OCaml.bool b
  | Int n -> OCaml.int n

and print_infix op x y = print_atom x ^^ space ^^ op ^^ space ^^ print_atom y

and print_list l =
  match l with
  | [] -> !^"MAP.EMPTY()"
  | _ ->
      prefix 2 0 !^"MAP"
        (parens (separate_map (comma ^^ space) print_expression l))

and print_prim_fun id args =
  let args =
    match args with
    | [ arg ] -> print_expression arg
    | _ -> separate_map (comma ^^ space) print_expression args
  in
  group (id ^^ parens args)

and print_expression exp =
  match exp with
  | App (x, y) -> prefix 2 1 (print_expression x) (print_atom y)
  | Primitive x -> print_primitive x
  | _ -> print_atom exp

and print_primitive p =
  match p with
  | Input -> empty
  | Fst x -> print_prim_fun !^"FST" [ x ]
  | Snd x -> print_prim_fun !^"SND" [ x ]
  | NegB x -> print_prim_fun !^"NOT" [ x ]
  | Plus (x, y) -> print_infix plus x y
  | Minus (x, y) -> print_infix minus x y
  | Times (x, y) -> print_infix star x y
  | Div (x, y) -> print_infix slash x y
  | EqB (x, y) -> print_infix equals x y
  | AndB (x, y) -> print_prim_fun !^"AND" [ x; y ]
  | OrB (x, y) -> print_prim_fun !^"OR" [ x; y ]
  | IfThenElse (c, x, y) -> print_prim_fun !^"IF" [ c; x; y ]
  | MapUpdate (key, value, update_fn, map) ->
      print_prim_fun !^"MAP.UPD" [ key; value; update_fn; map ]
  | MapMap (fn, map) -> print_prim_fun !^"MAP.MAP" [ fn; map ]

and print_atom exp =
  match exp with
  | Var x -> string x
  | Const x -> print_const x
  | Primitive Input -> empty
  | At (Var v, x, None) -> string v ^^ brackets (OCaml.int x)
  | Pair (x, y) -> print_prim_fun !^"PAIR" [ x; y ]
  | List vs -> print_list vs
  | At (Var v, x, Some RPos) -> string v ^^ brackets (plus ^^ OCaml.int x)
  | At (Var v, x, Some RNeg) -> string v ^^ brackets (minus ^^ OCaml.int x)
  | App _ | Primitive _ -> group (parens (print_expression exp))
  | _ -> assert false

and print_formula exp =
  match exp with
  | Var _ | Const _ | Primitive Input -> print_atom exp
  | _ -> equals ^^ print_expression exp

let string_of_formula d = Utils.Printer.document_to_string print_formula d
let print_sheet s = List.(map (map string_of_formula)) s

let string_of_sheet s =
  let b = Buffer.create 256 in
  (print_sheet s |> Csv.transpose
  |> Csv.(output_all (to_buffer ~separator:'|' b)));
  Buffer.contents b
