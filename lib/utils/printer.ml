let document_to_string print_document doc =
  let buffer = Buffer.create 256 and ppv = print_document doc in
  PPrint.ToBuffer.pretty 0.9 80 buffer ppv;
  Buffer.contents buffer

let document_to_format print_document fmt doc =
  let doc = print_document doc in
  PPrint.ToFormatter.pretty 0.9 80 fmt doc
