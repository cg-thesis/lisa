type 'a t = { mutable init : 'a; mutable size : int; mutable data : 'a array }

let make n init =
  if n < 0 || n > Sys.max_array_length then invalid_arg "Vector.make";
  { init; size = 0; data = Array.make n init }

let create init = make 0 init
let length t = t.size

let get a i =
  if i < 0 || i >= a.size then invalid_arg "Vector.get";
  Array.unsafe_get a.data i

let set a i v =
  if i < 0 || i >= a.size then invalid_arg "Vector.set";
  Array.unsafe_set a.data i v

let resize a s =
  if s < 0 then invalid_arg "Vector.resize";
  let n = Array.length a.data in
  if s > n then (
    (* reallocate into a larger array *)
    if s > Sys.max_array_length then invalid_arg "Vector.resize: cannot grow";
    let n' = min (max (2 * n) s) Sys.max_array_length in
    let a' = Array.make n' a.init in
    Array.blit a.data 0 a' 0 a.size;
    a.data <- a');
  a.size <- s

let is_empty a = length a = 0
let clear a = resize a 0

let push a v =
  let n = a.size in
  resize a (n + 1);
  Array.unsafe_set a.data n v

let list_of t = Array.sub t.data 0 t.size |> Array.to_list

let slice_from t start =
  Array.sub t.data start (t.size - start) |> Array.to_list
