let trivial () = Alcotest.(check bool) "trivial" true true

let () =
  Alcotest.run "Utils"
    [ ("Utils", [ Alcotest.test_case "trivial" `Quick trivial ]) ]
