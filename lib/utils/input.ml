open Lexing

let open_in filename =
  let lexbuf = from_channel (open_in filename) in
  lexbuf.lex_curr_p <-
    { pos_fname = filename; pos_lnum = 1; pos_bol = 0; pos_cnum = 0 };
  lexbuf

let newline lexbuf =
  let pos = lexbuf.lex_curr_p in
  lexbuf.lex_curr_p <-
    { pos with pos_lnum = pos.pos_lnum + 1; pos_bol = pos.pos_cnum }

let prompt = ref ""
let set_prompt = ( := ) prompt

let print_prompt () =
  output_string stdout !prompt;
  flush stdout

let input_char =
  let display_prompt = ref true in
  let ask stdin =
    if !display_prompt then (
      display_prompt := false;
      print_prompt ());
    let c = input_char stdin in
    if c = '\n' then display_prompt := true;
    String.make 1 c
  in
  ask

let read () =
  let b = Buffer.create 13 in
  let rec read prev =
    let c = input_char stdin in
    if c = "\n" then
      if prev <> "\\" then (
        Buffer.add_string b prev;
        let b = Buffer.contents b in
        if b = "" then read "" else b)
      else (
        set_prompt "....> ";
        read c)
    else (
      Buffer.add_string b prev;
      read c)
  in
  read ""
