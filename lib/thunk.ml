open Utils

type ('a, 'b, 'c) t = (* Initialized of 'a | *)
  | Running of ('a, 'b, 'c) state

and ('a, 'b, 'c) state = {
  offset : int;
  definition : 'a;
  def_context : 'b;
  last : ('a, 'b, 'c) last;
  mutable kind : kind;
}

and ('a, 'b, 'c) last = {
  history : 'c Vector.t;
  mutable access : access_label;
  mutable code : 'a;
  mutable context : 'b;
}

and access_label = AInProgress of int | ADone of int | ANil
and kind = KStream | KRegular

let get_history state n = Vector.get state.last.history (n + state.offset)

and update_history s value context code label kind =
  Utils.Vector.push s.last.history value;
  s.last.code <- code;
  s.last.context <- context;
  s.last.access <- label;
  s.kind <- kind

and set_in_progress s =
  match s.access with
  | ANil ->
      s.access <- AInProgress 1;
      0
  | ADone k ->
      s.access <- AInProgress (k + 1);
      k
  | AInProgress _ -> failwith "Blackhole"

and list_from history offset = Vector.slice_from history offset
