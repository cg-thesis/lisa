open Syntax
open PPrint

let r = ref (Char.code 'A')
let memoize_flag = ref false

let fresh_variable () =
  let label = String.make 1 (Char.chr !r) in
  incr r;
  Var label

let tbl = Hashtbl.create 512

let rec print_type x = group (print_type_arrow x)

and print_type_arrow t =
  match t with
  | TyArrow (t1, t2) -> infix 2 1 !^"->" (print_type t1) (print_type_atom t2)
  | TyList t -> prefix 2 1 (print_type_atom t) !^"list"
  | TyStream t -> prefix 2 1 (print_type_atom t) !^"stream"
  | TyThunk t -> prefix 2 1 (print_type_atom t) !^"U"
  | _ -> print_type_atom t

and print_type_atom t =
  match t with
  | TyVar x -> bquote ^^ OCaml.int x
  | TyUnit -> !^"unit"
  | TyInt -> !^"int"
  | TyBool -> !^"bool"
  | TyProduct ts (* (t1, t2) *) ->
      surround 2 0 lparen (separate_map !^" * " print_type ts) rparen
  | TyArrow _ | TyList _ | TyStream _ | TyThunk _ -> parens (print_type t)

let print_typed_expression print_expression e t =
  let typ = print_type t in
  let exp = print_expression e in
  exp ^^ colon ^^ typ

let print_tuple print_component components =
  let contents =
    match components with
    | [ component ] -> print_component component ^^ comma
    | _ -> separate_map (comma ^^ space) print_component components
  in
  surround 2 0 lparen contents rparen

let print_term_list print_term list =
  match list with
  | Some xs, _ ->
      let contents =
        match xs with
        | [ component ] -> print_term component
        | _ -> separate_map (semi ^^ space) print_term xs
      in
      surround 2 0 lbracket contents rbracket
  | _, Some xs -> print_term xs
  | _ -> assert false

let print_term_stream print_component components =
  separate_map (twice colon) print_component components

let print_term_binding print_term (x, t) =
  !^x ^^ !^" =" ^^ jump 1 1 (print_term t)

let print_term_mutuple print_term ts =
  let contents =
    match ts with
    | [ eq ] -> print_term_binding print_term eq
    | _ ->
        separate_map (hardline ^^ !^"and ") (print_term_binding print_term) ts
  in
  !^"rectup " ^^ contents

let rec print_term t = print_term_abs t

and print_term_abs t =
  match t with
  | Fun (x, t) -> !^"fun " ^^ !^x ^^ !^" -> " ^^ print_term_abs t
  | Fix (x, t) -> !^"fix " ^^ !^x ^^ !^" . " ^^ print_term_atom t
  | MuTuple ts ->
      if !memoize_flag then (
        try print_term_atom (Hashtbl.find tbl t)
        with Not_found ->
          let u = fresh_variable () in
          Hashtbl.add tbl t u;
          !^"let " ^^ print_term_atom u
          ^^ prefix 1 1 !^" =" (print_term_mutuple print_term_abs ts))
      else print_term_mutuple print_term_abs ts
  | t -> print_term_app t

and print_term_app t =
  match t with
  | App (t1, t2) -> print_term_app t1 ^^ space ^^ print_term_atom t2
  | Thunk t -> prefix 1 1 !^"thunk" (print_term_atom t)
  | Force t -> prefix 1 1 !^"force" (print_term_atom t)
  | At (s, Fun (x, t)) ->
      print_term_atom s ^^ lbracket ^^ !^x ^^ dot ^^ print_term_abs t
      ^^ rbracket
  | Iterate (m, n, p, t) ->
      prefix 1 1 !^"iterate" (OCaml.int m)
      ^^ jump 1 1 (OCaml.int n)
      ^^ jump 1 1 (print_term_list print_term_app (Some p, None))
      ^^ jump 1 1 (print_term_atom t)
  | Primitive p -> print_term_primitive p
  | Fby _ ->
      let rec walk_stream exp =
        match exp with Fby (a, b) -> a :: walk_stream b | _ -> [ exp ]
      in
      print_term_stream print_term_atom (walk_stream t)
  | t -> print_term_atom t

and print_list x =
  let rec walk_list x =
    match x with
    | Primitive (Cons (x, y)) -> (
        match walk_list y with
        | Some xs, _ -> (Some (x :: xs), None)
        | _, Some xs -> (None, Some (App (App (Var "cons", x), xs)))
        | _ -> assert false)
    | Const Nil -> (Some [], None)
    | x -> (None, Some x)
  in
  print_term_list print_term (walk_list x)

and print_term_atom t =
  match t with
  | Var x -> !^x
  | Const c -> print_term_constant c
  | Tuple t -> print_tuple print_term t
  | Primitive (Cons _) as xs -> print_list xs
  | TypedExp (e, t) -> print_typed_expression print_term e t
  | App _ | Fun _ | MuTuple _ | Primitive _ | At _ | Iterate _ | Fix _ | Force _
  | Fby _ | Thunk _ ->
      group (parens (print_term t))

and print_term_constant c =
  match c with
  | Unit -> parens empty
  | Int i -> OCaml.int i
  | Bool b -> OCaml.bool b
  | Nil -> brackets empty

and print_term_infix_app op t1 t2 =
  group (infix 1 1 op (print_term_atom t1) (print_term_atom t2))

and print_term_primitive p =
  match p with
  | Input l -> prefix 1 1 !^"input" (surround 1 0 dquote !^l dquote)
  | Proj (i, m, MuTuple ts) ->
      if !memoize_flag then (
        let t = Primitive p in
        try
          let u = Hashtbl.find tbl t in
          print_term_atom u
        with Not_found ->
          let u = fresh_variable () in
          Hashtbl.add tbl t u;
          !^"let " ^^ print_term_atom u
          ^^ prefix 1 1 !^" ="
               (prefix 1 1
                  (!^"proj " ^^ OCaml.int i ^^ space ^^ OCaml.int m)
                  (print_term_atom (MuTuple ts))))
      else
        prefix 1 1
          (!^"proj " ^^ OCaml.int i ^^ space ^^ OCaml.int m)
          (print_term_atom (MuTuple ts))
  | Proj (i, m, t) ->
      prefix 1 1
        (!^"proj " ^^ OCaml.int i ^^ space ^^ OCaml.int m)
        (print_term_atom t)
  | NegB t -> prefix 1 1 !^"not" (print_term_atom t)
  | Plus (x, y) -> print_term_infix_app plus x y
  | Minus (x, y) -> print_term_infix_app minus x y
  | Mult (x, y) -> print_term_infix_app star x y
  | Div (x, y) -> print_term_infix_app slash x y
  | Eq (x, y) -> print_term_infix_app equals x y
  | Neq (x, y) -> print_term_infix_app (angles empty) x y
  | AndB (x, y) -> print_term_infix_app (twice ampersand) x y
  | OrB (x, y) -> print_term_infix_app (twice bar) x y
  | Map (x, y) ->
      prefix 1 1 !^"map" (print_term_atom x) ^^ jump 1 1 (print_term_atom y)
  | FoldL (x, y, z) ->
      prefix 1 1 !^"fold_left" (print_term_atom x)
      ^^ jump 1 1 (print_term_atom y)
      ^^ jump 1 1 (print_term_atom z)
  | FoldR (x, y, z) ->
      prefix 1 1 !^"fold_right" (print_term_atom x)
      ^^ jump 1 1 (print_term_atom y)
      ^^ jump 1 1 (print_term_atom z)
  | Iter (x, y) ->
      prefix 1 1 !^"iter" (print_term_atom x) ^^ jump 1 1 (print_term_atom y)
  | IfThenElse (c, x, y) ->
      prefix 1 1 !^"if" (print_term_atom c)
      ^^ jump 0 1 !^"then"
      ^^ jump 1 1 (print_term_atom x)
      ^^ jump 0 1 !^"else"
      ^^ jump 1 1 (print_term_atom y)
  | _ -> print_term_atom (Primitive p)

let print_context_infix_app print_left print_right op t1 t2 =
  infix 1 1 op (print_left t1) (print_right t2)

let rec print_context c = print_context_app c

and print_context_app c =
  match c with
  | LeApp (c, t) -> print_context_app c ^^ print_term t
  | RiApp (t, c) -> print_term t ^^ print_context_atom c
  | LeAt (c, t) -> print_context_atom c ^^ at ^^ print_term t
  | RiAt (t, c) -> print_term t ^^ at ^^ print_context_atom c
  | More (n, m, p, c) ->
      prefix 1 1 !^"more"
        (OCaml.int n ^^ space ^^ OCaml.int m ^^ space ^^ OCaml.list print_term p
       ^^ print_context_atom c)
  | PrimArgs p -> print_context_primitive p
  | _ -> print_context_atom c

and print_context_infix_app_left op lhs rhs =
  print_context_infix_app print_context_atom print_term_atom op lhs rhs

and print_context_infix_app_right op lhs rhs =
  print_context_infix_app print_term_atom print_context_atom op lhs rhs

and print_context_primitive p =
  match p with
  | CNegB c -> prefix 1 1 !^"not" (print_context_atom c)
  | CHead c -> prefix 1 1 !^"head" (print_context_atom c)
  | CTail c -> prefix 1 1 !^"tail" (print_context_atom c)
  | ProjArg (i, m, c) ->
      prefix 1 1
        (!^"proj " ^^ OCaml.int i ^^ space ^^ OCaml.int m)
        (print_context_atom c)
  | LePlus (c, t) -> print_context_infix_app_left plus c t
  | RiPlus (t, c) -> print_context_infix_app_right plus t c
  | LeMinus (c, t) -> print_context_infix_app_left minus c t
  | RiMinus (t, c) -> print_context_infix_app_right minus t c
  | LeTimes (c, t) -> print_context_infix_app_left star c t
  | RiTimes (t, c) -> print_context_infix_app_right star t c
  | LeDiv (c, t) -> print_context_infix_app_left slash c t
  | RiDiv (t, c) -> print_context_infix_app_right slash t c
  | LeEq (c, t) -> print_context_infix_app_left equals c t
  | RiEq (t, c) -> print_context_infix_app_right equals t c
  | LeNeq (c, t) -> print_context_infix_app_left !^"<>" c t
  | RiNeq (t, c) -> print_context_infix_app_right !^"<>" t c
  | LeAndB (c, t) -> print_context_infix_app_left (twice ampersand) c t
  | RiAndB (t, c) -> print_context_infix_app_right (twice ampersand) t c
  | LeOrB (c, t) -> print_context_infix_app_left (twice bar) c t
  | RiOrB (t, c) -> print_context_infix_app_right (twice bar) t c
  | LeCons (c, t) ->
      prefix 1 1 !^"cons" (print_context_atom c) ^^ jump 1 1 (print_term_atom t)
  | RiCons (t, c) ->
      prefix 1 1 !^"cons" (print_term_atom t) ^^ jump 1 1 (print_context_atom c)
  | LeMap (c, t) ->
      prefix 1 1 !^"map" (print_context_atom c) ^^ jump 1 1 (print_term_atom t)
  | RiMap (t, c) ->
      prefix 1 1 !^"map" (print_term_atom t) ^^ jump 1 1 (print_context_atom c)
  | LeIter (c, t) ->
      prefix 1 1 !^"iter" (print_context_atom c) ^^ jump 1 1 (print_term_atom t)
  | RiIter (t, c) ->
      prefix 1 1 !^"iter" (print_term_atom t) ^^ jump 1 1 (print_context_atom c)
  | FstFoldL (c, t, u) ->
      prefix 1 1 !^"fold_left" (print_context_atom c)
      ^^ jump 1 1 (print_term_atom t)
      ^^ jump 1 1 (print_term_atom u)
  | SndFoldL (t, c, u) ->
      prefix 1 1 !^"fold_left" (print_term_atom t)
      ^^ jump 1 1 (print_context_atom c)
      ^^ jump 1 1 (print_term_atom u)
  | ThrdFoldL (t, u, c) ->
      prefix 1 1 !^"fold_left" (print_term_atom t)
      ^^ jump 1 1 (print_term_atom u)
      ^^ jump 1 1 (print_context_atom c)
  | FstFoldR (c, t, u) ->
      prefix 1 1 !^"fold_right" (print_context_atom c)
      ^^ jump 1 1 (print_term_atom t)
      ^^ jump 1 1 (print_term_atom u)
  | SndFoldR (t, c, u) ->
      prefix 1 1 !^"fold_right" (print_term_atom t)
      ^^ jump 1 1 (print_context_atom c)
      ^^ jump 1 1 (print_term_atom u)
  | ThrdFoldR (t, u, c) ->
      prefix 1 1 !^"fold_right" (print_term_atom t)
      ^^ jump 1 1 (print_term_atom u)
      ^^ jump 1 1 (print_context_atom c)
  | CIfThenElse (c, x, y) ->
      prefix 1 1 !^"if" (print_context_atom c)
      ^^ jump 0 1 !^"then"
      ^^ jump 1 1 (print_term_atom x)
      ^^ jump 0 1 !^"else"
      ^^ jump 1 1 (print_term_atom y)

and print_context_atom c =
  match c with
  | Hole -> !^"•"
  | LeFby (c, t) -> print_context c ^^ twice colon ^^ print_term t
  | RiFby (t, c) -> print_term t ^^ twice colon ^^ print_context c
  | EltTuple (preds, c, succs) ->
      let pretty_tuple xs = separate_map !^", " print_term xs in
      let preds = pretty_tuple preds in
      let succs = pretty_tuple succs in
      surround 1 0 lparen
        (separate !^", " [ preds; print_context c; succs ])
        rparen
  | _ -> group (parens (print_context c))

let document_to_string = Utils.Printer.document_to_string
let string_of_term t = document_to_string print_term t
let string_of_type t = document_to_string print_type t
let string_of_context c = document_to_string print_context c

let document_to_format print_document fmt doc =
  let doc = print_document doc in
  PPrint.ToFormatter.pretty 0.9 80 fmt (group doc)

let fmt_of_term fmt t = document_to_format print_term fmt t
let fmt_of_type fmt t = document_to_format print_type fmt t
let fmt_of_context fmt c = document_to_format print_context fmt c

let memoize term =
  if not (Hashtbl.mem tbl term) then Format.printf "@[%a@]@." fmt_of_term term
  else ()

let remember term flag =
  let rec aux term =
    match term with
    | Var _ | Const _ | Primitive (Input _) -> ()
    | Fun (_, t)
    | Primitive (NegB t)
    | Primitive (Head t)
    | Primitive (Tail t)
    | Force t
    | Thunk t
    | Fix (_, t) ->
        aux t
    | App (t, u)
    | Fby (t, u)
    | At (t, u)
    | Primitive (Plus (t, u))
    | Primitive (Minus (t, u))
    | Primitive (Mult (t, u))
    | Primitive (Div (t, u))
    | Primitive (Eq (t, u))
    | Primitive (Neq (t, u))
    | Primitive (AndB (t, u))
    | Primitive (OrB (t, u))
    | Primitive (Cons (t, u))
    | Primitive (Map (t, u))
    | Primitive (Iter (t, u)) ->
        aux t;
        aux u
    | Primitive (IfThenElse (c, t, u))
    | Primitive (FoldL (c, t, u))
    | Primitive (FoldR (c, t, u)) ->
        aux c;
        aux t;
        aux u
    | Tuple ts -> List.iter aux ts
    | MuTuple ts as t ->
        List.iter (fun (_, t) -> aux t) ts;
        memoize t
    | Primitive (Proj (_, _, MuTuple ts)) as t ->
        aux (MuTuple ts);
        memoize t
    | Primitive (Proj (_, _, t)) | TypedExp (t, _) -> aux t
    | Iterate _ -> assert false
  in
  memoize_flag := flag;
  if !memoize_flag then aux term else ()

let forget () = Hashtbl.reset tbl
