open PrettyPrinter

let remember t = PrettyPrinter.remember t
let forget = PrettyPrinter.forget

let remember_binding x =
  match x with
  | Syntax.Def (_, _, x) -> remember x
  | Syntax.RecValues xs ->
      let xs = List.map (fun (x, _, t) -> (x, t)) xs in
      PrettyPrinter.remember (Syntax.MuTuple xs)
  | Syntax.RecDef (_, _, x) -> remember x

let remember_bindings xs t = List.iter (fun x -> remember_binding x t) xs

let ppf_term memoize t =
  match t with
  | Syntax.TypedExp (t, ty) ->
      remember t memoize;
      let s =
        Printf.sprintf " - : %s = %s" (string_of_type ty) (string_of_term t)
      in
      forget ();
      s
  | _ ->
      remember t memoize;
      let s = Printf.sprintf "%s" (string_of_term t) in
      forget ();
      s

let print_binding ?memoize:(memo = false) binding =
  match binding with
  | Syntax.Def (x, _, t) -> Printf.sprintf "let %s = %s\n" x (string_of_term t)
  | Syntax.RecDef (x, _, t) ->
      Printf.sprintf "let rec %s = %s\n" x (string_of_term t)
  | Syntax.RecValues xs ->
      let xs = List.map (fun (x, _, t) -> (x, t)) xs in
      Printf.sprintf "%s" (ppf_term memo (Syntax.MuTuple xs))

let process_bindings ~lexer_init ~input =
  try Parser.program Lexer.token (lexer_init input)
  with Sys_error msg -> failwith (Format.asprintf "%s during parsing." msg)

let parse_binding, parse_file =
  let parse lexer_init input = process_bindings ~lexer_init ~input in
  let parse_string = parse Lexing.from_string in
  let parse_file f = parse Lexing.from_channel (open_in f) in
  (parse_string, parse_file)

let process_expression ~lexer_init ~input =
  try Parser.exp Lexer.token (lexer_init input)
  with Sys_error msg -> failwith (Format.asprintf "%s during parsing." msg)

let parse_expression =
  let parse lexer_init input = process_expression ~lexer_init ~input in
  let parse_string = parse Lexing.from_string in
  parse_string

let process_formula ~lexer_init ~input =
  try Libformula.Parser.formula Libformula.Lexer.token (lexer_init input)
  with Sys_error msg -> failwith (Format.asprintf "%s during parsing." msg)

let parse_formula =
  let parse_formula_string lexer_init input =
    process_formula ~lexer_init ~input
  in
  parse_formula_string Lexing.from_string

let csv_mapi f x =
  let xs = Csv.transpose x in
  List.(
    mapi (fun i xs ->
        mapi
          (fun j x ->
            if x = "" && j > 0 then Libformula.Syntax.(Primitive Input)
            else f i j x)
          xs))
    xs

let parse_csv x : Libformula.Syntax.program =
  Csv.load x ~separator:'|'
  |> csv_mapi (fun col line x ->
         try parse_formula x
         with Utils.Error.SyntaxError pos ->
           Printf.eprintf "%s:\n %s\n at col: %d line: %d while parsing %s \n"
             (Utils.Position.string_of_pos pos)
             "Syntax error." col line x;
           exit 1)

let prompt = ref ""
let set_prompt = ( := ) prompt

let print_prompt () =
  output_string stdout !prompt;
  flush stdout

let input_char =
  let display_prompt = ref true in
  let ask stdin =
    if !display_prompt then (
      display_prompt := false;
      print_prompt ());
    let c = input_char stdin in
    if c = '\n' then display_prompt := true;
    String.make 1 c
  in
  ask

let read () =
  let b = Buffer.create 13 in
  let rec read prev =
    let c = input_char stdin in
    if c = "\n" then
      if prev <> "\\" then (
        Buffer.add_string b prev;
        let b = Buffer.contents b in
        if b = "" then read "" else b)
      else (
        set_prompt "....> ";
        read c)
    else (
      Buffer.add_string b prev;
      read c)
  in
  read ""

let parseij_formula col line x =
  try parse_formula x
  with Utils.Error.SyntaxError pos ->
    Printf.eprintf "%s:\n %s\n at col: %d line: %d"
      (Utils.Position.string_of_pos pos)
      "Syntax error." col line;
    exit 1

let string_of_exp b exp =
  let s =
    Buffer.clear b;
    PPrint.ToBuffer.pretty 0.9 80 b (Libformula.Printer.print_formula exp);
    Buffer.contents b
  in
  s

let map_pprint exp_csv =
  let b = Buffer.create 256 in
  List.map (fun col -> List.map (fun cell -> string_of_exp b cell) col) exp_csv

let pprint_sheet x = Csv.transpose (map_pprint x)

let rec concat_map_row f row sheet =
  match (sheet, row) with
  | [], [] -> []
  | c :: columns, "" :: row ->
      (Libformula.Syntax.(Primitive Input) :: c) :: concat_map_row f row columns
  | c :: columns, cell :: row -> (f cell :: c) :: concat_map_row f row columns
  | _, [] -> sheet
  | [], "" :: row -> [] :: concat_map_row f row []
  | [], cell :: row -> [ f cell ] :: concat_map_row f row []

let parse_sheet x =
  let csv = Csv.(of_string ~separator:'|' x) in
  Csv.fold_right ~f:(concat_map_row parse_formula) csv []
