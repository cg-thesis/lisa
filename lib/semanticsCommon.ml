open Syntax

let env : (string, (int * expression) list) Hashtbl.t ref =
  ref (Hashtbl.create 1)

let get_value label time =
  let ( let* ) = Option.bind in
  let* vs = Hashtbl.find_opt !env label in
  List.assoc_opt time vs

let set_value label time v =
  let tbl = !env in
  let vs = match Hashtbl.find_opt tbl label with None -> [] | Some xs -> xs in
  Hashtbl.replace tbl label ((time, v) :: vs)

let rec is_val = function
  | Fun _ | Const _ -> true
  | Fby (a, _) -> is_val a
  | Primitive (Cons (x, xs)) -> is_val x && is_val xs
  | Tuple es -> List.for_all is_val es
  | MuTuple xs -> List.for_all (fun (_, t) -> is_val t) xs
  | TypedExp (a, _) -> is_val a
  | Iterate (m, m', _, _) when m >= m' -> true
  | Thunk _ -> true
  | _ -> false

exception Normal_form
exception Value_missing_at of int

let length s =
  let rec aux acc = function
    | Fby (a, s) when is_val a -> aux (succ acc) s
    | _ -> acc
  in
  let n = aux 0 s in
  (* Printf.printf "length (%s) = %d\n%!"
   *   (string_of_exp s) n; *)
  n

let time_of_term a =
  match a with
  | Fby _ -> length a
  | Tuple ts ->
      let lisalength = length in
      List.(map lisalength ts |> sort Stdlib.compare |> hd)
  | _ -> 0

let list_at time stream =
  let rec aux i acc = function
    | Fby (a, _) when i = time -> a :: acc
    | Fby (a, t) -> aux (succ i) (a :: acc) t
    | a when i = time -> a :: acc
    | _ -> raise (Value_missing_at time)
  in
  aux 0 [] stream

let nth_at k t = match list_at k t with x :: _ -> x | _ -> raise Normal_form

let find_slowest ts =
  match ts with
  | x :: xs ->
      let i =
        List.fold_left
          (fun acc (i, len) -> if List.assoc acc ts < len then acc else i)
          (fst x) xs
      in
      Some i
  | [] -> None

let make_env x =
  let tbl = Hashtbl.create (List.length x) in
  List.iter (fun x -> Hashtbl.add tbl x []) x;
  env := tbl

let var x = Var x
let lam x t = Fun (x, t)
let app a1 a2 = App (a1, a2)
let prim x = Primitive x
let cprim x = PrimArgs x
let unit = Const Unit
let integer x = Const (Int x)
let boolean x = Const (Bool x)
let inpt x = prim (Input x)
let negb x = prim (NegB x)
let head x = prim (Head x)
let tail x = prim (Tail x)
let plus (x, y) = prim (Plus (x, y))
let minus (x, y) = prim (Minus (x, y))
let mult (x, y) = prim (Mult (x, y))
let div (x, y) = prim (Div (x, y))
let eq (x, y) = prim (Eq (x, y))
let neq (x, y) = prim (Neq (x, y))
let andb (x, y) = prim (AndB (x, y))
let orb (x, y) = prim (OrB (x, y))
let cons (x, y) = prim (Cons (x, y))
let map (x, y) = prim (Map (x, y))
let iter (x, y) = prim (Iter (x, y))
let foldl (x, y, z) = prim (FoldL (x, y, z))
let foldr (x, y, z) = prim (FoldR (x, y, z))
let ite (c, x, y) = prim (IfThenElse (c, x, y))
let fby x xs = Fby (x, xs)
let tup t = Tuple t
let mtup x = MuTuple x
let proj i m t = Primitive (Proj (i, m, t))

let obs s k rel =
  match rel with
  | None -> At (s, lam "i" k)
  | Some Libformula.Syntax.RPos -> At (s, lam "i" (plus (var "i", k)))
  | Some Libformula.Syntax.RNeg -> At (s, lam "i" (minus (var "i", k)))

let lsucc =
  lam "x" (tup [ proj 0 2 (var "x"); plus (proj 1 2 (var "x"), integer 1) ])

let isucc = lam "x" (plus (var "x", integer 1))
let thunk t = Thunk t
let force t = Force t

let mem_assoc =
  lam "x"
    (lam "xs"
       (foldl
          ( lam "res"
              (lam "y" (orb (eq (var "x", proj 0 2 (var "y")), var "res"))),
            boolean false,
            var "xs" )))

let map_update =
  lam "key"
    (lam "value"
       (lam "upfn"
          (lam "m"
             (ite
                ( app (app (var "mem_assoc") (var "key")) (var "m"),
                  map (app (app (var "upfn") (var "key")) (var "value"), var "m"),
                  cons (tup [ var "key"; var "value" ], var "m") )))))

let ( *: ) = fby
let ( <@: ) = obs

let map_neg =
  lam "key"
    (lam "value"
       (lam "entry"
          (ite
             ( eq (var "key", proj 0 2 (var "entry")),
               tup [ proj 0 2 (var "entry"); negb (proj 1 2 (var "entry")) ],
               var "entry" ))))
