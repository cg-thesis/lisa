let compile input =
  let ast = IO.parse_binding input in
  let _xast = Typechecker.translate [] ast in
  (* FIXME: List.map DirectTranslate.translate xast *)
  assert false

let run = DirectSemantic.run
let take = DirectSemantic.take
