type 'a seq = 'a Seq.t
type time

val origin : time
val tick : time -> time
val ( !! ) : (int -> int) -> time -> time

type 'a t = private time -> 'a

val pure : 'a -> 'a t
val lam : ('a -> 'b t) -> ('a -> 'b) t
val app : ('a -> 'b) t -> 'a t -> 'b t
val rec1 : ('a t -> 'a t) -> 'a t
val rec2 : ('a t -> 'b t -> ('a * 'b) t) -> ('a * 'b) t
val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
val observe : 'a seq t -> time t -> 'a t
val emit : 'a t -> 'a seq t -> 'a seq t
val run : 'a t -> 'a
val take : int -> 'a seq -> 'a list
