module T = Syntax
open Liblustre.Syntax

let rec def_to_lisa v e =
  match e with
  | Arrow (x, y) ->
      let v_left = v ^ "_aleft" in
      let v_right = v ^ "_aright" in
      let v_next = v ^ "_anext" in
      T.(
        MuTuple
          (flatten
             [
               def_to_lisa v_left x;
               def_to_lisa v_right y;
               MuTuple
                 [
                   ( v,
                     Fby
                       ( at_now v_left,
                         Primitive
                           (Proj
                              ( 0,
                                1,
                                T.MuTuple
                                  [ (v_next, Fby (at_now v_right, Var v_next)) ]
                              )) ) );
                 ];
             ]))
  | Fby (x, y) ->
      let v_left = v ^ "_fleft" in
      let v_right = v ^ "_fright" in
      let v_next = v ^ "_fnext" in
      T.(
        MuTuple
          (flatten
             [
               def_to_lisa v_left x;
               def_to_lisa v_right y;
               MuTuple
                 [
                   ( v,
                     Fby
                       ( at_now v_left,
                         Primitive
                           (Proj
                              ( 0,
                                1,
                                T.MuTuple
                                  [ (v_next, Fby (at_pre v_right, Var v_next)) ]
                              )) ) );
                 ];
             ]))
  | Const x -> T.(MuTuple [ (v, Fby (const_to_lisa x, Var v)) ])
  | Var x -> T.(MuTuple [ (v, Var x) ])
  | Primitive p -> prim_to_lisa v p

and prim_to_lisa v p =
  match p with
  | Plus (x, y) ->
      let x_plus = v ^ "_left_plus" in
      let y_plus = v ^ "_right_plus" in
      let lhs = def_to_lisa x_plus x in
      let rhs = def_to_lisa y_plus y in
      T.(
        MuTuple
          (flatten
             [
               lhs;
               rhs;
               MuTuple
                 [
                   ( v,
                     Fby (Primitive (Plus (at_now x_plus, at_now y_plus)), Var v)
                   );
                 ];
             ]))
  | _ -> assert false

and unbox x = match x with T.MuTuple xs -> xs | _ -> assert false

and flatten xs =
  let open List in
  flatten (map unbox xs)

and const_to_lisa x =
  match x with Bool b -> T.(Const (Bool b)) | Int i -> T.(Const (Int i))

and at_now x = T.(At (Var x, Fun ("i", Var "i")))

and at_pre x =
  T.(At (Var x, Fun ("i", Primitive (Minus (Var "i", Const (Int 1))))))
