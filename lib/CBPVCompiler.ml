module S = Syntax
module T = Libcbpv_compiler.Syntax
open T

let tbl = Hashtbl.create 26

let fresh_var (x : string) =
  match Hashtbl.find_opt tbl x with
  | Some x' ->
      incr x';
      x ^ string_of_int !x'
  | None ->
      Hashtbl.add tbl x (ref 1);
      x

let pos_as_proj n i wrap tup =
  let rec aux j acc =
    match j with
    | j when j = n - 1 -> fun x -> acc x
    | _ when j = i && i = 0 -> fun x -> T.(Prim (Fst x))
    | _ when j = i -> fun x -> T.(Prim (Fst (acc x)))
    | j -> aux (j + 1) (fun x -> acc (T.Prim (Snd x)))
  in
  wrap (aux 0 Fun.id (t tup))

let rec translate_val_const c : const =
  match c with
  | S.Unit -> Unit
  | S.Int i -> Int i
  | S.Bool b -> Bool b
  | S.Nil -> Nil

and translate_comp_prim p : tycomp term =
  match p with
  | S.NegB x ->
      let vx = fresh_var "_x" in
      (translate_comp x =: vx) (Return (Prim (Not (t vx))))
  | S.Plus (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (Plus (t vx, t vy)))))
  | S.Minus (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (Minus (t vx, t vy)))))
  | S.Mult (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (Mult (t vx, t vy)))))
  | S.Div (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (Div (t vx, t vy)))))
  | S.Eq (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (Eq (t vx, t vy)))))
  | S.Neq (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (Neq (t vx, t vy)))))
  | S.AndB (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (AndB (t vx, t vy)))))
  | S.OrB (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (OrB (t vx, t vy)))))
  | S.Cons (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Prim (Cons (t vx, t vy)))))
  | S.Proj (0, 2, pair) ->
      let vp = fresh_var "_p" in
      (translate_comp pair =: vp) (Return (Prim (Fst (t vp))))
  | S.Proj (1, 2, pair) ->
      let vp = fresh_var "_p" in
      (translate_comp pair =: vp) (Return (Prim (Snd (t vp))))
  | _ -> failwith "Unsupported primitive"

and translate_comp term : tycomp term =
  match term with
  | S.Var x -> Return (Var x)
  | S.Const c -> Return (Const (translate_val_const c))
  | S.Fun (id, t) -> Return (Thunk (Fun (id, None, translate_comp t)))
  | S.Fby (x, y) ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Fby (t vx, t vy))))
  | S.Tuple [ x; y ] ->
      let vx = fresh_var "_x" and vy = fresh_var "_y" in
      (translate_comp x =: vx)
        ((translate_comp y =: vy) (Return (Pair (t vx, t vy))))
  | S.Primitive (Proj (0, 1, MuTuple [ (id, t) ])) ->
      Rec (id, None, translate_comp t)
  | S.Primitive (Proj (i, n, MuTuple ts)) ->
      let tup = lower_rec_tupple ts in
      let tvar = fresh_var "_tup" in
      (tup =: tvar) (pos_as_proj n i (fun x -> Return x) tvar)
  | S.Primitive
      (S.(
         ( NegB _ | Plus _ | Minus _ | Mult _ | Div _ | Eq _ | Neq _ | AndB _
         | Cons _ | OrB _ | Proj _ )) as p) ->
      translate_comp_prim p
  | S.App (m, n) ->
      let vf = fresh_var "_f" and vx = fresh_var "_x" in
      (translate_comp n =: vx)
        ((translate_comp m =: vf) (tforce (t vf) @@! t vx))
  | S.MuTuple [ (id, t) ] -> Rec (id, None, translate_comp t)
  | S.MuTuple bs ->
      lower_rec_tupple bs
      (* MuRecord (List.map (fun (id, t) -> (id, None, translate_comp t)) bindings) *)
  | S.Primitive (IfThenElse (c, x, y)) ->
      let vc = fresh_var "_x" in
      (translate_comp c =: vc)
        (IfThenElse (t vc, translate_comp x, translate_comp y))
  | S.At (s, Fun (id, o)) ->
      let vs = fresh_var "_s" in
      (translate_comp s =: vs) (T.At (t vs, T.Fun (id, None, translate_comp o)))
  | S.Thunk u -> Return (Thunk (translate_comp u))
  | S.Force t ->
      let vu = fresh_var "_u" in
      (translate_comp t =: vu) (Force (Var vu))
  | S.Fix (x, t) -> Rec (x, None, translate_comp t)
  | _ -> failwith "Not a primitive"

and lower_rec_tupple term =
  let ids, terms = List.split term in
  let trec = fresh_var "_trec" in
  let vrec = fresh_var "_vrec" in
  let acc x = T.Thunk ((tforce (t trec) =: vrec) (T.Return x)) in
  let id_projs n ids =
    List.mapi (fun i a -> (a, pos_as_proj n i acc vrec)) ids
  in
  let rec_env = id_projs (List.length ids) ids in
  let vars = List.map (fun id -> fresh_var ("_" ^ id)) ids in
  let tuple ids =
    let rec aux ids =
      match ids with
      | [] | [ _ ] -> assert false
      | [ a; b ] -> Pair (t a, t b)
      | x :: xs -> Pair (t x, aux xs)
    in
    aux ids
  in
  let rec aux ids xs =
    match (ids, xs) with
    | [ ida; idb ], [ a; b ] ->
        (translate_comp a =: ida)
          ((translate_comp b =: idb) (Return (tuple vars)))
    | idx :: ids, x :: xs -> (translate_comp x =: idx) (aux ids xs)
    | _, _ -> assert false
  in
  Rec
    ( trec,
      None,
      List.fold_right
        (fun (id, proj) acc -> T.Let (id, None, proj, acc))
        rec_env (aux vars terms) )

and translate_projmutuple i n bindings : tycomp term =
  match bindings with
  | [ (id, t) ] when i = 0 && n = 1 -> Rec (id, None, translate_comp t)
  | _ when List.length bindings = n && i < n ->
      let id = fst (List.nth bindings i) in
      RecordField (translate_comp (MuTuple bindings), id)
  | _ -> failwith "Unsafe projection"

and translate t =
  Hashtbl.clear tbl;
  translate_comp t
