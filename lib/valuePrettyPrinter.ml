open PPrint
open Syntax

let print_tuple print_component components =
  let contents =
    match components with
    | [ component ] -> print_component component
    | _ -> separate_map (comma ^^ space) print_component components
  in
  surround 2 0 lparen contents rparen

let print_stream print_val vals =
  let contents = separate_map (colon ^^ colon) print_val vals in
  contents ^^ (colon ^^ colon) ^^ angles (string "stream")

let rec print_value_aux v =
  match v with
  | VInt i -> OCaml.int i
  | VBool b -> OCaml.bool b
  | VUnit -> parens empty
  | VClosure _ -> angles (string "fun")
  | VFby (a, VThunk { value = Running state }) ->
      let open Thunk in
      let h = state.last.history in
      let l =
        if h.size - state.offset > 0 then
          Array.sub h.data state.offset (h.size - state.offset) |> Array.to_list
        else []
      in
      print_stream print_value (a :: l)
  | VFby (a, _) -> print_stream print_value [ a ]
  | VTuple vs -> print_tuple print_value vs
  | VThunk _ -> angles (string "thunk")
  | VList vs -> OCaml.list print_value vs

and print_value v = print_value_aux v

let string_of_value v =
  let buffer = Buffer.create 256 in
  print_value v |> PPrint.ToBuffer.pretty 0.9 80 buffer;
  Buffer.contents buffer

let fmt_of_value fmt v = print_value v |> PPrint.ToFormatter.pretty 0.9 80 fmt
