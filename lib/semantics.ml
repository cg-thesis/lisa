open SemanticsCommon
open Syntax
open PrettyPrinter
open PrimitiveSemantics

exception Value_missing_at of int

let env1 = ref []

let rec time_of_ t c =
  match c with
  | Hole -> t
  | RiFby (_, c) -> time_of_ (succ t) c
  | LeApp (c, _) | RiApp (_, c) | LeFby (c, _) | EltTuple (_, c, _) | RiAt (_, c)
    ->
      time_of_ t c
  | LeAt (c, _) -> time_of_ 0 c
  | CForce c -> time_of_ t c
  | More (t, _, _, c) -> time_of_ t c
  | PrimArgs c -> time_of_prim t c time_of_

let time_of context = time_of_ 0 context

let rec subst sigma a =
  match a with
  | Var v -> ( try List.assoc v sigma with Not_found -> Var v)
  | Const _ -> a
  | Fun (v, a1) ->
      let sigma = List.filter (fun (x, _) -> x <> v) sigma in
      Fun (v, subst sigma a1)
  | Thunk t -> Thunk (subst sigma t)
  | Force t -> Force (subst sigma t)
  | Fix (v, a1) ->
      let sigma = List.filter (fun (x, _) -> x <> v) sigma in
      Fix (v, subst sigma a1)
  | Iterate _ -> a
  | App (a1, a2) -> App (subst sigma a1, subst sigma a2)
  | Fby (t, u) -> Fby (subst sigma t, subst sigma u)
  | At (t, k) -> At (subst sigma t, subst sigma k)
  | Tuple es -> Tuple (List.map (subst sigma) es)
  | MuTuple ts ->
      let sigma = List.filter (fun (x, _) -> not (List.mem_assoc x ts)) sigma in
      MuTuple (List.map (fun (x, t) -> (x, subst sigma t)) ts)
  | Primitive p -> subst_prim sigma p subst
  | TypedExp (e, t) -> TypedExp (subst sigma e, t)

let rec apply_context c a =
  match c with
  | Hole -> a
  | LeApp (c, n) -> App (apply_context c a, n)
  | RiApp (m, c) -> App (m, apply_context c a)
  | LeFby (c, t) -> Fby (apply_context c a, t)
  | RiFby (m, c) -> Fby (m, apply_context c a)
  | EltTuple (ts, c, ts') ->
      let v = apply_context c a in
      Tuple (ts @ (v :: ts'))
  | CForce c -> Force (apply_context c a)
  | RiAt (t, c) -> At (t, apply_context c a)
  | LeAt (c, k) -> At (apply_context c a, k)
  | More (n, m, p, c) -> Iterate (n, m, p, apply_context c a)
  | PrimArgs c -> apply_prim_context c a apply_context

let destruct_stream t =
  let rec walk (acc, len) t =
    match t with
    | Fby (a, t) when is_val a -> walk (a :: acc, succ len) t
    | _ -> (acc, len, t)
  in
  walk ([], 0) t

let rec destruct term =
  match term with
  | App (Fun _, a) when is_val a -> (Hole, term)
  | App (m, n) when is_val m ->
      let c, t = destruct n in
      (RiApp (m, c), t)
  | App (m, n) ->
      let c, t = destruct m in
      (LeApp (c, n), t)
  | Fby (m, n) when is_val m ->
      let c, t = destruct n in
      (RiFby (m, c), t)
  | Fby (m, n) ->
      let c, t = destruct m in
      (LeFby (c, n), t)
  | Tuple xs ->
      let preds, (c, t), tail = select xs in
      (EltTuple (preds, c, tail), t)
  | At (Iterate (m, m', _, _), n) when m >= m' && is_val n -> (Hole, term)
  | At ((Thunk _ as m), n) when is_val n -> (Hole, At (m, n))
  | At (m, n) when is_val n ->
      let c, t = destruct m in
      (LeAt (c, n), t)
  | At (m, n) ->
      let c, t = destruct n in
      (RiAt (m, c), t)
  | Primitive p -> destruct_prim p destruct
  | TypedExp (e, _) -> destruct e
  | Var _ -> (Hole, term)
  | Fix _ -> (Hole, term)
  | Force (Thunk _) -> (Hole, term)
  | Force t ->
      let c, t = destruct t in
      (CForce c, t)
  | Iterate (m, m', p, t) when is_val t && m < m' ->
      (Hole, Iterate (m, m', p, t))
  | Iterate (m, m', p, t) when m < m' ->
      let c, t = destruct t in
      (More (m, m', p, c), t)
  | _ -> raise Normal_form

and select ts =
  let rec select_first_reducible acc = function
    | x :: xs -> (
        try (List.rev acc, destruct x, xs)
        with Normal_form -> select_first_reducible (x :: acc) xs)
    | _ -> raise Normal_form
  in
  let rec select_slowest slowest i acc = function
    | x :: xs when i < slowest -> select_slowest slowest (succ i) (x :: acc) xs
    | x :: xs when i = slowest -> (
        try (List.rev acc, destruct x, xs)
        with Normal_form -> select_first_reducible [] ts)
    | _ -> assert false
  in
  let xs = List.mapi (fun i x -> (i, length x)) ts in
  match find_slowest xs with
  | Some slowest_index -> select_slowest slowest_index 0 [] ts
  | None -> raise Normal_form

and reduce time a =
  match a with
  | Var _ -> subst !env1 a
  | App (Fun (x, a1), a2) when is_val a2 -> subst ((x, a2) :: !env1) a1
  | Iterate (m, n, p, Thunk t) -> Iterate (m, n, p, t)
  | Iterate (m, n, p, Fby (a, t)) when m < n -> Iterate (succ m, n, a :: p, t)
  | At ((* (Fby (a, b) as m) *) Thunk m, n) ->
      let i =
        match eval (App (n, Const (Int time))) with
        | Some (Const (Int i)) -> i
        | _ -> assert false
      in
      At (Iterate (0, i + 1, [], m), n)
  | At (Iterate (m, n, xs, _), _) when m >= n -> List.nth xs (m - n)
  | MuTuple ts as mt ->
      let len = List.length ts in
      let sigma = List.mapi (fun i (x, _) -> (x, Thunk (proj i len mt))) ts in
      Tuple (List.map (fun (_, t) -> subst sigma t) ts)
  | Fix (x, t) -> subst ((x, Thunk a) :: !env1) t
  | Force (Thunk t) -> t
  | Primitive p -> delta_reduce time p
  | TypedExp (e, _) -> reduce time e
  | _ -> raise Normal_form

and proj i m term = prim (Proj (i, m, term))

and try_step exp =
  try
    let c, t = destruct exp in
    let time = time_of c in
    let u = reduce time t in
    let t' = apply_context c u in
    Some (t', time)
  with
  | Normal_form -> None
  | Value_missing_at time ->
      let c, t = destruct exp in
      Format.printf
        "Value missing at time : %d when stepping on @ term : @ %a @ with \
         context: @ %a @ and redex :@ %a @."
        time fmt_of_term exp fmt_of_context c fmt_of_term t;
      raise Exit

and eval a =
  match try_step a with
  | Some (a, _) -> eval a
  | None ->
      if is_val a then Some a
      else (
        Format.printf "%a Not a value.\n" PrettyPrinter.fmt_of_term a;
        None)

and iterate n t =
  match t with
  | Fby (_, _) -> (
      match eval (Iterate (0, n + 1, [], t)) with
      | Some (Iterate (m, n, p, Thunk t)) when m >= n ->
          List.fold_left (fun x y -> Fby (y, Thunk x)) t p
      | _ -> t)
  | _ -> ( match eval t with Some t -> t | _ -> t)

and debug a =
  let t = Option.get (eval a) in
  let c, r = destruct t in
  let time = time_of c in
  Format.eprintf "TERM:\n@[%a@]@.CONTEXT:\n@[%a@]@.REDEX:\n@[%a@]@.TIME:\n%d\n"
    fmt_of_term t fmt_of_context c fmt_of_term r time;
  Format.print_flush ();
  flush_all ();
  t

and take n xs =
  let tup = reduce 0 (MuTuple xs) in
  match tup with
  | Tuple ts -> List.map (fun x -> iterate n x) ts
  | _ -> assert false

let rec recvals xs tys ts =
  match (xs, tys, ts) with
  | [], [], [] -> []
  | x :: xs, ty :: tys, t :: ts -> (x, ty, t) :: recvals xs tys ts
  | _ -> assert false

let binding _callback _ev n = function
  | Def (x, ty, e) ->
      let v = eval e in
      Def (x, ty, Option.get v)
  | RecValues xs ->
      let xs, tys, ts =
        List.fold_right
          (fun (x, ty, t) (xs, tys, ts) -> (x :: xs, ty :: tys, t :: ts))
          xs ([], [], [])
      in
      let ts = take n (List.combine xs ts) in
      RecValues (recvals xs tys ts)
  | RecDef (x, ty, e) ->
      let v = Option.get (eval (Fix (x, e))) in
      env1 := (x, v) :: !env1;
      RecDef (x, ty, v)
