open Syntax

let tbl = Hashtbl.create 26

let fresh_var (x : string) =
  match Hashtbl.find_opt tbl x with
  | Some x' ->
      incr x';
      x ^ string_of_int !x'
  | None ->
      Hashtbl.add tbl x (ref 0);
      x

(* let return_t o h x = Fun (o, Fun (h, x)) *)
let return_t o h x = Fun (o, None, Fun (h, None, Return x))

let nth n x =
  (trec "list_nth"
     (tfunn
        [ ("_xs", None); ("_i", None) ]
        (tif
           (t "_i" ==! tint 0)
           (tlhd (t "_xs"))
           ((tltl (t "_xs") =: "_xxs")
              ((tforce (t "list_nth") @@! t "_xxs") @@! (t "_i" -! tint 1)))))
  @@! x)
  @@! n

let rec _translate_aux : type a. a term -> a term =
 fun term ->
  match term with
  | TRange (_, t) | TypedTerm (_, t) -> _translate_aux t
  | Const _ ->
      (* 〚 c 〛= c *)
      term
  | Prim (Hd _) | Prim (Tl _) ->
      (* FIXME : translate stream primitves *)
      failwith "FIXME: translate stream primitives"
  | Prim (LHd v) ->
      let v = _translate_aux v in
      Prim (LHd v)
  | Prim (LTl v) ->
      let v = _translate_aux v in
      Prim (LTl v)
  (* FIXME : translate primitves *)
  | Prim (Print (v, m)) ->
      let v = _translate_aux v in
      let m = _translate_aux m in
      Prim (Print (v, m))
  | Prim (Fst v) ->
      let v = _translate_aux v in
      Prim (Fst v)
  | Prim (Snd v) ->
      let v = _translate_aux v in
      Prim (Snd v)
  | Prim (Plus (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Plus (v, w))
  | Prim (Minus (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Minus (v, w))
  | Prim (Div (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Div (v, w))
  | Prim (Mult (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Mult (v, w))
  | Prim (Eq (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Eq (v, w))
  | Prim (Neq (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Neq (v, w))
  | Prim (AndB (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (AndB (v, w))
  | Prim (OrB (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (OrB (v, w))
  | Prim (Not v) ->
      let v = _translate_aux v in
      Prim (Not v)
  | Prim (Cons (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Cons (v, w))
  | Var _ ->
      (* 〚 v 〛= v *)
      term
  | Fun (x, _, term) ->
      (* 〚 λ x. M 〛 = λ x . 〚 M 〛*)
      Fun (x, None, _translate_aux term)
  | App (t1, t2) ->
      let t1 = _translate_aux t1 and t2 = _translate_aux t2 in
      App (t1, t2)
  | Rec (x, _, term) -> Rec (x, None, _translate_aux term)
  | Pair (t1, t2) ->
      let t1 = _translate_aux t1 and t2 = _translate_aux t2 in
      Pair (t1, t2)
  | Let (x, _, t1, t2) ->
      let t1 = _translate_aux t1 and t2 = _translate_aux t2 in
      Let (x, None, t1, t2)
  | To (t1, x, _, t2) ->
      (* 〚 M to x . in N 〛 = λ o h. 〚 M 〛o h to m in 〚 N 〛 o h*)
      let vo = fresh_var "_o" in
      let vh = fresh_var "_h" in
      let t1 = _translate_aux t1 and t2 = _translate_aux t2 in
      Fun
        ( vo,
          None,
          Fun
            ( vh,
              None,
              To
                ( App (App (t1, Var vo), Var vh),
                  x,
                  None,
                  App (App (t2, Var vo), Var vh) ) ) )
  | Force term ->
      (* 〚 force V 〛 = force 〚 V 〛*)
      Force (_translate_aux term)
  | Thunk term ->
      (* 〚 thunk M 〛 = thunk 〚 M 〛*)
      Thunk (_translate_aux term)
  | Return term ->
      (* 〚 return V 〛 = returnₜ〚 A 〛 *)
      let vo = fresh_var "_o" in
      let vh = fresh_var "_h" in
      return_t vo vh (_translate_aux term)
  | Fby (x, xs) ->
      (* 'a stream = 'a * U F ('a stream)
         〚 'a stream 〛 = 〚 'a * U F ('a stream) 〛 =
          U (nat -> nat -> 'a list)
          〚 x :: xs 〛 =
              thunk (fun o h -> (if o = h then [] else ((force xs) (o+1) h)) to n.
              return (cons x n))
      *)
      let vo = fresh_var "_o" in
      let vh = fresh_var "_h" in
      let n = fresh_var "_n" in
      let x = _translate_aux x in
      let xs = _translate_aux xs in
      Thunk
        (Fun
           ( vo,
             None,
             Fun
               ( vh,
                 None,
                 (tif
                    (t vo ==! t vh)
                    (treturn (Const Nil))
                    ((tforce xs @@! (t vo +! tint 1)) @@! t vh)
                 =: n)
                   (treturn (tcons x (t n))) ) ))
  | IfThenElse (c, t1, t2) ->
      let c = _translate_aux c
      and t1 = _translate_aux t1
      and t2 = _translate_aux t2 in
      IfThenElse (c, t1, t2)
  | At (s, p) ->
      let vh = fresh_var "_h" in
      let vo = fresh_var "_o" in
      let vn = fresh_var "_n" in
      let vs = fresh_var "_s" in
      let s = _translate_aux s in
      Fun
        ( vo,
          None,
          Fun
            ( vh,
              None,
              To
                ( (tforce s @@! tint 0) @@! t vh,
                  vs,
                  None,
                  To (App (p, Var vo), vn, None, nth (t vs) (t vn)) ) ) )
  | MuRecord bindings ->
      MuRecord
        (List.map
           (fun (id, ann, exp) -> (id, ann, _translate_aux exp))
           bindings)
  | RecordField (record, field) -> RecordField (_translate_aux record, field)

let translate x =
  Hashtbl.clear tbl;
  _translate_aux x
