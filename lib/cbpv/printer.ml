open PPrint
open Syntax

let print_id_ty id ty =
  let id = string id in
  match ty with
  | Some ty ->
      group
        (parens (infix 2 1 colon id (Typechecker.deep_print_ty ty |> string)))
  | None -> id

let print_record print_doc bindings =
  let contents =
    match bindings with
    | [ (id, ty, value) ] -> print_id_ty id ty ^^ colon ^^ print_doc value
    | bs ->
        separate_map (semi ^^ space)
          (fun (id, ty, value) ->
            infix 2 1 equals (print_id_ty id ty) (print_doc value))
          bs
  in
  braces contents

let rec print_term_list print_term list =
  match list with
  | Some xs, _ ->
      let contents =
        match xs with
        | [ component ] -> print_term component
        | _ -> separate_map (semi ^^ space) print_term xs
      in
      surround 2 0 lbracket contents rbracket
  | _, Some xs -> print_cons xs
  | _ -> assert false

and print_cons : type t. t term -> PPrint.document =
 fun xs ->
  match xs with
  | Prim (Cons (x, xs)) ->
      prefix 2 1
        (prefix 2 1 !^"cons" (print_term_atom x))
        (group (parens (print_cons xs)))
  | x -> print_term_atom x

and print_term : type a. a term -> PPrint.document = fun t -> print_term_fun t

and print_term_fun : type a. a term -> PPrint.document =
 fun t ->
  match t with
  | TypedTerm (_, t) | TRange (_, t) -> print_term_fun t
  | Fun (x, ty, t) ->
      !^"fun " ^^ print_id_ty x ty ^^ !^" ->" ^^ jump 2 1 (print_term_fun t)
  | Rec (x, ty, t) ->
      !^"rec " ^^ print_id_ty x ty ^^ !^" =" ^^ jump 2 1 (print_term_fun t)
      |> parens
  | Let (x, ty, t, c) ->
      prefix 1 1 !^"let" (print_id_ty x ty)
      ^^ jump 1 1 equals
      ^^ jump 1 1 (print_term_fun t)
      ^^ jump 2 1 !^"in"
      ^^ jump 2 1 (print_term_fun c)
  | To (t, x, ty, c) ->
      print_term_fun t ^^ !^" =:"
      ^^ jump 2 1 (print_id_ty x ty)
      ^^ jump 2 1 !^"in"
      ^^ jump 2 1 (print_term_fun c)
  | Force t -> prefix 1 1 !^"force" (print_term_atom t)
  | Thunk t -> prefix 1 1 !^"thunk" (print_term_atom t)
  | Return t -> prefix 1 1 !^"return" (print_term_atom t)
  | _ -> print_term_app t

and print_term_app : type a. a term -> PPrint.document =
 fun t ->
  match t with
  | TypedTerm (_, t) | TRange (_, t) -> print_term_app t
  | App (t1, t2) -> prefix 2 1 (print_term_app t1) (print_term_atom t2)
  | IfThenElse (c, x, y) ->
      prefix 1 1 !^"if" (print_term_atom c)
      ^^ jump 0 1 !^"then"
      ^^ jump 1 1 (print_term x)
      ^^ jump 0 1 !^"else"
      ^^ jump 1 1 (print_term y)
  | At (t1, t2) ->
      jump 1 1 (print_term_atom t1)
      ^^ jump 2 1 !^"@"
      ^^ jump 2 1 (print_term_atom t2)
  | Prim (Cons _) as xs -> print_list xs
  | Fby _ ->
      let rec walk_stream : type a. a term -> document list = function
        | Fby (a, b) -> print_term a :: walk_stream b
        | exp -> [ print_term exp ]
      in
      print_term_stream (fun x -> x) (walk_stream t)
  | Prim t -> print_term_prim t
  | RecordField (r, f) ->
      group (print_term_atom r ^^ !^"." ^^ print_term_atom (Var f))
  | _ -> print_term_atom t

and print_term_atom : type a. a term -> PPrint.document =
 fun t ->
  match t with
  | TypedTerm (_, t) | TRange (_, t) -> print_term_atom t
  | Var x -> !^x
  | Const c -> print_term_constant c
  | MuRecord bindings -> print_record print_term bindings
  | Pair (x, y) ->
      surround 2 0 lparen
        (separate_map (comma ^^ space) print_term [ x; y ])
        rparen
  | _ -> group (parens (print_term t))

and print_list x =
  let rec walk_list x =
    match x with
    | Prim (Cons (x, y)) -> (
        match walk_list y with Some xs -> Some (x :: xs) | None -> None)
    | Const Nil -> Some []
    | _ -> None
  in
  let l =
    match walk_list x with Some l -> (Some l, None) | None -> (None, Some x)
  in
  print_term_list print_term l

and print_term_constant c =
  match c with
  | Unit -> parens empty
  | Int i -> OCaml.int i
  | Bool b -> OCaml.bool b
  | Nil -> brackets empty

and print_term_stream print_component components =
  separate_map (twice colon) print_component components

and print_term_infix_app : type a b. document -> a term -> b term -> document =
 fun op t1 t2 -> infix 1 1 op (print_term_atom t1) (print_term_atom t2)

and print_term_prim : type a. a prim -> document =
 fun p ->
  match p with
  | Hd t -> prefix 1 1 !^"hd" (print_term_atom t)
  | Tl t -> prefix 1 1 !^"tl" (print_term_atom t)
  | LHd t -> prefix 1 1 !^"list_hd" (print_term_atom t)
  | LTl t -> prefix 1 1 !^"list_tl" (print_term_atom t)
  | Plus (x, y) -> print_term_infix_app plus x y
  | Minus (x, y) -> print_term_infix_app minus x y
  | Mult (x, y) -> print_term_infix_app star x y
  | Div (x, y) -> print_term_infix_app slash x y
  | Eq (x, y) -> print_term_infix_app equals x y
  | Neq (x, y) -> print_term_infix_app (angles empty) x y
  | AndB (x, y) -> print_term_infix_app (twice ampersand) x y
  | OrB (x, y) -> print_term_infix_app (twice bar) x y
  | Fst t -> prefix 1 1 !^"fst" (print_term_atom t)
  | Snd t -> prefix 1 1 !^"snd" (print_term_atom t)
  | Not t -> prefix 1 1 !^"not" (print_term_atom t)
  | Print (t1, t2) ->
      prefix 1 1 !^"print" (print_term_atom t1)
      ^^ jump 0 1 semi
      ^^ jump 0 1 (print_term t2)
  | _ -> print_term_atom (Prim p)

let rec print_terminal : type a. a terminal -> document =
 fun t ->
  match t with
  | VUnit -> parens empty
  | VNil -> brackets empty
  | VBool b -> OCaml.bool b
  | VInt i -> OCaml.int i
  | VPair (a, b) ->
      parens (separate_map (comma ^^ space) print_terminal [ a; b ])
  | VList xs -> group (brackets (separate_map semi print_terminal xs))
  | VFby (x, y) -> infix 2 1 (twice colon) (print_terminal x) (print_terminal y)
  | VThunk t when t.kind = KRegular -> angles !^"thunk"
  | VThunk t ->
      let lh =
        List.init
          (Vector.length t.last.history - t.offset)
          (fun i -> Vector.get t.last.history (i + t.offset))
      in
      if lh = [] then angles !^"thunk"
      else
        concat_map
          (fun x ->
            (string "memo" ^^ space ^^ lparen)
            ^^ print_terminal x ^^ break 1 ^^ string "::" ^^ break 1)
          lh
        ^^ angles (string "thunk")
        ^^ space
        ^^ repeat (List.length lh) rparen
  | VFun (_, _, _) -> angles !^"fun"
  (* | VRec _ -> angles (!^"…") *)
  | VReturn t -> prefix 2 1 !^"return" (print_terminal t)
  | VRecord bindings ->
      print_record print_terminal
        (List.map (fun (id, t) -> (id, None, t)) bindings)

let print_env e =
  separate_map hardline
    (fun (l, v) ->
      prefix 2 1 (enclose !^l equals colon) @@ print_terminal (fst !v))
    e

let string_of_env e = Utils.Printer.document_to_string print_env e

let fmt_of_term : type a. Format.formatter -> a term -> unit =
 fun fmt t -> Utils.Printer.document_to_format print_term fmt t

let fmt_of_terminal : type a. Format.formatter -> a terminal -> unit =
 fun fmt t -> Utils.Printer.document_to_format print_terminal fmt t

let print_termln : type a. a term -> unit =
 fun t -> Format.printf "%a\n" fmt_of_term t

let print_terminalln : type a. a terminal -> unit =
 fun t -> Format.printf "%a\n" fmt_of_terminal t

let string_of_term : type a. a term -> string =
 fun t -> Utils.Printer.document_to_string print_term t

let string_of_terminal : type a. a terminal -> string =
 fun t -> Utils.Printer.document_to_string print_terminal t
