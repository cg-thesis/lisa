open Syntax

let rec subst : type a. string -> tyval term -> a term -> a term =
 fun id v t ->
  match t with
  | Var x when id = x -> v
  | Fun (x, ty, c) when x <> id -> Fun (x, ty, subst id v c)
  | Rec (x, ty, c) when x <> id -> Rec (x, ty, subst id v c)
  | App (a, b) -> App (subst id v a, subst id v b)
  | Pair (a, b) -> Pair (subst id v a, subst id v b)
  | Let (x, ty, b, c) when x <> id -> Let (x, ty, subst id v b, subst id v c)
  | Let (x, ty, b, c) -> Let (x, ty, subst id v b, c)
  | To (b, x, ty, c) when x <> id -> To (subst id v b, x, ty, subst id v c)
  | To (b, x, ty, c) -> To (subst id v b, x, ty, c)
  | Thunk t -> Thunk (subst id v t)
  | Force t -> Force (subst id v t)
  | Return t -> Return (subst id v t)
  | IfThenElse (c, bt, bf) ->
      IfThenElse (subst id v c, subst id v bt, subst id v bf)
  | Prim (Plus (x, y)) -> Prim (Plus (subst id v x, subst id v y))
  | Prim (Minus (x, y)) -> Prim (Minus (subst id v x, subst id v y))
  | Prim (Mult (x, y)) -> Prim (Mult (subst id v x, subst id v y))
  | Prim (Div (x, y)) -> Prim (Div (subst id v x, subst id v y))
  | Prim (Eq (x, y)) -> Prim (Eq (subst id v x, subst id v y))
  | Prim (Neq (x, y)) -> Prim (Neq (subst id v x, subst id v y))
  | Prim (AndB (x, y)) -> Prim (AndB (subst id v x, subst id v y))
  | Prim (OrB (x, y)) -> Prim (OrB (subst id v x, subst id v y))
  | Prim (Fst x) -> Prim (Fst (subst id v x))
  | Prim (Snd x) -> Prim (Snd (subst id v x))
  | Prim (Hd x) -> Prim (Hd (subst id v x))
  | Prim (Tl x) -> Prim (Tl (subst id v x))
  | Prim (LHd x) -> Prim (LHd (subst id v x))
  | Prim (LTl x) -> Prim (LTl (subst id v x))
  | Prim (Not x) -> Prim (Not (subst id v x))
  | Prim (Cons (x, y)) -> Prim (Cons (subst id v x, subst id v y))
  | Fby (x, y) -> Fby (subst id v x, subst id v y)
  | Const _ | Var _ | Fun _ | Rec _ -> t
  | TypedTerm (ty, t) -> TypedTerm (ty, subst id v t)
  | TRange (r, t) -> TRange (r, subst id v t)
  | At _ | MuRecord _ | RecordField _ | Prim (Print _) -> t
