let process_term ~lexer_init ~input =
  try Parser.program Lexer.token (lexer_init input)
  with Sys_error msg -> failwith (Format.asprintf "%s during parsing." msg)

let parse =
  let parse lexer_init input = process_term ~lexer_init ~input in
  let parse_string = parse Lexing.from_string in
  parse_string

let parse_file path =
  let parse lexer_init input = process_term ~lexer_init ~input in
  let parse_string = parse Utils.Input.open_in path in
  parse_string

let read = Utils.Input.read

let check ts t =
  let tc =
    match ts with "pure" -> Typechecker.mcheck | _ -> Typechecker.check
  in
  tc t

let eval : string -> Syntax.tycomp Syntax.term -> Syntax.tycomp Syntax.terminal
    =
  let open Syntax in
  fun ts t -> Semantics.eval' (check ts t) { index = 0; env = [] }

let eval_interactive ts s = eval ts @@ parse s
let eval_file ts f = eval ts @@ parse_file f
let to_string t = Printer.string_of_terminal t
let print t = ", " ^ Printer.string_of_terminal t |> print_endline

let rec read_eval_print ts () =
  Utils.Input.set_prompt "cbpv>";
  read () |> eval_interactive ts |> print;
  Format.print_flush ();
  read_eval_print ts ()
