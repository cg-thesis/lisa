open Syntax

let id_horizon = "h"
let var_horizon = t id_horizon
let id_origin = "o"
let var_origin = t id_origin
let tbl = Hashtbl.create 26

let fresh_var (x : string) =
  match Hashtbl.find_opt tbl x with
  | Some x' ->
      incr x';
      x ^ string_of_int !x'
  | None ->
      Hashtbl.add tbl x (ref 0);
      x

let rec derivate_comp : tycomp term -> tycomp term =
 fun m ->
  match m with
  | TypedTerm _ -> assert false
  | Fun (x, ann, m) ->
      tfunn
        [ (id_origin, None); (id_horizon, None); (x, ann) ]
        ((superforce_comp m @@! var_origin) @@! var_horizon)
  | Return _ | App _ | Force _ | Let _ | To _ | Rec _ | At _ | IfThenElse _
  | Prim _ | MuRecord _ ->
      superforce_comp m
  | TRange (_, m) -> derivate_comp m
  | RecordField _ -> assert false

and translate_binary_val a b prim =
  let id_a = fresh_var "da" in
  let id_b = fresh_var "db" in
  tfunn
    [ (id_origin, None); (id_horizon, None) ]
    (((superforce_val a @@! var_origin) @@! var_horizon =: id_a)
       (((superforce_val b @@! var_origin) @@! var_horizon =: id_b)
          (treturn (prim id_a id_b))))

and translate_unary_val a prim =
  let id_a = fresh_var "da" in
  tfunn
    [ (id_origin, None); (id_horizon, None) ]
    (((superforce_val a @@! var_origin) @@! var_horizon =: id_a)
       (treturn (prim id_a)))

and translate_unary_comp a prim =
  let id_a = fresh_var "da" in
  ((superforce_val a @@! var_origin) @@! var_horizon =: id_a) (prim id_a)

and superforce_val : tyval term -> tycomp term =
 fun v ->
  match v with
  | TypedTerm (TyApp (("U", 1), [ TyApp (("stream", _), _) ]), (Var _ as v)) ->
      tforce v
  | TypedTerm (_, (Var _ as v)) | (Const _ as v) ->
      tfunn [ (id_origin, None); (id_horizon, None) ] (treturn v)
  | Pair (a, b) -> translate_binary_val a b (fun ida idb -> Pair (t ida, t idb))
  | Thunk m ->
      tfunn
        [ (id_origin, None); (id_horizon, None) ]
        (treturn (tthunk (superforce_comp m)))
  | Fby (x, xs) ->
      let id_a = fresh_var "da" in
      let id_b = fresh_var "db" in
      let id_n = fresh_var "dn" in
      tfunn
        [ (id_origin, None); (id_horizon, None) ]
        (tif
           (var_origin ==! var_horizon)
           (treturn (Const Nil))
           (((superforce_val x @@! var_origin) @@! var_horizon =: id_a)
              (((superforce_val xs @@! var_origin) @@! var_horizon =: id_b)
                 (((tforce (t id_b) @@! (var_origin +! tint 1)) @@! var_horizon
                  =: id_n)
                    (treturn (tcons (t id_a) (t id_n)))))))
  | Prim (Plus (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (Plus (t ida, t idb)))
  | Prim (Minus (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (Minus (t ida, t idb)))
  | Prim (Mult (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (Mult (t ida, t idb)))
  | Prim (Div (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (Div (t ida, t idb)))
  | Prim (Cons (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (Cons (t ida, t idb)))
  | Prim (AndB (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (AndB (t ida, t idb)))
  | Prim (OrB (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (OrB (t ida, t idb)))
  | Prim (Eq (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (Eq (t ida, t idb)))
  | Prim (Neq (a, b)) ->
      translate_binary_val a b (fun ida idb -> Prim (Neq (t ida, t idb)))
  | Prim (Not a) -> translate_unary_val a (fun ida -> Prim (Not (t ida)))
  | Prim (Fst a) -> translate_unary_val a (fun ida -> Prim (Fst (t ida)))
  | Prim (Snd a) -> translate_unary_val a (fun ida -> Prim (Snd (t ida)))
  | TypedTerm (_, v) -> superforce_val v
  | TRange (_, v) -> superforce_val v
  | Var _ -> tfunn [ (id_origin, None); (id_horizon, None) ] (treturn v)
(* failwith "Cannot discriminate variable type." *)

and superforce_comp : tycomp term -> tycomp term =
 fun m ->
  match m with
  | Return v -> superforce_val v
  | Fun (x, ann, m') -> Fun (x, ann, superforce_comp m')
  | App (a, b) ->
      let id_b = fresh_var "db" in
      ((superforce_val b @@! var_origin) @@! var_horizon =: id_b)
        (superforce_comp a @@! t id_b)
  (* Idee, superforce est parametré par l'observation et le type *)
  | Force v ->
      (* | TypedTerm(TyApp (("F", _), [TyApp (("stream", _), _)]), Force v) -> *)
      let id_v = fresh_var "dv" in
      tfunn
        [ (id_origin, None); (id_horizon, None) ]
        (((superforce_val v @@! var_origin) @@! var_horizon =: id_v)
           (tif
              (var_origin ==! var_horizon)
              (treturn tnil)
              ((tforce (t id_v) @@! tint 0) @@! var_origin)))
  (* | Force (v) ->
   *     let id_v = fresh_var "dv" in
   *     ((superforce_val v @@! var_origin) @@! var_horizon =: id_v)
   *       (tforce (t id_v)) *)
  | Let (x, Some ty, v, m') ->
      ((superforce_val v @@! var_origin) @@! var_horizon =: x)
        ~ty (superforce_comp m')
  | Let (x, None, v, m') ->
      ((superforce_val v @@! var_origin) @@! var_horizon =: x)
        (superforce_comp m')
  | To (n, x, Some ty, m') ->
      ((superforce_comp n @@! var_origin) @@! var_horizon =: x)
        ~ty (superforce_comp m')
  | To (n, x, None, m') ->
      ((superforce_comp n @@! var_origin) @@! var_horizon =: x)
        (superforce_comp m')
  | Rec (x, ann, m) -> Rec (x, ann, superforce_comp m)
  | Prim (Print (a, m)) ->
      translate_unary_comp a (fun ida ->
          Prim (Print (t ida, superforce_comp m)))
  | Prim (Hd a) -> translate_unary_comp a (fun ida -> Prim (Hd (t ida)))
  | Prim (Tl a) -> translate_unary_comp a (fun ida -> Prim (Tl (t ida)))
  | Prim (LHd a) -> translate_unary_comp a (fun ida -> Prim (LHd (t ida)))
  | Prim (LTl a) -> translate_unary_comp a (fun ida -> Prim (LTl (t ida)))
  | IfThenElse (c, bt, bf) ->
      translate_unary_comp c (fun idc ->
          IfThenElse (t idc, superforce_comp bt, superforce_comp bf))
  | At (s, f) ->
      let id_i = fresh_var "i" in
      let id_s = fresh_var "s" in
      (f @@! var_origin =: id_i)
        (((superforce_val s @@! tint 0) @@! var_origin =: id_s)
           ((tforce (t "nth") @@! t id_s) @@! t id_i))
  | MuRecord ms ->
      MuRecord (List.map (fun (id, ann, m) -> (id, ann, superforce_comp m)) ms)
  | RecordField _ -> assert false
  | TRange (_, m) -> superforce_comp m
  | TypedTerm (_, c) -> superforce_comp c

let derivate_val v =
  Hashtbl.clear tbl;
  Hashtbl.add tbl id_horizon (ref 0);
  Hashtbl.add tbl id_origin (ref 0);
  superforce_val v

let derivate t =
  Hashtbl.clear tbl;
  Hashtbl.add tbl id_horizon (ref 0);
  Hashtbl.add tbl id_origin (ref 0);
  derivate_comp t

(** Idée: traduction optimisée. elle est dirigée par les types, tout
   calcul paramétré par un horizon, où seul les valeurs
   forçables sont traduite par des calculs. Par exemple, tout stream
   est traduit par le calcul qui force le stream jusqu'à
   l'horizon. **)
