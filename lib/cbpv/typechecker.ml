module CBPV = Syntax
open CBPV

module S = struct
  type 'a structure = TyApp of tykon * 'a list

  let map f (TyApp (t, ts)) = TyApp (t, List.map f ts)
  let fold f (TyApp (_, ts)) accu = List.fold_right f ts accu
  let iter f (TyApp (_, ts)) = List.iter f ts

  exception Iter2

  let iter2 f (TyApp (t, ts)) (TyApp (u, us)) =
    if t = u then List.iter2 f ts us else raise Iter2

  exception InconsistentConjunction = Iter2

  let conjunction f t u =
    iter2 f t u;
    t

  let pprint leaf ty =
    let open PPrint in
    match ty with
    | TyApp ((ty, 0), []) -> string ty
    | TyApp ((ty, 1), [ a ]) -> group (prefix 0 1 (leaf a) (string ty))
    | TyApp ((ty, 2), params) -> group (separate_map (string ty) leaf params)
    | _ -> failwith "Unknow type structure"
end

module O = struct
  type tyvar = int
  type 'a structure = 'a S.structure
  type ty = htyp

  let inject n = 2 * n

  (* let solver_tyvar n = n *)

  let name_of_tyvar =
    let tbl = Hashtbl.create 32 in
    fun x ->
      match Hashtbl.find_opt tbl x with
      | Some x' -> x'
      | None ->
          let n = Printf.sprintf "'a_%d" x in
          Hashtbl.add tbl x n;
          n

  let variable (x : tyvar) = CBPV.TyVar (name_of_tyvar x)
  let structure (S.TyApp (t, ts)) = CBPV.TyApp (t, ts)
  let mu _ _ = failwith "Rectypes unsupported"

  type scheme = tyvar list * ty
end

module X = struct
  type t = Var of string | Sym of int

  let fresh : unit -> t =
    let gensym = Inferno.Utils.gensym () in
    fun () -> Sym (gensym ())

  let hash = Hashtbl.hash
  let compare v1 v2 = Stdlib.compare v1 v2
  let equal v1 v2 = compare v1 v2 = 0
  let to_string v = match v with Var v -> v | Sym n -> string_of_int n
end

module Solver = Inferno.Solver.Make (X) (S) (O)
open Solver

let returnty ty : 'a S.structure = S.TyApp (("F", 1), [ ty ])
let thunkty ty : 'a S.structure = S.TyApp (("U", 1), [ ty ])
let arrowty ty1 ty2 : 'a S.structure = S.TyApp (("->", 2), [ ty1; ty2 ])
let unitty : 's S.structure = S.TyApp (("unit", 0), [])
let intty : 's S.structure = S.TyApp (("int", 0), [])
let boolty : 's S.structure = S.TyApp (("bool", 0), [])
let prodty n ty : 's S.structure = S.TyApp (("*", n), ty)
let streamty ty : 's S.structure = S.TyApp (("stream", 1), [ ty ])
let listty ty : 's S.structure = S.TyApp (("list", 1), [ ty ])
let recordty n tys : 's S.structure = S.TyApp (("", n), tys)

let rec convert_deep (ty : htyp) =
  let conv = convert_deep in
  let deeps t = DeepStructure t in
  match ty with
  | TyApp (c, ts) ->
      let ts = List.map conv ts in
      deeps (TyApp (c, ts))
  | TyVar _ -> failwith "Types should be monomorphic"

let convert ty =
  let deep_ty = convert_deep ty in
  deep deep_ty

let force_annot ty v co =
  match ty with
  | Some ty ->
      let+ t =
        convert ty (fun v1 ->
            let+ () = v -- v1 and+ t = co in
            t)
      in
      t
  | None ->
      let+ t = co in
      t

let exist2n_ n k =
  let rec aux vs accu =
    match accu with
    | 0 -> k (List.rev vs)
    | n ->
        let@ v = exist in
        let@ w = exist in
        aux ((v, w) :: vs) (pred n)
  in
  aux [] n

let rec defs xs tys ts c =
  match (xs, tys, ts) with
  | [], _, [] -> c
  | x :: xs, ty :: tys, (v, w) :: ts ->
      force_annot ty v
        (let+ t = def (Var x) v (defs xs tys ts c) and+ () = v --- thunkty w in
         t)
  | _, _, _ -> assert false

let conj_of_list f ts vs =
  List.fold_left2
    (fun cs t (_, w) ->
      let+ _ = f t w and+ _ = cs in
      ())
    (pure ()) ts vs

let murecord ds hastype t =
  let n = List.length ds in
  let xs, tys, ts = Utils.ListExtra.split_3 ds in
  exist2n_ n (fun vs ->
      defs xs tys vs
        (let+ () = t --- recordty n (List.split vs |> fst)
         and+ t = conj_of_list hastype ts vs in
         t))

let general_co m _ f = m -- f

let monadic_co m i f =
  let+ () = i --- intty and+ () = m --- arrowty i f in
  ()

exception Poly of htyp

let rec is_var (ty : htyp) =
  match ty with
  | TyVar _ -> Some ty
  | TyApp (_, ts) ->
      List.fold_left
        (fun maybe_var ty -> match maybe_var with None -> is_var ty | t -> t)
        None ts

let is_monomorphic ty = match is_var ty with Some _ -> false | _ -> true

let rec ghastype :
    type t. 'a -> t term -> variable -> ((string * CBPV.htyp) list -> t term) co
    =
 fun _mod term w ->
  let hastype :
      type t. t term -> variable -> ((string * CBPV.htyp) list -> t term) co =
   fun term w -> ghastype _mod term w
  in
  match term with
  | CBPV.TypedTerm _ ->
      let+ () = pure () in
      fun _ -> term
  | CBPV.TRange (r, t) ->
      let+ x = correlate r (hastype t w) in
      fun env -> CBPV.TRange (r, x env)
  | CBPV.Var x -> (
      (* Γ, x: A, Γ' ⊢ᵛ x : A *)
      let+ _ = instance (Var x) w in
      fun ty_env ->
        try CBPV.(TypedTerm (List.assoc x ty_env, term))
        with Not_found ->
          let m = Printf.sprintf "Unbound variable %s during typechecking." x in
          failwith m)
  | CBPV.Return t ->
      (* Γ ⊢ⱽ V : A
          ——————————
          Γ ⊢ᶜ return V : F A *)
      let@ v = exist in
      let+ () = w --- returnty v and+ term = hastype t v and+ ty = decode v in
      fun env ->
        let term = term env in
        CBPV.(TypedTerm (TyApp (("F", 1), [ ty ]), Return term))
  | CBPV.Thunk t ->
      (* Γ ⊢ᶜ M : Ḇ
          ——————————
          Γ ⊢ⱽ thunk M : U Ḇ *)
      let@ v = exist in
      let+ () = w --- thunkty v and+ term = hastype t v and+ ty = decode v in
      fun env ->
        let term = term env in
        CBPV.(TypedTerm (TyApp (("U", 1), [ ty ]), Thunk term))
  | CBPV.Force t ->
      (* Γ ⊢ᵛ V : U Ḇ
         —————————————————–
         Γ ⊢ᶜ force V : Ḇ *)
      let+ term = lift hastype t (thunkty w) and+ ty = decode w in
      fun env -> CBPV.TypedTerm (ty, Force (term env))
  | CBPV.Fun (x, ty, term) ->
      (* Γ, x : A ⊢ᶜ M : Ḇ
         —————————————————–—
         Γ ⊢ᶜ λ x. M : A → Ḇ *)
      (* exist (fun v1 ->
       *     force_annot ty v1
       *       (exist (fun v2 ->
       *            (w --- arrowty v1 v2) ^^ def x v1 (hastype term v2))))
       * <$$> fun (ty1, (ty2, term)) env -> *)
      let@ v1 = exist in
      let@ v2 = exist in
      force_annot ty v1
        (let+ () = w --- arrowty v1 v2
         and+ term = def (Var x) v1 (hastype term v2)
         and+ ty1 = decode v1
         and+ ty2 = decode v2 in
         fun env ->
           if is_monomorphic ty1 then
             CBPV.TypedTerm
               ( TyApp (("->", 2), [ ty1; ty2 ]),
                 Fun (x, Some ty1, term ((x, ty1) :: env)) )
           else raise (Poly ty1))
  | CBPV.App (t1, t2) ->
      (* Γ ⊢ᵛ V : A  Γ ⊢ᶜ M : A → Ḇ
         —————————————————–———————–
              Γ ⊢ᶜ M V : Ḇ *)
      let@ v = exist in
      let+ t1 = lift hastype t1 (arrowty v w)
      and+ t2 = hastype t2 v
      and+ ty = decode w in
      fun env -> CBPV.TypedTerm (ty, App (t1 env, t2 env))
  | CBPV.Rec (x, ty, t) ->
      (* Γ,x : U Ḇ ⊢ᶜ M : Ḇ
         —————————————————–
          Γ ⊢ᶜ μ x. M : Ḇ *)
      let@ v = exist in
      let+ t =
        force_annot ty v
          (let+ () = v --- thunkty w
           and+ term = def (Var x) v (hastype t w)
           and+ ty1 = decode w in
           fun env ->
             if is_monomorphic ty1 then
               CBPV.(
                 TypedTerm
                   (ty1, Rec (x, Some ty1, term ((x, ty_thunk ty1) :: env))))
             else raise (Poly ty1))
      in
      t
  | CBPV.Pair (f, s) ->
      (* Γ ⊢ᵛ V : A  Γ,x : A ⊢ᵛ W : A'
          —————————————————–———————–———–
               Γ ⊢ᵛ (V, W) : A * A' *)
      let@ v1 = exist in
      let@ v2 = exist in
      let+ () = w --- prodty 2 [ v1; v2 ]
      and+ t1 = hastype f v1
      and+ t2 = hastype s v2
      and+ ty1 = decode v1
      and+ ty2 = decode v2 in
      fun env ->
        CBPV.TypedTerm (TyApp (("*", 2), [ ty1; ty2 ]), Pair (t1 env, t2 env))
  | CBPV.Let (x, Some ty, t, u) ->
      (* Γ ⊢ᵛ V : A  Γ,x : A ⊢ᶜ M : Ḇ
         —————————————————–———————–———–
              Γ ⊢ᶜ let x = V in M : Ḇ *)
      let@ v1 = exist in
      let@ annot = convert ty in
      let+ () = annot -- v1
      and+ ty1 = decode v1
      and+ ty2 = decode w
      and+ _, _, t1, t2 =
        let1 (Var x)
          (fun v ->
            let+ () = v -- v1 and+ t = hastype t v in
            t)
          (hastype u w)
      in
      fun env ->
        if is_monomorphic ty1 then
          CBPV.TypedTerm (ty2, Let (x, Some ty1, t1 env, t2 ((x, ty1) :: env)))
        else raise (Poly ty1)
  | CBPV.Let (x, None, t, u) ->
      (* Γ ⊢ᵛ V : A  Γ,x : A ⊢ᶜ M : Ḇ
         —————————————————–———————–———–
              Γ ⊢ᶜ let x = V in M : Ḇ *)
      let@ v1 = exist in
      let+ _, _, t1, t2 =
        let1 (Var x)
          (fun v ->
            let+ () = v -- v1 and+ t = hastype t v in
            t)
          (hastype u w)
      and+ ty1 = decode v1
      and+ ty2 = decode w in
      fun env ->
        if is_monomorphic ty1 then
          CBPV.TypedTerm (ty2, Let (x, Some ty1, t1 env, t2 ((x, ty1) :: env)))
        else raise (Poly ty1)
  | CBPV.To (m, x, Some ty, n) ->
      (* Γ ⊢ᶜ M : F A  Γ,x : A ⊢ᶜ N : Ḇ
         —————————————————–———————–———–——
              Γ ⊢ᶜ M to x in N : Ḇ *)
      let@ v1 = exist in
      let@ annot = convert ty in
      let+ () = annot -- v1
      and+ ty1 = decode v1
      and+ ty2 = decode w
      and+ _, _, t1, t2 =
        let1 (Var x)
          (fun v ->
            let+ () = v -- v1 and+ t = lift hastype m (returnty v) in
            t)
          (hastype n w)
      in
      fun env ->
        if is_monomorphic ty1 then
          CBPV.(
            TypedTerm
              (ty2, To (t1 env, x, Some (ty_return ty1), t2 ((x, ty1) :: env))))
        else raise (Poly ty1)
  | CBPV.To (m, x, None, n) ->
      (* Γ ⊢ᶜ M : F A  Γ,x : A ⊢ᶜ N : Ḇ
         —————————————————–———————–———–——
              Γ ⊢ᶜ M to x in N : Ḇ *)
      let@ v1 = exist in
      let+ _, _, t1, t2 =
        let1 (Var x)
          (fun v ->
            let+ () = v -- v1 and+ t = lift hastype m (returnty v) in
            t)
          (hastype n w)
      and+ ty1 = decode v1
      and+ ty2 = decode w in
      fun env ->
        if is_monomorphic ty1 then
          CBPV.(TypedTerm (ty2, To (t1 env, x, Some ty1, t2 ((x, ty1) :: env))))
        else raise (Poly ty1)
  | CBPV.Fby (x, xs) ->
      (* Γ ⊢ᵛ V : A  Γ ⊢ᵛ W : U T (stream A)
         —————————————————–———————–———–
              Γ ⊢ᵛ V::W : stream A *)
      let@ v = exist in
      let@ f = exist in
      let@ m = exist in
      let@ i = exist in
      let+ () = w --- streamty v
      and+ () = f --- returnty w
      and+ () = _mod m i f
      and+ x = hastype x v
      and+ xs = lift hastype xs (thunkty m)
      and+ ty = decode v in
      fun env ->
        CBPV.(TypedTerm (TyApp (("stream", 1), [ ty ]), Fby (x env, xs env)))
  | CBPV.IfThenElse (c, b1, b2) ->
      (* Γ,V : A ⊢ᶜ M : Ḇ Γ,V : A ⊢ᶜ N : Ḇ
         —————————————————–———————–———–————–
           Γ ⊢ᶜ if V then M else N : Ḇ *)
      let+ c = lift hastype c boolty
      and+ bt = hastype b1 w
      and+ bf = hastype b2 w
      and+ ty = decode w in
      fun env -> CBPV.TypedTerm (ty, IfThenElse (c env, bt env, bf env))
  | CBPV.MuRecord xs ->
      (* Γ,x : U Ḇ, y : U Ḇ' ⊢ᶜ rec { x = t; y = u} : { x : Ḇ; y : Ḇ' }
         —————————————————–———————–———–————–——————————–———————————————–
                 Γ ⊢ᶜ rec {x = t; y = u} : { x : Ḇ; y : Ḇ'} *)
      let+ _ = murecord xs hastype w in
      fun _ -> term
  | CBPV.RecordField (_, _) ->
      (* ? *)
      let+ () = pure () in
      fun _ -> term
  | CBPV.At (a, t) ->
      (* Γ ⊢ᶜ f : int -> F int Γ ⊢ᵛ s : U (T stream A)
          —————————————————–———————–———–————–————
            Γ ⊢ᶜ a @ f : T A *)
      let@ i = exist in
      let@ fi = exist in
      let@ v = exist in
      let@ s = exist in
      let@ f = exist in
      let@ m = exist in
      let+ () = i --- intty
      and+ () = fi --- returnty i
      and+ () = w --- returnty v
      and+ () = s --- streamty v
      and+ () = f --- returnty s
      and+ () = _mod m i f
      and+ s = lift hastype a (thunkty m)
      and+ o = lift hastype t (arrowty i fi)
      and+ ty = decode w in
      fun env -> CBPV.(TypedTerm (ty, At (s env, o env)))
  | CBPV.Const c ->
      (* Γ ⊢ᵛ c: A *)
      hastype_const c w
  | CBPV.Prim (LHd xs) ->
      (* Γ ⊢ᵛ V : list A
         —————————————————–———
         Γ ⊢ᶜ list_hd V :  F A *)
      let@ v = exist in
      let+ () = w --- returnty v
      and+ term = lift hastype xs (listty v)
      and+ ty = decode w in
      fun env -> CBPV.TypedTerm (ty, Prim (LHd (term env)))
  | CBPV.Prim (LTl xs) ->
      (* Γ ⊢ᵛ V : list A
         —————————————————–———————
         Γ ⊢ᵛ list_tl V : list F A *)
      let@ v = exist in
      let@ r = exist in
      let+ ty = decode w
      and+ () = r --- listty v
      and+ () = w --- returnty r
      and+ term = lift hastype xs (listty v) in
      fun env -> CBPV.(TypedTerm (ty, Prim (LTl (term env))))
  | CBPV.Prim (Hd xs) ->
      (* Γ ⊢ᵛ t : stream A
         ———————————————
         Γ ⊢ᶜ hd t : F A *)
      let@ v = exist in
      let@ s = exist in
      let@ i = exist in
      let@ f = exist in
      let+ ty = decode v
      and+ () = s --- streamty v
      and+ () = f --- returnty v
      and+ () = general_co w i f
      and+ term = ghastype general_co xs s in
      fun env -> CBPV.TypedTerm (ty, Prim (Hd (term env)))
  | CBPV.Prim (Tl xs) ->
      (* Γ ⊢ᵛ t : stream A
         ———————————————————————————————–
         Γ ⊢ᶜ tl t : T U T (stream A) *)
      (* i ~ int, v ~ stream int, f ~ F stream int,
         m ~ int -> F stream int | m ~ F stream int,
         u ~ U (int -> F stream int) | u  ~ U (F stream int),
         f' ~ int -> F (U (int -> F stream int)) | f' ~ F U (F stream int) *)
      let@ v = exist in
      let@ s = exist in
      let@ f = exist in
      let@ u = exist in
      let@ i = exist in
      let@ t = exist in
      let@ f' = exist in
      let+ ty = decode v
      and+ () = s --- streamty v
      and+ () = f --- returnty s
      and+ () = _mod t i f
      and+ () = u --- thunkty t
      and+ () = f' --- returnty u
      and+ () = general_co w i f'
      and+ term = ghastype general_co xs s in
      fun env -> CBPV.TypedTerm (ty, Prim (Tl (term env)))
  | CBPV.Prim (Print (x, c)) ->
      (* Γ ⊢ᵛ t : A   Γ ⊢ᶜ u : Ḇ
         ————————————–—–————————
         Γ ⊢ᶜ print t; u : Ḇ *)
      let@ v = exist in
      let+ ty = decode w and+ t1 = hastype x v and+ t2 = hastype c w in
      fun env -> CBPV.TypedTerm (ty, Prim (Print (t1 env, t2 env)))
  | CBPV.Prim (Fst t) ->
      (* Γ ⊢ᵛ t : A * A'
         ————————————–—–
         Γ ⊢ᵛ fst t : A *)
      let@ v = exist in
      let+ term = lift hastype t (prodty 2 [ w; v ]) and+ ty = decode w in
      fun env -> CBPV.TypedTerm (ty, Prim (Fst (term env)))
  | CBPV.Prim (Snd t) ->
      (* Γ ⊢ᵛ t : A * A'
         ————————————–—–
         Γ ⊢ᵛ fst t : A' *)
      let@ v = exist in
      let+ term = lift hastype t (prodty 2 [ v; w ]) and+ ty = decode w in
      fun env -> CBPV.TypedTerm (ty, Prim (Snd (term env)))
  | CBPV.Prim (Plus (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : int   Γ ⊢ᵛ u : int
         ————————————–—–—————————————
               Γ ⊢ᵛ t op u : int *)
      let+ () = w --- intty and+ t1 = hastype lhs w and+ t2 = hastype rhs w in
      fun env ->
        CBPV.(TypedTerm (TyApp (("int", 0), []), Prim (Plus (t1 env, t2 env))))
  | CBPV.Prim (Minus (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : int   Γ ⊢ᵛ u : int
         ————————————–—–—————————————
               Γ ⊢ᵛ t op u : int *)
      let+ () = w --- intty and+ t1 = hastype lhs w and+ t2 = hastype rhs w in
      fun env ->
        CBPV.(TypedTerm (TyApp (("int", 0), []), Prim (Minus (t1 env, t2 env))))
  | CBPV.Prim (Mult (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : int   Γ ⊢ᵛ u : int
         ————————————–—–—————————————
               Γ ⊢ᵛ t op u : int *)
      let+ () = w --- intty and+ t1 = hastype lhs w and+ t2 = hastype rhs w in
      fun env ->
        CBPV.(TypedTerm (TyApp (("int", 0), []), Prim (Mult (t1 env, t2 env))))
  | CBPV.Prim (Div (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : int   Γ ⊢ᵛ u : int
         ————————————–—–—————————————
               Γ ⊢ᵛ t op u : int *)
      let+ () = w --- intty and+ t1 = hastype lhs w and+ t2 = hastype rhs w in
      fun env ->
        CBPV.(TypedTerm (TyApp (("int", 0), []), Prim (Div (t1 env, t2 env))))
  | CBPV.Prim (Not t) ->
      (* Γ ⊢ᵛ t : bool
         ————————————–—–——
         Γ ⊢ᵛ not t : bool *)
      let+ () = w --- boolty and+ term = lift hastype t boolty in
      fun env ->
        CBPV.(TypedTerm (TyApp (("bool", 0), []), Prim (Not (term env))))
  | CBPV.Prim (Eq (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : A   Γ ⊢ᵛ u : A
         ————————————–—–—————————————
               Γ ⊢ᵛ t = u : bool *)
      let@ v = exist in
      let+ () = w --- boolty and+ t1 = hastype lhs v and+ t2 = hastype rhs v in
      fun env ->
        CBPV.(TypedTerm (TyApp (("bool", 0), []), Prim (Eq (t1 env, t2 env))))
  | CBPV.Prim (Neq (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : A   Γ ⊢ᵛ u : A
         ————————————–—–—————————————
               Γ ⊢ᵛ t = u : bool *)
      let@ v = exist in
      let+ () = w --- boolty and+ t1 = hastype lhs v and+ t2 = hastype rhs v in
      fun env ->
        CBPV.(TypedTerm (TyApp (("bool", 0), []), Prim (Neq (t1 env, t2 env))))
  | CBPV.Prim (AndB (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : bool   Γ ⊢ᵛ u : bool
           ————————————–—–—————————————
                 Γ ⊢ᵛ t op u : int *)
      let+ () = w --- boolty and+ t1 = hastype lhs w and+ t2 = hastype rhs w in
      fun env ->
        CBPV.(TypedTerm (TyApp (("bool", 0), []), Prim (AndB (t1 env, t2 env))))
  | CBPV.Prim (OrB (lhs, rhs)) ->
      (* Γ ⊢ᵛ t : bool   Γ ⊢ᵛ u : bool
         ————————————–—–—————————————
               Γ ⊢ᵛ t op u : int *)
      let+ () = w --- boolty and+ t1 = hastype lhs w and+ t2 = hastype rhs w in
      fun env ->
        CBPV.(TypedTerm (TyApp (("bool", 0), []), Prim (OrB (t1 env, t2 env))))
  | CBPV.Prim (Cons (x, xs)) ->
      (* Γ ⊢ᵛ V : A  Γ ⊢ᵛ W : list A
         —————————————————–———————–———–
              Γ ⊢ᵛ cons V W : list A *)
      let@ v = exist in
      let+ ty = decode v
      and+ () = w --- listty v
      and+ t1 = hastype x v
      and+ t2 = hastype xs w in
      fun env ->
        CBPV.TypedTerm
          (TyApp (("list", 1), [ ty ]), Prim (Cons (t1 env, t2 env)))

and hastype_const c w : ((string * CBPV.htyp) list -> tyval term) co =
  match c with
  | CBPV.Unit ->
      (* Γ ⊢ᵛ (): unit *)
      let+ () = w --- unitty in
      fun _ -> CBPV.(TypedTerm (TyApp (("unit", 0), []), Const c))
  | CBPV.Int _ ->
      (* Γ ⊢ᵛ c: int *)
      let+ () = w --- intty in
      fun _ -> CBPV.(TypedTerm (TyApp (("int", 0), []), Const c))
  | CBPV.Bool _ ->
      (* Γ ⊢ᵛ c: bool *)
      let+ () = w --- boolty in
      fun _ -> CBPV.(TypedTerm (TyApp (("bool", 0), []), Const c))
  | CBPV.Nil ->
      let@ a = exist in
      let+ () = w --- listty a and+ ty = decode a in
      fun _ -> CBPV.(TypedTerm (ty, Const c))

type range = Solver.range

exception Unbound = Solver.Unbound
exception Unify = Solver.Unify
exception Cycle = Solver.Cycle

open Printf

let is_dummy (pos1, pos2) = pos1 == Lexing.dummy_pos && pos2 == Lexing.dummy_pos

let print_range f ((pos1, pos2) as range) =
  if is_dummy range then fprintf f "At an unknown location:\n"
  else
    let file = pos1.pos_fname in
    let line = pos1.pos_lnum in
    let char1 = pos1.pos_cnum - pos1.pos_bol in
    let char2 = pos2.pos_cnum - pos1.pos_bol in
    (* yes, [pos1.pos_bol] *)
    fprintf f "File \"%s\", line %d, characters %d-%d:\n" file line char1 char2

let rec deep_print_ty t =
  match t with
  | TyVar t -> t
  | TyApp ((tid, 0), []) -> tid
  | TyApp ((tid, 1), [ ty ]) -> Printf.sprintf "%s (%s)" tid (deep_print_ty ty)
  | TyApp (("->", 2), [ t1; t2 ]) ->
      Printf.sprintf "%s -> %s" (deep_print_ty t1) (deep_print_ty t2)
  | TyApp (("*", 2), [ t1; t2 ]) ->
      Printf.sprintf "%s * %s" (deep_print_ty t1) (deep_print_ty t2)
  | TyApp ((id, _), tys) ->
      Printf.sprintf "%s (%s)" id
        (List.fold_left (fun a ty -> a ^ deep_print_ty ty ^ ", ") "" tys)

let explain e =
  match e with
  | Unbound (_, Var x) -> eprintf "The variable \"%s\" is unbound.\n" x
  | Unify (_, ty1, ty2) ->
      eprintf "The following types cannot be unified:\n%s\n%s\n"
        (deep_print_ty ty1) (deep_print_ty ty2)
  | Cycle (_, ty) ->
      eprintf "The following type is cyclic:\n%s\n" (deep_print_ty ty)
  | _ -> assert false

let translate :
    type t.
    (variable -> variable -> variable -> unit co) ->
    t term ->
    (string * CBPV.htyp) list ->
    t term =
 fun mod_co t ->
  solve ~rectypes:false
    (let+ _, t =
       let0
         (let@ v = exist in
          let+ t = ghastype mod_co t v in
          t)
     in
     t)

let gcheck mod_co t =
  try translate mod_co t [] with
  | (Unify (r, _, _) | Unbound (r, _)) as e ->
      printf "TYPE ERROR\n";
      print_range stderr r;
      explain e;
      exit 0
  | Poly ty ->
      printf "TYPE ERROR\n";
      eprintf "Types should be monomorphic, \"%s\" isn't" (deep_print_ty ty);
      exit 0

let check : type t. t term -> t term = fun t -> gcheck general_co t
let mcheck : type t. t term -> t term = fun t -> gcheck monadic_co t
