open Libcbpv_compiler
open Syntax
open Translate

let translate_print : type a. a term -> string =
 fun t -> translate t |> Printer.string_of_term

let run_print : tycomp term -> string =
 fun t ->
  translate t |> fun t ->
  Semantics.eval (run_t t) { index = 0; env = [] } |> Printer.string_of_terminal

let translate_lit_unit () =
  Alcotest.(check string) "same string" "()" (translate_print tunit)

let translate_lit_nat () =
  Alcotest.(check string) "same string" "1" (translate_print (tint 1))

let translate_example_stream_nat_8 () =
  Alcotest.(check string)
    "same string"
    "fun (_atindex : int) ->\n\
    \  (force\n\
    \   (thunk\n\
    \    ((rec nat =\n\
    \      fun (_index : int) ->\n\
    \        return\n\
    \         (0::thunk\n\
    \          (fun (_index2 : int) ->\n\
    \            (force\n\
    \             (thunk\n\
    \              ((rec next =\n\
    \                fun (_index3 : int) ->\n\
    \                  ((fun (_atindex2 : int) ->\n\
    \                    (force nat) 0 =: _s2 in\n\
    \                      (fun i -> return (i - 1)) _atindex2 =: (_oindex2 : \
     int) in\n\
    \                        ((rec nth =\n\
    \                          fun xs ->\n\
    \                            fun (_i : int) ->\n\
    \                              fun (_index_ : int) ->\n\
    \                                if (_i = 0) then hd xs else\n\
    \                                 tl xs =: mtail in\n\
    \                                   hd xs =: head in\n\
    \                                     (force mtail) (_index_ + 1) =: tail in\n\
    \                                       (force nth) tail (_i - 1) (_index_ \
     + 1)))\n\
    \                          _s2\n\
    \                          _oindex2\n\
    \                          0)\n\
    \                    _index3 =: (v : int) in\n\
    \                    fun (_index4 : int) ->\n\
    \                      return\n\
    \                       (v + 1::thunk\n\
    \                        (fun (_index5 : int) -> (force next) _index5)))\n\
    \                    _index3))))\n\
    \              _index2))))))\n\
    \    0 =: _s in\n\
    \    (fun _ -> return 8) _atindex =: (_oindex : int) in\n\
    \      ((rec nth =\n\
    \        fun xs ->\n\
    \          fun (_i : int) ->\n\
    \            fun (_index_ : int) ->\n\
    \              if (_i = 0) then hd xs else\n\
    \               tl xs =: mtail in\n\
    \                 hd xs =: head in\n\
    \                   (force mtail) (_index_ + 1) =: tail in\n\
    \                     (force nth) tail (_i - 1) (_index_ + 1)))\n\
    \        _s\n\
    \        _oindex\n\
    \        0"
    (translate_print (tthunk nat_obs @! tfun "_" (treturn (tint 8))))

let translate_lit_nat2 () =
  Alcotest.(check string) "same string" "54" (translate_print (tint 54))

let translate_lit_bool () =
  Alcotest.(check string) "same string" "true" (translate_print ttrue)

let translate_lit_bool2 () =
  Alcotest.(check string) "same string" "false" (translate_print tfalse)

let translate_term_fun () =
  Alcotest.(check string)
    "same string"
    "fun (_index : int) -> fun x -> fun (_index2 : int) -> return x"
    (translate_print (tfun "x" (treturn (t "x"))))

let translate_term_thunk () =
  Alcotest.(check string)
    "same string"
    "thunk (fun (_index : int) -> fun x -> fun (_index2 : int) -> return x)"
    (translate_print (tthunk (tfun "x" (treturn (t "x")))))

let translate_term_force () =
  Alcotest.(check string)
    "same string"
    "force\n\
    \ (thunk (fun (_index : int) -> fun x -> fun (_index2 : int) -> return x))"
    (translate_print (tforce (tthunk (tfun "x" (treturn (t "x"))))))

let translate_term_pair () =
  Alcotest.(check string)
    "same string" "(42, true)"
    (translate_print (tprod (tint 42) ttrue))

let translate_term_var () =
  Alcotest.(check string) "same string" "x" (translate_print (t "x"))

let translate_term_app () =
  Alcotest.(check string)
    "same string"
    "fun (_index : int) ->\n\
    \  (fun x -> fun (_index2 : int) -> return x) 42 _index"
    (translate_print (tfun "x" (treturn (t "x")) @@! tint 42))

let translate_term_tail_app () =
  Alcotest.(check string)
    "same string"
    "fun (_index : int) ->\n\
    \  (fun a ->\n\
    \    fun (_index2 : int) ->\n\
    \      fun b ->\n\
    \        fun (_index3 : int) ->\n\
    \          fun c ->\n\
    \            fun (_index4 : int) ->\n\
    \              fun d -> fun (_index5 : int) -> return ((a + b) + c))\n\
    \    5\n\
    \    _index\n\
    \    8\n\
    \    _index\n\
    \    45\n\
    \    _index\n\
    \    42\n\
    \    _index"
    (translate_print
       (App
          ( App
              ( App
                  ( App
                      ( Fun
                          ( "a",
                            None,
                            Fun
                              ( "b",
                                None,
                                Fun
                                  ( "c",
                                    None,
                                    Fun
                                      ( "d",
                                        None,
                                        Return
                                          (Prim
                                             (Plus
                                                ( Prim (Plus (Var "a", Var "b")),
                                                  Var "c" ))) ) ) ) ),
                        Const (Int 5) ),
                    Const (Int 8) ),
                Const (Int 45) ),
            Const (Int 42) )))

let translate_term_return () =
  Alcotest.(check string)
    "same string"
    "fun (_index : int) ->\n\
    \  return\n\
    \   (thunk (fun (_index2 : int) -> fun x -> fun (_index3 : int) -> return \
     x))"
    (translate_print (treturn (tthunk (tfun "x" (treturn (t "x"))))))

let translate_term_let () =
  Alcotest.(check string)
    "same string" "let ans = true in fun (_index : int) -> return ans"
    (translate_print (tlet "ans" ttrue (treturn (t "ans"))))

let translate_term_to () =
  Alcotest.(check string)
    "same string"
    "fun (_index : int) ->\n\
    \  return\n\
    \   (thunk\n\
    \    (fun (_index2 : int) -> fun a -> fun (_index3 : int) -> return (a + \
     1))) =:\n\
    \    succ in (fun (_index4 : int) -> (force succ) _index4 41 _index4) \
     _index"
    (translate_print
       ((treturn (tthunk (tfun "a" (treturn (t "a" +! tint 1)))) =: "succ")
          (tforce (t "succ") @@! tint 41)))

let translate_term_if () =
  Alcotest.(check string)
    "same string"
    "if true then fun (_index : int) -> return false else\n\
    \ fun (_index2 : int) -> return true"
    (translate_print (tif ttrue (treturn tfalse) (treturn ttrue)))

let translate_term_if2 () =
  Alcotest.(check string)
    "same string"
    "if false then fun (_index : int) -> return false else\n\
    \ fun (_index2 : int) -> return true"
    (translate_print (tif tfalse (treturn tfalse) (treturn ttrue)))

let translate_term_murecord () =
  Alcotest.(check string)
    "same string"
    "{x =\n\
    \  fun (_index : int) ->\n\
    \    return (true::thunk (fun (_index2 : int) -> (force y) _index2)); y =\n\
    \  fun (_index3 : int) ->\n\
    \    return (false::thunk (fun (_index4 : int) -> (force x) _index4))}"
    (translate_print
       (MuRecord
          [
            ("x", None, treturn (ttrue *: t "y"));
            ("y", None, treturn (tfalse *: t "x"));
          ]))

let translate_term_record_field () =
  Alcotest.(check string)
    "same string"
    "{x =\n\
    \  fun (_index : int) ->\n\
    \    return (true::thunk (fun (_index2 : int) -> (force y) _index2)); y =\n\
    \  fun (_index3 : int) ->\n\
    \    return (false::thunk (fun (_index4 : int) -> (force x) _index4))}.y"
    (translate_print
       (RecordField
          ( MuRecord
              [
                ("x", None, treturn (ttrue *: t "y"));
                ("y", None, treturn (tfalse *: t "x"));
              ],
            "y" )))

let translate_prim_hd () =
  Alcotest.(check string)
    "same string" "hd x =: _m in fun (_index : int) -> return _m"
    (translate_print (thd (t "x")))

let translate_prim_tl () =
  Alcotest.(check string)
    "same string" "tl x =: _m in fun (_index : int) -> return _m"
    (translate_print (ttl (t "x")))

let translate_prim_fst () =
  Alcotest.(check string)
    "same string" "fst (42, true)"
    (translate_print (tfst (tprod (tint 42) ttrue)))

let translate_prim_snd () =
  Alcotest.(check string)
    "same string" "snd (42, true)"
    (translate_print (tsnd (tprod (tint 42) ttrue)))

let translate_prim_not () =
  Alcotest.(check string)
    "same string" "not true"
    (translate_print (tnot ttrue))

let translate_prim_plus () =
  Alcotest.(check string)
    "same string" "41 + 1"
    (translate_print (tint 41 +! tint 1))

let translate_prim_minus () =
  Alcotest.(check string)
    "same string" "43 - 1"
    (translate_print (tint 43 -! tint 1))

let translate_prim_indexs () =
  Alcotest.(check string)
    "same string" "21 * 2"
    (translate_print (tint 21 *! tint 2))

let translate_prim_div () =
  Alcotest.(check string)
    "same string" "82 / 2"
    (translate_print (tint 82 /! tint 2))

let translate_prim_eq () =
  Alcotest.(check string)
    "same string" "true = false"
    (translate_print (ttrue ==! tfalse))

let translate_prim_neq () =
  Alcotest.(check string)
    "same string" "true <> false"
    (translate_print (ttrue <>! tfalse))

let translate_prim_andb () =
  Alcotest.(check string)
    "same string" "true && false"
    (translate_print (ttrue &&! tfalse))

let translate_prim_orb () =
  Alcotest.(check string)
    "same string" "true || false"
    (translate_print (ttrue ||! tfalse))

let translate_prim_cons () =
  Alcotest.(check string)
    "same string" "[1; 2]"
    (translate_print (tcons (tint 1) (tcons (tint 2) tnil)))

let translate_term_fby () =
  Alcotest.(check string)
    "same string"
    "(rec ones =\n\
    \  fun (_index : int) ->\n\
    \    return (1::thunk (fun (_index2 : int) -> (force ones) _index2)))"
    (translate_print (trec "ones" (treturn (tint 1 *: t "ones"))))

let translate_example_rec_fact_5 () =
  Alcotest.(check string)
    "same string"
    "fun (_index : int) ->\n\
    \  ((rec fact =\n\
    \    fun (_index2 : int) ->\n\
    \      fun x ->\n\
    \        if (x = 1) then fun (_index3 : int) -> return 1 else\n\
    \         fun (_index4 : int) ->\n\
    \           return (x - 1) =: y in\n\
    \             (fun (_index5 : int) ->\n\
    \               ((fun (_index6 : int) -> (force fact) _index6 y _index6) \
     _index5 =:\n\
    \                 z in fun (_index7 : int) -> return (x * z))\n\
    \                 _index5)\n\
    \               _index4))\n\
    \    _index\n\
    \    5\n\
    \    _index"
    (translate_print
       (tint 5
       |>! trec "fact"
             (tfun "x"
                (tif
                   (t "x" ==! tint 1)
                   (treturn (tint 1))
                   ((treturn (t "x" -! tint 1) =: "y")
                      ((t "y" |>! tforce (t "fact") =: "z")
                         (treturn (t "x" *! t "z"))))))))

let translate_example_nth_stream_nat_8 () =
  Alcotest.(check string)
    "same string" "return 8"
    (run_print (tthunk nat_obs @! tfun "_" (treturn (tint 8))))

let () =
  Alcotest.run ~bail:false ~show_errors:true "CBPV monadic transalation"
    [
      ( "Literals",
        [
          Alcotest.test_case "nat" `Quick translate_lit_unit;
          Alcotest.test_case "nat" `Quick translate_lit_nat;
          Alcotest.test_case "nat" `Quick translate_lit_nat2;
          Alcotest.test_case "bool" `Quick translate_lit_bool;
          Alcotest.test_case "bool" `Quick translate_lit_bool2;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "fun" `Quick translate_term_fun;
          Alcotest.test_case "thunk" `Quick translate_term_thunk;
          Alcotest.test_case "force" `Quick translate_term_force;
          Alcotest.test_case "pair" `Quick translate_term_pair;
          Alcotest.test_case "var" `Quick translate_term_var;
          Alcotest.test_case "app" `Quick translate_term_app;
          Alcotest.test_case "tail_app" `Quick translate_term_tail_app;
          Alcotest.test_case "return" `Quick translate_term_return;
          Alcotest.test_case "let" `Quick translate_term_let;
          Alcotest.test_case "to" `Quick translate_term_to;
          Alcotest.test_case "if" `Quick translate_term_if;
          Alcotest.test_case "if" `Quick translate_term_if2;
          Alcotest.test_case "fby" `Quick translate_term_fby;
          Alcotest.test_case "murecord" `Quick translate_term_murecord;
          Alcotest.test_case "murecord" `Quick translate_term_record_field;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "hd" `Quick translate_prim_hd;
          Alcotest.test_case "tl" `Quick translate_prim_tl;
          Alcotest.test_case "fst" `Quick translate_prim_fst;
          Alcotest.test_case "snd" `Quick translate_prim_snd;
          Alcotest.test_case "not" `Quick translate_prim_not;
          Alcotest.test_case "eq" `Quick translate_prim_eq;
          Alcotest.test_case "neq" `Quick translate_prim_neq;
          Alcotest.test_case "and" `Quick translate_prim_andb;
          Alcotest.test_case "or" `Quick translate_prim_orb;
          Alcotest.test_case "cons" `Quick translate_prim_cons;
          Alcotest.test_case "plus" `Quick translate_prim_plus;
          Alcotest.test_case "minus" `Quick translate_prim_minus;
          Alcotest.test_case "indexs" `Quick translate_prim_indexs;
          Alcotest.test_case "div" `Quick translate_prim_div;
        ] );
      ( "Examples",
        [
          Alcotest.test_case "fact" `Quick translate_example_rec_fact_5;
          Alcotest.test_case "nats" `Quick translate_example_stream_nat_8;
          Alcotest.test_case "nats_nth" `Quick
            translate_example_nth_stream_nat_8;
        ] );
    ]
