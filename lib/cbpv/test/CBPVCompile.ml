open Libcbpv_compiler
open Compiler
open Syntax

let cbpv_val : tyval term Alcotest.testable =
  Alcotest.testable Printer.fmt_of_term ( = )

let cbpv_comp : tycomp term Alcotest.testable =
  Alcotest.testable Printer.fmt_of_term ( = )

let subst_var_ok () =
  Alcotest.(check cbpv_val)
    "same term" ttrue
    (CBPVToCBV.subst "y" ttrue (t "y"))

let subst_var_ko () =
  Alcotest.(check cbpv_val)
    "same term" (t "x")
    (CBPVToCBV.subst "y" ttrue (t "x"))

let subst_fun_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tfun "y" (treturn (t "y")))
    (CBPVToCBV.subst "x" ttrue (tfun "y" (treturn (t "y"))))

let subst_fun_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    (tfun "y" (treturn ttrue))
    (CBPVToCBV.subst "x" ttrue (tfun "y" (treturn (t "y"))))

let subst_fun2_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tfun "x" (treturn (t "y")))
    (CBPVToCBV.subst "x" ttrue (tfun "x" (treturn (t "y"))))

let subst_fun2_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    (tfun "x" (treturn ttrue))
    (CBPVToCBV.subst "x" ttrue (tfun "x" (treturn (t "y"))))

let subst_rec_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    (trec "y" (treturn (t "y")))
    (CBPVToCBV.subst "x" (t "y") (trec "y" (treturn (t "y"))))

let subst_rec_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    (trec "y" (treturn (t "x")))
    (CBPVToCBV.subst "x" (t "y") (trec "y" (treturn (t "y"))))

let subst_rec2_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    (trec "x" (treturn (t "x")))
    (CBPVToCBV.subst "x" ttrue (trec "x" (treturn (t "x"))))

let subst_rec2_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    (trec "x" (treturn ttrue))
    (CBPVToCBV.subst "x" ttrue (trec "x" (treturn (t "y"))))

let subst_app_left () =
  Alcotest.(check cbpv_comp)
    "same term"
    (t "z" |>! tfun "w" (treturn (t "y")))
    (CBPVToCBV.subst "x" (t "y") (t "z" |>! tfun "w" (treturn (t "x"))))

let subst_app_right () =
  Alcotest.(check cbpv_comp)
    "same term"
    (t "y" |>! tfun "z" (treturn (t "z")))
    (CBPVToCBV.subst "x" (t "y") (t "x" |>! tfun "z" (treturn (t "z"))))

let subst_pair_left () =
  Alcotest.(check cbpv_val)
    "same term"
    (tprod (t "y") ttrue)
    (CBPVToCBV.subst "x" (t "y") (tprod (t "x") ttrue))

let subst_pair_right () =
  Alcotest.(check cbpv_val)
    "same term"
    (tprod ttrue (t "y"))
    (CBPVToCBV.subst "x" (t "y") (tprod ttrue (t "x")))

let subst_let_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tlet "y" ttrue (treturn tfalse))
    (CBPVToCBV.subst "x" tfalse (tlet "y" ttrue (treturn (t "x"))))

let subst_let_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    (tlet "y" ttrue (treturn (t "x")))
    (CBPVToCBV.subst "x" tfalse (tlet "y" ttrue (treturn (t "x"))))

let subst_let2_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tlet "x" ttrue (treturn (t "x")))
    (CBPVToCBV.subst "x" tfalse (tlet "x" ttrue (treturn (t "x"))))

let subst_let2_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    (tlet "x" ttrue (treturn tfalse))
    (CBPVToCBV.subst "x" tfalse (tlet "x" ttrue (treturn (t "x"))))

let subst_to_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    ((treturn ttrue =: "y") (treturn tfalse))
    (CBPVToCBV.subst "x" tfalse ((treturn ttrue =: "y") (treturn (t "x"))))

let subst_to_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    ((treturn ttrue =: "y") (treturn (t "x")))
    (CBPVToCBV.subst "x" tfalse ((treturn ttrue =: "y") (treturn (t "x"))))

let subst_to2_ok () =
  Alcotest.(check cbpv_comp)
    "same term"
    ((treturn ttrue =: "x") (treturn (t "x")))
    (CBPVToCBV.subst "x" tfalse ((treturn ttrue =: "x") (treturn (t "x"))))

let subst_to2_ko () =
  Alcotest.(check (neg cbpv_comp))
    "same term"
    ((treturn ttrue =: "x") (treturn tfalse))
    (CBPVToCBV.subst "x" tfalse ((treturn ttrue =: "x") (treturn (t "x"))))

let subst_thunk () =
  Alcotest.(check cbpv_val)
    "same term"
    (tthunk (treturn tfalse))
    (CBPVToCBV.subst "x" tfalse (tthunk (treturn (t "x"))))

let subst_force () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tforce (tthunk (treturn (tint 1))))
    (CBPVToCBV.subst "x" (tthunk (treturn (tint 1))) (tforce (t "x")))

let subst_if () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tif ttrue (treturn ttrue) (treturn ttrue))
    (CBPVToCBV.subst "x" ttrue
       (tif (t "x") (treturn (t "x")) (treturn (t "x"))))

let subst_plus () =
  Alcotest.(check cbpv_val)
    "same term"
    (tint 1 +! tint 1)
    (CBPVToCBV.subst "x" (tint 1) (t "x" +! t "x"))

let subst_minus () =
  Alcotest.(check cbpv_val)
    "same term"
    (tint 1 -! tint 1)
    (CBPVToCBV.subst "x" (tint 1) (t "x" -! t "x"))

let subst_mult () =
  Alcotest.(check cbpv_val)
    "same term"
    (tint 1 *! tint 1)
    (CBPVToCBV.subst "x" (tint 1) (t "x" *! t "x"))

let subst_div () =
  Alcotest.(check cbpv_val)
    "same term"
    (tint 1 /! tint 1)
    (CBPVToCBV.subst "x" (tint 1) (t "x" /! t "x"))

let subst_eq () =
  Alcotest.(check cbpv_val)
    "same term"
    (tint 1 ==! tint 1)
    (CBPVToCBV.subst "x" (tint 1) (t "x" ==! t "x"))

let subst_neq () =
  Alcotest.(check cbpv_val)
    "same term"
    (tint 1 <>! tint 1)
    (CBPVToCBV.subst "x" (tint 1) (t "x" <>! t "x"))

let subst_and () =
  Alcotest.(check cbpv_val)
    "same term" (ttrue &&! ttrue)
    (CBPVToCBV.subst "x" ttrue (t "x" &&! t "x"))

let subst_or () =
  Alcotest.(check cbpv_val)
    "same term" (ttrue ||! ttrue)
    (CBPVToCBV.subst "x" ttrue (t "x" ||! t "x"))

let subst_fst () =
  Alcotest.(check cbpv_val)
    "same term"
    (tfst (tprod ttrue tfalse))
    (CBPVToCBV.subst "x" (tprod ttrue tfalse) (tfst (t "x")))

let subst_snd () =
  Alcotest.(check cbpv_val)
    "same term"
    (tsnd (tprod ttrue tfalse))
    (CBPVToCBV.subst "x" (tprod ttrue tfalse) (tsnd (t "x")))

let subst_hd () =
  Alcotest.(check cbpv_comp)
    "same term"
    (thd (ttrue *: tfalse))
    (CBPVToCBV.subst "x" (ttrue *: tfalse) (thd (t "x")))

let subst_tl () =
  Alcotest.(check cbpv_comp)
    "same term"
    (ttl (ttrue *: tfalse))
    (CBPVToCBV.subst "x" (ttrue *: tfalse) (ttl (t "x")))

let subst_lhd () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tlhd (tcons ttrue tnil))
    (CBPVToCBV.subst "x" (tcons ttrue tnil) (tlhd (t "x")))

let subst_ltl () =
  Alcotest.(check cbpv_comp)
    "same term"
    (tltl (tcons ttrue tnil))
    (CBPVToCBV.subst "x" (tcons ttrue tnil) (tltl (t "x")))

let subst_neg () =
  Alcotest.(check cbpv_val)
    "same term" (tnot ttrue)
    (CBPVToCBV.subst "x" ttrue (tnot (t "x")))

let subst_cons () =
  Alcotest.(check cbpv_val)
    "same term" (tcons ttrue tnil)
    (CBPVToCBV.subst "x" ttrue (tcons (t "x") tnil))

let subst_fby () =
  Alcotest.(check cbpv_val)
    "same term" (ttrue *: tnil)
    (CBPVToCBV.subst "x" ttrue (t "x" *: tnil))

let compile_lit_unit () =
  Alcotest.(check unit) "same value" () (eval (treturn tunit))

let compile_lit_nat () =
  Alcotest.(check int) "same value" 1 (eval (treturn (tint 1)))

let compile_lit_nat2 () =
  Alcotest.(check int) "same value" 54 (eval (treturn (tint 54)))

let compile_lit_bool () =
  Alcotest.(check bool) "same value" true (eval (treturn ttrue))

let compile_lit_bool2 () =
  Alcotest.(check bool) "same value" false (eval (treturn tfalse))

let compile_val_pair () =
  Alcotest.(check (pair int bool))
    "same value" (1, false)
    (eval (tlet "x" (tprod (tint 1) tfalse) (treturn (t "x"))))

let compile_val_thunk () =
  Alcotest.(check string)
    "same string"
    "LAMBDA (pair unit int) bool {\n\
    \ UNPAIR;\n\
    \ SWAP;\n\
    \ PUSH bool true;\n\
    \ DIP {DROP 2}};\n\
     DUP;\n\
     LAMBDA (pair (lambda (pair unit int) bool) int) (lambda\n\
     (pair unit int) bool) {\n\
    \ UNPAIR;\n\
    \ SWAP;\n\
    \ DUP 1;\n\
    \ DIP {DROP 2}};\n\
     APPLY;\n\
     PUSH int 0;\n\
     DUP;\n\
     DUP 2;\n\
     EXEC;\n\
     DIP {DROP};\n\
     DIP {DROP};\n\
     DIP {DROP}"
    (compile (tlet "x" (tthunk (treturn ttrue)) (treturn (t "x"))))

let compile_term_fun () =
  Alcotest.(check int)
    "same value" 42
    (eval (tint 42 |>! tfun "x" (treturn (t "x"))))

let compile_term_force () =
  Alcotest.(check int)
    "same value" 42
    (eval (tforce (tthunk (treturn (tint 42)))))

let compile_term_pair () =
  Alcotest.(check (pair int bool))
    "same value" (42, true)
    (eval (treturn (tprod (tint 42) ttrue)))

let compile_term_app () =
  Alcotest.(check int)
    "same value" 42
    (eval (tfun "x" (treturn (t "x")) @@! tint 42))

let compile_term_return () =
  Alcotest.(check bool) "same value" true (eval (treturn ttrue))

let compile_term_let () =
  Alcotest.(check bool)
    "same value" true
    (eval (tlet "ans" ttrue (treturn (t "ans"))))

let compile_term_let2 () =
  Alcotest.(check bool)
    "same value" true
    (eval
       (tlet "ans" ttrue
          (tlet "ans2" (t "ans") (tlet "ans3" (t "ans2") (treturn (t "ans3"))))))

let compile_term_let3 () =
  Alcotest.(check bool)
    "same value" true
    (eval (tlet "ans" (tthunk (treturn ttrue)) (tforce (t "ans"))))

let compile_term_to () =
  Alcotest.(check int)
    "same value" 42
    (eval
       ((treturn (tthunk (tfun "a" (treturn (t "a" +! tint 1)))) =: "succ")
          (tforce (t "succ") @@! tint 41)))

let compile_term_if () =
  Alcotest.(check bool)
    "same value" false
    (eval (tif ttrue (treturn tfalse) (treturn ttrue)))

let compile_term_if2 () =
  Alcotest.(check bool)
    "same value" true
    (eval (tif tfalse (treturn tfalse) (treturn ttrue)))

let compile_prim_hd () =
  Alcotest.(check int)
    "same value" 1
    (eval ((example_stream =: "ones") (thd (t "ones"))))

let compile_prim_tl () =
  Alcotest.(check int)
    "same value" 1
    (eval
       ((example_stream =: "ones")
          ((ttl (t "ones") =: "ux") ((tforce (t "ux") =: "x") (thd (t "x"))))))

let compile_prim_fst () =
  Alcotest.(check int)
    "same value" 42
    (eval (treturn (tfst (tprod (tint 42) ttrue))))

let compile_prim_snd () =
  Alcotest.(check bool)
    "same value" true
    (eval (treturn (tsnd (tprod (tint 42) ttrue))))

let compile_prim_not () =
  Alcotest.(check bool) "same value" false (eval (treturn (tnot ttrue)))

let compile_prim_plus () =
  Alcotest.(check int) "same value" 42 (eval (treturn (tint 41 +! tint 1)))

let compile_prim_minus () =
  Alcotest.(check int) "same value" 42 (eval (treturn (tint 43 -! tint 1)))

let compile_prim_times () =
  Alcotest.(check int) "same value" 42 (eval (treturn (tint 21 *! tint 2)))

let compile_prim_div () =
  Alcotest.(check int) "same value" 41 (eval (treturn (tint 82 /! tint 2)))

let compile_prim_eq () =
  Alcotest.(check bool) "same value" false (eval (treturn (ttrue ==! tfalse)))

let compile_prim_neq () =
  Alcotest.(check bool) "same value" true (eval (treturn (ttrue <>! tfalse)))

let compile_prim_andb () =
  Alcotest.(check bool) "same string" false (eval (treturn (ttrue &&! tfalse)))

let compile_prim_orb () =
  Alcotest.(check bool) "same value" true (eval (treturn (ttrue ||! tfalse)))

let compile_prim_cons () =
  Alcotest.(check (list int))
    "same string" [ 1; 2 ]
    (eval (treturn (tcons (tint 1) (tcons (tint 2) tnil))))

let compile_example_rec_fact_5 () =
  Alcotest.(check int)
    "same value" 120
    (eval
       (tint 5
       |>! trec "fact"
             (tfun "x"
                (tif
                   (t "x" ==! tint 1)
                   (treturn (tint 1))
                   ((treturn (t "x" -! tint 1) =: "y")
                      ((t "y" |>! tforce (t "fact") =: "z")
                         (treturn (t "x" *! t "z"))))))))

let compile_example_stream_nat_8 () =
  Alcotest.(check int)
    "same value" 8
    (eval
       (Interpreter.parse
          "(thunk (rec nat =\n\
          \  return 0 =: (xx : int) in\n\
          \  return\n\
          \     (thunk\n\
          \      ((rec next =\n\
          \        return nat =: ss in\n\
          \        (ss @(fun (i : int) ->\n\
          \              return i =: (xx4 : int) in\n\
          \              return 1 =: (yy4 : int) in return (xx4 - yy4))) =: \
           (xx3 : int) in\n\
          \          return 1 =: (yy3 : int) in return (xx3 + yy3) =: (xx2 : \
           int) in\n\
          \          return next =: yy2 in return (xx2::yy2)))) =:\n\
          \      yy in return (xx::yy)))@(fun (i:int) -> return 8) "))

let () =
  Alcotest.run ~bail:true ~show_errors:true "CBPV compiler"
    [
      ( "Substitution",
        [
          Alcotest.test_case "var" `Quick subst_var_ok;
          Alcotest.test_case "var" `Quick subst_var_ko;
          Alcotest.test_case "fun ok" `Quick subst_fun_ok;
          Alcotest.test_case "fun ko" `Quick subst_fun_ko;
          Alcotest.test_case "fun ok" `Quick subst_fun2_ok;
          Alcotest.test_case "fun ko" `Quick subst_fun2_ko;
          Alcotest.test_case "rec ok" `Quick subst_rec_ok;
          Alcotest.test_case "rec ko" `Quick subst_rec_ko;
          Alcotest.test_case "rec ok" `Quick subst_rec2_ok;
          Alcotest.test_case "rec ko" `Quick subst_rec2_ko;
          Alcotest.test_case "app left" `Quick subst_app_left;
          Alcotest.test_case "app right" `Quick subst_app_right;
          Alcotest.test_case "pair left" `Quick subst_pair_left;
          Alcotest.test_case "pair right" `Quick subst_pair_right;
          Alcotest.test_case "let ok" `Quick subst_let_ok;
          Alcotest.test_case "let ko" `Quick subst_let_ko;
          Alcotest.test_case "let ok" `Quick subst_let2_ok;
          Alcotest.test_case "let ko" `Quick subst_let2_ko;
          Alcotest.test_case "to ok" `Quick subst_to_ok;
          Alcotest.test_case "to ko" `Quick subst_to_ko;
          Alcotest.test_case "to ok" `Quick subst_to2_ok;
          Alcotest.test_case "to ko" `Quick subst_to2_ko;
          Alcotest.test_case "thunk" `Quick subst_thunk;
          Alcotest.test_case "force" `Quick subst_force;
          Alcotest.test_case "force" `Quick subst_if;
          Alcotest.test_case "if" `Quick subst_plus;
          Alcotest.test_case "prim" `Quick subst_minus;
          Alcotest.test_case "prim" `Quick subst_mult;
          Alcotest.test_case "prim" `Quick subst_div;
          Alcotest.test_case "prim" `Quick subst_eq;
          Alcotest.test_case "prim" `Quick subst_neq;
          Alcotest.test_case "prim" `Quick subst_and;
          Alcotest.test_case "prim" `Quick subst_or;
          Alcotest.test_case "prim" `Quick subst_fst;
          Alcotest.test_case "prim" `Quick subst_snd;
          Alcotest.test_case "prim" `Quick subst_hd;
          Alcotest.test_case "prim" `Quick subst_tl;
          Alcotest.test_case "prim" `Quick subst_lhd;
          Alcotest.test_case "prim" `Quick subst_ltl;
          Alcotest.test_case "prim" `Quick subst_neg;
          Alcotest.test_case "prim" `Quick subst_cons;
          Alcotest.test_case "fby" `Quick subst_fby;
        ] );
      ( "Literals",
        [
          Alcotest.test_case "unit" `Quick compile_lit_unit;
          Alcotest.test_case "nat" `Quick compile_lit_nat;
          Alcotest.test_case "nat" `Quick compile_lit_nat2;
          Alcotest.test_case "bool" `Quick compile_lit_bool;
          Alcotest.test_case "bool" `Quick compile_lit_bool2;
        ] );
      ( "Values",
        [
          Alcotest.test_case "pair" `Quick compile_val_pair;
          Alcotest.test_case "thunk" `Quick compile_val_thunk;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "fun" `Quick compile_term_fun;
          Alcotest.test_case "force" `Quick compile_term_force;
          Alcotest.test_case "pair" `Quick compile_term_pair;
          Alcotest.test_case "app" `Quick compile_term_app;
          Alcotest.test_case "return" `Quick compile_term_return;
          Alcotest.test_case "let" `Quick compile_term_let;
          Alcotest.test_case "let" `Quick compile_term_let2;
          Alcotest.test_case "let" `Quick compile_term_let3;
          Alcotest.test_case "to" `Quick compile_term_to;
          Alcotest.test_case "if" `Quick compile_term_if;
          Alcotest.test_case "if" `Quick compile_term_if2;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "hd" `Quick compile_prim_hd;
          Alcotest.test_case "tl" `Quick compile_prim_tl;
          Alcotest.test_case "fst" `Quick compile_prim_fst;
          Alcotest.test_case "snd" `Quick compile_prim_snd;
          Alcotest.test_case "not" `Quick compile_prim_not;
          Alcotest.test_case "eq" `Quick compile_prim_eq;
          Alcotest.test_case "neq" `Quick compile_prim_neq;
          Alcotest.test_case "and" `Quick compile_prim_andb;
          Alcotest.test_case "or" `Quick compile_prim_orb;
          Alcotest.test_case "fby" `Quick compile_prim_cons;
          Alcotest.test_case "plus" `Quick compile_prim_plus;
          Alcotest.test_case "minus" `Quick compile_prim_minus;
          Alcotest.test_case "times" `Quick compile_prim_times;
          Alcotest.test_case "div" `Quick compile_prim_div;
        ] );
      ( "Eval with mimi",
        [
          Alcotest.test_case "fact 5" `Quick compile_example_rec_fact_5;
          Alcotest.test_case "nat@8" `Quick compile_example_stream_nat_8;
        ] );
    ]
