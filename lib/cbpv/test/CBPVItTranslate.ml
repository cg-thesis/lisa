open Libcbpv_compiler
open Syntax
open Derivate

let translate_print : tycomp term -> string =
 fun t -> derivate t |> Printer.string_of_term

let translate_print_value v = derivate_val v |> Printer.string_of_term

(* let run_print : tycomp term -> string = *)
(*  fun t -> *)
(*   translate t |> fun t -> *)
(*   Semantics.eval (t @@! tint 0) { index = 0; env = [] } *)
(*   |> Printer.string_of_terminal *)

let translate_lit_unit () =
  Alcotest.(check string)
    "same string" "fun o -> fun h -> return ()"
    (translate_print_value tunit)

let translate_lit_nat () =
  Alcotest.(check string)
    "same string" "fun o -> fun h -> return 1"
    (translate_print_value (tint 1))

let translate_example_stream_nat_8 () =
  Alcotest.(check string)
    "same string"
    "fun _time ->\n\
    \  ((rec nat =\n\
    \    fun _time2 ->\n\
    \      return\n\
    \       0::thunk\n\
    \        (fun _n ->\n\
    \          (force\n\
    \           (thunk\n\
    \            (rec next =\n\
    \              fun _time3 ->\n\
    \                ((force nat) _time3 to n .\n\
    \                  fun _time4 ->\n\
    \                    ((fun _time5 ->\n\
    \                      (fun i -> return (i - 1)) _time5 to _otime .\n\
    \                        (((rec nth =\n\
    \                          fun xs ->\n\
    \                            fun _i ->\n\
    \                              fun Time ->\n\
    \                                if (_i = 0) then (hd xs) else\n\
    \                                 (tl xs to mtail .\n\
    \                                   hd xs to head .\n\
    \                                     (force mtail) Time to tail .\n\
    \                                       (((force nth) tail) _i - 1) Time + \
     1))\n\
    \                          n)\n\
    \                          _otime)\n\
    \                          0)\n\
    \                      _time4 to v .\n\
    \                      fun _time6 ->\n\
    \                        return v + 1::thunk (fun _n2 -> (force next) _n2 \
     + 1))\n\
    \                      _time4)\n\
    \                  _time3)))\n\
    \            _n + 1))\n\
    \    _time to m .\n\
    \    fun _time7 ->\n\
    \      (fun _ -> return 8) _time7 to _otime2 .\n\
    \        (((rec nth =\n\
    \          fun xs ->\n\
    \            fun _i ->\n\
    \              fun Time ->\n\
    \                if (_i = 0) then (hd xs) else\n\
    \                 (tl xs to mtail .\n\
    \                   hd xs to head .\n\
    \                     (force mtail) Time to tail .\n\
    \                       (((force nth) tail) _i - 1) Time + 1))\n\
    \          m)\n\
    \          _otime2)\n\
    \          0)\n\
    \    _time" (translate_print nat_obs)

let translate_lit_nat2 () =
  Alcotest.(check string)
    "same string" "fun o -> fun h -> return 54"
    (translate_print_value (tint 54))

let translate_lit_bool () =
  Alcotest.(check string)
    "same string" "fun o -> fun h -> return true"
    (translate_print_value ttrue)

let translate_lit_bool2 () =
  Alcotest.(check string)
    "same string" "fun o -> fun h -> return false"
    (translate_print_value tfalse)

let translate_term_fun () =
  Alcotest.(check string)
    "same string" "fun o -> fun h -> fun x -> (fun o -> fun h -> return x) o h"
    (translate_print (tfun "x" (treturn (TypedTerm (ty_int, t "x")))))

let translate_term_thunk () =
  Alcotest.(check string)
    "same string"
    "fun o -> fun h -> return (thunk (fun x -> fun o -> fun h -> return x))"
    (translate_print_value
       (tthunk (tfun "x" (treturn (TypedTerm (ty_int, t "x"))))))

let translate_term_force () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    ((fun o -> fun h -> return (thunk (fun x -> fun o -> fun h -> return \
     x)))\n\
    \      o\n\
    \      h =: dv in force dv)\n\
    \      o\n\
    \      h"
    (translate_print
       (tforce (tthunk (tfun "x" (treturn (TypedTerm (ty_int, t "x")))))))

let translate_term_pair () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return 42) o h =: da in\n\
    \      (fun o -> fun h -> return true) o h =: db in return (da, db)"
    (translate_print_value (tprod (tint 42) ttrue))

let translate_term_var () =
  Alcotest.(check string)
    "same string" "fun o -> fun h -> return x"
    (translate_print_value (TypedTerm (ty_int, t "x")))

let translate_term_app () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    ((fun o -> fun h -> return 42) o h =: db in\n\
    \      (fun x -> fun o -> fun h -> return x) db)\n\
    \      o\n\
    \      h"
    (translate_print
       (tfun "x" (treturn (TypedTerm (TyApp (("int", 0), []), t "x")))
       @@! tint 42))

let translate_term_return () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return (thunk (fun x -> fun o -> fun h -> return \
     x)))\n\
    \      o\n\
    \      h"
    (translate_print
       (treturn (tthunk (tfun "x" (treturn (TypedTerm (ty_int, t "x")))))))

let translate_term_let () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    ((fun o -> fun h -> return true) o h =: ans in\n\
    \      fun o -> fun h -> return ans)\n\
    \      o\n\
    \      h"
    (translate_print
       (tlet "ans" ttrue (treturn (TypedTerm (ty_bool, t "ans")))))

let translate_term_to () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    ((fun o ->\n\
    \      fun h ->\n\
    \        return\n\
    \         (thunk\n\
    \          (fun a ->\n\
    \            fun o ->\n\
    \              fun h ->\n\
    \                (fun o -> fun h -> return a) o h =: da in\n\
    \                  (fun o -> fun h -> return 1) o h =: db1 in return (da + \
     db1))))\n\
    \      o\n\
    \      h =: succ in\n\
    \      (fun o -> fun h -> return 41) o h =: db in\n\
    \        ((fun o -> fun h -> return succ) o h =: dv in force dv) db)\n\
    \      o\n\
    \      h"
    (translate_print
       ((treturn
           (tthunk (tfun "a" (treturn (TypedTerm (ty_int, t "a") +! tint 1))))
        =: "succ")
          (tforce
             (TypedTerm (ty_thunk (ty_arrow ty_int (ty_return ty_int)), t "succ"))
          @@! tint 41)))

let translate_term_if () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    ((fun o -> fun h -> return true) o h =: da in\n\
    \      if da then fun o -> fun h -> return false else\n\
    \       fun o -> fun h -> return true)\n\
    \      o\n\
    \      h"
    (translate_print (tif ttrue (treturn tfalse) (treturn ttrue)))

let translate_term_if2 () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    ((fun o -> fun h -> return false) o h =: da in\n\
    \      if da then fun o -> fun h -> return false else\n\
    \       fun o -> fun h -> return true)\n\
    \      o\n\
    \      h"
    (translate_print (tif tfalse (treturn tfalse) (treturn ttrue)))

let translate_term_murecord () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    {x =\n\
    \      fun o ->\n\
    \        fun h ->\n\
    \          if (o = h) then return [] else\n\
    \           (fun o -> fun h -> return true) o h =: da in\n\
    \             (fun o -> fun h -> return y) o h =: db in\n\
    \               (force db) (o + 1) h =: dn in return (cons da (dn)); y =\n\
    \      fun o ->\n\
    \        fun h ->\n\
    \          if (o = h) then return [] else\n\
    \           (fun o -> fun h -> return false) o h =: da1 in\n\
    \             (fun o -> fun h -> return x) o h =: db1 in\n\
    \               (force db1) (o + 1) h =: dn1 in return (cons da1 (dn1))}\n\
    \      o\n\
    \      h"
    (translate_print
       (MuRecord
          [
            ( "x",
              None,
              treturn
                (ttrue *: TypedTerm (ty_thunk (ty_return stream_bool), t "y"))
            );
            ( "y",
              None,
              treturn
                (tfalse *: TypedTerm (ty_thunk (ty_return stream_bool), t "x"))
            );
          ]))

let translate_term_record_field () =
  Alcotest.(check string)
    "same string"
    "{x = fun _time -> return true::thunk (fun _n -> (force y) _n + 1); y =\n\
    \  fun _time2 -> return false::thunk (fun _n2 -> (force x) _n2 + 1)}.y"
    (translate_print
       (RecordField
          ( MuRecord
              [
                ( "x",
                  None,
                  treturn
                    (ttrue
                    *: TypedTerm (ty_thunk (ty_return stream_bool), t "y")) );
                ( "y",
                  None,
                  treturn
                    (tfalse
                    *: TypedTerm (ty_thunk (ty_return stream_bool), t "x")) );
              ],
            "y" )))

let translate_prim_hd () =
  Alcotest.(check string)
    "same string"
    "fun o -> fun h -> ((fun o -> fun h -> return x) o h =: da in hd da) o h"
    (translate_print (thd (t "x")))

let translate_prim_tl () =
  Alcotest.(check string)
    "same string"
    "fun o -> fun h -> ((fun o -> fun h -> return x) o h =: da in tl da) o h"
    (translate_print (ttl (t "x")))

let translate_prim_fst () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o ->\n\
    \      fun h ->\n\
    \        (fun o -> fun h -> return 42) o h =: da1 in\n\
    \          (fun o -> fun h -> return true) o h =: db in return (da1, db))\n\
    \      o\n\
    \      h =: da in return (fst da)"
    (translate_print_value (tfst (tprod (tint 42) ttrue)))

let translate_prim_snd () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o ->\n\
    \      fun h ->\n\
    \        (fun o -> fun h -> return 42) o h =: da1 in\n\
    \          (fun o -> fun h -> return true) o h =: db in return (da1, db))\n\
    \      o\n\
    \      h =: da in return (snd da)"
    (translate_print_value (tsnd (tprod (tint 42) ttrue)))

let translate_prim_not () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h -> (fun o -> fun h -> return true) o h =: da in return (not da)"
    (translate_print_value (tnot ttrue))

let translate_prim_plus () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return 41) o h =: da in\n\
    \      (fun o -> fun h -> return 1) o h =: db in return (da + db)"
    (translate_print_value (tint 41 +! tint 1))

let translate_prim_minus () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return 43) o h =: da in\n\
    \      (fun o -> fun h -> return 1) o h =: db in return (da - db)"
    (translate_print_value (tint 43 -! tint 1))

let translate_prim_times () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return 21) o h =: da in\n\
    \      (fun o -> fun h -> return 2) o h =: db in return (da * db)"
    (translate_print_value (tint 21 *! tint 2))

let translate_prim_div () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return 82) o h =: da in\n\
    \      (fun o -> fun h -> return 2) o h =: db in return (da / db)"
    (translate_print_value (tint 82 /! tint 2))

let translate_prim_eq () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return true) o h =: da in\n\
    \      (fun o -> fun h -> return false) o h =: db in return (da = db)"
    (translate_print_value (ttrue ==! tfalse))

let translate_prim_neq () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return true) o h =: da in\n\
    \      (fun o -> fun h -> return false) o h =: db in return (da <> db)"
    (translate_print_value (ttrue <>! tfalse))

let translate_prim_andb () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return true) o h =: da in\n\
    \      (fun o -> fun h -> return false) o h =: db in return (da && db)"
    (translate_print_value (ttrue &&! tfalse))

let translate_prim_orb () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return true) o h =: da in\n\
    \      (fun o -> fun h -> return false) o h =: db in return (da || db)"
    (translate_print_value (ttrue ||! tfalse))

let translate_prim_cons () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (fun o -> fun h -> return 1) o h =: da in\n\
    \      (fun o ->\n\
    \        fun h ->\n\
    \          (fun o -> fun h -> return 2) o h =: da1 in\n\
    \            (fun o -> fun h -> return []) o h =: db1 in return (cons da1 \
     (db1)))\n\
    \        o\n\
    \        h =: db in return (cons da (db))"
    (translate_print_value (tcons (tint 1) (tcons (tint 2) tnil)))

let translate_term_fby () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    (rec ones =\n\
    \      fun o ->\n\
    \        fun h ->\n\
    \          if (o = h) then return [] else\n\
    \           (fun o -> fun h -> return 1) o h =: da in\n\
    \             (fun o -> fun h -> return ones) o h =: db in\n\
    \               (force db) (o + 1) h =: dn in return (cons da (dn)))\n\
    \      o\n\
    \      h"
    (translate_print
       (trec "ones"
          (treturn
             (tint 1 *: TypedTerm (ty_thunk (ty_return stream_int), t "ones")))))

let translate_example_rec_fact_5 () =
  Alcotest.(check string)
    "same string"
    "fun o ->\n\
    \  fun h ->\n\
    \    ((fun o -> fun h -> return 5) o h =: db in\n\
    \      (rec fact =\n\
    \        fun x ->\n\
    \          (fun o ->\n\
    \            fun h ->\n\
    \              (fun o -> fun h -> return x) o h =: da4 in\n\
    \                (fun o -> fun h -> return 1) o h =: db4 in return (da4 = \
     db4))\n\
    \            o\n\
    \            h =: da in\n\
    \            if da then fun o -> fun h -> return 1 else\n\
    \             (fun o ->\n\
    \               fun h ->\n\
    \                 (fun o -> fun h -> return x) o h =: da3 in\n\
    \                   (fun o -> fun h -> return 1) o h =: db3 in return (da3 \
     - db3))\n\
    \               o\n\
    \               h =: y in\n\
    \               (fun o -> fun h -> return y) o h =: da1 in\n\
    \                 print da1 ;\n\
    \                 ((fun o -> fun h -> return y) o h =: db2 in\n\
    \                   ((fun o -> fun h -> return fact) o h =: dv in force \
     dv) db2)\n\
    \                   o\n\
    \                   h =: z in\n\
    \                   fun o ->\n\
    \                     fun h ->\n\
    \                       (fun o -> fun h -> return x) o h =: da2 in\n\
    \                         (fun o -> fun h -> return z) o h =: db1 in\n\
    \                           return (da2 * db1))\n\
    \        db)\n\
    \      o\n\
    \      h"
    (translate_print
       (tint 5
       |>! trec "fact"
             (tfun "x"
                (tif
                   (TypedTerm (ty_int, t "x") ==! tint 1)
                   (treturn (tint 1))
                   ((treturn (TypedTerm (ty_int, t "x") -! tint 1) =: "y")
                      (tprint
                         (TypedTerm (ty_int, t "y"))
                         ((tforce
                             (TypedTerm
                                ( ty_thunk (ty_arrow ty_int (ty_return ty_int)),
                                  t "fact" ))
                           @@! TypedTerm (ty_int, t "y")
                          =: "z")
                            (treturn
                               (TypedTerm (ty_int, t "x")
                               *! TypedTerm (ty_int, t "z"))))))))))

let translate_example_nth_stream_nat_8 () =
  Alcotest.(check string) "same string" "return 8" (translate_print nat_obs)

let () =
  Alcotest.run ~show_errors:true "CBPV iterative monadic transalation"
    [
      ( "Literals",
        [
          Alcotest.test_case "nat" `Quick translate_lit_unit;
          Alcotest.test_case "nat" `Quick translate_lit_nat;
          Alcotest.test_case "nat" `Quick translate_lit_nat2;
          Alcotest.test_case "bool" `Quick translate_lit_bool;
          Alcotest.test_case "bool" `Quick translate_lit_bool2;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "fun" `Quick translate_term_fun;
          Alcotest.test_case "thunk" `Quick translate_term_thunk;
          Alcotest.test_case "force" `Quick translate_term_force;
          Alcotest.test_case "pair" `Quick translate_term_pair;
          Alcotest.test_case "var" `Quick translate_term_var;
          Alcotest.test_case "app" `Quick translate_term_app;
          Alcotest.test_case "return" `Quick translate_term_return;
          Alcotest.test_case "let" `Quick translate_term_let;
          Alcotest.test_case "to" `Quick translate_term_to;
          Alcotest.test_case "if" `Quick translate_term_if;
          Alcotest.test_case "if" `Quick translate_term_if2;
          Alcotest.test_case "fby" `Quick translate_term_fby;
          Alcotest.test_case "murecord" `Quick translate_term_murecord;
          Alcotest.test_case "murecord" `Quick translate_term_record_field;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "hd" `Quick translate_prim_hd;
          Alcotest.test_case "tl" `Quick translate_prim_tl;
          Alcotest.test_case "fst" `Quick translate_prim_fst;
          Alcotest.test_case "snd" `Quick translate_prim_snd;
          Alcotest.test_case "not" `Quick translate_prim_not;
          Alcotest.test_case "eq" `Quick translate_prim_eq;
          Alcotest.test_case "neq" `Quick translate_prim_neq;
          Alcotest.test_case "and" `Quick translate_prim_andb;
          Alcotest.test_case "or" `Quick translate_prim_orb;
          Alcotest.test_case "fby" `Quick translate_prim_cons;
          Alcotest.test_case "plus" `Quick translate_prim_plus;
          Alcotest.test_case "minus" `Quick translate_prim_minus;
          Alcotest.test_case "times" `Quick translate_prim_times;
          Alcotest.test_case "div" `Quick translate_prim_div;
        ] );
      ( "Examples",
        [
          Alcotest.test_case "fact" `Quick translate_example_rec_fact_5;
          Alcotest.test_case "nats" `Quick translate_example_stream_nat_8;
        ] );
    ]
