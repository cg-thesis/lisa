open Libcbpv_compiler
open Syntax
open Semantics

let eval_print : type a. a term -> string =
 fun t ->
  eval t { index = 0; env = [ ("x", ref (VInt 42, Return (tint 42))) ] }
  |> Printer.string_of_terminal

let run_origin x = x @@! tint 0

let eval_lit_unit () =
  Alcotest.(check string) "same string" "()" (eval_print tunit)

let eval_lit_nat () =
  Alcotest.(check string) "same string" "1" (eval_print (tint 1))

let meval_lit_nat () =
  Alcotest.(check string)
    "same string" "return [0;1;2;3;4;5;6;7;8]"
    (eval_print
       ((run_origin (Translate.translate nat_obs) =: "n")
          (Translate.take (tint 8) (t "n") (tint 0))))

(* let meval_lit_nat () =
 *   Alcotest.(check string)
 *     "same string" "return [0;1;2;3;4;5;6;7;8]"
 *     (eval_print ((obs =: "n") (t "n" @! tfun "_" (treturn (tint 9))))) *)

let eval_lit_nat2 () =
  Alcotest.(check string) "same string" "54" (eval_print (tint 54))

let eval_lit_bool () =
  Alcotest.(check string) "same string" "true" (eval_print ttrue)

let eval_lit_bool2 () =
  Alcotest.(check string) "same string" "false" (eval_print tfalse)

let eval_term_fun () =
  Alcotest.(check string)
    "same string" "<fun>"
    (eval_print (tfun "x" (treturn (t "x"))))

let eval_term_thunk () =
  Alcotest.(check string)
    "same string" "<thunk>"
    (eval_print (tthunk (tfun "x" (treturn (t "x")))))

let eval_term_force () =
  Alcotest.(check string)
    "same string" "<fun>"
    (eval_print (tforce (tthunk (tfun "x" (treturn (t "x"))))))

let eval_term_pair () =
  Alcotest.(check string)
    "same string" "(42, true)"
    (eval_print (tprod (tint 42) ttrue))

let eval_term_var () =
  Alcotest.(check string) "same string" "42" (eval_print (t "x"))

let eval_term_app () =
  Alcotest.(check string)
    "same string" "return 42"
    (eval_print (tfun "x" (treturn (t "x")) @@! tint 42))

let eval_term_return () =
  Alcotest.(check string)
    "same string" "return <thunk>"
    (eval_print (treturn (tthunk (tfun "x" (treturn (t "x"))))))

let eval_term_let () =
  Alcotest.(check string)
    "same string" "return true"
    (eval_print (tlet "ans" ttrue (treturn (t "ans"))))

let eval_term_to () =
  Alcotest.(check string)
    "same string" "return 42"
    (eval_print
       ((treturn (tthunk (tfun "a" (treturn (t "a" +! tint 1)))) =: "succ")
          (tforce (t "succ") @@! tint 41)))

let eval_term_if () =
  Alcotest.(check string)
    "same string" "return false"
    (eval_print (tif ttrue (treturn tfalse) (treturn ttrue)))

let eval_term_if2 () =
  Alcotest.(check string)
    "same string" "return true"
    (eval_print (tif tfalse (treturn tfalse) (treturn ttrue)))

let eval_term_murecord () =
  Alcotest.(check string)
    "same string" "{x = return true :: <thunk>; y = return false :: <thunk>}"
    (eval_print
       (MuRecord
          [
            ("x", None, treturn (ttrue *: t "y"));
            ("y", None, treturn (tfalse *: t "x"));
          ]))

let eval_term_record_field () =
  Alcotest.(check string)
    "same string" "return false :: <thunk>"
    (eval_print
       (RecordField
          ( MuRecord
              [
                ("x", None, treturn (ttrue *: t "y"));
                ("y", None, treturn (tfalse *: t "x"));
              ],
            "y" )))

let eval_term_record_field_nth_1 () =
  Alcotest.(check string)
    "same string" "return true"
    (eval_print
       ((RecordField
           ( MuRecord
               [
                 ("x", None, treturn (ttrue *: t "y"));
                 ("y", None, treturn (tfalse *: t "x"));
               ],
             "y" )
        =: "a")
          ((trec "nth"
              (tfun "xs"
                 (tfun "i"
                    (tif
                       (t "i" ==! tint 0)
                       (thd (t "xs"))
                       (t "i" -! tint 1
                       |>! (ttl (t "xs") =: "tail")
                             ((tforce (t "tail") =: "t")
                                (t "t" |>! tforce (t "nth")))))))
           @@! t "a")
          @@! tint 1)))

let eval_prim_fst () =
  Alcotest.(check string)
    "same string" "42"
    (eval_print (tfst (tprod (tint 42) ttrue)))

let eval_prim_snd () =
  Alcotest.(check string)
    "same string" "true"
    (eval_print (tsnd (tprod (tint 42) ttrue)))

let eval_prim_not () =
  Alcotest.(check string) "same string" "false" (eval_print (tnot ttrue))

let eval_term_cons () =
  Alcotest.(check string)
    "same string" "return 1 :: <thunk>"
    (eval_print (trec "ones" (treturn (tint 1 *: t "ones"))))

let eval_example_rec_fact_5 () =
  Alcotest.(check string)
    "same string" "return 120"
    (eval_print
       (tint 5
       |>! trec "fact"
             (tfun "x"
                (tif
                   (t "x" ==! tint 1)
                   (treturn (tint 1))
                   ((treturn (t "x" -! tint 1) =: "y")
                      (tprint (t "y")
                         ((t "y" |>! tforce (t "fact") =: "z")
                            (treturn (t "x" *! t "z")))))))))

let eval_example_nth_stream_nat_8 () =
  Alcotest.(check string) "same string" "return 8" (eval_print nat_8)

let eval_scoping () =
  Alcotest.(check string)
    "same string" "return 1"
    (eval_print
       (tlet "x" (tint 1)
          (tlet "y"
             (tthunk (tfun "a" (treturn (t "x"))))
             (tlet "x" ttrue (tfalse |>! tforce (t "y"))))))

let () =
  Alcotest.run ~bail:true ~show_errors:true "CBPV semantics"
    [
      (* ("Monadic eval", [ Alcotest.test_case "nat" `Quick meval_lit_nat ]); *)
      ( "literals",
        [
          Alcotest.test_case "nat" `Quick eval_lit_unit;
          Alcotest.test_case "nat" `Quick eval_lit_nat;
          Alcotest.test_case "nat" `Quick eval_lit_nat2;
          Alcotest.test_case "bool" `Quick eval_lit_bool;
          Alcotest.test_case "bool" `Quick eval_lit_bool2;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "fun" `Quick eval_term_fun;
          Alcotest.test_case "thunk" `Quick eval_term_thunk;
          Alcotest.test_case "force" `Quick eval_term_force;
          Alcotest.test_case "pair" `Quick eval_term_pair;
          Alcotest.test_case "var" `Quick eval_term_var;
          Alcotest.test_case "app" `Quick eval_term_app;
          Alcotest.test_case "return" `Quick eval_term_return;
          Alcotest.test_case "let" `Quick eval_term_let;
          Alcotest.test_case "to" `Quick eval_term_to;
          Alcotest.test_case "if" `Quick eval_term_if;
          Alcotest.test_case "if" `Quick eval_term_if2;
          Alcotest.test_case "cons" `Quick eval_term_cons;
          Alcotest.test_case "murecord" `Quick eval_term_murecord;
          Alcotest.test_case "murecord" `Quick eval_term_record_field;
          Alcotest.test_case "murecord" `Quick eval_term_record_field_nth_1;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "pair" `Quick eval_prim_fst;
          Alcotest.test_case "pair" `Quick eval_prim_snd;
          Alcotest.test_case "not" `Quick eval_prim_not;
        ] );
      ( "Examples",
        [
          Alcotest.test_case "fact" `Quick eval_example_rec_fact_5;
          Alcotest.test_case "nats" `Quick eval_example_nth_stream_nat_8;
          Alcotest.test_case "scoping" `Quick eval_scoping;
        ] );
    ]
