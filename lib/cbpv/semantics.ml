open Syntax

type 'a eval_env = eenv -> 'a
type gterm = T : 'a term -> gterm

exception TypeError of gterm

let return (x : 'a) : 'a eval_env = fun _ -> x

let bind (m : 'a eval_env) (f : 'a -> 'b eval_env) : 'b eval_env =
 fun e -> f (m e) e

let ( let* ) = bind

let get x context =
  let env = context.env in
  try !(List.assoc x env) |> fst
  with Not_found -> failwith (Printf.sprintf "Unbound %s\n" x)

let put x v def m context =
  let env = context.env in
  m { context with env = (x, ref (v, def)) :: env }

let index context = context.index
let tick m context = m { context with index = succ context.index }
let reset m context = m { context with index = 0 }

let show_index context m =
  let* () = return (Printf.printf "INDEX: %d%!\n" context.index) in
  m

let print_env context =
  let env = context.env in
  Printf.printf "ENV : %s\n%!" (Printer.string_of_env env);
  return ()

let rec eval : type a. a term -> a terminal eval_env =
 fun t ->
  match (t : a term) with
  | TypedTerm (_, t) -> eval' t
  | TRange (r, t) -> (
      try eval' t
      with TypeError (T t) ->
        Printf.eprintf "TypeError when interpreting %s"
          (Printer.string_of_term t);
        Typechecker.print_range stderr r;
        exit 1)
  | Fun (x, _, t) -> fun context -> VFun (x, t, context.env)
  | Force t -> cforce t
  | Thunk t -> vthunk t
  | Var x -> get x
  | Const g -> vconst g
  | Prim (Print (t, u)) -> cprint t u
  | Prim (Fst t) -> vproj true t
  | Prim (Snd t) -> vproj false t
  | Prim (Hd t) -> vhd t
  | Prim (Tl t) -> vtl t
  | Prim (LHd t) -> vhd t
  | Prim (LTl t) -> vtl t
  | Prim (Plus (x, y)) -> vbin_arith ( + ) x y
  | Prim (Mult (x, y)) -> vbin_arith ( * ) x y
  | Prim (Minus (x, y)) -> vbin_arith ( - ) x y
  | Prim (Div (x, y)) -> vbin_arith ( / ) x y
  | Prim (Eq (x, y)) -> veq x y
  | Prim (Neq (x, y)) -> vneq x y
  | Prim (AndB (x, y)) -> vbin_bool ( && ) x y
  | Prim (OrB (x, y)) -> vbin_bool ( || ) x y
  | Prim (Not x) -> vnot x
  | Prim (Cons (x, xs)) ->
      let* head = eval' x in
      let* tail = eval' xs in
      return (vcons head tail)
  | App (t, u) -> capp t u
  | Return t -> creturn t
  | To (t, x, _, u) -> cto t x u
  | Let (x, _, t, u) -> clet x t u
  | Rec (x, _, t) -> crec x t
  | MuRecord bindings -> cmurecord bindings
  | RecordField (record, id) -> crecordfield record id
  | Pair (x, y) ->
      let* f = eval' x in
      let* s = eval' y in
      return (vpair f s)
  | Fby (x, Var y) ->
      fun c ->
        let head = eval' x c in
        let def = !(List.assoc y c.env) |> snd in
        let env =
          match !(List.assoc y c.env) |> fst with
          | VThunk t -> t.last.context.env
          | _ -> assert false
        in
        let thunk =
          VThunk
            {
              last =
                {
                  term = def;
                  context = { index = c.index + 1; env };
                  history = Vector.make 0 (VReturn VNil);
                  state = SNil;
                };
              offset = 0;
              kind = KRegular;
            }
        in
        return (vfby head thunk) c
  | Fby (x, y) ->
      let* head = eval' x in
      let* thunk = tick (eval' y) in
      return (vfby head thunk)
  | IfThenElse (c, x, y) -> cite c x y
  | At (s, f) -> cat s f

and eval_debug : type a. a term -> a terminal eval_env =
 fun t ->
  Printf.printf "----------------------------------\n";
  let* _ = print_env in
  let* _ = show_index in
  Printf.printf "EVAL TERM : %s%!\n" (Printer.string_of_term t);
  flush_all ();
  eval t

and eval' : type a. a term -> a terminal eval_env =
 fun t ->
  match t with
  | TRange (r, t) -> (
      try eval t
      with TypeError (T t) ->
        Printf.eprintf "TypeError when interpreting %s"
          (Printer.string_of_term t);
        Typechecker.print_range stderr r;
        exit 1)
  | t -> eval t

and cat s f : tycomp terminal eval_env =
 fun context ->
  let target =
    let t = index context in
    match eval' (App (f, Const (Int t))) { index = 0; env = [] } with
    | VReturn (VInt t) -> t
    | _ -> raise (TypeError (T (At (s, f))))
  in
  reset (nth s target) context

and vthunk term : tyval terminal eval_env =
 fun context ->
  VThunk
    {
      last =
        { term; context; history = Vector.make 0 (VReturn VNil); state = SNil };
      offset = 0;
      kind = KRegular;
    }

and clet x t u : tycomp terminal eval_env =
  let* t' = eval' t in
  put x t' (Return t) (eval' u)

and creturn t : tycomp terminal eval_env =
  let* u = eval' t in
  return (VReturn u)

and capp t u : tycomp terminal eval_env =
  let* v = eval' u in
  let* f = eval' t in
  match f with
  | VFun (x, t, env) ->
      fun context ->
        eval' t { context with env = (x, ref (v, Return u)) :: env }
  | _ -> raise (TypeError (T t))

and cforce : tyval term -> tycomp terminal eval_env =
 fun t c ->
  let t' = eval' t c in
  let t' =
    match t' with
    | VThunk t -> super_force (1 + t.offset) t
    | _ -> raise (TypeError (T (Force t)))
  in
  if t'.kind = KStream then
    VReturn
      (vfby
         (open_vreturn (Vector.get t'.last.history t'.offset))
         (VThunk { t' with offset = t'.offset + 1 }))
  else Vector.get t'.last.history 0

and super_force n t =
  match t.last.state with
  | SInProgress k when n < k -> t
  | SInProgress k when n >= k ->
      if t.kind = KStream then raise BlackHole
      else (
        t.last.state <- SDone 0;
        step_force t;
        t)
  | SDone k when n <= k -> t
  | (SDone _ | SNil) when n > 0 ->
      step_force t;
      t.last.context <- { t.last.context with index = t.last.context.index + 1 };
      super_force n t
  | _ -> failwith "Out of fuel"

and step_force t =
  let old_fuel = set_in_progress t in
  match (eval' t.last.term) t.last.context with
  | VReturn (VFby (h, VThunk u)) ->
      Vector.push t.last.history (VReturn h);
      t.last.context <- { t.last.context with env = u.last.context.env };
      t.last.term <- u.last.term;
      t.last.state <- SDone (old_fuel + 1);
      t.kind <- KStream
  | res ->
      Vector.push t.last.history res;
      t.last.state <- SDone max_int;
      t.kind <- KRegular

and set_in_progress t =
  match t.last.state with
  | SNil ->
      t.last.state <- SInProgress 1;
      0
  | SDone k ->
      t.last.state <- SInProgress (k + 1);
      k
  | SInProgress _ -> raise BlackHole

and get_history t n = Vector.get t.last.history (n + t.offset)

and nth s n context =
  let v = eval' (Force s) context in
  match v with
  | VReturn (VFby (a, _)) when n = 0 -> VReturn a
  | VReturn (VFby (_, VThunk t)) ->
      let t' = super_force (n + t.offset) t in
      get_history t' (n - 1)
  | _ -> failwith "Ill type at"

and open_vreturn v = match v with VReturn v -> v | _ -> assert false

and cto t x u : tycomp terminal eval_env =
  let* t' = eval' t in
  match t' with
  | VReturn v -> put x v t (eval' u)
  | _ -> raise (TypeError (T t))

and crec x t : tycomp terminal eval_env =
 fun context ->
  let env = context.env in
  let env = (x, ref (VUnit, t)) :: env in
  let rect = vthunk t { context with env } in
  List.assoc x env := (rect, t);
  eval' (Force (Var x)) { context with env }

and cmurecord bindings : tycomp terminal eval_env =
 fun context ->
  let env = context.env in
  let env =
    List.fold_left
      (fun env (id, _, t) -> (id, ref (VUnit, t)) :: env)
      env bindings
  in
  List.iter
    (fun (x, _, t) ->
      let rect = vthunk t { context with env } in
      List.assoc x env := (rect, t))
    bindings;
  VRecord
    (List.map
       (fun (x, _, _) -> (x, eval' (Force (Var x)) { context with env }))
       bindings)

and crecordfield record id : tycomp terminal eval_env =
  let* r = eval' record in
  let record =
    match r with
    | VRecord record -> record
    | _ -> failwith "Ill typed record field"
  in
  match List.assoc_opt id record with
  | Some field -> return field
  | None -> failwith (Printf.sprintf "Field %s is not defined\n" id)

and cite c x y : tycomp terminal eval_env =
  let* c' = eval' c in
  match c' with
  | VBool t -> if t then eval' x else eval' y
  | _ -> raise (TypeError (T c))

and vconst t : tyval terminal eval_env =
  match t with
  | Unit -> return vunit
  | Bool b -> return (vbool b)
  | Int i -> return (vint i)
  | Nil -> return (VList [])

and vproj i t : tyval terminal eval_env =
  let* p = eval' t in
  match (i, p) with
  | true, VPair (x, _) -> return x
  | false, VPair (_, y) -> return y
  | _ -> raise (TypeError (T t))

and vhd t : tycomp terminal eval_env =
  let* t' = eval' t in
  match t' with
  | VFby (x, _) -> return (VReturn x)
  | VList (x :: _) -> return (VReturn x)
  | VList [] -> failwith "Exception: Failure \"list_hd\""
  | _ ->
      Printer.string_of_terminal t' |> print_endline |> flush_all;
      raise (TypeError (T t))

and vtl t : tycomp terminal eval_env =
  let* t' = eval' t in
  match t' with
  | VFby (_, xs) -> return (VReturn xs)
  | VList (_ :: _ as xs) -> return (VReturn (VList (List.tl xs)))
  | _ ->
      Printer.string_of_terminal t' |> print_endline |> flush_all;
      raise (TypeError (T t))

and vbin_arith op x y : tyval terminal eval_env =
  let* lhs = eval' x in
  let* rhs = eval' y in
  match (lhs, rhs) with
  | VInt x, VInt y -> return (vint (op x y))
  | _, _ -> raise (TypeError (T x))

and vbin_bool op x y : tyval terminal eval_env =
  let* lhs = eval' x in
  let* rhs = eval' y in
  match (lhs, rhs) with
  | VBool x, VBool y -> return (vbool (op x y))
  | _, _ -> raise (TypeError (T x))

and veq x y : tyval terminal eval_env =
  let* lhs = eval' x in
  let* rhs = eval' y in
  return (VBool (lhs = rhs))

and vneq x y : tyval terminal eval_env =
  let* lhs = eval' x in
  let* rhs = eval' y in
  return (VBool (lhs <> rhs))

and vnot x : tyval terminal eval_env =
  let* u = eval' x in
  match u with
  | VBool v -> return (VBool (not v))
  | _ -> raise (TypeError (T x))

and cprint t u : tycomp terminal eval_env =
  let* x = eval' t in
  Printf.printf "%s" (Printer.string_of_terminal x);
  let r = eval' u in
  r

let run t : eenv -> tycomp terminal = fun e -> eval' t e
