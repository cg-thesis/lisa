open Syntax

let tbl = Hashtbl.create 26

let fresh_var (x : string) =
  match Hashtbl.find_opt tbl x with
  | Some x' ->
      incr x';
      x ^ string_of_int !x'
  | None ->
      Hashtbl.add tbl x (ref 1);
      x

let return_t v x = Fun (v, Some ty_int, Return x)

let bind :
    tycomp term -> tycomp term -> string -> htyp option -> string -> tycomp term
    =
 fun m f vm ty vindex ->
  Fun
    (vindex, Some ty_int, App (To (App (m, Var vindex), vm, ty, f), Var vindex))

(*  (|x: stream |) = fun i -> stream *)

(** mtail : U (int -> F ('a stream))
    rec nth xs i index : 'a stream -> int -> int -> 'a =
    if i = 0 then hd xs else
    tl xs to mtail in
    (force mtail) (index) to tail in
    (force nth) tail (pred i) (succ index)
   **)
let nth x n index =
  ((trec "nth"
      (tfunn
         [ ("xs", None); ("_i", Some ty_int); ("_index_", Some ty_int) ]
         (tif
            (t "_i" ==! tint 0)
            (thd (t "xs"))
            ((ttl (t "xs") =: "mtail")
               ((thd (t "xs") =: "head")
                  ((tforce (t "mtail") @@! (t "_index_" +! tint 1) =: "tail")
                     (((tforce (t "nth") @@! t "tail") @@! (t "_i" -! tint 1))
                     @@! (t "_index_" +! tint 1)))))))
   @@! x)
  @@! n)
  @@! index

(** rec take xs i index =
    if i = 0 then
      hd xs to h in return (cons h [])
    else
      hd xs to head
      tl xs to mtail in
      (force mtail) index to tail in
      (force take) tail (pred i) (succ index) to ttail in
      cons head ttail
take : 'a stream -> int -> int -> int list
i : int
hd : 'a stream -> F 'a
tl : 'a stream -> F (U (int -> F 'a stream)
xs : 'a stream
head : 'a
mtail : U (int -> F 'a stream)
index : int
tail : 'a stream
pred,succ : int -> int
ttail : int list
cons : 'a -> 'a list -> 'a list
**)

let take n x index =
  ((trec "take"
      (tfunn
         [ ("xs", None); ("i", Some ty_int); ("_index_", Some ty_int) ]
         (tif
            (t "i" ==! tint 0)
            ((thd (t "xs") =: "h") (treturn (tcons (t "h") tnil)))
            ((thd (t "xs") =: "head")
               ((ttl (t "xs") =: "mtail")
                  ((tforce (t "mtail") @@! t "_index_" =: "tail")
                     ((((tforce (t "take") @@! t "tail") @@! (t "i" -! tint 1))
                       @@! (t "_index_" +! tint 1)
                      =: "ttail")
                        (treturn (tcons (t "head") (t "ttail")))))))))
   @@! x)
  @@! n)
  @@! index

let ( >>= ) = bind
let ( let* ) = bind
let run_t t = App (t, tint 0)

let rec _translate_aux : type a. a term -> a term =
 fun term ->
  match term with
  | TRange (_, t) | TypedTerm (_, t) -> _translate_aux t
  | Const _ ->
      (* 〚 c 〛= c *)
      term
  | Prim (Hd v) ->
      let v = _translate_aux v in
      let vt = fresh_var "_index" in
      let vm = fresh_var "_m" in
      To (Prim (Hd v), vm, None, return_t vt (t vm))
  | Prim (Tl v) ->
      let v = _translate_aux v in
      let vt = fresh_var "_index" in
      let vm = fresh_var "_m" in
      To (Prim (Tl v), vm, None, return_t vt (t vm))
  | Prim (LHd v) ->
      let v = _translate_aux v in
      Prim (LHd v)
  | Prim (LTl v) ->
      let v = _translate_aux v in
      Prim (LTl v)
  (* FIXME : translate primitves *)
  | Prim (Print (v, m)) ->
      let v = _translate_aux v in
      let m = _translate_aux m in
      Prim (Print (v, m))
  | Prim (Fst v) ->
      let v = _translate_aux v in
      Prim (Fst v)
  | Prim (Snd v) ->
      let v = _translate_aux v in
      Prim (Snd v)
  | Prim (Plus (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Plus (v, w))
  | Prim (Minus (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Minus (v, w))
  | Prim (Div (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Div (v, w))
  | Prim (Mult (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Mult (v, w))
  | Prim (Eq (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Eq (v, w))
  | Prim (Neq (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Neq (v, w))
  | Prim (AndB (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (AndB (v, w))
  | Prim (OrB (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (OrB (v, w))
  | Prim (Not v) ->
      let v = _translate_aux v in
      Prim (Not v)
  | Prim (Cons (v, w)) ->
      let v = _translate_aux v in
      let w = _translate_aux w in
      Prim (Cons (v, w))
  | Var _ ->
      (* 〚 v 〛= v *)
      term
  (* | Fun (x, Some ty, term) -> *)
  (*     (\* 〚 λ x. M 〛 = λ x . 〚 M 〛*\) *)
  | Fun (x, ty, term) ->
      (* 〚 λ x. M 〛 = λ x . 〚 M 〛*)
      let vi = fresh_var "_index" in
      tfunn
        [ (vi, Some ty_int); (x, Option.map translate_ty ty) ]
        (_translate_aux term)
  | App (t1, t2) ->
      let vi = fresh_var "_index" in
      let t2 = _translate_aux t2 in
      let k c = tfun vi ~ty:ty_int (App (App (c, t2), Var vi)) in
      tail_app t1 vi k
  | Rec (x, _, term) -> Rec (x, None, _translate_aux term)
  | Pair (t1, t2) ->
      let t1 = _translate_aux t1 and t2 = _translate_aux t2 in
      Pair (t1, t2)
  | Let (x, ty, t1, t2) ->
      let t1 = _translate_aux t1 and t2 = _translate_aux t2 in
      Let (x, Option.map translate_ty ty, t1, t2)
  | To (Return v, x, ty, t2) ->
      (* 〚 M to x . in N 〛 = λ index. 〚 M 〛index to m in 〚 N 〛*)
      let vindex = fresh_var "_index" in
      let v = _translate_aux v and t2 = _translate_aux t2 in
      tfun vindex ~ty:ty_int (To (Return v, x, ty, App (t2, Var vindex)))
  | To (t1, x, ty, t2) ->
      (* 〚 M to x . in N 〛 = λ index. 〚 M 〛index to m in 〚 N 〛*)
      let vindex = fresh_var "_index" in
      let t1 = _translate_aux t1 and t2 = _translate_aux t2 in
      (t1 >>= t2) x (Option.map translate_ty ty) vindex
  | Force term ->
      (* 〚 force V 〛 = force 〚 V 〛*)
      Force (_translate_aux term)
  | Thunk term ->
      (* 〚 thunk M 〛 = thunk 〚 M 〛*)
      Thunk (_translate_aux term)
  | Return term ->
      (* 〚 return V 〛 = returnₜ〚 A 〛 *)
      let vt = fresh_var "_index" in
      return_t vt (_translate_aux term)
  | Fby (x, xs) ->
      (* 'a stream = 'a * U F ('a stream)
         〚 'a stream 〛 = 〚 'a * U F ('a stream) 〛 =
          'a * U T ('a stream)
          〚 x :: xs 〛 = 〚 x 〛 :: thunk (λ index. (force 〚 xs 〛) (index + 1))
      *)
      let vi = fresh_var "_index" in
      let x = _translate_aux x in
      let xs = _translate_aux xs in
      Fby (x, tthunk (tfun vi ~ty:ty_int (t vi |>! tforce xs)))
  | IfThenElse (c, t1, t2) ->
      let c = _translate_aux c
      and t1 = _translate_aux t1
      and t2 = _translate_aux t2 in
      IfThenElse (c, t1, t2)
  | At (x, t) ->
      let vi = fresh_var "_atindex" in
      let vo = fresh_var "_oindex" in
      let vs = fresh_var "_s" in
      Fun
        ( vi,
          Some ty_int,
          (tforce (_translate_aux x) @@! tint 0 =: vs)
            (To
               (App (t, Var vi), vo, Some ty_int, nth (Var vs) (Var vo) (tint 0)))
        )
  | MuRecord bindings ->
      MuRecord
        (List.map (fun (id, _, exp) -> (id, None, _translate_aux exp)) bindings)
  | RecordField (record, field) -> RecordField (_translate_aux record, field)

and tail_app :
    tycomp term -> string -> (tycomp term -> tycomp term) -> tycomp term =
 fun c vi k ->
  match c with
  | App (t1, t2) ->
      let t2 = _translate_aux t2 in
      let k' c = App (App (c, t2), Var vi) in
      k (tail_app t1 vi k')
  | Fun (x, _, term) -> k (Fun (x, None, _translate_aux term))
  | t -> k (App (_translate_aux t, Var vi))

and translate_ty ty = ty
(* match ty with *)
(* | TyApp (("stream", 1), [ param ]) -> ty_stream (translate_ty param) *)
(* | TyApp (("list", 1), [ param ]) -> ty_list (translate_ty param) *)
(* | TyApp (("U", 1), [ param ]) -> *)
(*     ty_thunk (ty_arrow ty_int (translate_ty param)) *)
(* | TyApp (("F", 1), [ param ]) -> ty_return (translate_ty param) *)
(* | TyApp (("->", 2), [ dom; codom ]) -> *)
(*     ty_arrow (translate_ty dom) (ty_arrow ty_int (translate_ty codom)) *)
(* | TyApp (("*", 2), [ left; right ]) -> *)
(*     ty_prod (translate_ty left) (translate_ty right) *)
(* | TyApp ((name, _), _) -> translate_by_name name *)
(* | _ -> assert false *)

and translate_by_name name =
  match name with
  | "int" -> ty_int
  | "bool" -> ty_bool
  | "unit" -> ty_unit
  | _ -> assert false

let translate x =
  Hashtbl.clear tbl;
  _translate_aux x
