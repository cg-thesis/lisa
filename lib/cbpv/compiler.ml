module CBPVToCBV = struct
  module S = Syntax
  module T = Libcbv.Syntax.Terms
  module TTypes = Libcbv.Syntax.Types
  include Subst

  let empty = []
  let bind_val env x v = List.cons (x, v) env
  let get env x = List.assoc x env

  type env = (string * S.tyval S.term) list

  let rec translate_ty : S.htyp -> TTypes.ty =
   fun ty ->
    match ty with
    | S.TyApp (("unit", 0), []) -> TTypes.TyUnit
    | S.TyApp (("int", 0), []) -> TTypes.TyInt
    | S.TyApp (("index", 0), []) -> TTypes.TyInt
    | S.TyApp (("bool", 0), []) -> TTypes.TyBool
    | S.TyApp (("->", 2), [ a; b ]) ->
        TTypes.TyArrow ([ translate_ty a ], translate_ty b)
    | S.TyApp (("*", 2), [ a; b ]) ->
        TTypes.TyProd (translate_ty a, translate_ty b)
    | S.TyApp (("F", 1), [ a ]) -> translate_ty a
    | S.TyApp (("U", 1), [ a ]) -> TTypes.(TyArrow ([ TyUnit ], translate_ty a))
    | S.TyApp (("stream", 1), [ ty ]) -> TTypes.TyStream (translate_ty ty)
    | _ -> failwith "CBPV type cannot be tranlsated. "

  let rec cbpv_val_to_cbv (v : S.tyval S.term) : T.term =
    match v with
    | S.Var x -> T.Var x
    | S.Const (Int i) -> T.Lit (Int i)
    | S.Const (Bool b) -> T.Lit (Bool b)
    | S.Const Nil -> T.List []
    | S.Const Unit -> T.Lit Unit
    | S.Pair (a, b) -> T.Pair (cbpv_val_to_cbv a, cbpv_val_to_cbv b)
    | S.Thunk c -> T.(Lam ("u", TTypes.TyUnit, cbpv_comp_to_cbv c))
    | S.TypedTerm (_, t) ->
        let t = cbpv_val_to_cbv t in
        t
    | S.TRange (_, t) -> cbpv_val_to_cbv t
    | S.Fby (a, t) -> T.(Stream (cbpv_val_to_cbv a, cbpv_val_to_cbv t))
    | S.Prim (Fst t) -> T.Fst (cbpv_val_to_cbv t)
    | S.Prim (Snd t) -> T.Snd (cbpv_val_to_cbv t)
    | S.Prim (Plus (x, y)) ->
        T.BinArith (cbpv_val_to_cbv x, Add, cbpv_val_to_cbv y)
    | S.Prim (Minus (x, y)) ->
        T.BinArith (cbpv_val_to_cbv x, Minus, cbpv_val_to_cbv y)
    | S.Prim (Mult (x, y)) ->
        T.BinArith (cbpv_val_to_cbv x, Mult, cbpv_val_to_cbv y)
    | S.Prim (Div (x, y)) ->
        T.BinArith (cbpv_val_to_cbv x, Div, cbpv_val_to_cbv y)
    | S.Prim (Eq (x, y)) -> T.BinBool (cbpv_val_to_cbv x, Eq, cbpv_val_to_cbv y)
    | S.Prim (Neq (x, y)) ->
        T.BinBool (cbpv_val_to_cbv x, NEq, cbpv_val_to_cbv y)
    | S.Prim (AndB (x, y)) ->
        T.BinBool (cbpv_val_to_cbv x, And, cbpv_val_to_cbv y)
    | S.Prim (OrB (x, y)) -> T.BinBool (cbpv_val_to_cbv x, Or, cbpv_val_to_cbv y)
    | S.Prim (Cons (x, y)) -> T.Cons (cbpv_val_to_cbv x, cbpv_val_to_cbv y)
    | S.Prim (Not x) -> T.Not (cbpv_val_to_cbv x)

  and cbpv_comp_to_cbv (t : S.tycomp S.term) : T.term =
    match t with
    | S.TRange (_, t) -> cbpv_comp_to_cbv t
    | S.Fun (x, Some ty, b) -> T.Lam (x, translate_ty ty, cbpv_comp_to_cbv b)
    | S.App (a, b) -> T.App (cbpv_comp_to_cbv a, cbpv_val_to_cbv b)
    | S.Let (id, _, TypedTerm (_, (Const _ as b)), c) ->
        let c = subst id b c in
        cbpv_comp_to_cbv c
    | S.Let (id, Some ty, b, c) ->
        let b = cbpv_val_to_cbv b in
        let c = cbpv_comp_to_cbv c in
        let ty = translate_ty ty in
        T.(Let (id, ty, b, c))
    | S.Rec (id, Some ty, b) ->
        let b = cbpv_comp_to_cbv b in
        let ty = translate_ty ty in
        T.(
          LetRec
            ( id,
              TTypes.(TyArrow ([ TyUnit ], ty)),
              T.Lam ("u", TyUnit, b),
              App (Var id, Lit Unit) ))
    | S.To (a, id, Some ty, b) ->
        T.Let (id, translate_ty ty, cbpv_comp_to_cbv a, cbpv_comp_to_cbv b)
    | S.Force a -> T.App (cbpv_val_to_cbv a, Lit Unit)
    | S.Return a -> cbpv_val_to_cbv a
    | S.IfThenElse (c, bt, bf) ->
        T.If (cbpv_val_to_cbv c, cbpv_comp_to_cbv bt, cbpv_comp_to_cbv bf)
    | S.Prim (Hd x) -> T.Head (cbpv_val_to_cbv x)
    | S.Prim (Tl x) -> T.Tail (cbpv_val_to_cbv x)
    | S.Prim (LHd _)
    | S.Prim (LTl _)
    | S.Prim (Print _)
    | S.MuRecord _ | S.RecordField _ ->
        failwith "NYI"
    | S.At _ -> failwith "Unssuported term"
    | S.TypedTerm (_, t) ->
        let t = cbpv_comp_to_cbv t in
        t
    | S.Let _ -> failwith "Untyped term not translatable let"
    | S.Fun _ -> failwith "Untyped term not translatable fun"
    | S.To _ -> failwith "Untyped term not translatable to"
    | S.Rec _ -> failwith "Untyped term not translatable rec"
end

let print = Libtransform.ANFToMimi.string_of_mimi
let print_cbv = Libcbv.(IO.document_to_string Printer.print_term)
let run term = Syntax.(tint 0 |>! term)

let compile cbpv_term =
  let monadic_term = Translate.translate cbpv_term in
  let explicitly_typed_term = Typechecker.mcheck (run monadic_term) in
  let cbv_term = CBPVToCBV.cbpv_comp_to_cbv explicitly_typed_term in
  let open Libtransform in
  let mimi_term = CBVToMimi.translate cbv_term in
  Libcbv.IO.document_to_string Libmimi__Printer.print_term mimi_term

let eval p =
  let monadic = Translate.translate p in
  Printer.string_of_term monadic |> print_endline;
  let typed = Typechecker.mcheck Syntax.(tint 0 |>! monadic) in
  let cbv = CBPVToCBV.cbpv_comp_to_cbv typed in
  let open Libtransform in
  let open CBVToMimi in
  let p = translate cbv in
  Libmimi.Printer.string_of_mimi p |> print_endline;
  mimi_eval cbv |> fst
