%{ (* Emacs, open this with -*- tuareg -*- *)
   open Syntax

   let parsing_error pos msg =
     Printf.eprintf "%s:\n %s\n" (Utils.Position.string_of_pos pos) msg;
     exit 1

   type binop =
     | PPlus
     | PMinus
     | PMult
     | PDiv
     | PEq
     | PNeq
     | PAndB
     | POrB

   let make_binop op lhs rhs =
     match op with
     | PPlus  ->
    Plus (lhs, rhs)
     | PMinus ->
    Minus (lhs, rhs)
     | PMult ->
    Mult (lhs, rhs)
     | PDiv ->
    Div (lhs, rhs)
     | PEq ->
    Eq (lhs, rhs)
     | PNeq ->
    Neq (lhs, rhs)
     | PAndB ->
    AndB (lhs, rhs)
     | POrB ->
    OrB (lhs, rhs)

   let make_list xs =
     match xs with
     | [] -> Const Nil
     | _ ->
    List.fold_right (fun x acc -> Prim (Cons (x, acc))) xs (Const Nil)
%}


%token EOF
%token LET
%token REC
%token THUNK
%token FORCE
%token RETURN
%token FUN
%token FST
%token SND
%token HD
%token TL
%token LHD
%token LTL
%token NOT
%token IF
%token THEN
%token ELSE
%token EQUAL
%token NEQUAL
%token CONS
%token AT
%token ARROW
%token COMMA
%token DOT
%token LPAREN
%token RPAREN
%token LBRACE
%token RBRACE
%token PLUS
%token MINUS
%token MULT
%token DIV
%token LAND
%token LOR
%token NIL
%token LCONS
%token PRINT
%token SEMI_COLON
%token COLON
%token LBRACKET
%token RBRACKET
%token UNIT
%token UNIT_TY
%token INT_TY
%token BOOL_TY
%token LIST_TY
%token STREAM_TY
%token RETURN_TY
%token THUNK_TY
%token <bool> TRUE
%token <bool> FALSE
%token <string> ID
%token <int> INT
%token IN TO

%right CONS
%nonassoc EQUAL NEQUAL LAND LOR
%left PLUS MINUS
%left MULT DIV
%start <Syntax.tycomp Syntax.term> program
%%
program:
  | c=located(computation) EOF { c }
  | error
    {
      parsing_error (Utils.Position.lex_join $startpos $endpos) "Syntax error."
    }

computation:
  |  bc=located(simple_computation) TO b=binding IN c=located(computation)
    { To (bc, fst b, snd b, c) }
  | REC b=binding EQUAL c=located(computation)
    { Rec(fst b,snd b,c) }
  | LET b=binding EQUAL v=located(value) IN c=located(computation)
    { Let(fst b,snd b, v,c) }
  | t=func { t }
  | IF c=located(value) THEN bt=located(computation) ELSE bf=located(computation)
    { IfThenElse(c,bt,bf) }
  | lhs=located(simple_value) AT rhs=located(computation)
    { At(lhs,rhs) }
  | PRINT v=located(value) SEMI_COLON c=located(computation)
    { Prim (Print(v,c)) }
  | c=simple_computation { c }

%inline def:
  |  b=binding EQUAL c=located(computation)
     { (fst b, snd b, c) }

simple_computation:
  | c=located(simple_computation) v=located(atomic_value)
    { App (c, v) }
  | FORCE v=located(atomic_value)
    { Force v }
  | RETURN v=located(atomic_value)
    { Return v }
  | HD v=located(atomic_value)
    { Prim (Hd v) }
  | TL v=located(atomic_value)
    { Prim (Tl v) }
  | LHD v=located(atomic_value)
    { Prim (LHd v) }
  | LTL v=located(atomic_value)
    { Prim (LTl v) }
  | LBRACE cs = separated_nonempty_list(SEMI_COLON, def )  RBRACE
    { MuRecord cs }
  | c=located(simple_computation) DOT i=ID
    { RecordField(c,i) }
  | LPAREN c=computation RPAREN { c }

value:
  | lhs=located(value) CONS rhs=located(value)
    { Fby(lhs,rhs) }
  | lhs=located(value) op=bin_op rhs=located(value)
    { Prim (make_binop op lhs rhs) }
  | LPAREN lhs=located(value) COMMA rhs=located(value) RPAREN
    { Pair (lhs, rhs) }
  | LCONS lhs=located(atomic_value) rhs=located(atomic_value)
    { Prim (Cons (lhs, rhs)) }
  | v=simple_value { v }

simple_value:
  | THUNK c=located(simple_computation)
    { Thunk c }
  | NOT v=located(atomic_value)
    { Prim (Not v) }
  | FST v=located(atomic_value)
    { Prim (Fst v) }
  | SND v=located(atomic_value)
    { Prim (Snd v) }
  | v=atomic_value { v }

atomic_value:
  | v=located(literal_or_id)
  | LPAREN v=value RPAREN { v }
  | xs=tlist
    {
      make_list xs
    }

literal_or_id:
  | i=ID { Var i }
  | NIL  { Const Nil }
  | UNIT { Const Unit }
  | b=TRUE | b=FALSE { Const (Bool b) }
  | i=INT { Const (Int i) }

%inline func:
  | FUN b=binding ARROW c=located(computation) { Fun (fst b, snd b, c) }

%inline binding:
  | LPAREN i=ID COLON t=typ RPAREN { (i,Some t) }
  | i=ID { (i,None) }

%inline bin_op:
  | PLUS   { PPlus   }
  | MINUS  { PMinus  }
  | MULT   { PMult  }
  | DIV    { PDiv    }
  | EQUAL  { PEq     }
  | NEQUAL { PNeq    }
  | LAND   { PAndB   }
  | LOR    { POrB    }

%inline tlist:
  | LBRACKET xs=separated_list (SEMI_COLON, located(value)) RBRACKET { xs }

%inline located(X): x=X {
  TRange (($startpos, $endpos), x )
}


typ: x=type_constant
{
  x
}
| lhs=type_constant ARROW rhs=typ
{
  ty_arrow lhs rhs
}
| lhs=type_constant MULT rhs=typ
{
  ty_prod lhs rhs
}
| THUNK_TY  t=typ
{
  ty_thunk t
}
| RETURN_TY  t=typ
{
  ty_return t
}
| LIST_TY t=typ
{
  ty_list t
}
| STREAM_TY t=typ
{
  ty_stream t
}

type_constant:
INT_TY
{
 ty_int
}
| BOOL_TY
{
 ty_bool
}
| UNIT_TY
{
 ty_unit
}
| LPAREN t=typ RPAREN
{
  t
}
