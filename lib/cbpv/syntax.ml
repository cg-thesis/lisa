type range = Lexing.position * Lexing.position

type tykon = string * int
and htyp = TyVar of string | TyApp of tykon * htyp list

type _ term =
  | TRange : range * 'a term -> 'a term
  | TypedTerm : htyp * 'a term -> 'a term
  | Const : const -> tyval term
  | Prim : 'a prim -> 'a term
  | Var : string -> tyval term
  | Fun : string * htyp option * tycomp term -> tycomp term
  | App : tycomp term * tyval term -> tycomp term
  | Rec : string * htyp option * tycomp term -> tycomp term
  | MuRecord : (string * htyp option * tycomp term) list -> tycomp term
  | RecordField : tycomp term * string -> tycomp term
  | Pair : tyval term * tyval term -> tyval term
  | Let : string * htyp option * tyval term * tycomp term -> tycomp term
  | To : tycomp term * string * htyp option * tycomp term -> tycomp term
  | Force : tyval term -> tycomp term
  | Thunk : tycomp term -> tyval term
  | Return : tyval term -> tycomp term
  | Fby : tyval term * tyval term -> tyval term
  | IfThenElse : tyval term * tycomp term * tycomp term -> tycomp term
  | At : tyval term * tycomp term -> tycomp term

and tycomp = TyComp
and tyval = TyVal
and const = Unit | Bool of bool | Int of int | Nil

and _ prim =
  | Print : tyval term * tycomp term -> tycomp prim
  | Not : tyval term -> tyval prim
  | Fst : tyval term -> tyval prim
  | Snd : tyval term -> tyval prim
  | Plus : tyval term * tyval term -> tyval prim
  | Minus : tyval term * tyval term -> tyval prim
  | Mult : tyval term * tyval term -> tyval prim
  | Div : tyval term * tyval term -> tyval prim
  | Eq : tyval term * tyval term -> tyval prim
  | Neq : tyval term * tyval term -> tyval prim
  | AndB : tyval term * tyval term -> tyval prim
  | OrB : tyval term * tyval term -> tyval prim
  | Cons : tyval term * tyval term -> tyval prim
  | Hd : tyval term -> tycomp prim
  | Tl : tyval term -> tycomp prim
  | LHd : tyval term -> tycomp prim
  | LTl : tyval term -> tycomp prim

and _ terminal =
  | VUnit : tyval terminal
  | VNil : tyval terminal
  | VBool : bool -> tyval terminal
  | VInt : int -> tyval terminal
  | VPair : tyval terminal * tyval terminal -> tyval terminal
  | VList : tyval terminal list -> tyval terminal
  | VFby : tyval terminal * tyval terminal -> tyval terminal
  | VThunk : thunk -> tyval terminal
  | VFun : string * tycomp term * env -> tycomp terminal
  | VReturn : tyval terminal -> tycomp terminal
  | VRecord : (string * tycomp terminal) list -> tycomp terminal

and thunk = { last : last; offset : int; mutable kind : kind }

and last = {
  mutable context : eenv;
  history : tycomp terminal Vector.t;
  mutable state : state;
  mutable term : tycomp term;
}

and kind = KRegular | KStream
and state = SInProgress of int | SDone of int | SNil
and env = (string * (tyval terminal * tycomp term) ref) list
and eenv = { index : int; env : env }

exception BlackHole

let vint x = VInt x
let vbool x = VBool x
let vunit = VUnit
let vnil = VNil
let vpair x y = VPair (x, y)

let vcons (x : tyval terminal) (y : tyval terminal) : tyval terminal =
  match y with
  | VList xs -> VList (x :: xs)
  | VNil -> VList [ x ]
  | _ -> assert false

let vfby x y = VFby (x, y)
let tfst x = Prim (Fst x)
let tsnd x = Prim (Snd x)
let tprint t u = Prim (Print (t, u))
let tlet x ?ty t u = Let (x, ty, t, u)
let ( =: ) t x ?ty u = To (t, x, ty, u)
let ( |>! ) u t = App (t, u)
let ( @@! ) t u = App (t, u)
let trec x ?ty t = Rec (x, ty, t)
let tthunk t = Thunk t
let tfun x ?ty t = Fun (x, ty, t)
let tforce t = Force t
let treturn t = Return t
let t x = Var x
let tyapp name n params = TyApp ((name, n), params)
let ty_int = tyapp "int" 0 []
let ty_bool = tyapp "bool" 0 []
let ty_unit = tyapp "unit" 0 []
let ty_index = tyapp "index" 0 []
let ty_stream p = tyapp "stream" 1 [ p ]
let ty_list p = tyapp "list" 1 [ p ]
let stream_int = ty_stream ty_int
let stream_bool = ty_stream ty_bool
let ty_thunk p = tyapp "U" 1 [ p ]
let ty_return p = tyapp "F" 1 [ p ]
let ty_arrow ty1 ty2 = tyapp "->" 2 [ ty1; ty2 ]
let ty_prod ty1 ty2 = tyapp "*" 2 [ ty1; ty2 ]
let ( +! ) x t = Prim (Plus (x, t))
let ( *! ) x t = Prim (Mult (x, t))
let ( -! ) x t = Prim (Minus (x, t))
let ( /! ) x t = Prim (Div (x, t))
let ( ==! ) x y = Prim (Eq (x, y))
let ( <>! ) x y = Prim (Neq (x, y))
let ( &&! ) x y = Prim (AndB (x, y))
let ( ||! ) x y = Prim (OrB (x, y))
let tnot x = Prim (Not x)
let tunit = Const Unit
let tint x = Const (Int x)
let ttrue = Const (Bool true)
let tfalse = Const (Bool false)
let tprod x y = Pair (x, y)
let ( *: ) x y = Fby (x, y)
let tcons x y = Prim (Cons (x, y))
let tnil = Const Nil
let tif c x y = IfThenElse (c, x, y)
let thd x = Prim (Hd x)
let ttl x = Prim (Tl x)
let tlhd x = Prim (LHd x)
let tltl x = Prim (LTl x)
let ( @! ) x y = At (x, y)

let tfunn xs t =
  List.fold_right
    (fun (x, ty) t -> match ty with Some ty -> tfun x ~ty t | _ -> tfun x t)
    xs t

(** This term implements the following CBPV program :
      print "hello"^0;
      let x = 3 in
      let y = thunk (fun z ->
                      print "hello"^1;
                      print "hello"^43;
                      return x + z) in
      print "hello"^2;
      (print "hello"^3;
       7`(print "hello"^42;
          force y) to w.
      print "hello"^w;
      return (w+5)

      This program is a copy of Paul Blain's example in which
      print "just pushed 7" is replaced by print "hello"^42 and
      print "just popped "^z is replace by print "hello"^43
 *)
let example =
  tprint (tint 0)
    (tlet "x" (tint 3)
       (tlet "y"
          (tthunk
             (tfun "z"
                (tprint (tint 1) (tprint (tint 43) (treturn (t "x" +! t "z"))))))
          (tprint (tint 2)
             ((tprint (tint 3) (tint 7 |>! tprint (tint 42) (tforce (t "y")))
              =: "w")
                (tprint (t "w") (treturn (t "w" +! tint 5)))))))

(**
      This term implements the following CBPV program :
      0`(rec printsucc =
          fun x -> return x + 1 to y.
            print ("hello"^y);
            (return y)` force printsucc)
 *)
let example2 =
  tint 0
  |>! trec "printsucc"
        (tfun "x"
           ((treturn (t "x" +! tint 1) =: "y")
              (tprint (t "y") (t "y" |>! tforce (t "printsucc")))))

let example_stream = trec "ones" (treturn (tint 1 *: t "ones"))

(** This term implements the followin CBPV program :
    5` (rec fact = fun x ->
       if x = 1 then return 1 else
         return x-1 to y.
         return (y` (force fact) to z.
         return x * z))
 *)
let fact =
  tint 5
  |>! trec "fact"
        (tfun "x"
           (tif
              (t "x" ==! tint 0)
              (treturn (tint 1))
              ((treturn (t "x" -! tint 1) =: "y")
                 ((t "y" |>! tforce (t "fact") =: "z")
                    (treturn (t "x" *! t "z"))))))

(** This term implements the following CBPV program:
      (rec maps = fun f -> fun xs ->
        (hd (force xs))` (force f) to y in
        let ys = thunk ((tl (force xs))` (f`(force map))) in
        y *: ys)
       rec nat =
         let nats = thunk (nat` ((thunk (fun x -> x+1))` map) in
         (0 *: nats)
 **)
let map =
  trec "maps"
    (tfun "f"
       (tfun "xs"
          (((thd (t "xs") =: "head") (t "head" |>! tforce (t "f")) =: "y")
             (tlet "ys"
                (tthunk
                   ((ttl (t "xs") =: "tail")
                      ((tforce (t "tail") =: "t")
                         (t "t" |>! (t "f" |>! tforce (t "maps"))))))
                (treturn (t "y" *: t "ys"))))))

(** This term implement the following CBPV program:
      rec nth = fun xs -> fun i ->
      if i = 0 then return (hd (force xs))
      else ((tl (force xs))` (i-1)` (force nth))
 **)
let nth =
  tint 5
  |>! (trec "ones" (treturn (tint 1 *: t "ones")) =: "s")
        (t "s"
        |>! trec "nth"
              (tfun "xs"
                 (tfun "i"
                    (tif
                       (t "i" ==! tint 0)
                       (thd (t "xs"))
                       (t "i" -! tint 1
                       |>! (ttl (t "xs") =: "t")
                             ((tforce (t "t") =: "tt")
                                (t "tt" |>! tforce (t "nth"))))))))

(**
   { x = return 1 :: x ; y = return 0::thunk (return x@(fun x -> x)::y) }
 **)
let trivial_obs =
  RecordField
    ( MuRecord
        [
          ("inpt", None, treturn (tint 1 *: t "inpt"));
          ( "obs",
            None,
            (t "inpt" @! tfun "a" (treturn (t "a")) =: "m")
              (treturn
                 (tint 0 *: tthunk (treturn ((t "m" +! tint 1) *: t "obs")))) );
        ],
      "obs" )

(**
     rec nat =
        let s =
            thunk (rec next =
              force nat to nats.
              nats @ pred to pre.
              return (pre + 1)) : next
        in retrun 0 *: s

is to be translated to
    {
    nat = fun t ->
    return 0: thunk (rec next = return nat@pred  + 1 :next)
    }
     rec nat =
         fun time ->
         let s =
             thunk (rec next =
                     fun time ->
                     force nats to nat.
                       time` pred to i.
                         nats` pred` nth to pre.
                               return
                                 (succ pre : thunk (fun time -> time +1` force next))
         in
         return 0 :  thunk (fun time -> time + 1` (force s))
 **)
let nat_obs =
  trec "nat"
    ~ty:(ty_thunk (ty_stream ty_int))
    (treturn
       (tint 0
       *: tthunk
            (trec "next"
               ~ty:(ty_thunk (ty_stream ty_int))
               ((TypedTerm (ty_thunk (ty_stream ty_int), t "nat")
                 @! tfun "i" (treturn (t "i" -! tint 1))
                =: "v")
                  ~ty:ty_int
                  (treturn
                     ((t "v" +! tint 1)
                     *: TypedTerm (ty_thunk (ty_stream ty_int), t "next")))))))

(*
 rec nat.
     return 0 :: thunk
      (rec next. force nat to n in n @ pred to v in
         return succ v::next)
 *)

let ones =
  (* rec x. return 1:: x *)
  trec "x" (treturn (tint 1 *: t "x"))

let trivial =
  (* rec x. return (0 :: thunk (return 1 :: thunk (return 2:: x))) *)
  trec "x"
    (treturn
       (tint 0
       *: tthunk (treturn (tint 1 *: tthunk (treturn (tint 2 *: t "x"))))))

let obs =
  (* rec {from = fun x-> return x:: thunk (force from) (x+1);
          nat  = (force from) 0 } *)
  RecordField
    ( MuRecord
        [
          ( "from",
            None,
            tfun "x"
              (treturn
                 (t "x" *: tthunk (tforce (t "from") @@! (t "x" +! tint 1)))) );
          ("nat", None, tforce (t "from") @@! tint 0);
          ( "y",
            None,
            treturn
              (tint 1
              *: tthunk
                   ((tforce (t "nat") =: "n")
                      ((t "n" @! tfun "x" (treturn (t "x" /! tint 2)) =: "a")
                         (treturn (t "a" *: t "y"))))) );
        ],
      "y" )

(* trec "nat"
 *   (trec "succs"
 *      (((tforce (t"nat"))=:"s")
 *            ( (((t"s") @! (tfun "time"(treturn ((t"time")-! (tint 1)))))=:"pred")
 *                ((treturn (((tint 1)+!(t"pred")) *: (t"succs"))=:"ss")
 *                              (treturn ((tint 0)*: t"ss")))))) *)
let nats =
  tint 8
  |>! (trec "nat"
         (treturn
            (tint 0
            *: tthunk
                 ((tforce (t "nat") =: "s")
                    (t "s"
                    |>! (tthunk (tfun "x" (treturn (t "x" +! tint 1)))
                        |>! trec "maps"
                              (tfun "f"
                                 (tfun "xs"
                                    (((thd (t "xs") =: "head")
                                        (t "head" |>! tforce (t "f"))
                                     =: "y")
                                       (tlet "ys"
                                          (tthunk
                                             ((ttl (t "xs") =: "tail")
                                                ((tforce (t "tail") =: "t")
                                                   (t "t"
                                                   |>! (t "f"
                                                       |>! tforce (t "maps"))))))
                                          (treturn (t "y" *: t "ys")))))))))))
      =: "nats")
        (t "nats"
        |>! trec "nth"
              (tfun "xs"
                 (tfun "i"
                    (tif
                       (t "i" ==! tint 0)
                       (thd (t "xs"))
                       (t "i" -! tint 1
                       |>! (ttl (t "xs") =: "tail")
                             ((tforce (t "tail") =: "t")
                                (t "t" |>! tforce (t "nth"))))))))

let mut_obs =
  (RecordField
     ( MuRecord
         [
           ( "x",
             None,
             treturn
               (ttrue
               *: tthunk
                    ((tforce (t "y") =: "s")
                       ((t "s" @! tfun "i" (treturn (t "i" -! tint 1)) =: "v")
                          (treturn (t "v" *: t "x"))))) );
           ( "y",
             None,
             treturn
               (tfalse
               *: tthunk
                    ((tforce (t "x") =: "s")
                       ((t "s" @! tfun "i" (treturn (t "i" -! tint 1)) =: "v")
                          (treturn (t "v" *: t "y"))))) );
         ],
       "x" )
  =: "s")
    (t "s" @! tfun "" (treturn (tint 5)))

let nat_8 =
  (nat_obs =: "s") (tthunk (treturn (t "s")) @! tfun "" (treturn (tint 8)))

let ret1 = treturn (tint 1)
