{ (* Emacs, open this with -*- tuareg -*- *)
  open Parser

let enter_newline lexbuf =
  Utils.Input.newline lexbuf;
  lexbuf

exception SyntaxError of string
}

let newline = ('\013' | "\013\010" | '\010')
let whitespace = [ '\032' '\009' ]

let alpha = ['a'-'z''A'-'Z']

let digit = ['0'-'9']

let sym  = ['_']

let ident = alpha (alpha | digit | sym)*

rule token = parse
| newline     { enter_newline lexbuf |> token }
| whitespace  { token lexbuf          }
| digit+ as d { INT (int_of_string d) }
| "nil"       { NIL                   }
| "cons"      { LCONS                 }
| "true"      { TRUE true             }
| "false"     { FALSE false           }
| "="         { EQUAL                 }
| "<>"        { NEQUAL                }
| "let"       { LET                   }
| "fun"       { FUN                   }
| "rec"       { REC                   }
| "force"     { FORCE                 }
| "thunk"     { THUNK                 }
| "return"    { RETURN                }
| "if"        { IF                    }
| "then"      { THEN                  }
| "else"      { ELSE                  }
| "in"        { IN                    }
| "print"     { PRINT                 }
| "fst"       { FST                   }
| "snd"       { SND                   }
| "::"        { CONS                  }
| "@"         { AT                    }
| "=:"        { TO                    }
| "->"        { ARROW                 }
| ","         { COMMA                 }
| "."         { DOT                   }
| "("         { LPAREN                }
| ")"         { RPAREN                }
| ";"         { SEMI_COLON            }
| ":"         { COLON                 }
| "["         { LBRACKET              }
| "]"         { RBRACKET              }
| "{"         { LBRACE                }
| "}"         { RBRACE                }
| "not"       { NOT                   }
| "hd"        { HD                    }
| "tl"        { TL                    }
| "list_hd"   { LHD                   }
| "list_tl"   { LTL                   }
| "+"         { PLUS                  }
| "-"         { MINUS                 }
| "*"         { MULT                  }
| "/"         { DIV                   }
| "&&"        { LAND                  }
| "||"        { LOR                   }
| "()"        { UNIT_TY               }
| "bool"      { BOOL_TY               }
| "int"       { INT_TY                }
| "unit"      { UNIT_TY               }
| "list"      { LIST_TY               }
| "stream"    { STREAM_TY             }
| "U"         { THUNK_TY              }
| "F"         { RETURN_TY             }
| ident as id { ID id                 }
| eof         { EOF                   }
| _
      { raise (SyntaxError ("Syntax Error: " ^ Lexing.lexeme lexbuf))}
