%{ (* Emacs, open this with -*- tuareg -*- *)
   open Syntax

   type binop =
     | PPlus
     | PMinus
     | PTimes
     | PDiv

   let make_binop lhs op rhs =
     match op with
     | PPlus  ->
        Plus (lhs, rhs)
     | PMinus ->
        Minus (lhs, rhs)
     | PTimes ->
        Times (lhs, rhs)
     | PDiv ->
        Div (lhs, rhs)
%}

%token ARROW
%token FBY
%token PLUS
%token MINUS
%token DIV
%token MULT
%token EQUAL
%token SEMICOLON
%token LPAREN
%token RPAREN
%token <int> INT
%token <bool> TRUE
%token <bool> FALSE
%token <string> ID
%token EOF

%right FBY
%right ARROW
%left PLUS MINUS
%left MULT DIV

%start <Syntax.program> program

%%
program:
  | es=equation* EOF
    { es }
  | error
    { failwith "bug" }

%inline equation:
  | i=ID EQUAL t=expression SEMICOLON
    { Def(i, t) }

expression:
  | lhs = expression ARROW rhs = expression
    { Arrow(lhs, rhs) }
  | lhs = expression FBY rhs = expression
    { Fby(lhs, rhs) }
  | lhs=expression op=binop rhs=expression
    { Primitive (make_binop lhs op rhs) }
  | t=term
    { t }

term:
  | a = atomic_term
    { a }
  | e = delimited(LPAREN, expression, RPAREN)
    { e }

atomic_term:
  | x=ID
    { Var x }
  | b=TRUE | b=FALSE
    { Const (Bool b) }
  | x=INT
    { Const (Int x) }

%inline binop:
  | PLUS
     { PPlus   }
  | MINUS
     { PMinus  }
  | MULT
     { PTimes  }
  | DIV
     { PDiv    }
