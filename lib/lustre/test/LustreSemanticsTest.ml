open Liblustre
open Semantics

let lustre_value = Alcotest.testable Printer.fmt_of_value ( = )

let const_eval () =
  let open Syntax in
  Alcotest.(check lustre_value) "same value" (VInt 1) (const (Int 1))

let () =
  Alcotest.run "Lustre semantics"
    [ ("Atomic Terms", [ Alcotest.test_case "constants" `Quick const_eval ]) ]
