type 'a stream = Cons of 'a vals * (unit -> 'a stream)
and 'a vals = Nil | Val of 'a

(* let const x =
 *   let rec t () = Cons (Val x, t) in
 *   Cons (Val x, t)
 * 
 * let rec take n s =
 *   match s with
 *   | Cons _ when n = 0 -> []
 *   | Cons (x, t) -> x :: take (pred n) (t ())
 * 
 * let fby x y = Cons (x, y)
 * 
 * let arrow x y = match x with Cons (x, _) -> Cons (x, y)
 * 
 * let pre x = fby Nil x *)

type expression =
  | Var of string
  | Fby of expression * expression
  | Arrow of expression * expression
  | Const of const
  | Primitive of primitive

and primitive =
  | Plus of expression * expression
  | Minus of expression * expression
  | Times of expression * expression
  | Div of expression * expression

and const = Bool of bool | Int of int
and def = Def of string * expression
and program = def list
and value = VBool of bool | VInt of int | VNil
