{ (* Emacs, open this with -*- tuareg -*- *)
  open Parser

  let enter_newline lexbuf =
    Lexing.new_line lexbuf;
    lexbuf

  exception SyntaxError of string
}

let newline = ('\013' | "\013\010")

let whitespace = [ '\032' '\009' '\010' ]

let alpha = ['a'-'z''A'-'Z']

let digit = ['0'-'9']

let sym  = ['_']

let ident = alpha (alpha | digit | sym)*

let ftrue = "true"

let ffalse = "false"

let ident = alpha (alpha | digit | sym)*

rule token = parse
| newline     { enter_newline lexbuf |> token }
| whitespace  { token lexbuf          }
| digit+ as d { INT (int_of_string d) }
| ftrue       { TRUE true             }
| ffalse      { FALSE false           }
| "="         { EQUAL                 }
| "("         { LPAREN                }
| ")"         { RPAREN                }
| "->"        { ARROW                 }
| "fby"       { FBY                   }
| ";"         { SEMICOLON             }
| "+"         { PLUS                  }
| "-"         { MINUS                 }
| "*"         { MULT                  }
| "/"         { DIV                   }
| ident as id { ID id                 }
| eof         { EOF                   }
| _
      { raise (SyntaxError ("Syntax Error: " ^ Lexing.lexeme lexbuf))}
