open PPrint
open Syntax

let print_stream print_val vals = separate_map semi print_val vals ^^ semi

let print_eq print_val (label, vals) =
  string label ^^ equals ^^ print_stream print_val vals

let print_value x =
  match x with
  | VInt i -> OCaml.int i
  | VBool b -> OCaml.bool b
  | VNil -> string "nil"

let print vals = List.map (print_eq print_value) vals |> separate hardline

let document_to_format print_document fmt doc =
  let doc = print_document doc in
  PPrint.ToFormatter.pretty 0.9 80 fmt doc

let fmt_of_value fmt v = document_to_format print_value fmt v
