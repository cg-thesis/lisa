open Syntax

let empty_of defs n =
  List.map (function Def (x, _) -> (x, Array.make n VNil)) defs

let update env x v instant =
  let h = List.assoc x env in
  h.(instant) <- v

let history_of env instant x =
  let h =
    try List.assoc x env
    with Not_found -> failwith (Printf.sprintf "`%s` is unbound.\n" x)
  in
  h.(instant)

let rec expression instant env e =
  let h_i = history_of env instant in
  if instant < 0 then VNil
  else
    match e with
    | Arrow (x, _y) when instant = 0 -> expression instant env x
    | Arrow (_x, y) -> expression instant env y
    | Fby (x, _y) when instant = 0 -> expression instant env x
    | Fby (_x, y) -> expression (pred instant) env y
    | Var x -> h_i x
    | Primitive p -> primitive instant env p
    | Const x -> const x

and primitive b env p =
  match p with
  | Plus (lhs, rhs) ->
      let lhs = expression b env lhs in
      let rhs = expression b env rhs in
      bin_arith ( + ) lhs rhs
  | Minus (lhs, rhs) ->
      let lhs = expression b env lhs in
      let rhs = expression b env rhs in
      bin_arith ( - ) lhs rhs
  | Times (lhs, rhs) ->
      let lhs = expression b env lhs in
      let rhs = expression b env rhs in
      bin_arith ( * ) lhs rhs
  | Div (lhs, rhs) ->
      let lhs = expression b env lhs in
      let rhs = expression b env rhs in
      bin_arith ( / ) lhs rhs

and bin_arith op lhs rhs =
  match (lhs, rhs) with
  | VInt lhs, VInt rhs -> VInt (op lhs rhs)
  | _ -> assert false

and const x = match x with Int i -> VInt i | Bool b -> VBool b

let eval instant env e =
  match e with
  | Def (x, e) ->
      let v = expression instant env e in
      update env x v instant

let update_history defs instant env = List.iter (eval instant env) defs

let take n defs =
  let h = empty_of defs n in
  let rec get_history instant =
    if instant = n then List.rev_map (fun (x, hx) -> (x, Array.to_list hx)) h
    else (
      update_history defs instant h;
      get_history (succ instant))
  in
  let clock = 0 in
  get_history clock
