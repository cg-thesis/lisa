let process ~lexer_init ~input =
  try Parser.program Lexer.token (lexer_init input)
  with Sys_error msg -> failwith (Format.asprintf "%s during parsing." msg)

let parse =
  let parse_string lexer_init input = process ~lexer_init ~input in
  parse_string Lexing.from_string

let prompt = ref ""
let set_prompt = ( := ) prompt

let print_prompt () =
  output_string stdout !prompt;
  flush stdout

let input_char =
  let display_prompt = ref true in
  let ask stdin =
    if !display_prompt then (
      display_prompt := false;
      print_prompt ());
    let c = input_char stdin in
    if c = '\n' then display_prompt := true;
    String.make 1 c
  in
  ask

let read () =
  let b = Buffer.create 13 in
  let rec read prev =
    let c = input_char stdin in
    if c = "\n" then
      if prev <> "\\" then (
        Buffer.add_string b prev;
        Buffer.contents b)
      else (
        set_prompt "....> ";
        read c)
    else (
      Buffer.add_string b prev;
      read c)
  in
  read ""

let print_out b vals =
  let s =
    Buffer.clear b;
    PPrint.ToBuffer.pretty 0.9 80 b (Printer.print vals);
    Buffer.contents b
  in
  s

let buffer = Buffer.create 256

let rec repl n =
  set_prompt "lustre>";
  let prog = read () |> parse in
  let vals = Semantics.take n prog in
  print_endline (print_out buffer vals);
  repl n
