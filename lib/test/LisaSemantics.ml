open Lisa
open Semantics

let lisa_def =
  Alcotest.testable
    (fun fmt x -> Format.fprintf fmt "%s" (IO.print_binding x))
    ( = )

let parse_eval s = Option.get (eval (IO.parse_expression s))
let parse s = List.hd @@ IO.parse_binding s
let rep s = List.hd (List.map (binding () () 4) (IO.parse_binding s))

let eval_int () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 1") (rep "let x = 1")

let eval_int2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 23424564") (rep "let x = 23424564")

let eval_unit () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = ()") (rep "let x = ()")

let eval_nil () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = []") (rep "let x = []")

let eval_true () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true") (rep "let x = true")

let eval_false () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false") (rep "let x = false")

let eval_tuple () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse "let x = (1, true, fun x -> x)")
    (rep "let x = (1,true,fun x -> x)")

let eval_projnm1 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42")
    (rep "let x = proj 1 2 (flase, 42)")

let eval_projnm2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42")
    (rep "let x = proj 2 3 (86, true, 42)")

let eval_projnm3 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42")
    (rep "let x =proj 2 3 ((fun x -> x) (86 - 1, not true, 0 + 42))")

let eval_arith_plus () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42") (rep "let x = 1 + 41")

let eval_arith_plus2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42")
    (rep "let x =(41 + 0) + (0 + 1)")

(* let eval_arith_minus () = *)
(*   Alcotest.(check lisa_term) *)
(*     "same string" (Const (Int (-1))) (rep "42 - 43") *)

(* let eval_arith_minus2 () = *)
(*   Alcotest.(check lisa_term) *)
(*     "same string" (Const (Int (-1))) *)
(*     (rep "let x =(42 - 0) - (43 - 0)") *)

let eval_arith_times () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42") (rep "let x = 21 * 2")

let eval_arith_times2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42")
    (rep "let x = (20 + 1) * (2 * 1)")

let eval_arith_div () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 21") (rep "let x = 42 / 2")

let eval_arith_div2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 42")
    (rep "let x = (42 * 4) / (8 / 2) ")

let eval_bool_neg () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false") (rep "let x = not true")

let eval_bool_neg2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true") (rep "let x = not false")

let eval_bool_neg3 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = not (false && false)")

let eval_bool_and () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = false && true")

let eval_bool_and2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = true && false")

let eval_bool_and3 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = false && false")

let eval_bool_and4 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = true && true")

let eval_bool_and5 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = (not true) && (not true)")

let eval_bool_or () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = false || true")

let eval_bool_or2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = true || false")

let eval_bool_or3 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = true || true")

let eval_bool_or4 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = false || false")

let eval_bool_or5 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = (not false) || (not false)")

let eval_bool_eq () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = true = true")

let eval_bool_eq2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = false = false")

let eval_bool_eq3 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = false = true")

let eval_bool_eq4 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = true = false")

let eval_bool_eq5 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true") (rep "let x = 1 = 1")

let eval_bool_eq6 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true") (rep "let x = 762 = 762")

let eval_bool_eq7 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = 1 + 1 = 762")

let eval_bool_eq8 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = 762 = 1 + 1")

let eval_bool_eq9 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true") (rep "let x = [] = []")

let eval_bool_eq10 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = [1;2; 2 + 1] = [1;2; 2 + 1]")

let eval_bool_eq11 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = [] = [1;2; 2 + 1]")

let eval_bool_eq12 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = [1;2; 2 + 1] = []")

let eval_bool_neq () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = true <> true")

let eval_bool_neq2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = false <> false")

let eval_bool_neq3 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = false <> true")

let eval_bool_neq4 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = true <> false")

let eval_bool_neq5 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false") (rep "let x = 1 <> 1")

let eval_bool_neq6 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false") (rep "let x = 762 <> 762")

let eval_bool_neq7 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true") (rep "let x = 1 <> 762")

let eval_bool_neq8 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true") (rep "let x = 762 <> 1")

let eval_bool_neq9 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false") (rep "let x = [] <> []")

let eval_bool_neq10 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = false")
    (rep "let x = [1;2; 2+ 1] <> [1;2; 2 + 1]")

let eval_bool_neq11 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = [] <> [1;2; 2 + 1]")

let eval_bool_neq12 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = true")
    (rep "let x = [1;2] <> []")

let eval_list () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = [1; 2]") (rep "let x = [1;2]")

let eval_list_cons () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse "let x = [0;1;42;43; 44]")
    (rep "let x = cons 0 (cons 1 [42;43;43+1])")

let eval_list_map () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = [1; 2]")
    (rep "let x = map ((fun x -> x) (fun x -> x+1)) [0;1 + 0]")

let eval_list_iter () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = ()")
    (rep "let x = iter ((fun x -> x) (fun x -> ())) [1;3;4+1]")

let eval_list_foldl () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 6")
    (rep
       "let x = fold_left ((fun x -> x) (fun x -> (fun acc -> x + acc))) (0+0) \
        [1;1+1;3]")

let eval_list_foldr () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 2")
    (rep
       "let x = fold_right ((fun x -> x)(fun acc -> (fun x -> acc - x))) \
        [1;2;2+1] (0*0)")

let eval_if_then_else () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 1")
    (rep "let x = if true then 1 else 2")

let eval_if_then_else2 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 2")
    (rep "let x = if true then if false then 1 else 2 else 3")

let eval_if_then_else3 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 1")
    (rep "let x = if false then 2 else if true then 1 else 3")

let eval_if_then_else4 () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x = 2")
    (rep "let x = if if true then true else false then 2 else 3")

let eval_rectup_constant_stream () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse "rectup x = 1:*1:*1:*1:*1:*(proj 0 1 (rectup x = 1::x))")
    (rep "rectup x = 1 :: x")

let eval_rectup_nats () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse
       "rectup nats = 0:*1:*2:*3:*4:*(proj 0 1 (rectup next =\n\
       \        (((thunk\n\
       \         (proj 0 1\n\
       \          (rectup nats =\n\
       \           0::(thunk (proj 0 1 (rectup next = ((nats[i.i - 1]) + \
        1)::next))))))[i.i -\n\
       \         1]) +\n\
       \         1)::next))")
    (rep
       "rectup nats = 0 ::thunk (proj 0 1 (rectup next = ((nats[i.i - 1]) + 1) \
        :: next))")

let eval_rectup_fibo () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse
       "rectup fibo =\n\
       \ 0:*\n\
       \  1:*\n\
       \   1:*\n\
       \    2:*\n\
       \     3:*\n\
       \      (proj 0 1\n\
       \        (rectup next = (thunk (proj 0 1 (rectup fibo = 0 :: thunk (1 \
        :: thunk (proj 0 1 (rectup next = (fibo[i.i-1]) +  (fibo[i.i-2]) :: \
        next)))))[i.i-1]) +  (thunk (proj 0 1 ((rectup fibo = 0 :: thunk (1 :: \
        thunk (proj 0 1 (rectup next = (fibo[i.i-1]) +  (fibo[i.i-2]) :: \
        next))))))[i.i-2]) :: next))")
    (rep
       "rectup fibo = 0 :: thunk (1 :: thunk (proj 0 1 (rectup next = \
        (fibo[i.i-1]) +  (fibo[i.i-2]) :: next)))"
       (* |> IO.print_binding *))

let eval_rectup_mutual () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse
       "rectup x = 0:* 1:* 0:* 1:*0:*(proj 1 2 (rectup x = 0::y and y = 1::x))\n\
       \        and y = 1:*0:*1:*0:*1:*(proj 0 2 (rectup x = 0 :: y and y = \
        1:: x))")
    (rep "rectup x = 0::y and y = 1::x")

let eval_rectup_id () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse
       "rectup id = fun x -> x :* (fun x -> x) :* (fun x -> x) :* (fun x -> x) \
        :* (fun x -> x) :* (proj 0 1 (rectup id = fun x -> x::id))")
    (rep "rectup id = (fun x -> x)::id")

let eval_rectup_input () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse "rectup x = input \"x\"::x")
    (rep "rectup x = input \"x\":: x")

let eval_fun () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse "let x =fun x -> x")
    (rep "let x =fun x -> x")

let eval_app () =
  Alcotest.(check lisa_def)
    "same definition"
    (parse "let x =fun x -> x")
    (rep "let x =(fun x -> x) (fun x -> x)")

let eval_app_fun () =
  Alcotest.(check lisa_def)
    "same definition" (parse "let x =1")
    (rep "let x =(fun x -> x) 1")

let () =
  Alcotest.run "Lisa small step semantics"
    [
      ( "Literals",
        [
          Alcotest.test_case "integers" `Quick eval_int;
          Alcotest.test_case "integers" `Quick eval_int2;
          Alcotest.test_case "unit" `Quick eval_unit;
          Alcotest.test_case "list" `Quick eval_nil;
          Alcotest.test_case "bool" `Quick eval_true;
          Alcotest.test_case "bool" `Quick eval_false;
          Alcotest.test_case "tuples" `Quick eval_tuple;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "tuples" `Quick eval_projnm1;
          Alcotest.test_case "tuples" `Quick eval_projnm2;
          Alcotest.test_case "tuples" `Quick eval_projnm3;
          Alcotest.test_case "arith" `Quick eval_arith_plus;
          Alcotest.test_case "arith" `Quick eval_arith_plus2;
          (* Alcotest.test_case "arith" `Quick eval_arith_minus; *)
          (* Alcotest.test_case "arith" `Quick eval_arith_minus2; *)
          Alcotest.test_case "arith" `Quick eval_arith_times;
          Alcotest.test_case "arith" `Quick eval_arith_times2;
          Alcotest.test_case "arith" `Quick eval_arith_div;
          Alcotest.test_case "arith" `Quick eval_arith_div2;
          Alcotest.test_case "bool" `Quick eval_bool_neg;
          Alcotest.test_case "bool" `Quick eval_bool_neg2;
          Alcotest.test_case "bool" `Quick eval_bool_neg3;
          Alcotest.test_case "bool" `Quick eval_bool_and;
          Alcotest.test_case "bool" `Quick eval_bool_and2;
          Alcotest.test_case "bool" `Quick eval_bool_and3;
          Alcotest.test_case "bool" `Quick eval_bool_and4;
          Alcotest.test_case "bool" `Quick eval_bool_and5;
          Alcotest.test_case "bool" `Quick eval_bool_or;
          Alcotest.test_case "bool" `Quick eval_bool_or2;
          Alcotest.test_case "bool" `Quick eval_bool_or3;
          Alcotest.test_case "bool" `Quick eval_bool_or4;
          Alcotest.test_case "bool" `Quick eval_bool_or5;
          Alcotest.test_case "bool" `Quick eval_bool_eq;
          Alcotest.test_case "bool" `Quick eval_bool_eq2;
          Alcotest.test_case "bool" `Quick eval_bool_eq3;
          Alcotest.test_case "bool" `Quick eval_bool_eq4;
          Alcotest.test_case "bool" `Quick eval_bool_eq5;
          Alcotest.test_case "bool" `Quick eval_bool_eq6;
          Alcotest.test_case "bool" `Quick eval_bool_eq7;
          Alcotest.test_case "bool" `Quick eval_bool_eq8;
          Alcotest.test_case "bool" `Quick eval_bool_eq9;
          Alcotest.test_case "bool" `Quick eval_bool_eq10;
          Alcotest.test_case "bool" `Quick eval_bool_eq11;
          Alcotest.test_case "bool" `Quick eval_bool_eq12;
          Alcotest.test_case "bool" `Quick eval_bool_neq;
          Alcotest.test_case "bool" `Quick eval_bool_neq2;
          Alcotest.test_case "bool" `Quick eval_bool_neq3;
          Alcotest.test_case "bool" `Quick eval_bool_neq4;
          Alcotest.test_case "bool" `Quick eval_bool_neq5;
          Alcotest.test_case "bool" `Quick eval_bool_neq6;
          Alcotest.test_case "bool" `Quick eval_bool_neq7;
          Alcotest.test_case "bool" `Quick eval_bool_neq8;
          Alcotest.test_case "bool" `Quick eval_bool_neq9;
          Alcotest.test_case "bool" `Quick eval_bool_neq10;
          Alcotest.test_case "bool" `Quick eval_bool_neq11;
          Alcotest.test_case "bool" `Quick eval_bool_neq12;
          Alcotest.test_case "list" `Quick eval_list;
          Alcotest.test_case "list" `Quick eval_list_cons;
          Alcotest.test_case "list" `Quick eval_list_map;
          Alcotest.test_case "list" `Quick eval_list_iter;
          Alcotest.test_case "list" `Quick eval_list_foldr;
          Alcotest.test_case "list" `Quick eval_list_foldl;
          Alcotest.test_case "if" `Quick eval_if_then_else;
          Alcotest.test_case "if" `Quick eval_if_then_else2;
          Alcotest.test_case "if" `Quick eval_if_then_else3;
          Alcotest.test_case "if" `Quick eval_if_then_else4;
        ] );
      ( "Term",
        [
          (* Alcotest.test_case "input" `Quick eval_rectup_input; *)
          Alcotest.test_case "rectup" `Quick eval_rectup_id;
          Alcotest.test_case "rectup" `Quick eval_rectup_constant_stream;
          Alcotest.test_case "rectup" `Quick eval_rectup_nats;
          Alcotest.test_case "rectup" `Quick eval_rectup_fibo;
          Alcotest.test_case "rectup" `Quick eval_rectup_mutual;
          Alcotest.test_case "function" `Quick eval_fun;
          Alcotest.test_case "application" `Quick eval_app;
          Alcotest.test_case "application" `Quick eval_app_fun;
        ] );
    ]
