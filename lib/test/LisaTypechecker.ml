open Lisa

let elaborate x = Typechecker.translate [] @@ IO.parse_binding x
let print_type = PrettyPrinter.string_of_type

let rep def =
  match elaborate def with _, [ (_, ty) ] -> print_type ty | _ -> ""

let typecheck_int () =
  Alcotest.(check string) "same string" "int" (rep "let x = 1")

let typecheck_int2 () =
  Alcotest.(check string) "same string" "int" (rep "let x = 23424564")

let typecheck_unit () =
  Alcotest.(check string) "same string" "unit" (rep "let x = ()")

let typecheck_nil () =
  Alcotest.(check string) "same string" "int list" (rep "let x : int list = []")

let typecheck_true () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true")

let typecheck_false () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false")

let typecheck_tuple () =
  Alcotest.(check string)
    "same string" "(int * bool * int -> int)"
    (rep "let x = (1, true, (fun x -> x+1))")

let typecheck_projnm1 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = proj 1 2 (false, 42)")

let typecheck_projnm2 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = proj 2 3 (86, true, 42)")

let typecheck_projnm3 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = proj 2 3 ((fun x -> x) (86 - 1, not true, 0 + 42))")

let typecheck_arith_plus () =
  Alcotest.(check string) "same string" "int" (rep "let x = 1 + 41")

let typecheck_arith_plus2 () =
  Alcotest.(check string) "same string" "int" (rep "let x = (41 + 0) + (0 + 1)")

let typecheck_arith_minus () =
  Alcotest.(check string) "same string" "int" (rep "let x = 42 - 43")

let typecheck_arith_minus2 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = (42 - 0) - (43 - 0)")

let typecheck_arith_times () =
  Alcotest.(check string) "same string" "int" (rep "let x = 21 * 2")

let typecheck_arith_times2 () =
  Alcotest.(check string) "same string" "int" (rep "let x = (20 + 1) * (2 * 1)")

let typecheck_arith_div () =
  Alcotest.(check string) "same string" "int" (rep "let x = 42 / 2")

let typecheck_arith_div2 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = (42 * 4) / (8 / 2) ")

let typecheck_bool_neg () =
  Alcotest.(check string) "same string" "bool" (rep "let x = not true")

let typecheck_bool_neg2 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = not false")

let typecheck_bool_neg3 () =
  Alcotest.(check string)
    "same string" "bool"
    (rep "let x = not (false && false)")

let typecheck_bool_and () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false && true")

let typecheck_bool_and2 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true && false")

let typecheck_bool_and3 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false && false")

let typecheck_bool_and4 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true && true")

let typecheck_bool_and5 () =
  Alcotest.(check string)
    "same string" "bool"
    (rep "let x = (not true) && (not true)")

let typecheck_bool_or () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false || true")

let typecheck_bool_or2 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true || false")

let typecheck_bool_or3 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true || true")

let typecheck_bool_or4 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false || false")

let typecheck_bool_or5 () =
  Alcotest.(check string)
    "same string" "bool"
    (rep "let x = (not false) || (not false)")

let typecheck_bool_eq () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true = true")

let typecheck_bool_eq2 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false = false")

let typecheck_bool_eq3 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false = true")

let typecheck_bool_eq4 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true = false")

let typecheck_bool_eq5 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 1 = 1")

let typecheck_bool_eq6 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 762 = 762")

let typecheck_bool_eq7 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 1 + 1 = 762")

let typecheck_bool_eq8 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 762 = 1 + 1")

let typecheck_bool_eq9 () =
  Alcotest.(check string) "same string" "bool" (rep "let x =  [1] = []")

let typecheck_bool_eq10 () =
  Alcotest.(check string)
    "same string" "bool"
    (rep "let x = [1;2; 2 + 1] = [1;2; 2 + 1]")

let typecheck_bool_eq11 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = [] = [1;2; 2 + 1]")

let typecheck_bool_eq12 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = [1;2; 2 + 1] = []")

let typecheck_bool_neq () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true <> true")

let typecheck_bool_neq2 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false <> false")

let typecheck_bool_neq3 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = false <> true")

let typecheck_bool_neq4 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = true <> false")

let typecheck_bool_neq5 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 1 <> 1")

let typecheck_bool_neq6 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 762 <> 762")

let typecheck_bool_neq7 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 1 <> 762")

let typecheck_bool_neq8 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = 762 <> 1")

let typecheck_bool_neq9 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = [1] <> []")

let typecheck_bool_neq10 () =
  Alcotest.(check string)
    "same string" "bool"
    (rep "let x = [1;2; 2+ 1] <> [1;2; 2 + 1]")

let typecheck_bool_neq11 () =
  Alcotest.(check string)
    "same string" "bool"
    (rep "let x = [] <> [1;2; 2 + 1]")

let typecheck_bool_neq12 () =
  Alcotest.(check string) "same string" "bool" (rep "let x = [1;2] <> []")

let typecheck_list () =
  Alcotest.(check string) "same string" "int list" (rep "let x = [1;2]")

let typecheck_list_cons () =
  Alcotest.(check string)
    "same string" "int list"
    (rep "let x = cons 0 (cons 1 [42;43;43+1])")

let typecheck_list_map () =
  Alcotest.(check string)
    "same string" "int list"
    (rep "let x = map ((fun x -> x) (fun x -> x+1)) [0;1 + 0]")

let typecheck_list_iter () =
  Alcotest.(check string)
    "same string" "unit"
    (rep "let x = iter ((fun x -> x) (fun x -> ())) [1;3;4+1]")

let typecheck_list_foldl () =
  Alcotest.(check string)
    "same string" "int"
    (rep
       "let x = fold_left ((fun x -> x) (fun x -> (fun acc -> x + acc))) (0+0) \
        [1;1+1;3]")

let typecheck_list_foldr () =
  Alcotest.(check string)
    "same string" "int"
    (rep
       "let x = fold_right ((fun x -> x)(fun acc -> (fun x -> acc - x))) \
        [1;2;2+1] (0*0)")

let typecheck_if_then_else () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = if true then 1 else 2")

let typecheck_if_then_else2 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = if true then if false then 1 else 2 else 3")

let typecheck_if_then_else3 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = if false then 2 else if true then 1 else 3")

let typecheck_if_then_else4 () =
  Alcotest.(check string)
    "same string" "int"
    (rep "let x = if if true then true else false then 2 else 3")

let typecheck_rectup_id () =
  Alcotest.(check string) "same string" "" (rep "rectup id = fun x -> x+1::id")

let typecheck_rectup_input () =
  Alcotest.(check string)
    "same string" ""
    (rep "rectup x = input \"x\"::x and y = x[i.i]::y")

let typecheck_rectup_constant_stream () =
  Alcotest.(check string) "same string" "" (rep "rectup x = 1 :: x")

let typecheck_rectup_nats () =
  Alcotest.(check string)
    "same string" ""
    (rep
       "rectup nats = 0 :: thunk (proj 0 1 (rectup next = (nats[i.i-1]) + 1 :: \
        next))")

let typecheck_rectup_mutual () =
  Alcotest.(check string) "same string" "" (rep "rectup x = 0::y and y = 1::x")

let typecheck_fun () =
  Alcotest.(check string)
    "same string" "int -> int"
    (rep "let id = fun x -> x + 1")

let typecheck_app () =
  Alcotest.(check string)
    "same string" "bool -> bool"
    (rep "let id = (fun x -> x) (fun x -> not x)")

let typecheck_app_fun () =
  Alcotest.(check string) "same string" "int" (rep "let one = (fun x -> x) 1")

let () =
  Alcotest.run "Lisa typechecker"
    [
      ( "Literals",
        [
          Alcotest.test_case "integers" `Quick typecheck_int;
          Alcotest.test_case "integers" `Quick typecheck_int2;
          Alcotest.test_case "unit" `Quick typecheck_unit;
          Alcotest.test_case "list" `Quick typecheck_nil;
          Alcotest.test_case "bool" `Quick typecheck_true;
          Alcotest.test_case "bool" `Quick typecheck_false;
          Alcotest.test_case "tuples" `Quick typecheck_tuple;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "tuples" `Quick typecheck_projnm1;
          Alcotest.test_case "tuples" `Quick typecheck_projnm2;
          Alcotest.test_case "tuples" `Quick typecheck_projnm3;
          Alcotest.test_case "arith" `Quick typecheck_arith_plus;
          Alcotest.test_case "arith" `Quick typecheck_arith_plus2;
          Alcotest.test_case "arith" `Quick typecheck_arith_minus;
          Alcotest.test_case "arith" `Quick typecheck_arith_minus2;
          Alcotest.test_case "arith" `Quick typecheck_arith_times;
          Alcotest.test_case "arith" `Quick typecheck_arith_times2;
          Alcotest.test_case "arith" `Quick typecheck_arith_div;
          Alcotest.test_case "arith" `Quick typecheck_arith_div2;
          Alcotest.test_case "bool" `Quick typecheck_bool_neg;
          Alcotest.test_case "bool" `Quick typecheck_bool_neg2;
          Alcotest.test_case "bool" `Quick typecheck_bool_neg3;
          Alcotest.test_case "bool" `Quick typecheck_bool_and;
          Alcotest.test_case "bool" `Quick typecheck_bool_and2;
          Alcotest.test_case "bool" `Quick typecheck_bool_and3;
          Alcotest.test_case "bool" `Quick typecheck_bool_and4;
          Alcotest.test_case "bool" `Quick typecheck_bool_and5;
          Alcotest.test_case "bool" `Quick typecheck_bool_or;
          Alcotest.test_case "bool" `Quick typecheck_bool_or2;
          Alcotest.test_case "bool" `Quick typecheck_bool_or3;
          Alcotest.test_case "bool" `Quick typecheck_bool_or4;
          Alcotest.test_case "bool" `Quick typecheck_bool_or5;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq2;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq3;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq4;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq5;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq6;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq7;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq8;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq9;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq10;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq11;
          Alcotest.test_case "bool" `Quick typecheck_bool_eq12;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq2;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq3;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq4;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq5;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq6;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq7;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq8;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq9;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq10;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq11;
          Alcotest.test_case "bool" `Quick typecheck_bool_neq12;
          Alcotest.test_case "list" `Quick typecheck_list;
          Alcotest.test_case "list" `Quick typecheck_list_cons;
          Alcotest.test_case "list" `Quick typecheck_list_map;
          Alcotest.test_case "list" `Quick typecheck_list_iter;
          Alcotest.test_case "list" `Quick typecheck_list_foldr;
          Alcotest.test_case "list" `Quick typecheck_list_foldl;
          Alcotest.test_case "if" `Quick typecheck_if_then_else;
          Alcotest.test_case "if" `Quick typecheck_if_then_else2;
          Alcotest.test_case "if" `Quick typecheck_if_then_else3;
          Alcotest.test_case "if" `Quick typecheck_if_then_else4;
        ] );
      ( "Term",
        [
          Alcotest.test_case "input" `Quick typecheck_rectup_input;
          Alcotest.test_case "rectup" `Quick typecheck_rectup_constant_stream;
          Alcotest.test_case "rectup id" `Quick typecheck_rectup_id;
          Alcotest.test_case "rectup nats" `Quick typecheck_rectup_nats;
          Alcotest.test_case "rectup mutual" `Quick typecheck_rectup_mutual;
          Alcotest.test_case "function" `Quick typecheck_fun;
          Alcotest.test_case "application" `Quick typecheck_app;
          Alcotest.test_case "application" `Quick typecheck_app_fun;
        ] );
    ]
