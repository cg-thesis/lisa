open Lisa
module CBPV = Libcbpv_compiler
open CBPVCompiler
open CBPV.Syntax

let string_of_cbpv_term t = Libcbpv_compiler.Printer.string_of_term t

let compile_print e =
  IO.parse_expression e |> translate |> CBPV.Typechecker.check
  |> string_of_cbpv_term

let id x = x

let nth_n n x =
  let a =
    (x =: "a")
      ((trec "nth"
          (tfunn
             [ ("xs", None); ("i", None) ]
             (tif
                (t "i" ==! tint 0)
                (thd (t "xs"))
                ((ttl (t "xs") =: "r")
                   ((tforce (t "r") =: "t")
                      ((tforce (t "nth") @@! t "t") @@! (t "i" -! tint 1))))))
       @@! t "a")
      @@! tint n)
  in
  Libcbpv_compiler.Printer.string_of_term a |> print_endline;
  a

(** Parse a term, translate, typecheck, evaluate, print value **)

let pttep s =
  IO.parse_expression s |> translate |> CBPV.Typechecker.check |> fun t ->
  CBPV.Semantics.eval' t { index = 0; env = [] }
  |> CBPV.Printer.string_of_terminal

let compile_eval_map_print f e =
  IO.parse_expression e |> translate |> CBPV.Typechecker.check |> f |> fun x ->
  Libcbpv_compiler.Semantics.eval' x { index = 0; env = [] }
  |> Libcbpv_compiler.Printer.string_of_terminal

let compile_mimi f e =
  IO.parse_expression e |> translate |> f |> CBPV.Compiler.eval

let compile_print_nat () =
  Alcotest.(check string) "same string" "return 1" (pttep "1")

let compile_print_nat2 () =
  Alcotest.(check string) "same string" "return 23424564" (pttep " 23424564")

let compile_print_unit () =
  Alcotest.(check string) "same string" "return ()" (pttep " ()")

let compile_print_true () =
  Alcotest.(check string) "same string" "return true" (pttep " true")

let compile_print_false () =
  Alcotest.(check string) "same string" "return false" (pttep " false")

let compile_print_pair () =
  Alcotest.(check string)
    "same string" "return (1, <thunk>)"
    (pttep " (1,fun x -> x+0)")

let compile_eval_fst () =
  Alcotest.(check string)
    "same expression" "return 86"
    (pttep " proj 0 2 (86, true)")

let compile_eval_fst2 () =
  Alcotest.(check string)
    "same expression" "return false"
    (pttep " proj 0 2 ((fun x -> x) (not true), 0 + 42)")

let compile_eval_snd () =
  Alcotest.(check string)
    "same expression" "return 42"
    (pttep " proj 1 2 (false, 42)")

let compile_eval_arith_plus () =
  Alcotest.(check string) "same expression" "return 42" (pttep " 1 + 41")

let compile_eval_arith_plus2 () =
  Alcotest.(check string)
    "same expression" "return 42"
    (pttep " (41 + 0) + (0 + 1)")

let compile_eval_arith_minus () =
  Alcotest.(check string) "same expression" "return -1" (pttep " 42 - 43")

let compile_eval_arith_minus2 () =
  Alcotest.(check string)
    "same expression" "return -1"
    (pttep " (42 - 0) - (43 - 0)")

let compile_eval_arith_times () =
  Alcotest.(check string) "same expression" "return 42" (pttep " 21 * 2")

let compile_eval_arith_times2 () =
  Alcotest.(check string)
    "same expression" "return 42"
    (pttep " (20 + 1) * (2 * 1)")

let compile_eval_arith_div () =
  Alcotest.(check string) "same expression" "return 21" (pttep " 42 / 2")

let compile_eval_arith_div2 () =
  Alcotest.(check string)
    "same expression" "return 42"
    (pttep " (42 * 4) / (8 / 2) ")

let compile_eval_bool_neg () =
  Alcotest.(check string) "same expression" "return false" (pttep " not true")

let compile_eval_bool_neg2 () =
  Alcotest.(check string) "same expression" "return true" (pttep " not false")

let compile_eval_bool_neg3 () =
  Alcotest.(check string)
    "same expression" "return true"
    (pttep "not (false && false)")

let compile_eval_bool_and () =
  Alcotest.(check string)
    "same expression" "return false" (pttep "false && true")

let compile_eval_bool_and2 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep "true && false")

let compile_eval_bool_and3 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep "false && false")

let compile_eval_bool_and4 () =
  Alcotest.(check string) "same expression" "return true" (pttep "true && true")

let compile_eval_bool_and5 () =
  Alcotest.(check string)
    "same expression" "return false"
    (pttep "(not true) && (not true)")

let compile_eval_bool_or () =
  Alcotest.(check string)
    "same expression" "return true" (pttep "false || true")

let compile_eval_bool_or2 () =
  Alcotest.(check string)
    "same expression" "return true" (pttep "true || false")

let compile_eval_bool_or3 () =
  Alcotest.(check string) "same expression" "return true" (pttep "true || true")

let compile_eval_bool_or4 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep "false || false")

let compile_eval_bool_or5 () =
  Alcotest.(check string)
    "same expression" "return true"
    (pttep "(not false) || (not false)")

let compile_eval_bool_eq () =
  Alcotest.(check string) "same expression" "return true" (pttep " true = true")

let compile_eval_bool_eq2 () =
  Alcotest.(check string)
    "same expression" "return true" (pttep " false = false")

let compile_eval_bool_eq3 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep " false = true")

let compile_eval_bool_eq4 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep " true = false")

let compile_eval_bool_eq5 () =
  Alcotest.(check string) "same expression" "return true" (pttep " 1 = 1")

let compile_eval_bool_eq6 () =
  Alcotest.(check string) "same expression" "return true" (pttep " 762 = 762")

let compile_eval_bool_eq7 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep " 1 + 1 = 762")

let compile_eval_bool_eq8 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep " 762 = 1 + 1")

let compile_eval_bool_eq9 () =
  Alcotest.(check string) "same expression" "return true" (pttep " [] = []")

let compile_eval_bool_eq10 () =
  Alcotest.(check string)
    "same expression" "return true"
    (pttep " [1;2; (2 + 1)] = [1;2; (2 + 1)]")

let compile_eval_bool_eq11 () =
  Alcotest.(check string)
    "same expression" "return false"
    (pttep " [] = [1;2; (2 + 1)]")

let compile_eval_bool_eq12 () =
  Alcotest.(check string)
    "same expression" "return false"
    (pttep " [1;2; (2 + 1)] = []")

let compile_eval_bool_neq () =
  Alcotest.(check string)
    "same expression" "return false" (pttep " true <> true")

let compile_eval_bool_neq2 () =
  Alcotest.(check string)
    "same expression" "return false" (pttep " false <> false")

let compile_eval_bool_neq3 () =
  Alcotest.(check string)
    "same expression" "return true" (pttep " false <> true")

let compile_eval_bool_neq4 () =
  Alcotest.(check string)
    "same expression" "return true" (pttep " true <> false")

let compile_eval_bool_neq5 () =
  Alcotest.(check string) "same expression" "return false" (pttep " 1 <> 1")

let compile_eval_bool_neq6 () =
  Alcotest.(check string) "same expression" "return false" (pttep " 762 <> 762")

let compile_eval_bool_neq7 () =
  Alcotest.(check string) "same expression" "return true" (pttep " 1 <> 762")

let compile_eval_bool_neq8 () =
  Alcotest.(check string) "same expression" "return true" (pttep " 762 <> 1")

(* let compile_eval_bool_neq9 () =
 *   Alcotest.(check string)
 *     "same expression" ("return false")
 *     (pttep " [] <> []") *)
let compile_eval_bool_neq10 () =
  Alcotest.(check string)
    "same expression" "return false"
    (pttep " [1;2; 2+ 1] <> [1;2; 2 + 1]")

(* let compile_eval_bool_neq11 () = *)
(*   Alcotest.(check string) *)
(*     "same expression" ("return true") *)
(*     (pttep " [] <> [1;2; 2 + 1]") *)

(* let compile_eval_bool_neq12 () = *)
(*   Alcotest.(check string) *)
(*     "same expression" ("return true") *)
(*     (pttep " [1;2] <> []") *)

(* let compile_eval_list () =
 *   Alcotest.(check string)
 *     "same expression"
 *     (VList [ VInt 1; VInt 2 ])
 *     (pttep " [1;2]")
 *
 * let compile_eval_list_cons () =
 *   Alcotest.(check string)
 *     "same expression"
 *     (VList [ VInt 0; VInt 1; VInt 42; VInt 43; VInt 44 ])
 *     (pttep " cons 0 (cons 1 [42;43;43+1])")
 *
 * let compile_eval_list_map () =
 *   Alcotest.(check string)
 *     "same expression"
 *     (VList [ VInt 1; VInt 2 ])
 *     (pttep " map ((fun x -> x) (fun x -> x+1)) [0;1 + 0]")
 *
 * let compile_eval_list_iter () =
 *   Alcotest.(check string)
 *     "same expression" VUnit
 *     (pttep " iter ((fun x -> x) (fun x -> ())) [1;3;4+1]")
 *
 * let compile_eval_list_foldl () =
 *   Alcotest.(check string)
 *     "same expression" (VInt 6)
 *     (parse_eval
 *        "let x = fold_left ((fun x -> x) (fun x -> (fun acc -> x + acc))) (0+0) \
 *         [1;1+1;3]")
 *
 * let compile_eval_list_foldr () =
 *   Alcotest.(check string)
 *     "same expression" (VInt 2)
 *     (parse_eval
 *        "let x = fold_right ((fun x -> x)(fun acc -> (fun x -> acc - x))) \
 *         [1;2;2+1] (0*0)") *)

let compile_eval_if_then_else () =
  Alcotest.(check string)
    "same expression" "return 1"
    (pttep " if true then 1 else 2")

let compile_eval_if_then_else2 () =
  Alcotest.(check string)
    "same expression" "return 2"
    (pttep " if true then if false then 1 else 2 else 3")

let compile_eval_if_then_else3 () =
  Alcotest.(check string)
    "same expression" "return 1"
    (pttep " if false then 2 else if true then 1 else 3")

let compile_eval_if_then_else4 () =
  Alcotest.(check string)
    "same expression" "return 2"
    (pttep " if if true then true else false then 2 else 3")

(* let compile_eval_rectup_id () =
 *   Alcotest.(check string)
 *     "same string" "(<fun>::<fun>::<fun>::<fun>::<fun>::<stream>)"
 *     (rep "rectup id = fun x -> x::id")
 *
 * let compile_eval_rectup_input () =
 *   Alcotest.(check string)
 *     "same string" "(1::2::3::4::5::<stream>)"
 *     (rep "rectup x = input \"x\"::x")
 *
 * let compile_eval_rectup_constant_stream () =
 *   Alcotest.(check string)
 *     "same string" "(1::1::1::1::1::<stream>)" (rep "rectup x = 1 :: x")
 *
 * let compile_eval_rectup_nats () =
 *   Alcotest.(check string)
 *     "same string" "(0::1::2::3::4::<stream>)"
 *     (rep
 *        "rectup nats = 0 ::\n\
 *        \        proj 0 1 (rectup next = (nats @ (fun x -> x - 1)) + 1 :: next)")
 *
 * let compile_eval_rectup_mutual () =
 *   Alcotest.(check string)
 *     "same string" "(0::1::0::1::0::<stream>, 1::0::1::0::1::<stream>)"
 *     (rep "rectup x = 0::y and y = 1::x") *)

let compile_eval_fun () =
  Alcotest.(check string)
    "same expression" "return <thunk>" (pttep "fun x -> x+0")

let compile_eval_app () =
  Alcotest.(check string)
    "same expression" "return <thunk>"
    (pttep "(fun x -> x) (fun x -> x+0)")

let compile_print_term_app () =
  Alcotest.(check string) "same string" "return 42" (pttep "(fun x -> x) 42")

let compile_print_stream_two () =
  Alcotest.(check string)
    "same string" "return 2 :: <thunk>"
    (pttep "rectup two = 1 + 1::two")

let compile_print_stream_one () =
  Alcotest.(check string)
    "same string" "return 1"
    (compile_eval_map_print (nth_n 3) "proj 0 1 (rectup one = 1::one)")

let compile_print_stream_x_and_y () =
  Alcotest.(check string)
    "same string" "return true"
    (compile_eval_map_print (nth_n 1)
       "proj 1 2 (rectup k = true :: r and r = false :: k)")

let compile_mimi_stream_x_and_y () =
  Alcotest.(check (pair int bool))
    "same value" (1, true)
    (compile_mimi Fun.id
       {|(thunk (proj 3 5 (rectup
       k = true :: r and
       r = false :: k and
       z = 42::z and
       y = (1,true)::y and
       w = 1::z)))[i.16]|})

let compile_print_stream_nats () =
  Alcotest.(check string)
    "same string" "return 5"
    (compile_eval_map_print (nth_n 5) (* pttep *)
       "rectup nat = 0:* (proj 0 1 (rectup next = nat[i.i-1]+1::next))"
       (* "proj 1  2 (rectup from = (fun x -> (x :: thunk ((force from) (x+1)))) \ *)
          (*  and nat = ((force from) 0))" *))

let compile_mimi_stream_nats () =
  Alcotest.(check int)
    "same value" 8
    (compile_mimi Fun.id
       "(thunk (proj 0 1 (rectup nat = 0:* (proj 0 1 (rectup next = \
        nat[i.i-1]+1::next)))))[i.8]")

let () =
  Alcotest.run ~show_errors:true "Lisa -> CBPV compiler"
    [
      ( "Primitives",
        [
          Alcotest.test_case "tuples" `Quick compile_eval_fst;
          Alcotest.test_case "tuples" `Quick compile_eval_fst2;
          Alcotest.test_case "tuples" `Quick compile_eval_snd;
          Alcotest.test_case "arith" `Quick compile_eval_arith_plus;
          Alcotest.test_case "arith" `Quick compile_eval_arith_plus2;
          Alcotest.test_case "arith" `Quick compile_eval_arith_minus;
          Alcotest.test_case "arith" `Quick compile_eval_arith_minus2;
          Alcotest.test_case "arith" `Quick compile_eval_arith_times;
          Alcotest.test_case "arith" `Quick compile_eval_arith_times2;
          Alcotest.test_case "arith" `Quick compile_eval_arith_div;
          Alcotest.test_case "arith" `Quick compile_eval_arith_div2;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neg;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neg2;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neg3;
          Alcotest.test_case "bool" `Quick compile_eval_bool_and;
          Alcotest.test_case "bool" `Quick compile_eval_bool_and2;
          Alcotest.test_case "bool" `Quick compile_eval_bool_and3;
          Alcotest.test_case "bool" `Quick compile_eval_bool_and4;
          Alcotest.test_case "bool" `Quick compile_eval_bool_and5;
          Alcotest.test_case "bool" `Quick compile_eval_bool_or;
          Alcotest.test_case "bool" `Quick compile_eval_bool_or2;
          Alcotest.test_case "bool" `Quick compile_eval_bool_or3;
          Alcotest.test_case "bool" `Quick compile_eval_bool_or4;
          Alcotest.test_case "bool" `Quick compile_eval_bool_or5;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq2;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq3;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq4;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq5;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq6;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq7;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq8;
          (* Alcotest.test_case "bool" `Quick compile_eval_bool_eq9; *)
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq10;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq11;
          Alcotest.test_case "bool" `Quick compile_eval_bool_eq12;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq2;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq3;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq4;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq5;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq6;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq7;
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq8;
          (* Alcotest.test_case "bool" `Quick compile_eval_bool_neq9; *)
          Alcotest.test_case "bool" `Quick compile_eval_bool_neq10;
          (*        * Alcotest.test_case "bool" `Quick compile_eval_bool_neq11; *)
          (*        * Alcotest.test_case "bool" `Quick compile_eval_bool_neq12; *)
          Alcotest.test_case "if" `Quick compile_eval_if_then_else;
          Alcotest.test_case "if" `Quick compile_eval_if_then_else2;
          Alcotest.test_case "if" `Quick compile_eval_if_then_else3;
          Alcotest.test_case "if" `Quick compile_eval_if_then_else4;
        ] );
      ( "Term",
        [
          (* Alcotest.test_case "input" `Quick compile_eval_rectup_input; *)
          (*        * Alcotest.test_case "rectup" `Quick compile_eval_rectup_constant_stream; *)
          (*        * Alcotest.test_case "rectup id" `Quick compile_eval_rectup_id; *)
          (*        * Alcotest.test_case "rectup nats" `Quick compile_eval_rectup_nats; *)
          (*        * Alcotest.test_case "rectup mutual" `Quick compile_eval_rectup_mutual; *)
          Alcotest.test_case "function" `Quick compile_eval_fun;
          Alcotest.test_case "application" `Quick compile_eval_app
          (* Alcotest.test_case "application" `Quick compile_eval_app_fun; *);
        ] );
      ( "Literals",
        [
          Alcotest.test_case "nat" `Quick compile_print_nat;
          Alcotest.test_case "integers" `Quick compile_print_nat2;
          Alcotest.test_case "unit" `Quick compile_print_unit;
          Alcotest.test_case "bool" `Quick compile_print_true;
          Alcotest.test_case "bool" `Quick compile_print_false;
          Alcotest.test_case "tuples" `Quick compile_print_pair;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "rectup" `Quick compile_print_term_app;
          Alcotest.test_case "rectup" `Quick compile_print_stream_two;
          Alcotest.test_case "rectup" `Quick compile_print_stream_one;
        ] );
      ( "Example",
        [
          Alcotest.test_case "rectup" `Quick compile_print_stream_x_and_y;
          Alcotest.test_case "rectup" `Quick compile_mimi_stream_x_and_y;
          Alcotest.test_case "rectup" `Quick compile_print_stream_nats;
          Alcotest.test_case "rectup" `Quick compile_mimi_stream_nats;
        ] );
    ]
