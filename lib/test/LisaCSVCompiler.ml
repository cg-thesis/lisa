open Lisa

let string_of_def =
  List.fold_left
    (fun prog def ->
      let def = IO.print_binding def in
      prog ^ def)
    ""

let compile_print s =
  IO.parse_sheet s |> CSVCompiler.lisa_of_csv [] |> string_of_def

let compile_lit_var () =
  Alcotest.(check string) "same string" "rectup x = y::x" (compile_print "x\ny")

let compile_lit_map () =
  Alcotest.(check string)
    "same string" "rectup x = []::x"
    (compile_print "x\n=MAP.EMPTY()")

let compile_lit_map2 () =
  Alcotest.(check string)
    "same string" "rectup x = [(1, 2)]::x"
    (compile_print "x\n=MAP(PAIR(1,2))")

let compile_lit_unit () =
  Alcotest.(check string)
    "same string" "rectup x = ()::x" (compile_print "x\n()")

let compile_lit_nat () =
  Alcotest.(check string)
    "same string" "rectup one = 1::one" (compile_print "one\n1")

let compile_lit_nat2 () =
  Alcotest.(check string)
    "same string" "rectup x = 45::x" (compile_print "x\n45")

let compile_lit_bool () =
  Alcotest.(check string)
    "same string" "rectup x = true::x" (compile_print "x\ntrue")

let compile_lit_bool2 () =
  Alcotest.(check string)
    "same string" "rectup x = false::x" (compile_print "x\nfalse")

let compile_lit_pair () =
  Alcotest.(check string)
    "same string" "rectup a = (x, y)::a"
    (compile_print "a\n=PAIR(x, y)")

let compile_input () =
  Alcotest.(check string)
    "same string" "rectup x = (input \"x\")::x" (compile_print "x")

let compile_fst () =
  Alcotest.(check string)
    "same string" "rectup a = (proj 0 2 (1, 2))::a"
    (compile_print "a\n=FST(PAIR(1,2))")

let compile_snd () =
  Alcotest.(check string)
    "same string" "rectup a = (proj 1 2 (1, 2))::a"
    (compile_print "a\n=SND(PAIR(1,2))")

let compile_arith_plus () =
  Alcotest.(check string)
    "same string" "rectup a = (x + y)::a"
    (compile_print "a\n= x + y")

let compile_arith_minus () =
  Alcotest.(check string)
    "same string" "rectup a = (42 - 43)::a"
    (compile_print "a\n=42 - 43")

let compile_arith_times () =
  Alcotest.(check string)
    "same string" "rectup a = (42 * x)::a"
    (compile_print "a\n= 42 * x")

let compile_arith_div () =
  Alcotest.(check string)
    "same string" "rectup a = (y / 1)::a"
    (compile_print "a\n= y / 1")

let compile_arith () =
  Alcotest.(check string)
    "same string" "rectup a = ((1 + (2 * 3)) - (1 / 2))::a"
    (compile_print "a\n= 1 + 2 * 3 - 1 / 2")

let compile_bool_neg () =
  Alcotest.(check string)
    "same string" "rectup x = (not a)::x"
    (compile_print "x\n=NOT(a)")

let compile_bool_and () =
  Alcotest.(check string)
    "same string" "rectup a = (x && false)::a"
    (compile_print "a\n=AND(x,false)")

let compile_bool_and2 () =
  Alcotest.(check string)
    "same string" "rectup a = (x && (false && false))::a"
    (compile_print "a\n=AND(x,AND(false, false))")

let compile_bool_or () =
  Alcotest.(check string)
    "same string" "rectup a = (x || y)::a"
    (compile_print "a\n=OR(x, y)")

let compile_bool_eq () =
  Alcotest.(check string)
    "same string" "rectup a = (x = y)::a"
    (compile_print "a\n=x = y")

let compile_map_map () =
  Alcotest.(check string)
    "same string" "rectup a = (map succ xs)::a"
    (compile_print "a\n=MAP.MAP(succ,xs)")

let compile_map_upd () =
  Alcotest.(check string)
    "same string"
    "let mem_assoc = fun x -> fun xs -> fold_left\n\
    \ (fun res -> fun y -> (x = (proj 0 2 y)) || res) false xs\n\
     let map_update = fun key -> fun value -> fun upfn -> fun m -> if \
     (mem_assoc key m) then\n\
    \ (map (upfn key value) m) else cons (key, value) m\n\
     rectup a = (map_update x y succ xs)::a"
    (compile_print "a\n=MAP.UPD(x,y,succ,xs)")

let compile_if_then_else () =
  Alcotest.(check string)
    "same string" "rectup a = (if condition then branch_one else branch_two)::a"
    (compile_print "a\n=IF(condition, branch_one, branch_two)")

let compile_if_then_else2 () =
  Alcotest.(check string)
    "same string" "rectup a = (if true then (if false then 1 else 2) else 3)::a"
    (compile_print "a\n=IF(true, IF(false, 1, 2), 3)")

let compile_if_then_else3 () =
  Alcotest.(check string)
    "same string" "rectup a = (if true then 2 else (if false then 1 else 3))::a"
    (compile_print "a\n=IF(true, 2, IF(false, 1, 3))")

let compile_if_then_else4 () =
  Alcotest.(check string)
    "same string"
    "rectup a = (if (if true then true else false) then 2 else 3)::a"
    (compile_print "a\n=IF(IF(true, true, false), 2, 3)")

let compile_app () =
  Alcotest.(check string)
    "same string" "rectup x = (h (g x))::x"
    (compile_print "x\n = h (g x)")

let compile_at () =
  Alcotest.(check string)
    "same string" "rectup a = (x[i.i + 0])::a" (compile_print "a\n=x[]")

let () =
  Alcotest.run "Lisa CSV compiler"
    [
      ( "Literals",
        [
          Alcotest.test_case "var" `Quick compile_lit_var;
          Alcotest.test_case "map" `Quick compile_lit_map;
          Alcotest.test_case "map" `Quick compile_lit_map2;
          Alcotest.test_case "unit" `Quick compile_lit_unit;
          Alcotest.test_case "nat" `Quick compile_lit_nat;
          Alcotest.test_case "nat" `Quick compile_lit_nat2;
          Alcotest.test_case "bool" `Quick compile_lit_bool;
          Alcotest.test_case "bool" `Quick compile_lit_bool2;
          Alcotest.test_case "pair" `Quick compile_lit_pair;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "input" `Quick compile_input;
          Alcotest.test_case "pair" `Quick compile_fst;
          Alcotest.test_case "pair" `Quick compile_snd;
          Alcotest.test_case "arith" `Quick compile_arith;
          Alcotest.test_case "arith" `Quick compile_arith_plus;
          Alcotest.test_case "arith" `Quick compile_arith_minus;
          Alcotest.test_case "arith" `Quick compile_arith_times;
          Alcotest.test_case "arith" `Quick compile_arith_div;
          Alcotest.test_case "bool" `Quick compile_bool_neg;
          Alcotest.test_case "bool" `Quick compile_bool_and;
          Alcotest.test_case "bool" `Quick compile_bool_and2;
          Alcotest.test_case "bool" `Quick compile_bool_or;
          Alcotest.test_case "bool" `Quick compile_bool_eq;
          Alcotest.test_case "map" `Quick compile_map_map;
          Alcotest.test_case "map" `Quick compile_map_upd;
          Alcotest.test_case "if" `Quick compile_if_then_else;
          Alcotest.test_case "if" `Quick compile_if_then_else2;
          Alcotest.test_case "if" `Quick compile_if_then_else3;
          Alcotest.test_case "if" `Quick compile_if_then_else4;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "app" `Quick compile_app;
          Alcotest.test_case "at" `Quick compile_at;
        ] );
    ]
