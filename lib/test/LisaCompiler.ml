open Lisa
open SemanticsCommon

let to_csv p =
  let tup = LisaCompiler.get_tuple p in
  match tup with
  | MuTuple defs ->
      List.map (fun (id, exp) -> LisaCompiler.column_of_stream id exp) defs
  | _ -> []

let env =
  [
    Syntax.(Def ("y", None, integer 42));
    Syntax.(Def ("z", None, boolean true));
    Syntax.(Def ("succ", None, lsucc));
    Syntax.(Def ("isucc", None, isucc));
    Syntax.(Def ("mapneg", None, map_neg));
  ]

let plcp s =
  IO.parse_sheet s
  |> CSVCompiler.lisa_of_csv env
  |> to_csv |> Libformula.Printer.string_of_sheet

let compile_decompile_lit_var () =
  Alcotest.(check string) "same string" "x\ny\n" (plcp "x\ny")

let compile_decompile_lit_map () =
  Alcotest.(check string)
    "same string" "x\n=MAP.EMPTY()\n" (plcp "x\n=MAP.EMPTY()")

let compile_decompile_lit_unit () =
  Alcotest.(check string) "same string" "unit\n()\n" (plcp "unit\n()")

let compile_decompile_lit_nat () =
  Alcotest.(check string) "same string" "one\n1\n" (plcp "one\n1")

let compile_decompile_lit_nat2 () =
  Alcotest.(check string) "same string" "x\n45\n" (plcp "x\n45")

let compile_decompile_lit_bool () =
  Alcotest.(check string) "same string" "x\ntrue\n" (plcp "x\ntrue")

let compile_decompile_lit_bool2 () =
  Alcotest.(check string) "same string" "x\nfalse\n" (plcp "x\nfalse")

let compile_decompile_lit_pair () =
  Alcotest.(check string)
    "same string" "a\n=PAIR(x, y)\n" (plcp "a\n=PAIR(x, y)")

let compile_decompile_input () =
  Alcotest.(check string) "same string" "x\n\n" (plcp "x")

let compile_decompile_fst () =
  Alcotest.(check string) "same string" "a\n=FST(x)\n" (plcp "a\n=FST(x)")

let compile_decompile_snd () =
  Alcotest.(check string) "same string" "a\n=SND(x)\n" (plcp "a\n=SND(x)")

let compile_decompile_arith_plus () =
  Alcotest.(check string) "same string" "a\n=1 + y\n" (plcp "a\n= 1 + y")

let compile_decompile_arith_minus () =
  Alcotest.(check string) "same string" "a\n=42 - 43\n" (plcp "a\n=42 - 43")

let compile_decompile_arith_times () =
  Alcotest.(check string) "same string" "a\n=42 * y\n" (plcp "a\n= 42 * y")

let compile_decompile_arith_div () =
  Alcotest.(check string) "same string" "a\n=y / 1\n" (plcp "a\n= y / 1")

let compile_decompile_arith () =
  Alcotest.(check string)
    "same string" "a\n=(1 + (2 * 3)) - (1 / 2)\n"
    (plcp "a\n= 1 + 2 * 3 - 1 / 2")

let compile_decompile_bool_neg () =
  Alcotest.(check string) "same string" "x\n=NOT(z)\n" (plcp "x\n=NOT(z)")

let compile_decompile_bool_and () =
  Alcotest.(check string)
    "same string" "a\n=AND(z, false)\n" (plcp "a\n=AND(z,false)")

let compile_decompile_bool_and2 () =
  Alcotest.(check string)
    "same string" "a\n=AND(z, AND(false, false))\n"
    (plcp "a\n=AND(z,AND(false, false))")

let compile_decompile_bool_or () =
  Alcotest.(check string) "same string" "a\n=OR(z, z)\n" (plcp "a\n=OR(z, z)")

let compile_decompile_bool_eq () =
  Alcotest.(check string) "same string" "a\n=z = y\n" (plcp "a\n=z = y")

let compile_decompile_map_map () =
  Alcotest.(check string)
    "same string" "b|a\n=PAIR(0, 41)|=MAP.MAP(succ, b[1])\n"
    (plcp "b|a\n=PAIR(0,41)|=MAP.MAP(succ,b[1])")

let compile_decompile_map_upd () =
  Alcotest.(check string)
    "same string" "a|c\n=MAP.UPD(0, true, mapneg, c[1])|=PAIR(0, true)\n"
    (plcp "a|c\n=MAP.UPD(0,true,mapneg,c[1])|=PAIR(0, true)")

let compile_decompile_map_upd2 () =
  Alcotest.(check string)
    "same string"
    "e|d\n0|=PAIR(0, true)\n1|=MAP.UPD(e[1], true, mapneg, d[1])\n2\n"
    (plcp "e|d\n0|=PAIR(0,true)\n1|=MAP.UPD(e[1],true,mapneg,d[1])\n2|\n")

let compile_decompile_if_then_else () =
  Alcotest.(check string)
    "same string" "a\n=IF(true, 1, 2)\n"
    (plcp "a\n=IF(true, 1, 2)")

let compile_decompile_if_then_else2 () =
  Alcotest.(check string)
    "same string" "a\n=IF(true, IF(false, 1, 2), 3)\n"
    (plcp "a\n=IF(true, IF(false, 1, 2), 3)")

let compile_decompile_if_then_else3 () =
  Alcotest.(check string)
    "same string" "a\n=IF(true, 2, IF(false, 1, 3))\n"
    (plcp "a\n=IF(true, 2, IF(false, 1, 3))")

let compile_decompile_if_then_else4 () =
  Alcotest.(check string)
    "same string" "a\n=IF(IF(true, true, false), 2, 3)\n"
    (plcp "a\n=IF(IF(true, true, false), 2, 3)")

let compile_decompile_app () =
  Alcotest.(check string) "same string" "a\n=isucc y\n" (plcp "a\n=isucc (y)")

let compile_decompile_at () =
  Alcotest.(check string) "same string" "m|n\n=n[1]|42\n" (plcp "m|n\n=n[1]|42")

let compile_decompile_nat () =
  Alcotest.(check string)
    "same string" "nat\n0\n=nat[1] + 1\n" (plcp "nat\n0\n=nat[1]+1")

let compile_decompile_alice () =
  Alcotest.(check string)
    "same string"
    "user|amount|value|operations\n\
     ||=amount[1]|=IF(user[1] = 0, PAIR(0, 0), MAP.EMPTY())\n\
     ||=IF(user[2] = 0, 0, value[1] + amount[2])|=IF(user[2] = 0, PAIR(0, \
     value[1]), MAP.EMPTY())\n"
    (plcp
       "user|amount|value| operations\n\
        ||=amount[1]| =IF(user[1] = 0, PAIR(0, 0), MAP.EMPTY())\n\
        ||=IF(user[2] = 0, 0, value[1] + amount[2])|=IF(user[2] = 0, \
        PAIR(0,value[1]), MAP.EMPTY())")

let () =
  Alcotest.run ~show_errors:false "Lisa Compiler"
    [
      ( "Literals",
        [
          Alcotest.test_case "var" `Quick compile_decompile_lit_var;
          Alcotest.test_case "map" `Quick compile_decompile_lit_map;
          Alcotest.test_case "unit" `Quick compile_decompile_lit_unit;
          Alcotest.test_case "nat" `Quick compile_decompile_lit_nat;
          Alcotest.test_case "nat" `Quick compile_decompile_lit_nat2;
          Alcotest.test_case "bool" `Quick compile_decompile_lit_bool;
          Alcotest.test_case "bool" `Quick compile_decompile_lit_bool2;
          Alcotest.test_case "product" `Quick compile_decompile_lit_pair;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "input" `Quick compile_decompile_input;
          Alcotest.test_case "proj" `Quick compile_decompile_fst;
          Alcotest.test_case "proj" `Quick compile_decompile_snd;
          Alcotest.test_case "arith" `Quick compile_decompile_arith;
          Alcotest.test_case "arith" `Quick compile_decompile_arith_plus;
          Alcotest.test_case "arith" `Quick compile_decompile_arith_minus;
          Alcotest.test_case "arith" `Quick compile_decompile_arith_times;
          Alcotest.test_case "arith" `Quick compile_decompile_arith_div;
          Alcotest.test_case "bool" `Quick compile_decompile_bool_neg;
          Alcotest.test_case "bool" `Quick compile_decompile_bool_and;
          Alcotest.test_case "bool" `Quick compile_decompile_bool_and2;
          Alcotest.test_case "bool" `Quick compile_decompile_bool_or;
          Alcotest.test_case "bool" `Quick compile_decompile_bool_eq;
          Alcotest.test_case "map" `Quick compile_decompile_map_map;
          Alcotest.test_case "map" `Quick compile_decompile_map_upd;
          Alcotest.test_case "map" `Quick compile_decompile_map_upd2;
          Alcotest.test_case "if" `Quick compile_decompile_if_then_else;
          Alcotest.test_case "if" `Quick compile_decompile_if_then_else2;
          Alcotest.test_case "if" `Quick compile_decompile_if_then_else3;
          Alcotest.test_case "if" `Quick compile_decompile_if_then_else4;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "app" `Quick compile_decompile_app;
          Alcotest.test_case "at" `Quick compile_decompile_at;
        ] );
      ( "Examples",
        [
          Alcotest.test_case "Nat" `Quick compile_decompile_nat;
          Alcotest.test_case "Alice" `Quick compile_decompile_alice;
        ] );
    ]
