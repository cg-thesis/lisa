open Lisa
open SemanticsCommon

let eval ipt t p =
  let ids =
    List.map
      (function Libformula.Syntax.Var x :: _ -> x | _ -> assert false)
      p
  in
  CSV.eval ipt ids p
    [
      Syntax.(Def ("y", Some TyInt, integer 42));
      Syntax.(Def ("z", Some TyBool, boolean true));
      Syntax.(Def ("succ", None, lsucc));
      Syntax.(Def ("isucc", Some (TyArrow (TyInt, TyInt)), isucc));
      Syntax.(Def ("mapneg", None, map_neg));
    ]
    t

let rep ipt t s =
  IO.parse_sheet s |> eval ipt t |> Libformula.Printer.string_of_sheet

let dummy _ = unit

let csv_eval_lit_var () =
  Alcotest.(check string)
    "same string" "x\n42\n42\n42\n42\ny\n" (rep dummy 3 "x\ny")

let csv_eval_lit_map () =
  Alcotest.(check string)
    "same string"
    "x\n=MAP.EMPTY()\n=MAP.EMPTY()\n=MAP.EMPTY()\n=MAP.EMPTY()\n=MAP.EMPTY()\n"
    (rep dummy 3 "x\n=MAP.EMPTY()")

let csv_eval_lit_unit () =
  Alcotest.(check string)
    "same string" "unit\n()\n()\n()\n()\n()\n" (rep dummy 3 "unit\n()")

let csv_eval_lit_nat () =
  Alcotest.(check string)
    "same string" "one\n1\n1\n1\n1\n1\n" (rep dummy 3 "one\n1")

let csv_eval_lit_nat2 () =
  Alcotest.(check string)
    "same string" "x\n45\n45\n45\n45\n45\n" (rep dummy 3 "x\n45")

let csv_eval_lit_bool () =
  Alcotest.(check string)
    "same string" "x\ntrue\ntrue\ntrue\ntrue\ntrue\n" (rep dummy 3 "x\ntrue")

let csv_eval_lit_bool2 () =
  Alcotest.(check string)
    "same string" "x\nfalse\nfalse\nfalse\nfalse\nfalse\n"
    (rep dummy 3 "x\nfalse")

let csv_eval_lit_pair () =
  Alcotest.(check string)
    "same string"
    "a\n\
     =PAIR(42, true)\n\
     =PAIR(42, true)\n\
     =PAIR(42, true)\n\
     =PAIR(42, true)\n\
     =PAIR(y, z)\n"
    (rep dummy 3 "a\n=PAIR(y, z)")

let csv_eval_input () =
  let input x =
    match x with
    | "x", 0 -> integer 1
    | "x", 1 -> integer 2
    | "x", 2 -> integer 3
    | "x", 3 -> integer 4
    | _ -> assert false
  in
  Alcotest.(check string) "same string" "x\n1\n2\n3\n4\n" (rep input 3 "x")

let csv_eval_fst () =
  Alcotest.(check string)
    "same string" "a\n42\n42\n42\n42\n=FST(PAIR(42, true))\n"
    (rep dummy 3 "a\n=FST(PAIR(42, true))")

let csv_eval_snd () =
  Alcotest.(check string)
    "same string" "a\ntrue\ntrue\ntrue\ntrue\n=SND(PAIR(42, true))\n"
    (rep dummy 3 "a\n=SND(PAIR(42, true))")

let csv_eval_arith_plus () =
  Alcotest.(check string)
    "same string" "a\n42\n42\n42\n42\n=41 + 1\n" (rep dummy 3 "a\n=41 + 1")

let csv_eval_arith_minus () =
  Alcotest.(check string)
    "same string" "a\n-1\n-1\n-1\n-1\n=42 - 43\n"
    (rep dummy 3 "a\n=42 - 43")

let csv_eval_arith_times () =
  Alcotest.(check string)
    "same string" "a\n1764\n1764\n1764\n1764\n=42 * y\n"
    (rep dummy 3 "a\n= 42 * y")

let csv_eval_arith_div () =
  Alcotest.(check string)
    "same string" "a\n42\n42\n42\n42\n=y / 1\n" (rep dummy 3 "a\n= y / 1")

let csv_eval_arith () =
  Alcotest.(check string)
    "same string" "a\n7\n7\n7\n7\n=(1 + (2 * 3)) - (1 / 2)\n"
    (rep dummy 3 "a\n= 1 + 2 * 3 - 1 / 2")

let csv_eval_bool_neg () =
  Alcotest.(check string)
    "same string" "x\nfalse\nfalse\nfalse\nfalse\n=NOT(z)\n"
    (rep dummy 3 "x\n=NOT(z)")

let csv_eval_bool_and () =
  Alcotest.(check string)
    "same string" "a\nfalse\nfalse\nfalse\nfalse\n=AND(z, false)\n"
    (rep dummy 3 "a\n=AND(z,false)")

let csv_eval_bool_and2 () =
  Alcotest.(check string)
    "same string" "a\nfalse\nfalse\nfalse\nfalse\n=AND(z, AND(false, false))\n"
    (rep dummy 3 "a\n=AND(z,AND(false, false))")

let csv_eval_bool_or () =
  Alcotest.(check string)
    "same string" "a\ntrue\ntrue\ntrue\ntrue\n=OR(z, z)\n"
    (rep dummy 3 "a\n=OR(z, z)")

let csv_eval_bool_eq () =
  Alcotest.(check string)
    "same string" "a\nfalse\nfalse\nfalse\nfalse\n=z = y\n"
    (rep dummy 3 "a\n=z = y")

let csv_eval_map_map () =
  Alcotest.(check string)
    "same string"
    "b|a\n\
     =MAP(PAIR(0, 41))|=MAP(PAIR(0, 42))\n\
     =MAP(PAIR(0, 41))|=MAP(PAIR(0, 42))\n\
     =MAP(PAIR(0, 41))|=MAP(PAIR(0, 42))\n\
     =MAP(PAIR(0, 41))|=MAP(PAIR(0, 42))\n\
     =MAP(PAIR(0, 41))|=MAP.MAP(succ, b[+0])\n"
    (rep dummy 3 "b|a\n=MAP(PAIR(0,41))|=MAP.MAP(succ,b[])")

let csv_eval_map_upd () =
  Alcotest.(check string)
    "same string"
    "a|c\n\
     =MAP(PAIR(0, false))|=MAP(PAIR(0, true))\n\
     =MAP(PAIR(0, false))|=MAP(PAIR(0, true))\n\
     =MAP(PAIR(0, false))|=MAP(PAIR(0, true))\n\
     =MAP(PAIR(0, false))|=MAP(PAIR(0, true))\n\
     =MAP.UPD(0, true, mapneg, c[+0])|=MAP(PAIR(0, true))\n"
    (rep dummy 3 "a|c\n=MAP.UPD(0,true,mapneg,c[])|=MAP(PAIR(0, true))")

let csv_eval_map_upd2 () =
  Alcotest.(check string)
    "same string"
    "e|d\n\
     0|=MAP(PAIR(0, true))\n\
     1|=MAP(PAIR(0, false))\n\
     2|=MAP(PAIR(1, true), PAIR(0, false))\n\
     2|=MAP(PAIR(2, true), PAIR(1, true), PAIR(0, false))\n\
     2|=MAP.UPD(e[-1], true, mapneg, d[-1])\n"
    (rep dummy 3
       "e|d\n0|=MAP(PAIR(0,true))\n1|=MAP.UPD(e[-1],true,mapneg,d[-1])\n2|\n")

let csv_eval_if_then_else () =
  Alcotest.(check string)
    "same string" "a\n1\n1\n1\n1\n=IF(true, 1, 2)\n"
    (rep dummy 3 "a\n=IF(true, 1, 2)")

let csv_eval_if_then_else2 () =
  Alcotest.(check string)
    "same string" "a\n2\n2\n2\n2\n=IF(true, IF(false, 1, 2), 3)\n"
    (rep dummy 3 "a\n=IF(true, IF(false, 1, 2), 3)")

let csv_eval_if_then_else3 () =
  Alcotest.(check string)
    "same string" "a\n2\n2\n2\n2\n=IF(true, 2, IF(false, 1, 3))\n"
    (rep dummy 3 "a\n=IF(true, 2, IF(false, 1, 3))")

let csv_eval_if_then_else4 () =
  Alcotest.(check string)
    "same string" "a\n2\n2\n2\n2\n=IF(IF(true, true, false), 2, 3)\n"
    (rep dummy 3 "a\n=IF(IF(true, true, false), 2, 3)")

let csv_eval_app () =
  Alcotest.(check string)
    "same string" "a\n43\n43\n43\n43\n=isucc y\n"
    (rep dummy 3 "a\n=isucc (y)")

let csv_eval_at () =
  Alcotest.(check string)
    "same string" "m|n\n42|42\n42|42\n42|42\n42|42\n=n[+0]|42\n"
    (rep dummy 3 "m|n\n=n[]|42")

let csv_eval_nat () =
  Alcotest.(check string)
    "same string" "nat\n0\n1\n2\n3\n4\n5\n=nat[-1] + 1\n"
    (rep dummy 5 "nat\n0\n=nat[-1]+1")

let csv_eval_alice () =
  let input (id, time) =
    match (id, time) with
    | "user", 0 -> integer 1
    | "amount", 0 -> integer 50
    | "user", 1 -> integer 2
    | "amount", 1 -> integer 75
    | "user", 2 -> integer 0
    | "amount", 2 -> integer 0
    | _ -> assert false
  in
  let expected =
    {|user|amount|value|operations
1|50|50|=MAP.EMPTY()
2|75|125|=MAP.EMPTY()
0|0|0|=MAP(PAIR(0, 125))
||=IF(user[+0] = 0, 0, value[-1] + amount[+0])|=IF(user[+0] = 0, MAP(PAIR(0, value[-1])), MAP.EMPTY())
|}
  in
  Alcotest.(check string)
    "same string" expected
    (rep input 2
       "user|amount|value| operations\n\
        ||=amount[]| =IF(user[] = 0, MAP(PAIR(0, 0)), MAP.EMPTY())\n\
        ||=IF(user[] = 0, 0, value[-1] + amount[])|=IF(user[] = 0, \
        MAP(PAIR(0,value[-1])), MAP.EMPTY())")

let () =
  Alcotest.run "Lisa CSV Interpreter"
    [
      ( "Literals",
        [
          Alcotest.test_case "var" `Quick csv_eval_lit_var;
          Alcotest.test_case "map" `Quick csv_eval_lit_map;
          Alcotest.test_case "unit" `Quick csv_eval_lit_unit;
          Alcotest.test_case "nat" `Quick csv_eval_lit_nat;
          Alcotest.test_case "nat" `Quick csv_eval_lit_nat2;
          Alcotest.test_case "bool" `Quick csv_eval_lit_bool;
          Alcotest.test_case "bool" `Quick csv_eval_lit_bool2;
          Alcotest.test_case "pair" `Quick csv_eval_lit_pair;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "input" `Quick csv_eval_input;
          Alcotest.test_case "pair" `Quick csv_eval_fst;
          Alcotest.test_case "pair" `Quick csv_eval_snd;
          Alcotest.test_case "arith" `Quick csv_eval_arith;
          Alcotest.test_case "arith" `Quick csv_eval_arith_plus;
          Alcotest.test_case "arith" `Quick csv_eval_arith_minus;
          Alcotest.test_case "arith" `Quick csv_eval_arith_times;
          Alcotest.test_case "arith" `Quick csv_eval_arith_div;
          Alcotest.test_case "bool" `Quick csv_eval_bool_neg;
          Alcotest.test_case "bool" `Quick csv_eval_bool_and;
          Alcotest.test_case "bool" `Quick csv_eval_bool_and2;
          Alcotest.test_case "bool" `Quick csv_eval_bool_or;
          Alcotest.test_case "bool" `Quick csv_eval_bool_eq;
          Alcotest.test_case "map" `Quick csv_eval_map_map;
          Alcotest.test_case "map" `Quick csv_eval_map_upd;
          Alcotest.test_case "map" `Quick csv_eval_map_upd2;
          Alcotest.test_case "if" `Quick csv_eval_if_then_else;
          Alcotest.test_case "if" `Quick csv_eval_if_then_else2;
          Alcotest.test_case "if" `Quick csv_eval_if_then_else3;
          Alcotest.test_case "if" `Quick csv_eval_if_then_else4;
        ] );
      ( "Terms",
        [
          Alcotest.test_case "app" `Quick csv_eval_app;
          Alcotest.test_case "at" `Quick csv_eval_at;
        ] );
      ( "Examples",
        [
          Alcotest.test_case "Nat" `Quick csv_eval_nat;
          Alcotest.test_case "Alice" `Quick csv_eval_alice;
        ] );
    ]
