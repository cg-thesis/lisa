open Lisa
open BigStepSemantics
open Syntax

let lisa_value = Alcotest.testable ValuePrettyPrinter.fmt_of_value ( = )
let x = ref 0

let get_input _ =
  Printf.printf "Value of x : %d\n" !x;
  match !x with
  | 0 ->
      incr x;
      SemanticsCommon.integer 1
  | 1 ->
      incr x;
      SemanticsCommon.integer 2
  | 2 ->
      incr x;
      SemanticsCommon.integer 3
  | 3 ->
      incr x;
      SemanticsCommon.integer 4
  | 4 ->
      incr x;
      SemanticsCommon.integer 5
  | _ -> exit 1

let parse_eval s =
  List.hd
    (List.map
       (fun x -> snd @@ snd @@ eval get_input [] 4 x)
       (IO.parse_binding s))

let rep s = parse_eval s |> ValuePrettyPrinter.string_of_value
let parse = IO.parse_expression
let eval_int () = Alcotest.(check string) "same string" "1" (rep "let x = 1")

let eval_int2 () =
  Alcotest.(check string) "same string" "23424564" (rep "let x = 23424564")

let eval_unit () = Alcotest.(check string) "same string" "()" (rep "let x = ()")
let eval_nil () = Alcotest.(check string) "same string" "[]" (rep "let x = []")

let eval_true () =
  Alcotest.(check string) "same string" "true" (rep "let x = true")

let eval_false () =
  Alcotest.(check string) "same string" "false" (rep "let x = false")

let eval_tuple () =
  Alcotest.(check string)
    "same string" "(1, true, <fun>)"
    (rep "let x = (1,true,fun x -> x)")

let eval_projnm1 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = proj 1 2 (false, 42)")

let eval_projnm2 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = proj 2 3 (86, true, 42)")

let eval_projnm3 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = proj 2 3 ((fun x -> x) (86 - 1, not true, 0 + 42))")

let eval_arith_plus () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = 1 + 41")

let eval_arith_plus2 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = (41 + 0) + (0 + 1)")

let eval_arith_minus () =
  Alcotest.(check lisa_value)
    "same expression" (VInt (-1))
    (parse_eval "let x = 42 - 43")

let eval_arith_minus2 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt (-1))
    (parse_eval "let x = (42 - 0) - (43 - 0)")

let eval_arith_times () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = 21 * 2")

let eval_arith_times2 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = (20 + 1) * (2 * 1)")

let eval_arith_div () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 21)
    (parse_eval "let x = 42 / 2")

let eval_arith_div2 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 42)
    (parse_eval "let x = (42 * 4) / (8 / 2) ")

let eval_bool_neg () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = not true")

let eval_bool_neg2 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = not false")

let eval_bool_neg3 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = not (false && false)")

let eval_bool_and () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = false && true")

let eval_bool_and2 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = true && false")

let eval_bool_and3 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = false && false")

let eval_bool_and4 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = true && true")

let eval_bool_and5 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = (not true) && (not true)")

let eval_bool_or () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = false || true")

let eval_bool_or2 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = true || false")

let eval_bool_or3 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = true || true")

let eval_bool_or4 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = false || false")

let eval_bool_or5 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = (not false) || (not false)")

let eval_bool_eq () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = true = true")

let eval_bool_eq2 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = false = false")

let eval_bool_eq3 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = false = true")

let eval_bool_eq4 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = true = false")

let eval_bool_eq5 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = 1 = 1")

let eval_bool_eq6 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = 762 = 762")

let eval_bool_eq7 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = 1 + 1 = 762")

let eval_bool_eq8 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = 762 = 1 + 1")

let eval_bool_eq9 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = [] = []")

let eval_bool_eq10 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = [1;2; 2 + 1] = [1;2; 2 + 1]")

let eval_bool_eq11 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = [] = [1;2; 2 + 1]")

let eval_bool_eq12 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = [1;2; 2 + 1] = []")

let eval_bool_neq () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = true <> true")

let eval_bool_neq2 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = false <> false")

let eval_bool_neq3 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = false <> true")

let eval_bool_neq4 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = true <> false")

let eval_bool_neq5 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = 1 <> 1")

let eval_bool_neq6 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = 762 <> 762")

let eval_bool_neq7 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = 1 <> 762")

let eval_bool_neq8 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = 762 <> 1")

let eval_bool_neq9 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = [] <> []")

let eval_bool_neq10 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool false)
    (parse_eval "let x = [1;2; 2+ 1] <> [1;2; 2 + 1]")

let eval_bool_neq11 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = [] <> [1;2; 2 + 1]")

let eval_bool_neq12 () =
  Alcotest.(check lisa_value)
    "same expression" (VBool true)
    (parse_eval "let x = [1;2] <> []")

let eval_list () =
  Alcotest.(check lisa_value)
    "same expression"
    (VList [ VInt 1; VInt 2 ])
    (parse_eval "let x = [1;2]")

let eval_list_cons () =
  Alcotest.(check lisa_value)
    "same expression"
    (VList [ VInt 0; VInt 1; VInt 42; VInt 43; VInt 44 ])
    (parse_eval "let x = cons 0 (cons 1 [42;43;43+1])")

let eval_list_map () =
  Alcotest.(check lisa_value)
    "same expression"
    (VList [ VInt 1; VInt 2 ])
    (parse_eval "let x = map ((fun x -> x) (fun x -> x+1)) [0;1 + 0]")

let eval_list_iter () =
  Alcotest.(check lisa_value)
    "same expression" VUnit
    (parse_eval "let x = iter ((fun x -> x) (fun x -> ())) [1;3;4+1]")

let eval_list_foldl () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 6)
    (parse_eval
       "let x = fold_left ((fun x -> x) (fun x -> (fun acc -> x + acc))) (0+0) \
        [1;1+1;3]")

let eval_list_foldr () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 2)
    (parse_eval
       "let x = fold_right ((fun x -> x)(fun acc -> (fun x -> acc - x))) \
        [1;2;2+1] (0*0)")

let eval_if_then_else () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 1)
    (parse_eval "let x = if true then 1 else 2")

let eval_if_then_else2 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 2)
    (parse_eval "let x = if true then if false then 1 else 2 else 3")

let eval_if_then_else3 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 1)
    (parse_eval "let x = if false then 2 else if true then 1 else 3")

let eval_if_then_else4 () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 2)
    (parse_eval "let x = if if true then true else false then 2 else 3")

let eval_rectup_id () =
  Alcotest.(check string)
    "same string" "(<fun>::<fun>::<fun>::<fun>::<fun>::<stream>)"
    (rep "rectup id = fun x -> x::id")

let eval_rectup_input () =
  Alcotest.(check string)
    "same string" "(1::2::3::4::5::<stream>)"
    (rep "rectup x = input \"x\"::x")

let eval_rectup_constant_stream () =
  Alcotest.(check string)
    "same string" "(1::1::1::1::1::<stream>)" (rep "rectup x = 1 :: x")

let eval_rectup_nats () =
  Alcotest.(check string)
    "same string" "(0::1::2::3::4::<stream>)"
    (rep
       {|rectup nats =
        0 :: (thunk (proj 0 1 (rectup next = (1 + (nats[i.i-1])) :: next)))|})

let eval_rectup_mutual () =
  Alcotest.(check string)
    "same string" "(0::1::0::1::0::<stream>, 1::0::1::0::1::<stream>)"
    (rep "rectup x = 0::y and y = 1::x")

let eval_fun () =
  Alcotest.(check lisa_value)
    "same expression"
    (VClosure ("x", Var "x", []))
    (parse_eval "let id = fun x -> x")

let eval_app () =
  Alcotest.(check lisa_value)
    "same expression"
    (VClosure ("x", Var "x", []))
    (parse_eval "let id = (fun x -> x) (fun x -> x)")

let eval_app_fun () =
  Alcotest.(check lisa_value)
    "same expression" (VInt 1)
    (parse_eval "let one = (fun x -> x) 1")

let () =
  Alcotest.run "Lisa big step semantics"
    [
      ( "Literals",
        [
          Alcotest.test_case "integers" `Quick eval_int;
          Alcotest.test_case "integers" `Quick eval_int2;
          Alcotest.test_case "unit" `Quick eval_unit;
          Alcotest.test_case "list" `Quick eval_nil;
          Alcotest.test_case "bool" `Quick eval_true;
          Alcotest.test_case "bool" `Quick eval_false;
          Alcotest.test_case "tuples" `Quick eval_tuple;
        ] );
      ( "Primitives",
        [
          Alcotest.test_case "tuples" `Quick eval_projnm1;
          Alcotest.test_case "tuples" `Quick eval_projnm2;
          Alcotest.test_case "tuples" `Quick eval_projnm3;
          Alcotest.test_case "arith" `Quick eval_arith_plus;
          Alcotest.test_case "arith" `Quick eval_arith_plus2;
          Alcotest.test_case "arith" `Quick eval_arith_minus;
          Alcotest.test_case "arith" `Quick eval_arith_minus2;
          Alcotest.test_case "arith" `Quick eval_arith_times;
          Alcotest.test_case "arith" `Quick eval_arith_times2;
          Alcotest.test_case "arith" `Quick eval_arith_div;
          Alcotest.test_case "arith" `Quick eval_arith_div2;
          Alcotest.test_case "bool" `Quick eval_bool_neg;
          Alcotest.test_case "bool" `Quick eval_bool_neg2;
          Alcotest.test_case "bool" `Quick eval_bool_neg3;
          Alcotest.test_case "bool" `Quick eval_bool_and;
          Alcotest.test_case "bool" `Quick eval_bool_and2;
          Alcotest.test_case "bool" `Quick eval_bool_and3;
          Alcotest.test_case "bool" `Quick eval_bool_and4;
          Alcotest.test_case "bool" `Quick eval_bool_and5;
          Alcotest.test_case "bool" `Quick eval_bool_or;
          Alcotest.test_case "bool" `Quick eval_bool_or2;
          Alcotest.test_case "bool" `Quick eval_bool_or3;
          Alcotest.test_case "bool" `Quick eval_bool_or4;
          Alcotest.test_case "bool" `Quick eval_bool_or5;
          Alcotest.test_case "bool" `Quick eval_bool_eq;
          Alcotest.test_case "bool" `Quick eval_bool_eq2;
          Alcotest.test_case "bool" `Quick eval_bool_eq3;
          Alcotest.test_case "bool" `Quick eval_bool_eq4;
          Alcotest.test_case "bool" `Quick eval_bool_eq5;
          Alcotest.test_case "bool" `Quick eval_bool_eq6;
          Alcotest.test_case "bool" `Quick eval_bool_eq7;
          Alcotest.test_case "bool" `Quick eval_bool_eq8;
          Alcotest.test_case "bool" `Quick eval_bool_eq9;
          Alcotest.test_case "bool" `Quick eval_bool_eq10;
          Alcotest.test_case "bool" `Quick eval_bool_eq11;
          Alcotest.test_case "bool" `Quick eval_bool_eq12;
          Alcotest.test_case "bool" `Quick eval_bool_neq;
          Alcotest.test_case "bool" `Quick eval_bool_neq2;
          Alcotest.test_case "bool" `Quick eval_bool_neq3;
          Alcotest.test_case "bool" `Quick eval_bool_neq4;
          Alcotest.test_case "bool" `Quick eval_bool_neq5;
          Alcotest.test_case "bool" `Quick eval_bool_neq6;
          Alcotest.test_case "bool" `Quick eval_bool_neq7;
          Alcotest.test_case "bool" `Quick eval_bool_neq8;
          Alcotest.test_case "bool" `Quick eval_bool_neq9;
          Alcotest.test_case "bool" `Quick eval_bool_neq10;
          Alcotest.test_case "bool" `Quick eval_bool_neq11;
          Alcotest.test_case "bool" `Quick eval_bool_neq12;
          Alcotest.test_case "list" `Quick eval_list;
          Alcotest.test_case "list" `Quick eval_list_cons;
          Alcotest.test_case "list" `Quick eval_list_map;
          Alcotest.test_case "list" `Quick eval_list_iter;
          Alcotest.test_case "list" `Quick eval_list_foldr;
          Alcotest.test_case "list" `Quick eval_list_foldl;
          Alcotest.test_case "if" `Quick eval_if_then_else;
          Alcotest.test_case "if" `Quick eval_if_then_else2;
          Alcotest.test_case "if" `Quick eval_if_then_else3;
          Alcotest.test_case "if" `Quick eval_if_then_else4;
        ] );
      ( "Term",
        [
          Alcotest.test_case "input" `Quick eval_rectup_input;
          Alcotest.test_case "rectup" `Quick eval_rectup_constant_stream;
          Alcotest.test_case "rectup id" `Quick eval_rectup_id;
          Alcotest.test_case "rectup nats" `Quick eval_rectup_nats;
          Alcotest.test_case "rectup mutual" `Quick eval_rectup_mutual;
          Alcotest.test_case "function" `Quick eval_fun;
          Alcotest.test_case "application" `Quick eval_app;
          Alcotest.test_case "application" `Quick eval_app_fun;
        ] );
    ]
