type 'a seq = 'a Seq.t
type time = Time of int

let origin = Time 1
let tick (Time t) = Time (t + 1)
let ( !! ) f (Time t) = Time (f t)

type 'a t = time -> 'a

let pure x : 'a t = fun _ -> x
let bind a b now = b (a now) now
let ( let* ) = bind
let lam (f : 'a -> 'b t) : ('a -> 'b) t = fun t x -> f x t

let app (f : ('a -> 'b) t) (x : 'a t) : 'b t =
  let* x in
  let* f in
  pure (f x)

let rec1 (f : 'a t -> 'a t) =
  let rec g time = f g time in
  g

let rec2 (f : 'a t -> 'b t -> ('a * 'b) t) : ('a * 'b) t =
  let rec g1 time = fst (f g1 g2 time) and g2 time = snd (f g1 g2 time) in
  fun time -> (g1 time, g2 time)

let rec nth n s =
  match (n, s ()) with
  | 0, Seq.Cons (x, _) -> x
  | n, Seq.Cons (_, f) -> nth (pred n) f
  | _, _ -> assert false

let warp (type a) (f : time -> time) (s : a t) : a t = fun now -> s (f now)
let ( ** ) = warp

let last (type a) (s : a seq t) : a t =
 fun now ->
  let (Time t) = now in
  s now |> nth t

let observe (type a) (s : a seq t) (map : time -> time) : a t = last (map ** s)

(* let observe (type a) (s : a seq t) (map : time -> time) : a t = fun now ->
 *   let (Time t as now') = now in
 *   s now' |> nth t *)

let emit (type a) (x : a t) (next : a seq t) : a seq t =
 fun now () -> Seq.Cons (x now, next (tick now))

let run (type a) (m : a t) = m origin

(**

from n = n : from (n + 1)
x      = from 0
y      = 1 : observe x (fun t -> t / 2) : y

*)

(* let rec from n : int seq t = fun now ->
 *   emit (pure n) (from (succ n)) now
 * 
 * let rec x : int seq t = fun ((Time _) as now) ->
 *   from 0 now
 * 
 * and y : int seq t = fun now -> (
 *   emit (pure 1) @@
 *   emit (observe x (!! (fun t -> t / 2))) y
 * ) now
 * 
 * let rec take n s =
 *   match n, s () with
 *   | 0, _ -> []
 *   | n, Seq.Cons (x, f) -> x :: take (pred n) f
 *   | _, _ -> assert false *)

(*
let y_10 = run y |> take 100
*)

let rec take n s =
  match (n, s ()) with
  | 0, _ -> []
  | n, Seq.Cons (x, f) -> x :: take (pred n) f
  | _, _ -> assert false
