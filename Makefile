.PHONY: build run clean top install check coverage check-report fmt dev-deps

build:
	dune build @install

run: build
	@ rlwrap dune exec --display=quiet -- bin/main.exe interactive

fmt:
	dune build @fmt --auto-promote

check:
	dune runtest

check-report:
	dune runtest --instrument-with bisect_ppx --force

coverage: check-report
	bisect-ppx-report html

top:
	dune utop

install: build
	dune install

clean:
	dune clean
dev-deps:
	opam exec -- opam install --deps-only --assume-depexts -y ./dev-deps
