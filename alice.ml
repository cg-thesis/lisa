let transfer x = x

let prog =
  let value param state = if fst param = 0 then 0 else fst state + snd param in

  let operations param state =
    if fst param = 0 then [ transfer (0, fst state) ] else []
  in

  let init_state = (0, []) in

  let run param last_state =
    let state = if fst last_state = 0 then init_state else snd last_state in
    let time = fst last_state in
    let value' = value param state in
    let operation' = operations param state in
    let new_state = (time + 1, (value', operation')) in
    (operation', new_state)
  in
  run

let run_param = [ (1, 250); (2, 250); (0, 0); (1, 20) ]

let simulate (s, acc) =
  let param = List.nth run_param (fst s) in
  let res = prog param s in
  (snd res, fst res :: acc)
