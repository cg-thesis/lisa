(** The Alice contract is used to deposit money that only the user
Alice can withdraw. The inital value is equal to 0. There
are to ways to use this contract. For any user that is not Alice, the
amount passed as parameter is added to the stored value.  If the
current user is Alice, the last stored value is the transferred to
Alice and the stored value is set to 0.**)

Definition id : Type := nat.

Definition mutez : Type := nat.

Definition storage : Type := mutez.

Definition operations : Type :=
  list (id * mutez).

Inductive field : Type :=
  | Value.

Definition alice : id := 0.

Definition no_operations : operations := nil.

Definition transfer (to: id) (amount: mutez) (ops : operations) :=
  cons (to, amount) ops.

Definition get (s : storage) (f: field) :=
  match f with
  | Value => s
  end.

Definition set (s: storage) (f: field)  (x:mutez) : storage :=
  match f with
  | Value => x
  end.

Definition alice_contract (user: id) (amount: mutez)
  (s : storage) (ops : operations) : operations * storage :=
  match user with
  | O =>
    let v := get s Value in
    let storage := set s Value 0 in
    let operations := transfer alice (get s Value) ops in
    (ops, storage)
  | _ =>
    let storage := set s Value amount in
    (no_operations, storage)
  end.

(** Property p1, for any users the is not Alice, the amount passed as
parameter is added to the stored value. **)

Axiom p1_not_alice_updates_storage :
  forall (user : id) (s :storage) (previous_value : mutez)
  (amount : mutez),
  user <> alice <-> (get s Value) = previous_value + amount.

(** Property p2, for any users the is not Alice, no operation is issued. **)

Axiom p2_not_alice_no_operations :
  forall (user : id) (ops : operations),
    (user <> alice) <-> ops = no_operations.

(** Property p3, if user is Alice, then the stored value is equal to zero. **)

Axiom p3_alice_value_zero :
    forall (user : id) (s : storage), user = alice <-> (get s Value) = 0.

(** Property p4, if user is Alice, then the previous stored value is
transfered to Alice. **)

Axiom p4_alice_transfer_old_value :
    forall (user : id) (previous_value : mutez) (ops :operations),
    user = alice <-> ops = (transfer alice previous_value no_operations).

Theorem alice_contract_correct :
 (p1_not_alice_updates_storage /\ p2_not_alice_no_operations) \/
 (p4_alice_transfer_old_value  /\ p3_alice_value_zero) <-> alice_contract.
