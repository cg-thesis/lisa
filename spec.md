# Translating Streams

## Problem
Goal : Translate streams to Michelson.
Problem : Michelson doesn't provide native final coalgebras to encode streams.
How to encode Streams in Michelson without introducing a greatest fixed point
constructor for types in Set.

##

In CBPV we define:

    Stream A ≃ A × U (F Stream A)

    Γ ⊢ a : A     Γ ⊢ b : U (F Stream A)
    ————————————————————————–———————————–
        Γ ⊢ a :: b : Stream A

Streams can be implemented using recursion:

    μ ones. return (1 :: ones)

We recall the recursion typing rule


    Γ, x:U Ḇ ⊢  M : Ḇ
    —————————————————————-
    Γ ⊢ μ x . M : Ḇ

Idea: at a given instant streams need a value up to a certain instant, why not build the history in a list.

We extend the time Reader monad to replace streams with finite ranges over streams.

The time reader monad is specified as :

    〚 F A 〛 =  Nat → F 〚A〛

In particular we observe

    〚 Stream A  〛 ≃  A × U (Nat → F Stream A)

Extending the monad to replace streams with range generators

    〚 F A 〛 =  Nat → Nat → F A

and define streams such that  :

    〚 Stream A 〛 ≃  U (Nat → h:Nat →  F List_h A)


But this doesn't work with recursion:

    〚 μ ones. let s = 1 :: ones in return s〛 : F (Stream Nat) =
     μ ones. fun _o -> fun _h ->
       let s = thunk  (fun _o1 -> fun _h1 ->
         if (_o1 = _h1) then (return []) else
         (((force ones) (_o1 + 1)) _h1) to _n)
       in
       (force s) _o _h to s.
       return s
     : Nat -> Nat -> F (List Nat)

    〚 μ ones.  return (1 :: ones)〛 =
     μ ones. fun _o -> fun _h ->
       return  (thunk  (fun _o1 -> fun _h1 ->
       if (_o1 = _h1) then (return []) else  (((force ones) (_o1 + 1)) _h1) to _n .
       return cons 1 (_n)))

    'a stream = 'a * U F ('a stream)
             〚 'a stream 〛 ≃ 〚 'a * U F ('a stream) 〛 =
              U (nat -> nat -> F 'a list)
              〚 x :: xs 〛 =
                  thunk (fun o h -> (if o = h then [] else ((force xs) (o+1) h)) to n.
                  return (cons x n))

### CBPV + Streams

    Ḇ  ::= A → Ḇ | F A
    A  ::= A × A | Stream A | U Ḇ

    〚 Ḇ 〛?   〚 A → Ḇ 〛    = ∀ h. ℕ → Aₕ → Ḇₕ
               〚 F A  〛      = ∀ h. ℕ → F Aₕ

    〚 A 〛 ?   〚 A 〛        = ∀ h. ℕ → F Aₕ

    〚 Ḇ 〛ₕ?  〚 A → Ḇ 〛ₕ   = ∀ h. Aₕ → Ḇₕ
               〚 F A    〛ₕ   = ∀ h. F Aₕ

     〚A〛ₕ ?  〚A × A′〛ₕ     = Aₕ × A′ₕ
               〚 U Ḇ〛ₕ       = U (Ḇₕ)
               〚 Stream A 〛ₕ = List Aₕ-o



    〚V::V′〛ₕ = fun o h -> if o = h then [] else
                 〚V〛ₕ o h =: x in
                 〚V′〛ₕ o h = u_xs in
                 (force u_xs) (o+1) h =: xs in
                 return x::xs
    〚V@f〛ₕ = (f o) =: time in (〚 V 〛ₕ 0 o) =: s in (nth s time))


### CBPV + Streams type directed


    Ḇ  ::= A → Ḇ | F A
    A  ::= A × A | Stream A | U Ḇ

    〚 Ḇ 〛?   〚 A → Ḇ 〛    = A → Ḇ
              〚 Stream A  → Ḇ 〛    = ∀h. List 〚A〛ₕ → Ḇ
              〚 F Stream A〛 = ∀ h. List 〚A〛ₕ
              〚 F A  〛      = F A

    〚 A 〛 ?  〚 Stream A 〛 = List A
              〚 A 〛        = A

    〚 Ḇ 〛ₕ?  〚 A → Ḇ 〛ₕ   = ∀ h. Aₕ → Ḇₕ
               〚 F A    〛ₕ   = ∀ h. F Aₕ

     〚A〛ₕ ?  〚A × A′〛ₕ     = Aₕ × A′ₕ
               〚 U Ḇ〛ₕ       = U (Ḇₕ)
               〚 Stream A 〛ₕ = List Aₕ-o



    〚V::V′〛ₕ = fun o h -> if o = h then [] else
                 〚V〛ₕ o h =: x in
                 〚V′〛ₕ o h = u_xs in
                 (force u_xs) (o+1) h =: xs in
                 return x::xs
    〚V@f〛ₕ = (f o) =: time in (〚 V 〛ₕ 0 o) =: s in (nth s time))




### Yann playground

    Ḇ  ::= A → Ḇ | F A
    A  ::= A × A | Stream A | U Ḇ

    〚 Ḇ 〛?   〚 A → Ḇ 〛    = ∀ h. ℕ → Aₕ → Ḇₕ
               〚 F A  〛      = ∀ h. ℕ → F Aₕ

    〚 A 〛 ?   〚 A 〛        = ∀ h. ℕ → F Aₕ

    〚 Ḇ 〛ₕ?  〚 A → Ḇ 〛ₕ   = forall o, Aₕ → Ḇₕ
               〚 F A 〛ₕ   = forall o, F Aₕ

     〚A〛ₕ ?  〚A × A′〛ₕ       = Aₕ × A′ₕ
             〚 U Ḇ〛ₕ       = U (Ḇₕ)
             〚 Stream A 〛ₕ = forall o: Nat, List A_ₕ


    〚V::V′〛ₕ = fun o h -> if o = h then [] else
                 〚V〛ₕ o h =: x in
                 〚V′〛ₕ o h = u_xs in
                 (force u_xs) (o+1) h =: xs in
                 return x::xs
    〚V@f〛ₕ = (f o) =: time in (〚 V 〛ₕ 0 o) =: s in (nth s time))
