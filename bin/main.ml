open Lisa
open Cmdliner

let default_cmd =
  Cmdliner.(
    let doc = "Lisa compiler and interpreter." in
    let exits = Term.default_exits in
    (Term.(const ignore), Term.info "lisa" ~doc ~exits))

let main =
  Term.(
    exit
    @@ eval_choice default_cmd
         [
           Compiler.cmd;
           Interpreter.repl;
           Interpreter.take;
           Interpreter.csv_loop;
           Interpreter.cbpv_repl;
           Interpreter.cbpv_eval_file;
         ])
