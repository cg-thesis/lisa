# Lisa

Lisa is an indexed-stream based language designed to capture the
idioms of spreadsheet programming. Lisa features a toolchain to run
and compile CSV spreadsheets written with Formula, a spreadsheet-like
Lisa-based language for spreadsheets, to Lisa. It also provides the
bare bones to implement a compiler from Lisa to Michelson, the Tezos
smart-contract language.

## Prerequisites

Before you begin, ensure you have the following dependencies installed:
- [Opam](https://opam.ocaml.org/) version 2.0 or higher

## Installation

To install the required dependencies, run the following command:
```sh
opam install --deps-only .
```

## Compilation

Compile the project using the following command:
```sh
dune build @install @runtest
```

## Development Setup

To set up the development environment with a local Opam switch, follow these steps:

```sh
./configure
eval $(opam config env)
make dev-deps
```

## Running Examples

You can find different source files for `CSV`, Lisa, Core Lisa, and
Pure Lisa in the `./examples` directory. To use them, utilize the
`lisa` command with the batch interpreters. Core Lisa and Pure Lisa
are run with the same interpreter (`batch-cbpv`) but use the
appropriate type-checker with the switch `-c core` or `-c pure`. For
most batch evaluations the results of running a program stored in
`.out` file. For example, runnging the following command:

``` sh
lisa batch-cbpv -c pure path.plisa
```

will be stored in `path.plisa.out`.

To get documentation for each command, use:

```sh
lisa COMMAND --help
```

### CSV Programs with Lisa Scripts

Some CSV source programs (`filter.csv` and `investors.csv`) require
loading Lisa definitions provided in a separate `.lisa` file. These
can be run with the `eval` command using the option `-h path.lisa`.

```sh
lisa eval -h path.lisa  source.csv
```

## Contributing

If you want to contribute to the project, please follow these steps:
1. Fork the repository.
2. Create a new branch (`git checkout -b feature-foo`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add some foo'`).
5. Push to the branch (`git push origin feature-foo`).
6. Create a new Pull Request.

## Contact
If you have any questions or issues, please open an issue on Gitlab.
